**********************************************************************
Author: TMS Software
        Copyright � 1996-2010
        E-mail: info@tmssoftware.com
        Web: http://www.tmssoftware.com
**********************************************************************


TMS Advanced Smooth TrackBar


Files :
-------
  AdvSmoothTrackBarD6.zip    : component files for Delphi 6
  AdvSmoothTrackBarD7.zip    : component files for Delphi 7
  AdvSmoothTrackBarD2005.zip : component files for Delphi 2005
  AdvSmoothTrackBarD2006.zip : component files for Delphi 2006 & C++Builder 2006
  AdvSmoothTrackBarD2007.zip : component files for Delphi 2007 & C++Builder 2007
  AdvSmoothTrackBarD2009.zip : component files for Delphi 2009 & C++Builder 2009
  AdvSmoothTrackBarD2010.zip : component files for Delphi 2010 & C++Builder 2010
  AdvSmoothTrackBarDXE.zip   : component files for Delphi XE & C++Builder XE

Release 1.2.2.0 :
-----------------

- Smoothly animated trackbar
- Office 2003 / 2007 Style
- Progress and background appearance fill style.
- Minimum, maximum and step with tickmarks.
- Complex thumb, with fill support.

History :
---------
  v1.0.0.0 : First release
  v1.0.0.1 : Improved: Event OnPositionChanged called when user programmatically changes position
             or click above or under thumb
  v1.1.0.0 : New : Keyboard support to increase or decrease value
           : New : Focus indication on thumb fill
  v1.1.0.1 : Fixed: issue with saving width and height
  v1.1.1.0 : New : Support for Windows Vista and Windows Seven Style
  v1.1.2.0 : New : Built-in support for reduced color set for use with terminal servers
           : Fixed : Issue with cursor not showing
  v1.1.3.0 : New : Delphi 2010 Touch support
  v1.1.4.0 : New : Event to customize drawing of numbers and tickmarks
           : New : Exposed event OnPositionChanging
  v1.2.0.0 : New : PositionToolTip / PositionToolTipFormat added
           : Improved : OnPositionChanging and OnPositionChanged with keyboard interaction
           : Fixed : Issue with interaction when glowanimation is false
  v1.2.1.0 : New : Added Event OnGetToolTipText
  v1.2.1.1 : Improved : Transparency
  v1.2.1.2 : Fixed : Issue with setting Step to 0
  v1.2.2.0 : New : Built-in support for Office 2010 colors
Usage:
------
 Use of TMS software components in applications requires a license.
 A license can be obtained by registration. A single developer license
 registration is available as well as a site license.
 With the purchase of one single developer license, one developer in
 the company is entitled to:
 - use of registered version that contains full source code and no
   limitations 
 - free updates for a full version cycle
 - free email priority support & access to support newsgroups
 - discounts to purchases of other products

 With a site license, multiple developers in the company are entitled
 to:
 - use of registered version that contains full source code and no
   limitations 
 - add additional developers at any time who make use of the components
 - free updates for a full version cycle
 - free email priority support & access to support newsgroups
 - discounts to purchases of other products

 Online order information can be found at:
 http://www.tmssoftware.com/go/?orders

Note: 
-----

 The components are also part of the TMS Component Pack bundle, see 
 http://www.tmssoftware.com/go/?tmspack


Help, hints, tips, bug reports:
-------------------------------
 Send any questions/remarks/suggestions to : help@tmssoftware.com

 Before contacting support about a possible issue with the component
 you encounter, make sure that you are using the latest version of the 
 component.
 If a problem persists with the latest version, provide information 
 about which Delphi or C++Builder version you are using as well as
 the operating system and if possible, steps to reproduce the problem
 you encounter. That will guarantee the fastest turnaround times for
 your support case. 

