===========================================
 TeeChart Pro 7.02 VCL / CLX version
 Copyright (c) 1995-2004 by Steema Software
 All Rights Reserved.

Steema Software SL
http://www.steema.com
email: info@steema.com
===========================================


Release 7.02
============

 Additions:

 ----- TChart and TDBChart components installed at TeeChart palette.
 ----- Support for both Delphi 2005.Net and Delphi 2005.Win32
 ----- Some speed improvements in Delphi 2005 (inlining)
 ----- New property Chart1.Legend.Title.Caption (string)
 ----- New property Chart1.Title.Caption (string) and Chart1.Foot.Caption
 ----- New unit TeeHtml (used by TTeeShape.TextFormat property)
 ----- TeeOpenGL Canvas support for Delphi 8 .Net and Delphi 2005 .Net
 ----- New TLineSeries.Shadow property
 ----- New TCircledSeries.PointToRadius method
 ----- New global variable TeCanvas.TeeCheckPenWidth (HP-Laserjet printers bug)
 ----- New TTeeCommander.DefaultButton property.
 ----- New TChartPreview.HideCloseButton method.
 

 Known bugs:

 ----- TChartPen.SmallDots property not working under Delphi 2005.
 

Release 7.01
============

 #1460 TChartValueList.RecalcStats
 #1516 TShapeSeries location was not persistent
 #1505 AV when closing form having TTeeFunction
 #1499 TRotateTool and OpenGL conflict
 #1494 AV when removing a TDataSet assiged to a TDBCrossTabSource component.
 #1493 TeeCreateFlatFile.pas file not included in Examples folder
 #1472 TDrawLine.Pen is now local to each line
 #1470 TExtraLegendTool now works with automatic position
 #1468 TChartListBox now fires OnMouseDown event
 #1465 Missing entry in "Save as" combo at data export dialog
 #1332 Paging was not working fine when MaxPointsPerPage = 1
 #1074 TChartSeries.Marks.MultiLine bug fixed
 
 Fixed TeeSVGCanvas exception when Chart parent is nil.
 
 Exchanging from Surface to WaterFall series was not working fine.

 New OnGetPointerStyle at TSmithSeries

 New TChart OnBeforePrint event.

 Fixed CCI function calculation bug

 Fixed CLX (Qt) problem when resizing charts.

 New TFastLineSeries ExpandAxis property.

 New TeeDownSampling.pas unit with TDownSampling function.

 New Candle series "ColorStyle" property.



What's New in version 7 ?
=========================

For an updated list of changes, please follow this link:

http://www.steema.com/support/teechart/7/WhatsNew/WhatsNew.htm


Previous version 6:
--------------------

Release 6.01
============

Fixed bugs:

- Fixed series editor problem when
  changing custom axis "Horizontal" property.

- Fixed AV when drawing gradient filled
  annotation callout pointers in 3D mode.

- Fixed wrong series drawing order when
  Depth axis is "Inverted".

- Fixed TRotateTool editor problem,
  wrong setting of mouse button property.

- Fixed TRotateTool bug when using Outline,
  in 3D Orthogonal mode.

- Fixed problems when setting TeeDefs.inc file
  defines for "single" and "extended" value defaults.

- Fixed display using "Large fonts" of Gradient
  and Shadow editor dialogs.

- Fixed compilation for Borland Kylix 3 and Open Edition

- Fixed AV using NearestTool and clearing a Series.

- Fixed naming problem of TeeFunction components at 
  design-time under Delphi 4,5 and BCB 4,5.

- Minor fixes for language translation units.

- Fixed Line series wrong color display when 
  ColorEachLine=False and 3D mode.

- Several fixes for CLX/Linux editor dialogs.

- Fixed Area series in "Stairs" mode and Transparency.

- Some speed improvements in Canvas objects.

- Fixed bug saving a Metafile (in wmf format) from 
  a TeeCommander Save button.

- Fixed bug in ReplaceChart procedure, used by
  Axis Labels to show multi-lines labels.


Changes:
--------

* TCLVFunction renamed property:
   From "Acumulate" to "Accumulate" (wrong spelling)

===========================================
http://www.steema.com
email: support@steema.com
-------------------------------------------