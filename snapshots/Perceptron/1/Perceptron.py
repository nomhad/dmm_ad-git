# -*- coding: cp1252 -*-
import socket
import random
import PIL
import Image
import os
import PIL
import Image
import time
from numpy import ones, zeros, array

#D�claration globale
global Size, Position, Threshold, Map, Zone1, Zone2, Zone3, Zone4, Pattern_1, Pattern_2, Pattern_3, Pattern_4, Weight, fileLog, NbPatterns


Size = zeros((5,2)) # [[ W0.  H0.][  W1.   H1.][  W2.   H2.]]
Position = zeros((4,2))# [[ XZone1. YZone1.][  XZone2. YZone2.][  XZone3. YZone3.][XZone4. YZone4]]
NbPatterns = 4 #Nombre de patterns a reconnaitre
Threshold = 230 # Seuil
Nu =0.015# facteur de reconnaissance


def LOAD(PictureName, IdSize):
    #<--Preparation des donn�es image -->
    #chargement de l'image
    Img = Image.open(PictureName)

    if IdSize != 0:
        #couleur -> niveau de gris
        Img = Img.convert("L")
    
    #donnees image 
    DataImg = list(Img.getdata())
    
    #<-- chargement des listes de donn�es binaires -->
    #Taille image : 
    Size[IdSize]= Img.size
        
    #Initialisation de la matrice --> tous les pixels sont blanc.
    Matrice = zeros((Size[IdSize,1], Size[IdSize,0]))
    
    #Remplissage de la matrice de donn�es binaires:
    Pixel = 0 #indice de parcours du DataImg pixel par pixel
    for i in range(0,int(Size[IdSize,1]),1):
        for j in range(0,int(Size[IdSize,0]),1):
            if DataImg[Pixel] <= Threshold:
                Matrice[i,j] = 1 #le pixel passe noir
            Pixel += 1#On passe au pixel suivant
    return Matrice


def LEARN():
    global fileLog
    fileLog.write("Learning...\n")
    print "Learning..."
    for t in range (0,100,1):#--< nombre d'epochs.
        #--< On reconnait les patterns >--
        for Reseau in range(0,NbPatterns,1): # pour chaque reseau
            Sum=0
            # Sommation des poids du pattern � reconnaitre par le r�seau en cours
            if Reseau == 0:
                Sum = Weight[:,:,Reseau] * Pattern_1[:,:] 
            elif Reseau == 1:
                Sum = Weight[:,:,Reseau] * Pattern_2[:,:]
            elif Reseau == 2:
                Sum = Weight[:,:,Reseau] * Pattern_3[:,:]
            elif Reseau == 3:
                Sum = Weight[:,:,Reseau] * Pattern_4[:,:]
            Sum = Sum.sum()
            #Valorisation des poids du pattern � reconnaitre par le r�seau en cours (inclusion)
            if Sum <= Threshold:
                if Reseau == 0:
                    Weight[:,:,Reseau] += Nu * Pattern_1[:,:]
                elif Reseau == 1:
                    Weight[:,:,Reseau] += Nu * Pattern_2[:,:]
                elif Reseau == 2:
                    Weight[:,:,Reseau] += Nu * Pattern_3[:,:]
                elif Reseau == 3:
                    Weight[:,:,Reseau] += Nu * Pattern_4[:,:]
            
            #--< On exclu l'autre reseau >--
            #Sommation des poids du pattern � exclure par le r�seau en cours
            Sum = 0
            if Reseau == 0:
                Sum = Weight[:,:,Reseau] * Pattern_2[:,:]
                Sum += Weight[:,:,Reseau] * Pattern_3[:,:]
                Sum += Weight[:,:,Reseau] * Pattern_4[:,:]
            elif Reseau == 1:
                Sum = Weight[:,:,Reseau] * Pattern_1[:,:]
                Sum += Weight[:,:,Reseau] * Pattern_3[:,:]
                Sum += Weight[:,:,Reseau] * Pattern_4[:,:]
            elif Reseau == 2:
                Sum = Weight[:,:,Reseau] * Pattern_1[:,:]
                Sum += Weight[:,:,Reseau] * Pattern_2[:,:]
                Sum += Weight[:,:,Reseau] * Pattern_4[:,:]                
            elif Reseau == 3:
                Sum = Weight[:,:,Reseau] * Pattern_1[:,:]
                Sum += Weight[:,:,Reseau] * Pattern_2[:,:]
                Sum += Weight[:,:,Reseau] * Pattern_3[:,:]
                
            Sum = Sum.sum()
            
            #D�valorisation des poids du pattern � exclure par le r�seau en cours(exclusion)
            if Sum > Threshold:
                if Reseau == 0:
                    Weight[:,:,Reseau] -= Nu * Pattern_2[:,:]
                    Weight[:,:,Reseau] -= Nu * Pattern_3[:,:]
                    Weight[:,:,Reseau] -= Nu * Pattern_4[:,:]
                elif Reseau == 1:
                    Weight[:,:,Reseau] -= Nu * Pattern_1[:,:]
                    Weight[:,:,Reseau] -= Nu * Pattern_3[:,:]
                    Weight[:,:,Reseau] -= Nu * Pattern_4[:,:]
                elif Reseau == 2:
                    Weight[:,:,Reseau] -= Nu * Pattern_1[:,:]
                    Weight[:,:,Reseau] -= Nu * Pattern_2[:,:]
                    Weight[:,:,Reseau] -= Nu * Pattern_4[:,:]
                elif Reseau == 3:
                    Weight[:,:,Reseau] -= Nu * Pattern_1[:,:]
                    Weight[:,:,Reseau] -= Nu * Pattern_2[:,:]
                    Weight[:,:,Reseau] -= Nu * Pattern_3[:,:]
    fileLog.write("Learning : ok \n")
    print "Leanrning : ok \n"
    fileLog.write("     - Circle weight = "+str(Weight[:,:,0].sum())+"\n")
    fileLog.write("     - Square weight = "+str(Weight[:,:,1].sum())+"\n")
    fileLog.write("     - Diamond weight = "+str(Weight[:,:,2].sum())+"\n")
    fileLog.write("     - Triangle weight = "+str(Weight[:,:,3].sum())+"\n")
    print "     - Circle weight =",Weight[:,:,0].sum(),"\n"
    print "     - Square weight =",Weight[:,:,1].sum(),"\n"
    print "     - Diamond weight =",Weight[:,:,2].sum(),"\n"
    print "     - Triangle weight =",Weight[:,:,3].sum(),"\n"

def COUNT():
    global msg, fileLog, CountObject
    Res = zeros((int(Size[0,1]),int(Size[0,0])))
    msg = ""
    CountObject = 0
    for z in range(0,NbPatterns,1):
        flagFound = 0
        fileLog.write("     Detecting Zone "+ str(z+1)+"\n")
        print"      Detecting Zone",z+1
        if z ==0:
            if Zone1.sum()<1000:
                continue
            Height, Width = int(Zone1.shape[0]), int(Zone1.shape[1])
            ColumnMin, LineMin = int(Position[0,0]), int(Position[0,1])
            
        elif z ==1:
            if Zone2.sum()<1000:
                continue
            Height, Width = int(Zone2.shape[0]), int(Zone2.shape[1])
            ColumnMin, LineMin = int(Position[1,0]), int(Position[1,1])
            
        elif z ==2:
            if Zone3.sum()<1000:
                continue
            Height, Width = int(Zone3.shape[0]), int(Zone3.shape[1])
            ColumnMin, LineMin = int(Position[2,0]), int(Position[2,1])

        elif z ==3:
            if Zone4.sum()<1000:
                continue
            Height, Width = int(Zone4.shape[0]), int(Zone4.shape[1])
            ColumnMin, LineMin = int(Position[3,0]), int(Position[3,1])
            
        for Line in range(LineMin,((LineMin+Height)-int(Size[1,1])),1):
            for Column in range (ColumnMin,((ColumnMin+Width)-int(Size[1,0])),1):
                for r in range (0,4,1):
                    Sum = 0
                    if Res[Line:Line+int(Size[1,1]),Column:Column+int(Size[1,0])].sum() == 0:#Si la zone ne contient pas de pixels d�j� reconnus
                        Sum = Weight[:,:,r] * Map[Line:Line+int(Size[1,1]),Column:Column+int(Size[1,0])]
                        Sum = Sum.sum()
                        code = ""
                        if Sum > Threshold:
                            if r == 0:
                                Code = "1|"+str(z+1)+"|undefined"
                                fileLog.write("         Circle : "+str(Code)+"\n")
                                print "         Circle :",Code
                                
                            elif r == 1:
                                Code = "2|"+str(z+1)+"|undefined"
                                fileLog.write("         Square : "+str(Code)+"\n")
                                print "         Square :",Code
                               
                            elif r == 2:
                                Code = "3|"+str(z+1)+"|undefined"
                                fileLog.write("         Diamond : "+str(Code)+"\n")
                                print "         Diamond :",Code
                                
                            elif r == 3:
                                Code = "4|"+str(z+1)+"|undefined"
                                fileLog.write("         Triangle : "+str(Code)+"\n")
                                print "         Triangle :",Code

                            flagFound = 1
                            CountObject +=1

                            #On marque les pixels comme d�j� reconnu avec un contour de 5px (pour distinguer 2 motifs tr�s proches)
                            Res[Line:Line+int(Size[1,1]),Column:Column+int(Size[1,0])] = ones((Size[1,1],Size[1,0]))
                            if msg =="":
                                msg = Code
                            else:
                                msg += ";"+ Code
                    if flagFound == 1:
                        break
                if flagFound == 1:
                    break
            if flagFound == 1:
                break

def SESSION():
    #D�claration globale
    global Size, Position, Threshold, Map, Zone1, Zone2, Zone3, Zone4, Pattern_1, Pattern_2, Pattern_3, Pattern_4, Weight, msg, fileLog, CountObject
    try:
        port = 6667
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind(("", port))
        server_socket.listen(5)

        fileLog.write("\n Server start :"+time.strftime("%Y-%m-%d %H:%M:%S\n"))
        fileLog.write("\nWaiting for a client on port :"+str(port)+"\n")
        print"Waiting for a client..."
        client_socket, adress = server_socket.accept()

        fileLog.write("Client found : "+str(adress)+"\n")
        print"Client found:"+str(adress)
        client_socket.send("Connection OK")

        fileLog.write("\n> Session : engaged <\n")
        print "\n> Session : engaged <"
        
        
        i=0 #compteur image
        while 1:
            i+=1            
            data = ""
            
            #reception de la taille image:
            lg = client_socket.recv(6)
            if lg == "":
                fileLog.write("\n\n> Session : disengaged <\n")
                print "\n> Session : disengaged <\n"
                break
            else:
                fileLog.write("\n\n- Picture Treatment : "+str(i)+"\n")
                print "\n- Picture Treatment : ",i
                fileLog.write("     Picture data size : "+str(lg)+"\n")
                print "  Picture size recieved :",lg
            
            #reception des donnees de l'image
            while len(data) < int(lg):
                data += client_socket.recv(4096)
    
            fileLog.write("     Picture data recieved : Successful \n")
            print ("  Picture data recieved : Successful \n")
            
            #image RGB (originale)
            im = Image.fromstring("RGB",(640,480),data)
            im = im.convert("L")
            #donnees image 
            DataImg = list(im.getdata())
            
            for Pixel in range(0,(640*480),1):
                if DataImg[Pixel] <= 200:
                    DataImg[Pixel] = 0 #le pixel est vu noir
                else:
                    DataImg[Pixel] = 1 #le pixel est vu blanc

            im = Image.new("1",(640,480))
            im.putdata(DataImg)
            
            im.save("Map\\Patt_"+str(i)+".tiff")
            fileLog.write("     Map\Patt_"+str(i)+".tiff : Created \n")
            print "  RGB picture : ok"
            

            #--------------------< Traitement de l'image >----------------------------------------------------------------------------------------------------

            #Cr�ation de la matrice de donn�es de la map
            Map = LOAD("Map\\Patt_"+str(i)+".tiff",0)
            #On efface le centre de la map:
            Map[((int(Size[0,1])/2)-70):((int(Size[0,1])/2)+70) , ((int(Size[0,0])/2)-70):((int(Size[0,0])/2)+70)] = zeros((140,140))

            #On r�cup�re les 4 zones a analyser:
            Zone1 = Map[((int(Size[0,1])/2)-100):((int(Size[0,1])/2)+100) , ((int(Size[0,0])/2)+70):((int(Size[0,0])/2)+320)]
            Position[0,0],Position[0,1] = ((int(Size[0,0])/2)+70), ((int(Size[0,1])/2)-100)

            Zone2 = Map[((int(Size[0,1])/2)-240):((int(Size[0,1])/2)-70) , ((int(Size[0,0])/2)-120):((int(Size[0,0])/2)+120)]
            Position[1,0],Position[1,1] = ((int(Size[0,0])/2)-120), ((int(Size[0,1])/2)-240)

            Zone3 =Map[((int(Size[0,1])/2)-100):((int(Size[0,1])/2)+100) , ((int(Size[0,0])/2)-320):((int(Size[0,0])/2)-70)]
            Position[2,0],Position[2,1] = ((int(Size[0,0])/2)-320), ((int(Size[0,1])/2)-100)

            Zone4 =Map[((int(Size[0,1])/2)+70):((int(Size[0,1])/2)+240) , ((int(Size[0,0])/2)-120):((int(Size[0,0])/2)+120)]
            Position[3,0],Position[3,1] = ((int(Size[0,0])/2)-120), ((int(Size[0,1])/2)+70)

            
            #R�sultats
            COUNT()
            if CountObject!= 2:#Si on a pas detect� au moins 2 �l�ments, la detection est erron�e.
                msg = "Detection failed!"
                fileLog.write("\n   >>>Result sent : "+str(msg)+"\n")
                client_socket.send(msg)
                print "\n   Sent:",msg, "\n"
                break
                
            else:
                msg = str(CountObject+1)+";"+msg
            
            fileLog.write("\n   >>>Result sent : "+str(msg)+"\n")
            client_socket.send(msg)
            print "\n   Sent:",msg, "\n"
        client_socket.send("Connection Break")
        fileLog.write("Connection Break \n")
        print "Connection Break\n"
        server_socket.close()
        fileLog.write("End")
    except Exception, exp:
        exp = str(exp)
        fileLog.write("\n\n\n"+len(exp)*"="+"\nExit with error !!!\n"+len(exp)*"-"+"\n")
        print"Exit with error : \n"
        fileLog.write(str(exp)+"\n"+len(exp)*"="+"\n")
        print"  "+str(exp)+"\n"
    finally:
        fileLog.close()


#---------------< MainProcess >------------------------------------------
#Cr�ation d'un fichier Log
fileLog = open("Log_Recognition.log","w")
#Cr�ation des matrices de donn�es des patterns
Pattern_1 = LOAD("Patterns\\1.bmp",1)
Pattern_2 = LOAD("Patterns\\2.bmp",2)
Pattern_3 = LOAD("Patterns\\3.bmp",3)
Pattern_4 = LOAD("Patterns\\4.bmp",4)
#D�finition de la matrice poids:
Weight = zeros((Size[1,1],Size[1,0],4))
#Apprentissage
LEARN()


FlagEnd = 0
while FlagEnd != 1:
    fileLog = open("Log_Recognition.log","w")
    #Traitement d'une image
    SESSION()

    Ans = ""
    while (Ans != "N" and Ans != "Y"):
        Ans = raw_input("Load other session ? (Y/N) ").upper()
        print "Your choice : ", Ans
    if (Ans == "N"):
        FlagEnd = 1
        print "\nYou can close this process."
    else:
        print "\nLoading..."
