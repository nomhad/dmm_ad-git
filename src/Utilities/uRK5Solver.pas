{
This unit is used to perform a fifth order Runge Kutta integration
It is based on the C version from NetLib, but does not allow for multiple
equations to be integrated at the same time

See rk.pdf (in this directory) for values for the Cash-Karp step and
Runge-Kutta code
}
unit uRK5Solver;


interface

uses Classes, SysUtils, Math;

type
  TrkPoint = record
    X, Y: real;
  end;
  TDerivativeFunction = function(x,y: real): real of object;
  ERKException = class(Exception);

  TRK5Solver = class(TObject)
  private
    FMinStepSize,
    FMaxStepSize: real;
    FDerivativeFunction: TDerivativeFunction;
    function CashKarpStep(APoint: TrkPoint; StepSize: real;
                         var Error: real): real;
  public
    constructor Create;
    destructor Destroy;
    property MinStepSize: real read FMinStepSize write FMinStepSize;
    property MaxStepSize: real read FMaxStepSize write FMaxStepSize;
    property DerivativeFunction: TDerivativeFunction read FDerivativeFunction write FDerivativeFunction;
    function Integrate(APoint: TrkPoint; YScale, EPS: real;
                 var ActualError, StepSize: real): TrkPoint;
  end;

implementation
{ TRK5Solver }

constructor TRK5Solver.Create;
begin
  inherited;
end;

destructor TRK5Solver.Destroy;
begin
  inherited;
end;

function TRK5Solver.CashKarpStep(APoint: TrkPoint; StepSize: real; var Error: real): real;
const
  //These are the Cash-Karp parameters for the embedded fifth order
  //Runge-Kutta method
  a: array[2..6] of real = (0.2, 0.3, 0.6, 1.0, 0.875);
  b: array[2..6, 1..5] of real = ((0.2,0,0,0,0),
                                  (3/40, 9/40, 0,0,0),
                                  (0.3, -0.9, 1.2, 0,0),
                                  (-11/54, 2.5, -70/27, 35/27, 0),
                                  (1631/55296, 175/512, 575/13824, 44275/110592, 253/4096));
  c: array[1..6] of real = (37/378,
                            0,
                            250/621,
                            125/594,
                            0,
                            512/1771);
  c_star: array[1..6] of real = (2825/27648,
                                 0,
                                 18575/48384,
                                 13525/55296,
                                 277/14336,
                                 0.25);

var k: array[1..6] of real;
begin
  k[1] := FDerivativeFunction(APoint.X, APoint.Y);
// ?ignore k[2] as it is mulltiplied by c[2] and c_star[2] both of which = 0
  k[2] := FDerivativeFunction(APoint.X + (a[2] * StepSize),
                              APoint.Y + (b[2,1] * k[1]));
  k[3] := FDerivativeFunction(APoint.X + (a[3] * StepSize),
                              APoint.Y + (b[3,1] * k[1])
                                       + (b[3,2] * k[2]));
  k[4] := FDerivativeFunction(APoint.X + (a[4] * StepSize),
                              APoint.Y + (b[4,1] * k[1])
                                       + (b[4,2] * k[2])
                                       + (b[4,3] * k[3]));
  k[5] := FDerivativeFunction(APoint.X + (a[5] * StepSize),
                              APoint.Y + (b[5,1] * k[1])
                                       + (b[5,2] * k[2])
                                       + (b[5,3] * k[3])
                                       + (b[5,4] * k[4]));
  k[6] := FDerivativeFunction(APoint.X + (a[6] * StepSize),
                              APoint.Y + (b[6,1] * k[1])
                                       + (b[6,2] * k[2])
                                       + (b[6,3] * k[3])
                                       + (b[6,4] * k[4])
                                       + (b[6,5] * k[5]));

  result := APoint.Y + (StepSize * ((c[1] * k[1])
//                            + (c[2] * k[2])
                            + (c[3] * k[3])
                            + (c[4] * k[4])
//                            + (c[5] * k[5])
                            + (c[6] * k[6])));

  Error := StepSize * (((c_star[1] - c[1]) * k[1])
                     + ((c_star[3] - c[3]) * k[3])
                     + ((c_star[4] - c[4]) * k[4])
                     + ((c_star[5] - c[5]) * k[5])
                     + ((c_star[6] - c[6]) * k[6]));
end;

function TRK5Solver.Integrate(APoint: TrkPoint; YScale, EPS: real;
                   var ActualError, StepSize: real): TrkPoint;
const SafetyFactor = 0.9;
      ShrinkagePower = 0.25;
      GrowthPower = 0.2;
      ErrCon = 1.89E-4;
var eY,
    eError,
    eErrorPower,
    eStepSize: real;
begin
  repeat
    //do a single step calculation
    eY := CashKarpStep(APoint,StepSize,ActualError);
    //scale the error produced relative to required accuracy
    //which is 0.1 mv/ms
    eError := Abs(ActualError / YScale) / EPS;
    if eError > 1 then begin
      //the step has failed because the error produced was greater than
      //that allowed
      //so make the step size smaller and try again
      eErrorPower := Power(eError, ShrinkagePower);
      if eErrorPower > 1 then
        eErrorPower := 1;
      eStepSize := SafetyFactor * StepSize * eErrorPower;
      if eStepSize = 0 then
        raise ERKException.Create('Step size has decreased to zero');
      if abs(eStepSize) < abs((StepSize * 0.1)) then
        //don't decrease the step size by more than a factor of 10
        StepSize := StepSize * 0.1
      else
        StepSize := eStepSize;
    end;
  until eError < 1.0;

  //Step succeeded.  Compute estimated size of next step
  if eError > ErrCon then
    StepSize := SafetyFactor * StepSize * Power(eError, GrowthPower)
  else
    //never increase the step size by more than a factor of 5
    StepSize := StepSize * 5;
  //don't let the step size just keep increasing by factors of five
  //this can happen when the gradient is near zero for a length of time
  if StepSize > FMaxStepSize then
    StepSize := FMaxStepSize;
  if StepSize < FMinStepSize then
    StepSize := FMinStepSize;

  ActualError := eError;
  //return the new x and y values for the next step
  result.X := APoint.X + StepSize;
  result.Y := eY;
end;

end.
