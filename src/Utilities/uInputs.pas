unit uInputs;

interface

uses SysUtils;

function IsNumber(s: string): boolean;
function IsInteger(s: string): boolean;

implementation

function IsNumber(s: string): boolean;
var e: real;
begin
  result := false;
  if s = '-' then
    //allow just a minus sign
    EXIT;
  if s <> '' then try
    e := StrToFloat(s);
    result := true
  except
    //don't need to do anything
    Abort;
  end;
end;

function IsInteger(s: string): boolean;
var e: real;
begin
  result := false;
  if s = '-' then
    //allow just a minus sign
    EXIT;
  if s <> '' then try
    e := StrToInt(s);
    result := true
  except
    //don't need to do anything
    Abort;
  end;
end;

end.
