{
See Izhikevich's book, Dynamical systems in neuroscience for far more details
on these neuron models.
c:\AWork\References\Izhikevich 2006 Dynamical Systems in Neuroscience.pdf
Also the following papers:
Izhikevich, E. (2003). Simple Model of Spiking Neurons. IEEE Transactions on Neural Networks, 14, 1569-1572.
Izhikevich, E. (2004). Which Model to Use for Cortical Spiking Neurons?. IEEE Transactions on Neural Networks, 15, 1063-1070.
Izhikevich, E. (2007). Solving the Distal Reward Problem through Linkage of STDP and Dopamine Signaling. Cerebral Cortex, 17, 2443-2452.
Izhikevich, E. (2008). Large-Scale Model of Mammalian Thalamocortical Systems. Proceedings of the National Academy of Science, 105, 3593-3598.
}
unit uIzhikevich;

interface

uses Classes, Dialogs, SysUtils, Math;

type
  TNeuronType = (ntRegularSpiking, ntPhasicSpiking, ntTonicBursting,
                 ntPhasicBursting, ntFastSpikingOld, ntFastSpikingNew,
                 ntLowThresholdSpiking,
                 ntMixedMode, ntSpikeFrequencyAdaptation,
                 ntClass1, ntClass2, ntSpikeLatency, ntSubthresholdOscillations,
                 ntResonator, ntIntegrator, ntReboundSpike, ntReboundBurst,
                 ntThresholdVariability, ntBistability, ntDAP, ntAccommodation,
                 ntInhibitionInducedSpiking, ntInhibitionInducedBursting,
                 ntMSN, ntThalamocortical);

  TNeuronOutput = (noExcitatory, noInhibitory);

  ASynapse = record
    Fired: boolean;
    Post: integer; //number of the neuron that this synapse is on
    Weight,
    Lag: real;
  end;

  AnInput = record
   Time: real;
   Weight: real;
  end;

  TIzhikevich = class
  private
    Fa, Fb, Fc, Fd, FCurrent, Fu, Fk,
    FCm, FVm, FVRest, FVTh, FVPeak,
    FResolution: real;
    FBaseA, FBaseB, FBaseC, FBaseD, FBaseU, FBaseK,
    FBaseCm, FBaseVRest, FBaseVTh, FBaseVPeak: real;

    FFired: boolean;

    Duration,
    StartCurrent: integer;
    FNeuronType: TNeuronType;

    arOutputTo: array of TIzhikevich;
    FarOutgoing: array of ASynapse;
    FarInput: array of AnInput;
    FInputCount: integer;
    FSynapse,
    FIncomingCount: integer;
    FOutgoingCount: integer;
    FSynapseCount: integer;
    FarFired: array of real;
    FFiredCount: integer;
    FOutput: TNeuronOutput;
    FSdVTh: real;
    FSdVPeak: real;
    FSdBiasCurrent,
    FTargetCurrent,
    FLocalSynapticCurrent,
    FDistantSynapticCurrent: real;
    FSdVRest: real;
    FSDd: real;
    FSDk: real;
    FSDu: real;
    FSDb: real;
    FSDa: real;
    FSDc: real;
    FSDCm: real;
    FBiasCurrent,
    FSumOfFiring: real;
    FStructureName: string;
    FDecayTau: real;
    FJustFired: boolean;
    FTargetCurrentDelay: real;

    procedure CreateOutgoingSynapse;
    procedure RemoveOutgoingSynapse;

    function GetNeuronName: string;
    function GetNeuronTypeCount: integer;
    function GetNeuronTypeName(Index: integer): string;

    procedure SetCurrent(const Value: real);
    function GetCurrent: real;
    procedure SetOutgoingCount(const Value: integer);

    procedure CalculateMsnVm(WhatTime: real);
    procedure CalculateRsVm(WhatTime: real);
    procedure CalculateFsVm(WhatTime: real);
    procedure CalculateIbVm(WhatTime: real);
    procedure CalculateChVm(WhatTime: real);
    procedure CalculateLtsVm(WhatTime: real);
    procedure CalculateTcVm(WhatTime: real);
    procedure SetA(const Value: real);
    procedure SetB(const Value: real);
    procedure SetC(const Value: real);
    procedure SetD(const Value: real);
    procedure SetK(const Value: real);
    procedure SetU(const Value: real);
    procedure SetSDa(const Value: real);
    procedure SetSDb(const Value: real);
    procedure SetSDc(const Value: real);
    procedure SetSDd(const Value: real);
    procedure SetSDk(const Value: real);
    procedure SetSDu(const Value: real);
//    procedure SetSdCurrentBias(const Value: real);
    procedure SetSdVPeak(const Value: real);
    procedure SetSdVRest(const Value: real);
    procedure SetSdVTh(const Value: real);
    procedure SetBiasCurrent(const Value: real);
    procedure SetVPeak(const Value: real);
    procedure SetVRest(const Value: real);
    procedure SetVTh(const Value: real);
    procedure SetCm(const Value: real);
    procedure SetSDCm(const Value: real);

  public
    constructor Create;
    destructor Destroy; override;

    property a: real read Fa write SetA;
    property b: real read Fb write SetB;
    property c: real read Fc write SetC;
    property d: real read Fd write SetD;
    property u: real read Fu write SetU;
    property k: real read Fk write SetK;

    property SDa: real read FSDa write SetSDa;
    property SDb: real read FSDb write SetSDb;
    property SDc: real read FSDc write SetSDc;
    property SDd: real read FSDd write SetSDd;
    property SDu: real read FSDu write SetSDu;
    property SDk: real read FSDk write SetSDk;

    property Cm: real read FCm write SetCm;
    property SDCm: real read FSDCm write SetSDCm;

    property Vm: real read FVm write FVm;
    property Current: real read GetCurrent write SetCurrent;
    {The current bias is the minimum current needed to start the neuron firing
     This is different for different types of neurons. E.g. for RS it is 60, for
     CH it is 180. To create a continuum of neurons between two types this
     property is set to lie between the two values for the pure neuron type
    }
    property BiasCurrent: real read FBiasCurrent write SetBiasCurrent;
    property TargetCurrent: real read FTargetCurrent write FTargetCurrent;
    property TargetCurrentDelay: real read FTargetCurrentDelay write FTargetCurrentDelay;
    property LocalSynapticCurrent: real read FLocalSynapticCurrent write FLocalSynapticCurrent;
    property DistantSynapticCurrent: real read FDistantSynapticCurrent write FDistantSynapticCurrent;

    property VRest: real read FVRest write SetVRest;
    property VThreshold: real read FVTh write SetVTh;
    property VPeak: real read FVPeak write SetVPeak;

    property SdVRest: real read FSdVRest write SetSdVRest;
    property SdVThreshold: real read FSdVTh write SetSdVTh;
    property SdVPeak: real read FSdVPeak write SetSdVPeak;

    property Resolution: real read FResolution write FResolution;
    property Fired: boolean read FFired write FFired;
    property JustFired: boolean read FJustFired write FJustFired;
    procedure Fire(WhatTime: real);
    property DecayTau: real read FDecayTau write FDecayTau;
    property SumOfFiring: real read FSumOfFiring;
    function UpdateSumOfFiring(WhatTime: real): real;

    procedure InputOccurs(WhatTime, Weight: real);
    procedure RemoveUsedInputs(WhatTime: real);
    procedure ClearInputs;
    function GetSynapticCurrent(WhatTime: real): real;
    function SpikesInWindow(WhatTime: real; Window: integer): integer;

    property NeuronType: TNeuronType read FNeuronType;
    procedure SetNeuronType(NeuronType: TNeuronType);
    property NeuronTypeCount: integer read GetNeuronTypeCount;
    property NeuronTypeName[Index: integer]: string read GetNeuronTypeName;
    property Name: string read GetNeuronName;
    property Structure: string read FStructureName write FStructureName;
    property Output: TNeuronOutput read FOutput write FOutput;

    procedure AddOutput(ToNeuron: TIzhikevich; AvgWeight, Lag: real);
    procedure ClearOutputs;
    function OutputExists(ToNeuron: TIzhikevich): boolean;
    property OutgoingCount: integer read FOutgoingCount write SetOutgoingCount;
//    property OutgoingSynapse[Index: integer]: TSynapse read GetOutgoingSynapse;
    procedure SetAverageSynapticWeight(Weight: real);

    procedure CalculateMembranePotential(WhatTime: real);
    procedure ClearFiring;

end;

implementation

{ TIzhikevich }

Type
  Parameters = array[0..9] of real;

const
  NEURON_TYPES = 25;

  //parameters are a, b, c, d, CurrentBias , VRest, VThreshold, VPeak, Cm, k
  arParameters: array [0..NEURON_TYPES - 1] of Parameters =
    ((0.03, -2, -50, 100, 60, -60, -40, 35, 100, 0.7),  //regular spiking or tonic spinking
     (0.02, 0.25, -65, 6, 5, -64, 0, 0, 0, 0),           //phasic spiking
     (0.03, 1, -40, 150, 180, -60, -40, 25, 50, 1.5),    //tonic bursting or chattering
     (0.01, 5, -56, 130, 400, -75, -45, 50, 150, 1.2),   //intrinsic bursting
     (0.1, 0.2, -65, 2, 5, -55, -40, 25, 20, 1),         //fast spiking - using old equations
     (0.2, 0.025, -45, -55, 50, -55, -40, 25, 20, 1),     //fast spiking - using new equations
     (0.03, 8, -53, 20, 100, -56, -42, 40, 100, 1),      //low-threshold spiking
     (0.02, 0.2, -55, 4, 10, -70, 0, 0, 0, 0),           //mixed mode
     (0.01, 0.2, -65, 8, 30, -70, 0, 0, 0, 0),           //spike frequency adaptation
     (0.02, -0.1, -55, 6, 0, -60, 0, 0, 0, 0),
     (0.2, 0.26, -65, 0, 0, -64, 0, 0, 0, 0),
     (0.02, 0.2, -65, 6, 7, -70, 0, 0, 0, 0),
     (0.05, 0.26, -6, 0, 0, -62, 0, 0, 0, 0),
     (0.1, 0.26, -60, -1, 0, -62, 0, 0, 0, 0),
     (0.02, -0.1, -55, 6, 0, -60, 0, 0, 0, 0),
     (0.03, 0.25, -60, 4, 0, -64, 0, 0, 0, 0),
     (0.03, 0.25, -52, 0, 0, -64, 0, 0, 0, 0),
     (0.03, 0.25, -60, 4, 0, -64, 0, 0, 0, 0),
     (1, 1.5, -60, 0, -65, -61, 0, 0, 0, 0),
     (1, 0.2, -60, -21, 0, -70, 0, 0, 0, 0),
     (0.02, 1, -55, 4, 0, -65, 0, 0, 0, 0),
     (-0.02, -1, -60, 8, 80, -63.8, 0, 0, 0, 0),
     (-0.026, -1, -45, 0, 80, -63.8, 0, 0, 0, 0),
     (0.01, -20, -55, 150, 400, -80, -25, 40, 50, 1),  //MSN
     (0.01, 1, -60, 10, 100, -60, -50, 35, 200, 1.6)); //thalamocortical

  arNeuronTypes: array [0..NEURON_TYPES - 1] of string =
    ('regular spiking','phasic spiking','chattering','intrinsic bursting',
     'fast spiking - old','fast spiking - new','low-threshold spiking',
     'mixed mode','spike frequency adaptation',
     'class 1','class2','spike latency','subthreshold oscillations','resonator',
     'integrator','rebound spike','rebound burst','threshold variability',
     'bistability','DAP','accommodation','inhibition-induced spiking',
     'inhibition-induced bursting', 'Striatal medium spiny neuron',
     'thalamocortical neuron');

constructor TIzhikevich.Create;
begin
  inherited;
end;

destructor TIzhikevich.Destroy;
begin
  inherited;
end;

function TIzhikevich.GetNeuronName: string;
begin
  result := arNeuronTypes[integer(NeuronType)];
end;

function TIzhikevich.GetNeuronTypeCount: integer;
begin
  result := NEURON_TYPES;
end;

function TIzhikevich.GetNeuronTypeName(Index: integer): string;
begin
  result := arNeuronTypes[Index];
end;

procedure TIzhikevich.SetNeuronType(NeuronType: TNeuronType);
var i: integer;
begin
  FNeuronType := NeuronType;
  i := integer(NeuronType);
{
  Fa := arParameters[i,0];
  Fb := arParameters[i,1];
  Fc := arParameters[i,2];
  Fd := arParameters[i,3];
  FCurrent := arParameters[i,4];
//  FCurrentBias := arParameters[i,4];
  FVRest := arParameters[i,5];
  FVTh := arParameters[i,6];
  FVPeak := arParameters[i,7];
  FCm := arParameters[i,8];
  Fk := arParameters[i,9];
}
  FBasea := arParameters[i,0];
  FBaseb := arParameters[i,1];
  FBasec := arParameters[i,2];
  FBased := arParameters[i,3];
//  FBaseCurrentBias := arParameters[i,4];
  FBaseVRest := arParameters[i,5];
  FBaseVTh := arParameters[i,6];
  FBaseVPeak := arParameters[i,7];
  FBaseCm := arParameters[i,8];
  FBasek := arParameters[i,9];

  FSdA := 0.1 * FBasea;
  FSdB := 0.1 * FBaseb;
  FSdC := 0.1 * FBasec;
  FSdD := 0.1 * FBased;//  FSdCurrentBias := 0.1 * CurrentBias;
  FSdVRest := 0.1 * FBaseVRest;
  FSdVTh := 0.1 * FBaseVTh;
  FSdVPeak := 0.1 * FBaseVPeak;
  FSdCm := 0.1 * FBaseCm;
  FSdK := 0.1 * FBasek;

  a := FBasea;
  b := FBaseb;
  c := FBasec;
  d := FBased;
  k := FBasek;
  VRest := FBaseVRest;
  VThreshold := FBaseVTh;
  VPeak := FBaseVPeak;
  Cm := FBaseCm;
end;

{
The general equations for the simple Izhikevich model are:
Cdv/dt = k(v - vr)(v - vt) - u + I
du/dt = a[b(v - vr) - u]
If v >= vpeak then v <- c, u <- d

v: membrane potential (mV)
u: recovery current  (mV)
vr: resting membrane potential (mV)
vt: instantaneous threshold potential (mV)
vpeak: spike cutoff value (mV)
a: recovery time constant
b: sensitivity of u to sub-threeshold fluctiations of v. Greater values couple
    v and u more strongly resulting in possible sub-threshold oscillations
c: reset value of v after a spike
d: the sum of (outward - inward) currents activated during the spike and affecting
   after-spike behaviour
C: membrane capacitance (pF)
}
procedure TIzhikevich.CalculateMembranePotential(WhatTime: real);
var eSynapticCurrent: real;
begin
//  eSynapticCurrent := GetSynapticCurrent(WhatTime);
//  Current := Current + eSynapticCurrent;
  Case NeuronType of
    ntRegularSpiking:      CalculateRsVm(WhatTime);
    ntFastSpikingNew:      CalculateFsVm(WhatTime);
    ntMSN:                 CalculateMsnVm(WhatTime);
    ntTonicBursting:       CalculateChVm(WhatTime);
    ntPhasicBursting:      CalculateIbVm(WhatTime);
    ntLowThresholdSpiking: CalculateLtsVm(WhatTime);
    ntThalamocortical:     CalculateTcVm(WhatTime);
    else begin
      if FJustFired then begin
        //neuron has fired, Vm was set to 30 on the last iteration, now set it back
        //to c and set u
        Vm := c;
        u := u + d;
        FJustFired := false;
      end;
      Vm := Vm + (Resolution * ((0.04 * Vm * Vm) + (5 * Vm) + 140 - u + Current));
      u := u + (Resolution * (a * ((b * Vm) - u)));
      if Vm >= 30 then begin
        //neuron has fired, set Vm to 30, so all the spikes are the same size
        //and record that the neuron fired
        Vm := 30;
        Fire(WhatTime);
      end;
    end;
  end;
end;

{
the FS equation has a different form to all the other equations
need to generate the plateau potential
Equation comes from p311 of Izhikevich's book, dynamical systems in neuroscience
20dv/dt = (v+55)(v+40) - u + I
du/dt = 0.2[U(v) - u]
U(v) = 0 when v < vb, U(v) = 0.025(v - vb)pwr 3 when v>= vb
if v >= 25 then v <- -45
vr = -55mV
vt = -40mV
vpeak = 25mV
vb = -55
a = 0.2
b = 0.025 [in U(v)]
c = -45
d = ? (no reset value for u) use d in place of vb in the equations Izhikevich gives
k = 1
C = 20
}
procedure TIzhikevich.CalculateFsVm(WhatTime: real);
var Uv,
    Vb,
    DeltaV,
    DeltaU,
    eProportion: real;
begin
  if FJustFired then begin
    Vm := c;
//    u := u + d; //u is calculated at each time step, so can't be set here as for other neuron types
    FJustFired := false;
  end;
//   DeltaV := (Vm - FVRest) * (Vm - FVTh);
//  DeltaV := DeltaV - u + Current;
//DeltaV := DeltaV * Resolution / FCm;
//Vm := Vm + DeltaV;
  Vm := Vm + ((Resolution)* (((Vm - FVRest) * (Vm - FVTh)) - u + Current) / FCm);
//  Vm := Vm + ((Resolution / 2)* (((Vm - FVRest) * (Vm - FVTh)) - u + Current) / FCm);
//  u := u + (Resolution * (a * ((b * (Vm - FVRest)) - u)));
  if Vm < d then
    Uv := 0
  else begin
    DeltaV := Vm - d;
    Uv := b * Power((Vm - d), 3);
//    Uv := b * DeltaV * DeltaV * DeltaV;
  end;
  DeltaU := Uv - u;
  DeltaU := a * DeltaU;
  u := (Resolution) * (u + DeltaU);
//  u := (Resolution / 2) * (u + DeltaU);
  if Vm >= FVPeak then begin
//    eProportion := (Vm - FVPeak) / FVPeak;
//      u := u / eProportion;
    if u > 520 then
      u := 520;
    Vm := FVPeak;
    Fire(WhatTime);
  end;
end;

{
the MSN equation has a different form to all the other equations
need to generate the plateau potential
Equation comes from p311 of Izhikevich's book, dynamical systems in neuroscience
50dv/dt = (v+80)(v+25) - u + I
du/dt = 0.01[-20(v+80) - u]
if v >= 40 then v := -55, u := u + 150
vr = -80mV
vt = -25mV
vpeak = 40mV
a = 0.01
b = -20
c = -55
d = 150
k = 1
C = 50
}
procedure TIzhikevich.CalculateMsnVm(WhatTime: real);
begin
  if FJustFired then begin
    Vm := c;
    u := u + d;
    FJustFired := false;
  end;
  Vm := Vm + (Resolution* (((Vm - FVRest) * (Vm - FVTh)) - u + Current) / FCm);
//    if Vm > - 60 then begin
//      Vm := Vm - ((Vm + 60) * 0.55);
//    end;
  u := u + (Resolution * (a * ((b * (Vm - FVRest)) - u)));
  if Vm >= FVPeak then begin
    Vm := FVPeak;
    Fire(WhatTime);
  end;
end;

{
Equation comes from p283 of Izhikevich's book, dynamical systems in neuroscience
100dv/dt = 0.7(v+60)(v+40) - u + I
du/dt = 0.03[-2(v+60) - u]
if v >= 35 then v := -50, u := u + 100
vr = -60mV
vt = -40mV
vpeak = 35mV
a = 0.03
b = -2
c = -50
d = 100
k = 0.7
C = 100
}
procedure TIzhikevich.CalculateRsVm(WhatTime: real);
begin
  if FJustFired then begin
    Vm := c;
    u := u + d;
    FJustFired := false;
  end;
  Vm := Vm + (Resolution * ((k * (Vm - FVRest) * (Vm - FVTh)) - u + Current) / FCm);
  u := u + (Resolution * (a * ((b * (Vm - FVRest)) - u)));
  if Vm >= FVPeak then begin
    Vm := FVPeak;
    Fire(WhatTime);
  end;
end;

{
Equation comes from p290 of Izhikevich's book, dynamical systems in neuroscience
150dv/dt = 1.2(v+75)(v+45) - u + I
du/dt = 0.01[5(v+75) - u]
if v >= 50 then v := -56, u := u + 130
vr = -75mV
vt = -45mV
vpeak = 50mV
a = 0.01
b = 5
c = -56
d = 130
k = 1.2
C = 150
}
procedure TIzhikevich.CalculateIbVm(WhatTime: real);
begin
  if FJustFired then begin
    Vm := c;
    u := u + d;
    FJustFired := false;
  end;
  Vm := Vm + (Resolution * ((1.2* (Vm - FVRest) * (Vm - FVTh)) - u + Current) / FCm);
  u := u + (Resolution * (a * ((b * (Vm - FVRest)) - u)));
  if Vm >= FVPeak then begin
    Vm := FVPeak;
    Fire(WhatTime);
  end;
end;

{
Equation comes from p311 of Izhikevich's book, dynamical systems in neuroscience
50dv/dt = 1.5(v+60)(v+40) - u + I
du/dt = 0.03[(v+60) - u]
if v >= 25 then v := -40, u := u + 150
vr = -60mV
vt = -40mV
vpeak = 25mV
a = 0.03
b = 1
c = -40
d = 150
k = 1.5
C = 50
}
procedure TIzhikevich.CalculateChVm(WhatTime: real);
begin
  if FJustFired then begin
    Vm := c;
    u := u + d;
    FJustFired := false;
  end;
  Vm := Vm + (Resolution * ((1.5* (Vm - FVRest) * (Vm - FVTh)) - u + Current) / FCm);
  u := u + (Resolution * (a * ((b * (Vm - FVRest)) - u)));
  if Vm >= FVPeak then begin
    Vm := FVPeak;
    Fire(WhatTime);
  end;
end;

{
Equation comes from p297 of Izhikevich's book, dynamical systems in neuroscience
100dv/dt =  (v+56)(v+42) - u + I
du/dt = 0.03[8(v+56) - u]
if v >= 40 - 0.1u then v <- -53 + 0.04u, u <- min[u + 20, 670]
vr = -56mV
vt = -42mV
vpeak = 40 - 0.1u
a = 0.03
b = 8
c = -53 + 0.04u
d = 20
k = 1
C = 100
}
procedure TIzhikevich.CalculateLtsVm(WhatTime: real);
begin
  if FJustFired then begin
    Vm := c  + (0.04 * u);
    if (u + d) > 670 then
      u := 670
    else
      u := u + d;
    FJustFired := false;
  end;
  Vm := Vm + (Resolution * (((Vm - FVRest) * (Vm - FVTh)) - u + Current) / FCm);
  u := u + Resolution * (a * ((b * (Vm - FVRest)) - u));
  if Vm >= FVPeak - (0.1 * u) then begin
    Vm := FVPeak - (0.1 * u);
    Fire(WhatTime);
  end;
end;

{
Equation comes from p305 of Izhikevich's book, dynamical systems in neuroscience
200dv/dt =  1.6(v+60)(v+50) - u + I
du/dt = 0.01[b(v+65) - u]
b = 15 if v <= -65, otherwise b = 0
if v >= 35 + 0.1u then v <- -60 - 0.1u, u <- u + 10
vr = -60mV
vt = -50mV
vpeak = 35 + 0.1u
a = 0.01
b = if v <= -65, b = 15, else b = 0
c = -60 - 0.1u
d = 10
k = 1.6
C = 200
}
procedure TIzhikevich.CalculateTcVm(WhatTime: real);
begin
  if FJustFired then begin
    Vm := c  - (0.1 * u);
    u := u + d;
    FJustFired := false;
  end;
  Vm := Vm + (Resolution * (((Vm - FVRest) * (Vm - FVTh)) - u + Current) / FCm);
  if Vm <= -65 then
    b := 15
  else
    b := 0;
  u := u + Resolution * (a * ((b * (Vm - FVRest)) - u));
  if Vm >= FVPeak + (0.1 * u) then begin
    Vm := FVPeak + (0.1 * u);
    Fire(WhatTime);
  end;
end;

function TIzhikevich.GetCurrent: real;
begin
  result := BiasCurrent
          + TargetCurrent
          + LocalSynapticCurrent
          + DistantSynapticCurrent;;
end;

procedure TIzhikevich.SetCurrent(const Value: real);
begin
  FCurrent := Value;
end;

procedure TIzhikevich.SetOutgoingCount(const Value: integer);
begin
{
  while FOutgoingCount < Value do
    CreateOutgoingSynapse;
  while FOutgoingCount > Value do
    RemoveOutgoingSynapse;
}
end;

procedure TIzhikevich.CreateOutgoingSynapse;
begin
{
  FSynapse := TSynapse.Create;
  inc(FOutgoingCount);
  SetLength(FarOutgoing, FOutgoingCount);
  FarOutgoing[pred(FOutgoingCount)] := FSynapse;
  if FOutput = noExcitatory then
    FSynapse.SynapseType := stExcitatory
  else
    FSynapse.SynapseType := stInhibitory;
}
end;

procedure TIzhikevich.RemoveOutgoingSynapse;
begin
{
  dec(FOutgoingCount);
  FarOutgoing[FOutgoingCount].Free;
  SetLength(FarOutgoing, FOutgoingCount);
}
end;

procedure TIzhikevich.ClearInputs;
begin
  FInputCount := 0;
  if FarInput <> nil then
    SetLength(FarInput,FInputCount);
end;

{
When the neuron fires, make a record of the time and the weight in all the neurons
that this neuron connects to. These neurons are in other ensembles.
}
procedure TIzhikevich.Fire(WhatTime: real);
var iNeuron,
    iFired: integer;
begin

  inc(FFiredCount);
  SetLength(FarFired, FFiredCount);
  for iFired := FFiredCount - 2 downto 0 do
    //always put the last firing event at the 0 position
    FarFired[iFired+1] := FarFired[iFired];
  FarFired[0] := WhatTime;
  FFired := true;
  FJustFired := true;
end;

{
Each firing event decays exponentially and is removed from the list after 5
time constants. The SumOfFiring is the sum of the current levels of all firing
events in the list
}
function TIzhikevich.UpdateSumOfFiring(WhatTime: real): real;
var iFired: integer;
    eDelay,
    eProportion: real;
begin
  result := 0;
  for iFired := pred(FFiredCount) downto 0 do begin
    eDelay := WhatTime - FarFired[iFired];
    if eDelay >= 0 then begin
      //if the delay is less than 0, then the lag means that the input has not
      //reached this synapse yet
      eProportion := Exp(-1 * eDelay / FDecayTau);
      result := result + eProportion;
    end;
    FSumOfFiring := result;
    if eDelay >= (5 * FDecayTau) then begin
      //remove the final entry from the column
      if iFired <> pred(FFiredCount) then begin
        //something has gone wrong. Trying to remove a later firing before an earlier one
        ShowMessage('Removing wrong firing incident');
      end;
      dec(FFiredCount);
      SetLength(FarFired, FFiredCount);
    end;
  end;
  if FFiredCount = 0 then
    FFired := false;
end;

procedure TIzhikevich.InputOccurs(WhatTime, Weight: real);
begin
  inc(FInputCount);
  SetLength(FarInput, FInputCount);
  FarInput[pred(FInputCount)].Time := WhatTime;
  FarInput[pred(FInputCount)].Weight := Weight;
end;

function TIzhikevich.GetSynapticCurrent(WhatTime: real): real;
var iSynapse,
    iInput: integer;
    eConductance,
    eCurrent: real;
begin
  result := 0;
{
  for iInput := 0 to pred(FInputCount) do begin
    if WhatTime >= FarInput[iInput].Time then begin
      result := result + FarInput[iInput].Weight;
    end;
  end;
  RemoveUsedInputs(WhatTime);
}
end;

procedure TIzhikevich.RemoveUsedInputs(WhatTime: real);
var iInput,
    jInput: integer;
begin
  for iInput := pred(FInputCount) downto 0 do begin
    if FarInput[iInput].Time <= WhatTime then begin
      for jInput := iInput to FInputCount - 2 do begin
        FarInput[jInput].Time := FarInput[succ(jInput)].Time;
        FarInput[jInput].Weight := FarInput[succ(jInput)].Weight;
      end;
      dec(FInputCount);
      SetLength(FarInput,FInputCount);
    end;
  end;
end;

{
Return the number of spikes fired in a given period.
This is used to calculate the average firing rate for the ensemble
The function returns the number of spikes in the period (WhatTime - Window) to
WhatTime
}
function TIzhikevich.SpikesInWindow(WhatTime: real; Window: integer): integer;
var iWhichSpike: integer;
begin
  result := 0;
//  iWhichSpike := pred(FFiredCount);
  iWhichSpike := 0;
  while (iWhichSpike < FFiredCount) and (FarFired[iWhichSpike] > WhatTime - Window) do begin
    inc(result);
//    dec(iWhichSpike);
    inc(iWhichSpike);
  end;
end;

{
Remove the record of the spikes fired last time
}
procedure TIzhikevich.ClearFiring;
var iNeuron: integer;
begin
  SetLength(FarFired, 0);
  FFiredCount := 0;
  FFired := false;
  for iNeuron := 0 to pred(FOutgoingCount)do
    arOutputTo[iNeuron].ClearInputs;
end;

procedure TIzhikevich.AddOutput(ToNeuron: TIzhikevich; AvgWeight, Lag: real);
begin
  inc(FOutgoingCount);
  SetLength(arOutputTo,FOutgoingCount);
  arOutputTo[pred(FOutgoingCount)] := ToNeuron;
  SetLength(FarOutgoing,FOutgoingCount);
  FarOutgoing[pred(FOutgoingCount)].Fired := false;
  FarOutgoing[pred(FOutgoingCount)].Weight := RandG(AvgWeight, AvgWeight / 10);
  FarOutgoing[pred(FOutgoingCount)].Lag := Lag;
end;

function TIzhikevich.OutputExists(ToNeuron: TIzhikevich): boolean;
var iNeuron: integer;
begin
  result := false;
  for iNeuron := 0 to pred(FOutgoingCount) do begin
    if arOutputTo[iNeuron] = ToNeuron then begin
      result := true;
      EXIT;
    end;
  end;
end;

procedure TIzhikevich.ClearOutputs;
var iNeuron: integer;
begin
  for iNeuron := 0 to pred(FOutgoingCount) do
    arOutputTo[iNeuron] := nil;
  FOutgoingcount := 0;
  SetLength(arOutputTo, FOutgoingcount);
  SetLength(FarOutgoing, FOutgoingcount);
end;

procedure TIzhikevich.SetAverageSynapticWeight(Weight: real);
var iOutput: integer;
begin
  for iOutput := 0 to pred(FOutgoingCount) do
//    FarOutgoing[iOutput].Weight := RandG(Weight, Weight / 10);
    FarOutgoing[iOutput].Weight := Random * Weight;
end;

procedure TIzhikevich.SetA(const Value: real);
begin
  FBaseA := Value;
  Fa := FBaseA + RandG(0,FSdA);
end;

procedure TIzhikevich.SetB(const Value: real);
begin
  FBaseB := Value;
  FB := FBaseB + RandG(0,FSdB);
end;

procedure TIzhikevich.SetC(const Value: real);
begin
  FBaseC := Value;
  FC := FBaseC + RandG(0,FSdC);
end;

procedure TIzhikevich.SetD(const Value: real);
begin
  FBaseD := Value;
  FD := FBaseD + RandG(0,FSdD);
end;

procedure TIzhikevich.SetK(const Value: real);
begin
  FBaseK := Value;
  FK := FBaseK + RandG(0,FSdK);
end;

procedure TIzhikevich.SetU(const Value: real);
begin
  FBaseU := Value;
  FU := FBaseU + RandG(0,FSdU);
end;

procedure TIzhikevich.SetSDa(const Value: real);
begin
  FSDA := Value;
  FA := FBaseA + RandG(0,FSdA);
end;

procedure TIzhikevich.SetSDb(const Value: real);
begin
  FSDB := Value;
  FB := FBaseB + RandG(0,FSdB);
end;

procedure TIzhikevich.SetSDc(const Value: real);
begin
  FSDC := Value;
  FC := FBaseC + RandG(0,FSdC);
end;

procedure TIzhikevich.SetSDd(const Value: real);
begin
  FSDD := Value;
  FD := FBaseD + RandG(0,FSdD);
end;

procedure TIzhikevich.SetSDk(const Value: real);
begin
  FSDK := Value;
  FK := FBaseK + RandG(0,FSdK);
end;

procedure TIzhikevich.SetSDu(const Value: real);
begin
  FSDU := Value;
  FU := FBaseU + RandG(0,FSdU);
end;

procedure TIzhikevich.SetCm(const Value: real);
begin
  FBaseCm := Value;
  FCm := abs(FBaseCm + RandG(0,FSdCm));
end;

{
procedure TIzhikevich.SetCurrentBias(const Value: real);
begin
  FBaseCurrentBias := Value;
  FCurrentBias := abs(FBaseCurrentBias + RandG(0,FSdCurrentBias));
end;
}
procedure TIzhikevich.SetVPeak(const Value: real);
begin
  FBaseVPeak := Value;
  FVPeak := FBaseVPeak + RandG(0,FSdVPeak);
end;

procedure TIzhikevich.SetVRest(const Value: real);
begin
  FBaseVRest := Value;
  FVRest := FBaseVRest + RandG(0,FSdVRest);
end;

procedure TIzhikevich.SetVTh(const Value: real);
begin
  FBaseVTh := Value;
  FVTh := FBaseVTh + RandG(0,FSdVTh);
end;

{
procedure TIzhikevich.SetSdCurrentBias(const Value: real);
begin
  FSdCurrentBias := Value;
  FCurrentBias := FBaseCurrentBias + RandG(0,FSdCurrentBias);
end;
}

procedure TIzhikevich.SetSdVPeak(const Value: real);
begin
  FSdVPeak := Value;
  FVPeak := FBaseVPeak + RandG(0,FSdVPeak);
end;

procedure TIzhikevich.SetSdVRest(const Value: real);
begin
  FSdVRest := Value;
  FVRest := FBaseVRest + RandG(0,FSdVRest);
end;

procedure TIzhikevich.SetSdVTh(const Value: real);
begin
  FSdVTh := Value;
  FVTh := FBaseVTh + RandG(0,FSdVTh);
end;

procedure TIzhikevich.SetSDCm(const Value: real);
begin
  FSdCm := Value;
  FCm := abs(FBaseCm + RandG(0,FSdCm));
end;

procedure TIzhikevich.SetBiasCurrent(const Value: real);
begin
  FBiasCurrent := Value;
end;

end.
