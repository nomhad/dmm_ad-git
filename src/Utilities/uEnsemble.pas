unit uEnsemble;
{
This unit creates an array of Izhikevich neurons
}

interface

uses Classes,  Dialogs, uIzhikevich, SysUtils;

type
  TEnsembleType = (etMotor, etCog, etAss, etOther);

  AConnection = record
    Neuron: TIzhikevich;
    EnsembleType: TEnsembleType;
    Fired: boolean;
    Post: integer; //number of the neuron that this synapse is on
    Weight: real;
    DecayTau: real;
    Lag: real;
  end;

  TEnsemble = class
  private
    arNeurons: array of TIzhikevich;
    arInterneurons: array of TIzhikevich;
    FNeuron: TIzhikevich;
    //How many projection neurons there are in the ensemble
    FNeuronCount: integer;
    //How many interneurons there are in the ensemble
    FInterneuronCount: integer;
    FAvgExcBiasCurrent,
    FAvgInhBiasCurrent,
    FAvgExcTargetCurrent,
    FAvgInhTargetCurrent,
    FSdExcBiasCurrent,
    FSdInhBiasCurrent,
    FSdExcTargetCurrent,
    FSdInhTargetCurrent : real;

    //How many neurons in the same ensemble each neuron connects to
    FTotalLocalConnections: integer;
    //How many neurons in a distant ensemble each neuron connects to
    FTotalProjectionConnections: integer;

    //List of all the neurons in the same ensemble that each neuron
    //has a connection to
    FLocalConnection: array of array of AConnection;
    //List of all the neurons in a distant ensemble (either in another structure
    //or the same structure) that each neuron has a connection to
    FProjectionConnection: array of array of AConnection;
    FInhSynW: real;
    FExcSynW: real;
    FStructureName: string;
    FDecayTau: real;
    FEnsembleType: TEnsembleType;

    function GetNeuron(Index: integer): TIzhikevich;
    procedure SetNeuron(Index: integer; const Value: TIzhikevich);
    procedure SetNeuronCount(Count: integer);
    procedure AddNeuron;
    procedure RemoveNeuron;
    procedure SetInterneuronCount(const Value: integer);
    procedure AddInterneuron;
    procedure RemoveInterneuron;
    procedure SetExcitatorySynapticWeight(Value: real);
    procedure SetInhibitorySynapticWeight(Value: real);
    function GetCurrent(Average, SD: real): real;
    function GetInterneuron(Index: integer): TIzhikevich;
    procedure SetInterneuron(Index: integer; const Value: TIzhikevich);
    procedure SetStructureName(const Value: string);
    procedure SetDecayTau(Value: real);
  public
    constructor Create;
    destructor Destroy; override;
    property EnsembleType: TEnsembleType read FEnsembleType write FEnsembleType;
    property StructureName: string read FStructureName write SetStructureName;
    
    property Neuron[Index: integer]: TIzhikevich read GetNeuron write SetNeuron;
    property NeuronCount: integer read FNeuronCount write SetNeuronCount;
    property Interneuron[Index: integer]: TIzhikevich read GetInterneuron write SetInterneuron;
    property InterneuronCount: integer read FInterneuronCount write SetInterneuronCount;
    procedure SetNeuronType(NeuronType: TNeuronType); overload;
    procedure SetNeuronType(First, Last: integer; NeuronType: TNeuronType); overload;
    procedure SetNeuronType(First, Last: integer; NeuronType: integer);  overload;
    procedure SetInterneuronType(NeuronType: TNeuronType); overload;
    procedure SetInterneuronType(First, Last: integer; NeuronType: TNeuronType); overload;

    procedure ClearProjectionNeuronConnections;
    procedure ConnectProjectionNeurons(ToEnsemble: TEnsemble; Proportion, AverageWeight: real);
    procedure SetNeuronOutput(Output: TNeuronOutput);

    procedure ClearInterneuronConnections;
    procedure ConnectInterneurons(Proportion, AverageExcitatoryWeight, AverageInhibitoryWeight, Lag: real);
    procedure DisconnectInterneurons;
    property ExcitatorySynapticWeight: real read FExcSynW write SetExcitatorySynapticWeight;
    property InhibitorySynapticWeight: real read FInhSynW write SetInhibitorySynapticWeight;
    procedure SetProjectionSynapticWeights(ToEnsembleType: TEnsembleType; ToStructureName: string; AverageWeight: real);

    property DecayTau: real read FDecayTau write SetDecayTau;
    procedure SetInterneuronDecayTau(Value: real);
    procedure SetLag(ToEnsemble: TEnsemble; Value: real);
    procedure SetTargetCurrentDelay(Average: real);

    procedure SetStartingValues;
    function GetBiasCurrent: real;
    function GetTargetCurrent: real;
    function SetTargetCurrent(WhatTime, MaxCurrent: real): real;
    procedure ClearTargetCurrent;
    function GetSynapticCurrent(WhatTime: real): real;
    procedure ClearDistantSynapticCurrent;
    function SetDistantSynapticInput(WhatTime: real): real;
    function AverageDistantSynapticCurrent: real;
    function AddUpCurrents: real;

    property AvgExcBiasCurrent: real read FAvgExcBiasCurrent write FAvgExcBiasCurrent;
    property AvgInhBiasCurrent: real read FAvgInhBiasCurrent write FAvgInhBiasCurrent;
    property AvgExcTargetCurrent: real read FAvgExcTargetCurrent write FAvgExcTargetCurrent;
    property AvgInhTargetCurrent: real read FAvgInhTargetCurrent write FAvgInhTargetCurrent;
    property SdExcBiasCurrent: real read FSdExcBiasCurrent write FSdExcBiasCurrent;
    property SInhBiasCurrent: real read FSdInhBiasCurrent write FSdInhBiasCurrent;
    property SdExcTargetCurrent: real read FSdExcTargetCurrent write FSdExcTargetCurrent;
    property SdInhTargetCurrent: real read FSdInhTargetCurrent write FSdInhTargetCurrent;

    function SetLocalSynapticCurrent(WhatTime: real): real;
    procedure ClearLocalSynapticCurrent;

    procedure CalculateMembranePotentials(WhatTime: real);
    function AverageNeuronFiringRate(WhatTime: real; Window: integer): real;
    function AverageInterneuronFiringRate(WhatTime: real; Window: integer): real;
    function UpdateSumOfFiring(WhatTime:real): real;
  end;

implementation

uses Math;

{ TEnsemble }

constructor TEnsemble.Create;
begin
  inherited;
  FNeuronCount := 0;
  FInterneuronCount := 0;
end;

destructor TEnsemble.Destroy;
begin
  SetNeuronCount(0);
  inherited Destroy;
end;

function TEnsemble.GetNeuron(Index: integer): TIzhikevich;
begin
  result := arNeurons[Index];
end;

procedure TEnsemble.SetNeuron(Index: integer; const Value: TIzhikevich);
begin
  arNeurons[Index] := Value;
end;

procedure TEnsemble.RemoveNeuron;
begin
  dec(FNeuronCount);
  arNeurons[FNeuronCount].Free;
  SetLength(arNeurons, FNeuronCount);
end;

procedure TEnsemble.AddNeuron;
begin
  FNeuron := TIzhikevich.Create;
  FNeuron.Resolution := 1;
  inc(FNeuronCount);
  SetLength(arNeurons, FNeuronCount);
  arNeurons[FNeuronCount-1] := FNeuron;
end;

procedure TEnsemble.SetNeuronCount(Count: integer);
begin
  while FNeuronCount < Count do
    AddNeuron;
  while FNeuronCount > Count do
    RemoveNeuron;
end;

function TEnsemble.GetInterneuron(Index: integer): TIzhikevich;
begin
  result := arInterneurons[Index];
end;

procedure TEnsemble.SetInterneuron(Index: integer; const Value: TIzhikevich);
begin
  arInterneurons[Index] := Value;
end;

procedure TEnsemble.RemoveInterneuron;
begin
  dec(FInterneuronCount);
  arNeurons[FInterneuronCount].Free;
  SetLength(arInterneurons, FInterneuronCount);
end;

procedure TEnsemble.AddInterneuron;
begin
  FNeuron := TIzhikevich.Create;
  FNeuron.Resolution := 1;
  inc(FInterneuronCount);
  SetLength(arInterneurons, FInterneuronCount);
  arInterneurons[FInterneuronCount-1] := FNeuron;
end;

procedure TEnsemble.SetInterneuronCount(const Value: integer);
begin
  while FInterneuronCount < Value do
    AddInterneuron;
  while FInterneuronCount > Value do
    RemoveInterneuron;
end;

procedure TEnsemble.SetNeuronType(NeuronType: TNeuronType);
var i: integer;
begin
  for i := 0 to pred(FNeuronCount) do
    arNeurons[i].SetNeuronType(NeuronType);
end;

procedure TEnsemble.SetNeuronType(First, Last: integer; NeuronType: TNeuronType);
var i: integer;
begin
  for i := First to Last do
    arNeurons[i].SetNeuronType(NeuronType);
end;

procedure TEnsemble.SetNeuronType(First, Last, NeuronType: integer);
begin
  SetNeuronType(First, Last, TNeuronType(NeuronType));
end;

procedure TEnsemble.SetInterneuronType(NeuronType: TNeuronType);
var i: integer;
begin
  for i := 0 to pred(FInterneuronCount) do
    arInterneurons[i].SetNeuronType(NeuronType);
end;

procedure TEnsemble.SetInterneuronType(First, Last: integer; NeuronType: TNeuronType);
var i: integer;
begin
  for i := First to Last do
    arInterneurons[i].SetNeuronType(NeuronType);
end;

procedure TEnsemble.ClearProjectionNeuronConnections;
var iFromNeuron,
    iToNeuron: integer;
begin
  for iFromNeuron := 0 to FNeuronCount - 1 do begin
    for iToNeuron := 0 to FTotalProjectionConnections - 1 do begin
      FProjectionConnection[iFromNeuron,iToNeuron].Fired := false;
      FProjectionConnection[iFromNeuron,iToNeuron].Post := 0;
      FProjectionConnection[iFromNeuron,iToNeuron].Weight := 0;
      FProjectionConnection[iFromNeuron,iToNeuron].Neuron := nil;
    end;
  end;
end;

{
This procedure connects the projection neurons of one ensemble to those of
another ensemble.
ToEnsemble is the ensemble that this ensemble is connecting to
The proportion is the proportion of neurons in the receiving ensemble that each
neuron in the sending ensemble forms a synapse with
}
procedure TEnsemble.ConnectProjectionNeurons(ToEnsemble: TEnsemble; Proportion, AverageWeight: real);
var iFromNeuron,
    iConnection,
    FConnectionsToMake: integer;
    k,
    iPost,
    iStart: integer;
    bExists: boolean;
begin
  //FTotalProjectionConnections is the number of synapses that each neuron in the sending
  //ensemble makes in total with neurons in the receiving ensemble
  if (FProjectionConnection = nil) or
  (length(FProjectionConnection) <> FNeuronCount) then
    SetLength(FProjectionConnection, FNeuronCount);
  FConnectionsToMake := trunc(ToEnsemble.NeuronCount * Proportion);

  for iFromNeuron := 0 to FNeuronCount - 1 do begin
    iStart := Length(FProjectionConnection[iFromNeuron]);
    FTotalProjectionConnections := FConnectionsToMake + iStart;
    SetLength(FProjectionConnection[iFromNeuron], FTotalProjectionConnections);
    for iConnection := iStart to FConnectionsToMake + iStart - 1 do begin
      repeat
        bExists := false;
        iPost := Random(ToEnsemble.FNeuronCount);
        for k := iStart to iConnection - 1 do
          if FProjectionConnection[iFromNeuron,k].Post = iPost then begin
            bExists := true; //don't connect more than once to one neuron
            //get out of the loop and get a new iPost
            BREAK;
          end;
      until not bExists;
      FProjectionConnection[iFromNeuron,iConnection].Post := iPost;
      FProjectionConnection[iFromNeuron,iConnection].Neuron := ToEnsemble.Neuron[iPost];
      FProjectionConnection[iFromNeuron,iConnection].Weight := RandG(AverageWeight, AverageWeight / 10);
      FProjectionConnection[iFromNeuron,iConnection].DecayTau := ToEnsemble.DecayTau;
      FProjectionConnection[iFromNeuron,iConnection].EnsembleType := ToEnsemble.EnsembleType;
    end;
  end;
end;

procedure TEnsemble.ClearInterneuronConnections;
var iFromNeuron,
    iToNeuron: integer;
begin
  for iFromNeuron := 0 to FNeuronCount - 1 do begin
    for iToNeuron := 0 to FTotalLocalConnections - 1 do begin
      FLocalConnection[iFromNeuron,iToNeuron].Fired := false;
      FLocalConnection[iFromNeuron,iToNeuron].Post := 0;
      FLocalConnection[iFromNeuron,iToNeuron].Weight := 0;
    end;
  end;

  //the FLocalConnection array has the interneurons directly after the projection
  //neurons
  for iFromNeuron := 0 to FInterneuronCount - 1 do begin
    for iToNeuron := 0 to FTotalLocalConnections - 1 do begin
      FLocalConnection[FNeuronCount + iFromNeuron,iToNeuron].Fired := false;
      FLocalConnection[FNeuronCount + iFromNeuron,iToNeuron].Post := 0;
      FLocalConnection[FNeuronCount + iFromNeuron,iToNeuron].Weight := 0;
    end;
  end;
end;

{
Internally connect an ensemble. Connect projection neurons both to interneurons
and other projection neurons. Connect interneurons only to projecton neurons.
The list of neurons that each neuron is connected to is contained in the
FLocalConnections array of FConnection.
}
procedure TEnsemble.ConnectInterneurons(Proportion, AverageExcitatoryWeight, AverageInhibitoryWeight, Lag: real);
var iFromNeuron,
    iToNeuron,
    k,
    iPost: integer;
    bExists: boolean;
begin
  //Calculate how many neurons this neuron is going to make a connection to. This
  // is the same for every neuron and interneuron in the ensemble.
  FTotalLocalConnections := trunc((FNeuronCount + FInterneuronCount) * Proportion);
  SetLength(FLocalConnection, FNeuronCount + FInterneuronCount, FTotalLocalConnections);

  //Clear everything down first
  ClearInterneuronConnections;
  //randomly connect the excitatory neurons in the proportion specified
  for iFromNeuron := 0 to FNeuronCount - 1 do begin
    for iToNeuron := 0 to FTotalLocalConnections - 1 do begin
      repeat
        bExists := false;
        repeat
            //excitatory neurons can synapse on other excitatory neurons or
            //on inhibitory neurons
          iPost := Random(FNeuronCount + FInterneuronCount)
        until iPost <> iFromNeuron; //don't connect a neuron to itself
        for k := 0 to iToNeuron - 1 do
          if FLocalConnection[iFromNeuron,k].Post = iPost then begin
            bExists := true; //don't connect more than once to one neuron
            //get out of the loop and get a new iPost
            BREAK;
          end;
      until not bExists;
      FLocalConnection[iFromNeuron,iToNeuron].Post := iPost;
      FLocalConnection[iFromNeuron,iToNeuron].Neuron := arNeurons[iPost];
      FLocalConnection[iFromNeuron,iToNeuron].Weight := RandG(AverageExcitatoryWeight, AverageExcitatoryWeight / 10);
      FLocalConnection[iFromNeuron,iToNeuron].Lag := RandG(Lag, Lag / 10);
    end;
  end;

  //randomly connect the inhibitory neurons in the proportion specified
  for iFromNeuron := FNeuronCount to FNeuronCount + FInterneuronCount - 1 do begin
    for iToNeuron := 0 to FTotalLocalConnections - 1 do begin
      repeat
        bExists := false;
        //inhibitory neurons synapse only on excitatory neurons
        iPost := Random(FNeuronCount);
        for k := 0 to iToNeuron - 1 do
          if FLocalConnection[iFromNeuron,k].Post = iPost then begin
            bExists := true; //don't connect more than once to one neuron
            BREAK;
          end;

      until not bExists;
      FLocalConnection[iFromNeuron,iToNeuron].Post := iPost;
      FLocalConnection[iFromNeuron,iToNeuron].Weight := RandG(AverageInhibitoryWeight, AverageInhibitoryWeight / 10);
      FLocalConnection[iFromNeuron,iToNeuron].Lag := RandG(Lag, Lag / 10);
    end;
  end;
end;

procedure TEnsemble.DisconnectInterneurons;
begin
  FTotalLocalConnections := 0;
  SetLength(FLocalConnection, 0, 0);
end;

{
Get the actual background current that is input to the neurons throughout the
simulation. If it is random, it is multiplied by the maximum by the calling procedure.
The random distribution is gaussian, so producing a normal distribution .
If it is absolute, the random distribution is refelected about zero. If negatives
are ignored, all values less than zero are set to zero
}
function TEnsemble.GetCurrent(Average, SD: real): real;
begin
  result := Abs(RandG(Average,SD));  //Gaussian distribution, reflect the distribution around 0
end;

{
The bias input to each neuron represents input coming from other sensory inputs
or events in the environment that are not being considered in the model.
Each neuron has a current bias that keeps it just at the level of excitation where
it starts to fire.
The maximum bias current can also be modified by a random factor
}
function TEnsemble.GetBiasCurrent: real;
var iNeuron: integer;
    eCurrent: real;
begin
  result := 0;
  for iNeuron := 0 to FNeuronCount - 1 do begin
    eCurrent := GetCurrent(FAvgExcBiasCurrent, FSdExcBiasCurrent);
    arNeurons[iNeuron].BiasCurrent := eCurrent;
    result := result + eCurrent;
  end;
  result := result / FNeuronCount;

  for iNeuron := 0 to FInterneuronCount - 1 do begin
    arInterneurons[iNeuron].BiasCurrent := GetCurrent(FAvgInhBiasCurrent, FSdInhBiasCurrent);
  end;
end;

{
The target current starts when the target is shown. It is added to the bias
current that is due to general sensory input
}
function TEnsemble.GetTargetCurrent: real;
var iNeuron: integer;
    eCurrent: real;
begin
  result := 0;
  for iNeuron := 0 to FNeuronCount - 1 do begin
    eCurrent := GetCurrent(FAvgExcTargetCurrent, FSdExcTargetCurrent);
    arNeurons[iNeuron].TargetCurrent := eCurrent;
    result := result + eCurrent;
  end;
  result := result / FNeuronCount;

  for iNeuron := 0 to FInterneuronCount - 1 do begin
    arInterneurons[iNeuron].TargetCurrent := GetCurrent(FAvgInhTargetCurrent, FSdInhTargetCurrent);
  end;
end;

function TEnsemble.SetTargetCurrent(WhatTime, MaxCurrent: real): real;
var iNeuron: integer;
    eCurrent: real;
begin
  result := 0;
  for iNeuron := 0 to FNeuronCount - 1 do begin
    if WhatTime > arNeurons[iNeuron].TargetCurrentDelay then begin
      eCurrent := GetCurrent(MaxCurrent, MaxCurrent/10);
      arNeurons[iNeuron].TargetCurrent := eCurrent;
      result := result + eCurrent;
    end;
  end;
  result := result / FNeuronCount;
  for iNeuron := 0 to FInterneuronCount - 1 do begin
    if WhatTime > arInterneurons[iNeuron].TargetCurrentDelay then begin
      eCurrent := GetCurrent(FAvgInhTargetCurrent, FSdInhTargetCurrent);
      arInterneurons[iNeuron].TargetCurrent := eCurrent;
    end;
  end;
end;

procedure TEnsemble.ClearTargetCurrent;
var iNeuron: integer;
begin
  for iNeuron := 0 to FNeuronCount - 1 do begin
    arNeurons[iNeuron].TargetCurrent := 0;
  end;
  for iNeuron := 0 to FInterneuronCount - 1 do begin
    arInterneurons[iNeuron].TargetCurrent := 0;
  end;
end;

procedure TEnsemble.SetTargetCurrentDelay(Average: real);
var iNeuron: integer;
    eDelay: real;
begin
  for iNeuron := 0 to FNeuronCount - 1 do begin
    repeat
      eDelay := RandG(Average, Average / 5);
    until eDelay >= 0;
    arNeurons[iNeuron].TargetCurrentDelay := eDelay;
  end;
  for iNeuron := 0 to FInterneuronCount - 1 do begin
    repeat
      eDelay := RandG(Average, Average / 5);
    until eDelay >= 0;
    arInterneurons[iNeuron].TargetCurrentDelay := eDelay;
  end;
end;

function TEnsemble.GetSynapticCurrent(WhatTime: real): real;
var iNeuron: integer;
    eCurrent: real;
begin
  result := 0;
  for iNeuron := 0 to FNeuronCount - 1 do begin
    eCurrent := arNeurons[iNeuron].GetSynapticCurrent(WhatTime);
    arNeurons[iNeuron].Current := arNeurons[iNeuron].Current + eCurrent;
    result := result + eCurrent;
  end;
  result := result / FNeuronCount;

  for iNeuron := 0 to FInterneuronCount - 1 do begin
    arInterneurons[iNeuron].Current := arInterneurons[iNeuron].Current +
      arInterneurons[iNeuron].GetSynapticCurrent(WhatTime);
  end;
end;

{
Local synaptic input comes from the connecitvity of an ensemble. The proportion
of connectivity controls how many synapses each neuron makes with other neurons
in the ensemble (see procedure ConnectInterneurons).
Each time a neuron in the ensemble fires, it goes through the list of neurons that
it is connected to and adds to the current in that neuron based on the current
synaptic weight between the 2 neurons. The current is added for connections from
excitatory (projection) neurons and subtracted for inhibitory (interneuron)
connections.
Each synapse has an input of 1 and only lasts one time step
}
function TEnsemble.SetLocalSynapticCurrent(WhatTime: real): real;
var i, j, iPost: integer;
    eCurrent: real;
//    eSum: real;
begin
  result := 0;

  for i := 0 to FNeuronCount - 1 do begin
    if arNeurons[i].Fired then begin
      //which neurons does this one input to?
      for j := 0 to FTotalLocalConnections - 1 do begin
        iPost := FLocalConnection[i,j].Post;
//        eProportion := arNeurons[i].GetSynapticProportion(WhatTime - FLocalConnection[i,j].Lag);
        //the firing neuron is excitatory
        eCurrent := FLocalConnection[i,j].Weight * arNeurons[i].SumOfFiring;
        result := result + eCurrent;
        if iPost < FNeuronCount then begin
          //each neuron may receive input from more than one other neuron at
          //each time step
          arNeurons[iPost].LocalSynapticCurrent := arNeurons[iPost].LocalSynapticCurrent + eCurrent;
        end
        else
          arInterneurons[iPost - FNeuronCount].LocalSynapticCurrent := arInterneurons[iPost - FNeuronCount].LocalSynapticCurrent + eCurrent;
      end;
    end;
  end;
  result := result / FNeuronCount;

  for i := 0 to FInterneuronCount - 1 do begin
    if arInterneurons[i].Fired then begin
      //which neurons does this one input to?
      for j := 0 to FTotalLocalConnections - 1 do begin
        iPost := FLocalConnection[FNeuronCount + i,j].Post;
//        eProportion := arInterneurons[i].GetSynapticProportion(WhatTime - FLocalConnection[FNeuronCount + i,j].Lag);
        eCurrent := FLocalConnection[FNeuronCount + i,j].Weight * arInterneurons[i].SumOfFiring;
        if iPost < FNeuronCount then
          arNeurons[iPost].LocalSynapticCurrent := arNeurons[iPost].LocalSynapticCurrent - eCurrent
        else
          arInterneurons[iPost - FNeuronCount].LocalSynapticCurrent := arInterneurons[iPost - FNeuronCount].LocalSynapticCurrent - eCurrent;
      end;
    end;
  end;
end;

procedure TEnsemble.ClearLocalSynapticCurrent;
var iNeuron: integer;
begin
  for iNeuron := 0 to FNeuronCount - 1 do begin
    arNeurons[iNeuron].LocalSynapticCurrent := 0;
  end;
  for iNeuron := 0 to FInterneuronCount - 1 do begin
    arInterneurons[iNeuron].LocalSynapticCurrent := 0;
  end;
end;

function TEnsemble.AddUpCurrents: real;
var iNeuron: integer;
    eCurrent: real;
begin
  result := 0;
  for iNeuron := 0 to FNeuronCount - 1 do begin
    eCurrent := arNeurons[iNeuron].Current;
//    arNeurons[iNeuron].Current := eCurrent;
    result := result + eCurrent;
  end;
  result := result / FNeuronCount;

  for iNeuron := 0 to FInterneuronCount - 1 do begin
    arInterneurons[iNeuron].Current;
{
    arInterneurons[iNeuron].Current := arInterneurons[iNeuron].BiasCurrent
                                     + arInterneurons[iNeuron].TargetCurrent
                                     + arInterneurons[iNeuron].LocalSynapticCurrent
                                     + arInterneurons[iNeuron].DistantSynapticCurrent;
}
  end;
end;

procedure TEnsemble.CalculateMembranePotentials(WhatTime: real);
var i: integer;
begin
  for i := 0 to FNeuronCount - 1 do
    arNeurons[i].CalculateMembranePotential(WhatTime);
  for i := 0 to FInterneuronCount - 1 do
    arInterneurons[i].CalculateMembranePotential(WhatTime);
end;

procedure TEnsemble.SetStartingValues;
var i: integer;
begin
  FNeuron.Vm := FNeuron.VRest;
//  FNeuron.u := FNeuron.b * FNeuron.Vm;
  FNeuron.u := 0;
  FNeuron.Current := 0;
  FNeuron.ClearFiring;
  for i := 0 to FNeuronCount - 1 do begin
    try
    arNeurons[i].Vm := arNeurons[i].VRest;
//    arNeurons[i].u := arNeurons[i].b * arNeurons[i].Vm;
    arNeurons[i].u := 0;
    arNeurons[i].Current := 0;
    arNeurons[i].ClearFiring;
    except
      on E: Exception do begin
        ShowMessage(E.Message);
        Abort;
      end;
    end;
  end;
  for i := 0 to FInterneuronCount - 1 do begin
    arInterneurons[i].Vm := arInterneurons[i].VRest;
//    arInterneurons[i].u := arInterneurons[i].b * arInterneurons[i].Vm;
    arInterneurons[i].u := 0;
    arInterneurons[i].Current := 0;
    arInterneurons[i].ClearFiring;
  end;
end;

function TEnsemble.AverageNeuronFiringRate(WhatTime: real; Window: integer): real;
var i,
    iTotalSpikes: integer;
begin
  result := 0;
  iTotalSpikes := 0;
  if WhatTime - Window > 0 then begin
    for i := 0 to FNeuronCount - 1 do begin
      iTotalSpikes := iTotalSpikes + arNeurons[i].SpikesInWindow(WhatTime, Window);
    end;
  end;
  result := (iTotalSpikes * 1000) / (Window * FNeuronCount);
end;

function TEnsemble.AverageInterneuronFiringRate(WhatTime: real; Window: integer): real;
var i,
    iTotalSpikes: integer;
begin
  result := 0;
  iTotalSpikes := 0;
  if WhatTime - Window > 0 then begin
    for i := 0 to FInterneuronCount - 1 do begin
      iTotalSpikes := iTotalSpikes + arInterneurons[i].SpikesInWindow(WhatTime, Window);
    end;
  end;
  if FInterneuronCount > 0 then
    result := (iTotalSpikes * 1000) / (Window * FInterneuronCount)
  else
    result := 0;
end;

{
Excitatory synaptic weights have gaussian distribution
}
procedure TEnsemble.SetExcitatorySynapticWeight(Value: real);
var i,j: integer;
begin
  FExcSynW := Value;
  for i := 0 to FNeuronCount - 1 do
    for j := 0 to FTotalLocalConnections - 1 do
//      FLocalConnection[i,j].Weight := FExcSynW * Random;
      FLocalConnection[i,j].Weight := RandG(FExcSynW, FExcSynW / 10);
//    arNeurons[i].SetAverageSynapticWeight(Value);
end;

procedure TEnsemble.SetInhibitorySynapticWeight(Value: real);
var i,j: integer;
begin
  FInhSynW := Value;
  for i := FNeuronCount to FInterneuronCount + FNeuronCount - 1 do
    for j := 0 to FTotalLocalConnections - 1 do
      FLocalConnection[i,j].Weight := randG(FInhSynW, FInhSynW / 10)
end;

procedure TEnsemble.SetDecayTau(Value: real);
var iNeuron: integer;
begin
  for iNeuron := 0 to pred(FNeuronCount) do
    arNeurons[iNeuron].DecayTau := Value;
end;

procedure TEnsemble.SetInterneuronDecayTau(Value: real);
var iNeuron: integer;
begin
  for iNeuron := 0 to pred(FInterneuronCount) do
    arInterneurons[iNeuron].DecayTau := Value;
end;

procedure TEnsemble.SetLag(ToEnsemble: TEnsemble; Value: real);
var iFromNeuron,
    iToNeuron: integer;
begin
  for iFromNeuron := 0 to FNeuronCount - 1 do begin
    for iToNeuron := 0 to FTotalLocalConnections - 1 do begin
      FLocalConnection[iFromNeuron,iToNeuron].Lag := RandG(Value, Value / 10);
    end;
  end;
end;

function TEnsemble.UpdateSumOfFiring(WhatTime: real): real;
var iNeuron: integer;
    eFiring: real;
begin
  eFiring := 0;
  for iNeuron := 0 to pred(FNeuronCount) do try
    eFiring := eFiring + arNeurons[iNeuron].UpdateSumOfFiring(WhatTime);
  except
    on E: EInvalidOp do begin
      ShowMessage(E.Message);
      eFiring := 0;
    end;
  end;
  for iNeuron := 0 to pred(FInterneuronCount) do
    eFiring := eFiring + arInterneurons[iNeuron].UpdateSumOfFiring(WhatTime);
  result := eFiring / (FNeuronCount + FInterneuronCount);
end;

procedure TEnsemble.SetStructureName(const Value: string);
var iNeuron: integer;
begin
  FStructureName := Value;
  for iNeuron := 0 to pred(FNeuronCount) do
    arNeurons[iNeuron].Structure := Value;
end;

procedure TEnsemble.SetProjectionSynapticWeights(ToEnsembleType: TEnsembleType; ToStructureName: string; AverageWeight: real);
var iFromNeuron,
    iToNeuron: integer;
begin
  for iFromNeuron := 0 to FNeuronCount - 1 do begin
    for iToNeuron := 0 to FTotalProjectionConnections - 1 do begin
      if FProjectionConnection[iFromNeuron,iToNeuron].EnsembleType = ToEnsembleType then begin
        if FProjectionConnection[iFromNeuron,iToNeuron].Neuron.Structure = ToStructureName then begin
          FProjectionConnection[iFromNeuron,iToNeuron].Weight := RandG(AverageWeight, AverageWeight * 0.1);
        end;
      end;
    end;
  end;
end;

procedure TEnsemble.SetNeuronOutput(Output: TNeuronOutput);
var i: integer;
begin
  for i := 0 to pred(FNeuronCount) do
    arNeurons[i].Output := Output;
end;

function TEnsemble.AverageDistantSynapticCurrent: real;
var i: integer;
    eCurrent: real;
begin
  result := 0;
  for i := 0 to FNeuronCount - 1 do begin
    eCurrent := arNeurons[i].DistantSynapticCurrent;
    result := result + eCurrent;
  end;
  result := result / FNeuronCount;
end;

function TEnsemble.SetDistantSynapticInput(WhatTime: real): real;
var i, j, iPost: integer;
    eCurrent,
    eSum: real;
    ANeuron: TIzhikevich;
begin
  result := 0;

  for i := 0 to FNeuronCount - 1 do begin
    if arNeurons[i].Fired then begin
      eSum := arNeurons[i].SumOfFiring;
      //which neurons does this one input to?
      for j := 0 to FTotalProjectionConnections - 1 do begin
        iPost := FProjectionConnection[i,j].Post;
        ANeuron := FProjectionConnection[i,j].Neuron;
//        eProportion := arNeurons[i].GetSynapticProportion(WhatTime - FLocalConnection[i,j].Lag);
        //the firing neuron is excitatory
        eCurrent := FProjectionConnection[i,j].Weight * eSum;
        if arNeurons[i].Output = noInhibitory then
          eCurrent := eCurrent * -1;
        ANeuron.DistantSynapticCurrent := ANeuron.DistantSynapticCurrent + eCurrent;
        result := result + eCurrent;
      end;
    end;
  end;
  result := result / FNeuronCount;
end;

{
Distant synaptic input is pushed from the incoming neuron. The receiving neuron may
have many different incoming neurons. So before calculating the distant synaptic
input on each time step, it is necessary to clear down the input from the last
time step
}
procedure TEnsemble.ClearDistantSynapticCurrent;
var i: integer;
begin
  for i := 0 to FNeuronCount - 1 do begin
    arNeurons[i].DistantSynapticCurrent := 0;
  end;
  //don't need to do this for interneurons as they do not project to other structures
end;

end.
