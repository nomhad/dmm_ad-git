{
Implements the Boltzmann equation for generating sigmoidal transfer function

y = Min + (Max - Min) / 1 + exp((Vh - x)/ Vc))

Let (Vh - x) / Vc = e
    (Max - Min) = A

then y = A / 1 + exp(e)

For the inverse:

x = Vh - Vc.ln(((Max - Min) / (y - Min)) - 1)

See:
 http://www.graphpad.com/help/prism5/prism5help.html?reg_classic_boltzmann.htm

This equation describes voltage dependent activation of ion channels. It describes
conductance (Y) as a function of the membrane potential (X). Conductance varies
from BOTTOM to TOP. V50 is the potential at which conductance is halfway between
BOTTOM and TOP. SLOPE describes the steepness of the curve, with a larger value
denoting a shallow curve. Slope is expressed in units of potential, usually mV,
and is positive for channels that activate upon depolarization.

Under appropriate experimental conditions, you can use SLOPE to calculate the valence
(charge) of the ion moving across the channel. SLOPE equals RT/zF where R is the
universal gas constant, T is temperature in �K, F is the Faraday constant, and z
is the valence. Since RT/F � -26 mV at 25�C, z = -26/SLOPE.

BOTTOM is commonly made a constant equal to 0.0. If you also make TOP a constant
equal to 1.0, then Y can be viewed as the fraction of channels that are activated.

This can also be used for any sigmoidal transfer function
}
unit uBoltzmann;

interface

uses Math;

type
  TBoltzmann = class
  private
    FVh: real;
    FMinOutput: real;
    FVc: real;
    FMaxOutput: real;
    FPos: boolean;
  public
    property MinOutput: real read FMinOutput write FMinOutput;
    property MaxOutput: real read FMaxOutput write FMaxOutput;
    property Vc: real read FVc write FVc;

    {
    The maximum output is reached at approximately twice the half-activation,
    but if the slope is too shallow, it is never reached
    }
    property Vh: real read FVh write FVh;

    {
    By having an absolute output, we can have a minimum less than zero and then
    have a zone of no output for an increasing input. This should be a good mimic
    for MSNs
    }
    property OutputPositiveOnly: boolean read FPos write FPos default true;

    function GetOutput(Input: real): real;
    function GetDifference(Y, DeltaX: real): real;
    function GetInput(Output: real): real;

  end;

implementation

{ TBoltzmann }

{
The input is the point on the x-axis. work out the output on the y-axis for the given
sigmoidal function
}
function TBoltzmann.GetOutput(Input: real): real;
var e,
    eDenominator,
    eNumerator: real;
begin
  e := (FVh - Input) / FVc;
  eNumerator := MaxOutput - MinOutput;
  eDenominator := 1 + Exp(e);
  result := MinOutput + (eNumerator / eDenominator);
  if FPos then
    if result < 0 then
      result := 0;
end;

{
For a given change on the x-axis, work out how much change that produces on the y-axis
}
function TBoltzmann.GetDifference(Y, DeltaX: real): real;
var e1, e2: real;
begin
  e1 := GetInput(Y);
  e2 := GetOutput(e1 + DeltaX);
  result := e2;
end;

{
 The inverse of GetOutput. Given a point on the y-axis, what is the point on the x-axis for a
 given sigmoidal function
}
function TBoltzmann.GetInput(Output: real): real;
var e: real;
begin
  e := ((MaxOutput - MinOutput) / (Output - MinOutput)) - 1;
  result := FVh - (FVc * ln(e));
end;

end.
 