unit uSynapse;

interface

uses SysUtils, Dialogs;

type

  TSynapseType = (stExcitatory, stInhibitory);
  TLtdMethod = (lmNone, lmLast50ms, lmProportional);

  TInputRecord = record
    StartTime,
    EndTime: real;
  end;

  TSynapse = class (TObject)
  private
    FSynapseType: TSynapseType;
    FInputMaxInfluence: integer;
    FInputDecayTau: real;
    FInputPeak: real;
    FInputRiseTime: real;
    FarInputs: array of TInputRecord;
    FSpiny: integer;
    FFirstInput: integer;
    FInputCount: integer;
    FWeight,
    FLtdFactor,
    FMaximumSynapticWeight: real;
    FInputTimeConstant: integer;
    FLtdMethod: TLtdMethod;

    procedure SetSynapseType(const Value: TSynapseType);
  public
    constructor Create;
    destructor Destroy; override;

    property SpinyNeuron: integer read FSpiny write FSpiny;

    property SynapseType: TSynapseType read FSynapseType write SetSynapseType;
    property InputRiseTime: real read FInputRiseTime write FInputRiseTime;
    property InputPeak: real read FInputPeak write FInputPeak;
    property InputDecayTau: real read FInputDecayTau write FInputDecayTau;
    property InputMaxInfluence: integer read FInputMaxInfluence write FInputMaxInfluence;
    property Weight: real read FWeight write FWeight;

    procedure InputOccurs(WhatTime: real);
    function GetConductance(WhatTime: real): real;
    procedure ClearInputs;

    property InputTimeConstant: integer read FInputTimeConstant write FInputTimeConstant;

    property LtdFactor: real read FLtdFactor write FLtdFactor;
    property LtdMethod: TLtdMethod read FLtdMethod write FLtdMethod;
    procedure DoLtd(TimeSpinyFired: real);

    procedure DoLtp(TimeSpinyFired, CurrentTime: real; NeuromodulatoryFactor: real);
    property MaximumSynapticWeight: real read FMaximumSynapticWeight write FMaximumSynapticWeight;

    procedure DoDisappointment(TimeSpinyFired, CurrentTime: real; NeuromodulatoryFactor: real);

    function LastInput: real;
    function LastInputBefore(WhatTime: real): real;
end;

implementation

{ TSynapse }

constructor TSynapse.Create;
begin
  inherited;
  ClearInputs;
  FWeight := 1;
end;

destructor TSynapse.Destroy;
begin
  ClearInputs;
  inherited;
end;

procedure TSynapse.SetSynapseType(const Value: TSynapseType);
begin
  FSynapseType := Value;
end;

{
Keep track of the time that each input starts. This will be called once for
each spike that occurs on each synapse
}
procedure TSynapse.InputOccurs(WhatTime: real);
begin
  inc(FInputCount);
  SetLength(FarInputs, FInputCount);
  FarInputs[FInputCount - 1].StartTime := WhatTime;
  //also track the time that the influence of this spike is deemed to end
  //as this saves time in the calculation of the conductance
  FarInputs[FInputCount - 1].EndTime := WhatTime
    + FInputRiseTime + (FInputDecayTau * FInputMaxInfluence);
end;

function TSynapse.GetConductance(WhatTime: real): real;
var iCount: integer;
    eConductance: real;
    eTime: real;
begin
  result := 0;
  for iCount := FFirstInput to pred(FInputCount) do begin
    if FarInputs[iCount].StartTime > 0 then begin
      if (WhatTime > FarInputs[iCount].EndTime) then begin
        //remove the input from the list
        inc(FFirstInput);
      end
      else if (WhatTime > FarInputs[iCount].StartTime) then begin
        //calculate the effect of this input at this time
        //model the input as a straight line rise over the rise time and then a
        //exponential fall until the max input time is exceeded
        if (WhatTime - FarInputs[iCount].StartTime) < FInputRiseTime then begin
          //input is rising, in a straight line
          eTime :=  (WhatTime - FarInputs[iCount].StartTime) / FInputRiseTime;
          eConductance := FInputPeak * eTime * FWeight;
        end
        else begin
          //input is decaying exponentially
          eTime := WhatTime - FarInputs[iCount].StartTime - FInputRiseTime;
          eConductance := FInputPeak * (Exp(-1 * eTime / FInputDecayTau)) * FWeight;
        end;
        result := result + eConductance;
      end;
    end;
  end;
end;

procedure TSynapse.ClearInputs;
begin
  FFirstInput := 0;
  FInputCount := 0;
  SetLength(FarInputs, FInputCount);
end;

function TSynapse.LastInput: real;
begin
  try
    if FInputCount > 0 then
      result := FarInputs[FInputCount - 1].StartTime
    else
      result := -10000;
  except
    Abort;
  end;
end;

{
This function is needed to implement LTD which only works on the timing of the
synaptic input which occurred before a spike
Work backwards to find the input which occurred most recently before the spike
}
function TSynapse.LastInputBefore(WhatTime: real): real;
var iInputDecrement: integer;
begin
  try
    if FInputCount > 0 then begin
      iInputDecrement := 1;  //zero based array so first input is FarInputs[0] when FInputCount = 1
      while FarInputs[FInputCount - iInputDecrement].StartTime > WhatTime do begin
        inc(iInputDecrement);
        if iInputDecrement > FInputCount then begin
//          ShowMessage('No inputs before spike!');
          result := -10000;
          EXIT;
        end;
      end;
      result := FarInputs[FInputCount - iInputDecrement].StartTime;
    end
    else
      result := -10000;
  except
    Abort;
  end;
end;

{
LTD occurs for every synapse on a MS neuron when the neuron fires and there is no
dopamine pulse
Note that the amount of LTD is related to the current weight, so larger
synapses will be depressed more
}
procedure TSynapse.DoLtd(TimeSpinyFired: real);
var eTime,
    eProportion: real;
begin
  case FLtdMethod of
    lmNone: EXIT; //not using ltd
    lmLast50ms: begin //any synapse with input in last 50ms
      if (TimeSpinyFired - LastInputBefore(TimeSpinyFired)) < 50 then
        Weight := Weight - (Weight * FLtdFactor);
    end;
    lmProportional: begin //Proportional to time of last input
      eTime := LastInputBefore(TimeSpinyFired) - TimeSpinyFired;
      eProportion := exp(eTime / FInputTimeConstant);
      if eProportion > 1 then
        //there must have been an input after the spiny fired returned by LastInputBefore
        ShowMessage('LTD proportion too large: ' + FloatToStrF(eProportion,ffFixed,4,1))
      else
      Weight := Weight - (Weight * FLtdFactor * eProportion);
    end;
  end;
end;

{
Each synapse calculates the LTP based on a 3 factor rule; the neuromodulation,
the time since the MS neuron fired and the time since the synapse last had an
excitatory input
The neuromodulatory factor is the same for all synapses co is passed in as a parameter.
The time since firing is the credit assignment factor from RL models
Note that the increase in synaptic weight is not related to the current weight,
unlike in LTD
}
procedure TSynapse.DoLtp(TimeSpinyFired, CurrentTime: real; NeuromodulatoryFactor: real);
var eTime,
    eSynapseProportion: real;
begin
  //use the current time as the backpropagating signal starts decaying as soon
  //as the spiny has fired
  eTime := LastInputBefore(TimeSpinyFired) - CurrentTime; //should be negative to account for -delta_t
//  eTime := LastInputBefore(TimeSpinyFired) - TimeSpinyFired; //should be negative to account for -delta_t
  eSynapseProportion := exp(eTime / FInputTimeConstant);
  Weight := Weight + (eSynapseProportion * NeuromodulatoryFactor);
  if Weight > FMaximumSynapticWeight then
    Weight := FMaximumSynapticWeight;
end;

{
The disappointment factor can be used when a neuron fires to activate a plan
which is impossible, such as moving through a wall.
It is calculated in the same way as LTD, but the factor can be specified separately
}
procedure TSynapse.DoDisappointment(TimeSpinyFired, CurrentTime: real; NeuromodulatoryFactor: real);
var eTime,
    eSynapseProportion: real;
begin
  eTime := LastInputBefore(TimeSpinyFired) - CurrentTime;
  eSynapseProportion := exp(eTime / FInputTimeConstant);
  Weight := Weight - (Weight * eSynapseProportion * NeuromodulatoryFactor);
//  Weight := Weight - Weight * (eSynapseProportion * (1 / (NeuromodulatoryFactor * 10)));
{
  if eProportion > 1 then
    ShowMessage('Disappointment LTD proportion too large: ' + FloatToStrF(eProportion,ffFixed,4,1))
  else
  Weight := Weight - (FDisappointmentFactor * Weight * eProportion);
}
end;

end.
