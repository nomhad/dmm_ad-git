unit uStrings;

interface

const
  CharSet: array [1..104] of string = ('A','B','C','D','E','F','G','H','I','J','K','L',
                                     'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                                     'AA','AB','AC','AD','AE','AF','AG','AH','AI',
                                     'AJ','AK','AL','AM','AN','AO','AP','AQ','AR',
                                     'AS','AT','AU','AV','AW','AX','AY','AZ',
                                     'BA','BB','BC','BD','BE','BF','BG','BH','BI',
                                     'BJ','BK','BL','BM','BN','BO','BP','BQ','BR',
                                     'BS','BT','BU','BV','BW','BX','BY','BZ',
                                     'CA','CB','CC','CD','CE','CF','CG','CH','CI',
                                     'CJ','CK','CL','CM','CN','CO','CP','CQ','CR',
                                     'CS','CT','CU','CV','CW','CX','CY','CZ');

  xlWBATWorksheet = -4167;

  NEW_FILE = '<New file>';
  NEW_SHEET = '<New sheet>';
  SETUP = 'Setup';

{  PARAMETER_COUNT = 20;
  arRunParameters: array[0..PARAMETER_COUNT-1] of string = (
    ' ', 'Inhibition start',
    'Inhibition count', 'Dopamine pulse start',
    'Temperature', 'Membrane capacitance', 'Excitation frequency',
    'Leakage conductance',
    'Ksi Vh','Ksi Vc', 'Ksi Max Conductance', 'Ksi Max Variable Conductance',
    'Kir2 Vh', 'Kir2 Vc', 'Kir2 Max Conductance',
    'L-Ca Vh', 'L-Ca Vc', 'Ca Max Permeability', 'Extracellular Ca', 'Intracellular Ca');

  arRunParameterUnits: array[0..PARAMETER_COUNT-1] of string = (
    ' ', ' ms',
    ' ', ' ms',
    'C', 'uF/cm2', ' Hz',
    ' mS/cm2',
    ' mV',' mV', ' mS/cm2', ' mS/cm2',
    ' mV', ' mV', ' mS/cm2',
    ' mV', ' mV', '', '', '');
}
  NO_YES: array[0..1] of string = ('No','Yes');
  YES_NO: array[0..1] of string = ('Yes','No');

  ROW_HEADER = 1;
  COL_RUN = 1;
  COL_TOTAL = COL_RUN + 1;
  COL_LEGAL = COL_RUN + 2;
  COL_ILLEGAL = COL_RUN + 3;
  COL_CONTENDED = COL_RUN + 4;
  COL_DURATION = COL_RUN + 5;
  COL_TOTAL_SYNAPTIC_WEIGHT = COL_RUN + 6;
  COL_AVERAGE_SYNAPTIC_WEIGHT = COL_RUN + 7;
  COL_REWARD_NEURONS = COL_RUN + 8;
  TEXT_RUN = 'Run';
  TEXT_TOTAL = 'Total';
  TEXT_LEGAL = 'Moves';
  TEXT_ILLEGAL = 'Illegal';
  TEXT_CONTENDED = 'Contended';
  TEXT_DURATION = 'Duration (s)';
  TEXT_RUN_ABORTED = 'Aborted';
  TEXT_STOPPED_AT = 'Stopped at';
  TEXT_TOTAL_SYNAPTIC_WEIGHT = 'Total synaptic weight';
  TEXT_AVERAGE_SYNAPTIC_WEIGHT = 'Average synaptic weight';
  TEXT_REWARD_NEURONS = 'Neurons firing for reward';
  TEXT_TOO_LONG = 'Too long waiting';

  COL_EXCEL_TEXT_1 = 1;
  COL_EXCEL_SETTINGS_1 = COL_EXCEL_TEXT_1 + 1;
  COL_EXCEL_TEXT_2 = COL_EXCEL_TEXT_1 + 2;
  COL_EXCEL_SETTINGS_2 = COL_EXCEL_TEXT_1 + 3;
  COL_EXCEL_TEXT_3 = COL_EXCEL_TEXT_1 + 4;
  COL_EXCEL_SETTINGS_3 = COL_EXCEL_TEXT_1 + 5;

  ROW_INIFILE_NAME = 1;
//  ROW_SIM_NUM = 2;
//  ROW_SIM_NAME = 3;
  ROW_RUNS_PER_SIMULATION = 2;
//  ROW_TRIALS_PER_RUN = 5;
//  TEXT_SIM_NUM = 'Simulation number';
//  TEXT_SIM_NAME = 'Simulation name';
  TEXT_INIFILE_NAME = 'Ini file name';
  TEXT_RUNS_PER_SIMULATION = 'Runs per simulation';
//  TEXT_TRIALS_PER_RUN = 'Trials per run';

  ROW_SIMULATION_OFFSET = ROW_RUNS_PER_SIMULATION + 1;
  ROW_SPINY_NEURON_COUNT = ROW_SIMULATION_OFFSET + 1;
  ROW_FEATURES_PER_MSN = ROW_SIMULATION_OFFSET + 2;

{
  ROW_ROW_COUNT = ROW_SIMULATION_OFFSET + 2;
  ROW_COL_COUNT = ROW_SIMULATION_OFFSET + 3;
  ROW_START_ROW = ROW_SIMULATION_OFFSET + 4;
  ROW_START_COL = ROW_SIMULATION_OFFSET + 5;
  ROW_REWARD_ROW = ROW_SIMULATION_OFFSET + 6;
  ROW_REWARD_COL = ROW_SIMULATION_OFFSET + 7;
  ROW_AVE_INPUTS_PER_POSITION = ROW_SIMULATION_OFFSET + 8;
  ROW_AVE_POSITIONS_PER_INPUT = ROW_SIMULATION_OFFSET + 9;
}
  TEXT_SPINY_NEURON_COUNT = 'Number of spiny neurons';
  TEXT_FEATURES_PER_MSN = 'Features per MSN';
{
  TEXT_ROW_COUNT = 'Row count';
  TEXT_COL_COUNT = 'Column count';
  TEXT_START_ROW = 'Start row';
  TEXT_START_COL = 'Start colum';
  TEXT_REWARD_ROW = 'Reward row';
  TEXT_REWARD_COL = 'Reward column';
  TEXT_AVE_INPUTS_PER_POSITION = 'Average inputs per position';
  TEXT_AVE_POSITIONS_PER_INPUT = 'Average positions per input';
}
  GENERAL_OFFSET = 0;
  ROW_MEMBRANE_CAPACITANCE = GENERAL_OFFSET + 1;
  ROW_TEMPERATURE = GENERAL_OFFSET + 2;
  ROW_FIRING_THRESHOLD = GENERAL_OFFSET + 3;

  TEXT_MEMBRANE_CAPACITANCE = 'Membrane capacitance (�F/cm2)';
  TEXT_TEMPERATURE = 'Temperature (�C)';
  TEXT_FIRING_THRESHOLD = 'Firing threshold (mV)';

  INPUT_OFFSET = ROW_FIRING_THRESHOLD + 1;
  ROW_IP_COUNT = INPUT_OFFSET + 1;
  ROW_IP_PROPORTION = INPUT_OFFSET + 2;
//  ROW_STAGGER_IP_START = INPUT_OFFSET + 3;
//  ROW_STAGGER_BY = INPUT_OFFSET + 3;
  ROW_IP_JITTER = INPUT_OFFSET + 3;
  ROW_EXCITATORY_REVERSAL_POTENTIAL = INPUT_OFFSET + 4;
  ROW_IP_AMPLITUDE = INPUT_OFFSET + 5;
  ROW_IP_RISE_TIME = INPUT_OFFSET + 6;
  ROW_IP_DECAY_TIME = INPUT_OFFSET + 7;
  ROW_IP_FREQUENCY = INPUT_OFFSET + 8;
  ROW_IP_FSD = INPUT_OFFSET + 9;
  ROW_IP_PAUSE_AFTER_MOVE = INPUT_OFFSET + 10;
  TEXT_IP_COUNT = 'Number of excitatory inputs';
  TEXT_IP_PROPORTION = 'Proportion of positions per input';
//  TEXT_IP_STAGGER = 'Stagger input starts?';
//  TEXT_IP_STAGGER_BY = 'Stagger by (ms)';
  TEXT_IP_JITTER = 'Input jitter (ms)';
  TEXT_EXCITATORY_REVERSAL_POTENTIAL = 'Excitation Reversal potential (mV)';
  TEXT_IP_AMPLITUDE = 'Excitation peak (�S/cm2)';
  TEXT_IP_RISE_TIME = 'Excitation rise time (ms)';
  TEXT_IP_DECAY_TIME = 'Excitation decay time constant (ms)';
  TEXT_IP_UP_STATE = 'Excitation firing rate(Hz)';
  TEXT_IP_FSD = 'Excitation standard deviation(Hz)';
  TEXT_IP_PAUSE_AFTER_MOVE = 'Length of pause after move (ms)';

  LEAKAGE_OFFSET = ROW_IP_PAUSE_AFTER_MOVE + 1;
  ROW_LEAKAGE_CONDUCTANCE = LEAKAGE_OFFSET + 1;
  ROW_LEAKAGE_REVERSAL_POTENTIAL = LEAKAGE_OFFSET + 2;
  TEXT_LEAKAGE_CONDUCTANCE = 'Leakage conductance (mS/cm2)';
  TEXT_LEAKAGE_REVERSAL_POTENTIAL = 'Leakage reversal potential (mV)';

  POTASSIUM_OFFSET = ROW_LEAKAGE_REVERSAL_POTENTIAL + 1;
  ROW_POTASSIUM_REVERSAL_POTENTIAL = POTASSIUM_OFFSET + 1;
  ROW_KIR2_MAXG = POTASSIUM_OFFSET + 2;
  ROW_KIR2_VH = POTASSIUM_OFFSET + 3;
  ROW_KIR2_VC = POTASSIUM_OFFSET + 4;
  ROW_KSI_MAXG_FIXED = POTASSIUM_OFFSET + 5;
  ROW_KSI_MAXG_VARIABLE = POTASSIUM_OFFSET + 6;
  ROW_KSI_VARIABLE_ACTIVATION_TIME = POTASSIUM_OFFSET + 7;
  ROW_KSI_VARIABLE_INACTIVATION_TIME = POTASSIUM_OFFSET + 8;
  ROW_KSI_VH = POTASSIUM_OFFSET + 9;
  ROW_KSI_VC = POTASSIUM_OFFSET + 10;
  TEXT_POTASSIUM_REVERSAL_POTENTIAL = 'Potassium reversal potential (mV)';
  TEXT_KIR2 = 'Kir2';
  TEXT_KIR2_MAXG = 'Kir2 maximum conductance (mS/cm2)';
  TEXT_KIR2_VH = 'Kir2 Vh (mV)';
  TEXT_KIR2_VC = 'Kir2 Vc (mV)';
  TEXT_KSI = 'Ksi';
  TEXT_KSI_MAXG_FIXED = 'Ksi max fixed conductance (mS/cm2)';
  TEXT_KSI_MAXG_VARIABLE = 'Ksi max variable conductance (mS/cm2)';
  TEXT_KSI_VARIABLE_ACTIVATION_TIME = 'Ksi variable conductance activation time (ms)';
  TEXT_KSI_VARIABLE_INACTIVATION_TIME = 'Ksi variable conductance inactivation time (ms)';
  TEXT_KSI_VH = 'Ksi Vh (mV)';
  TEXT_KSI_VC = 'Ksi Vc (mV)';

  CALCIUM_OFFSET = 0;
  ROW_CA_OUT = CALCIUM_OFFSET + 1;
  ROW_CA_IN = CALCIUM_OFFSET + 2;
  ROW_CA_MAXL = CALCIUM_OFFSET + 3;
  ROW_CA_VH = CALCIUM_OFFSET + 4;
  ROW_CA_VC = CALCIUM_OFFSET + 5;
  TEXT_CA_OUT = 'Calcium concentration outside';
  TEXT_CA_IN = 'Calcium concentration inside';
  TEXT_CA_MAXL = 'Calcium maximum permeability';
  TEXT_CA_VH = 'Calcium Vh (mV)';
  TEXT_CA_VC = 'Calcium Vc (mV)';

  DOPAMINE_OFFSET = ROW_CA_VC + 1;
  ROW_DOPAMINE_BACKGROUND = DOPAMINE_OFFSET + 1;
  ROW_DOPAMINE_HIGH = DOPAMINE_OFFSET + 2;
  ROW_DOPAMINE_LOW = DOPAMINE_OFFSET + 3;
  ROW_DOPAMINE_TIME_CONSTANT = DOPAMINE_OFFSET + 4;
  ROW_DELAY_TO_REWARD = DOPAMINE_OFFSET + 5;
  ROW_REWARD_TIMING = DOPAMINE_OFFSET + 6;
  ROW_DEVALUE_REWARD_BY_ROOM = DOPAMINE_OFFSET + 7;
  ROW_DEVALUE_REWARD_BY_ROOM_BY = DOPAMINE_OFFSET + 8;
  ROW_DEVALUE_REWARD_BY_TIMES_GAINED = DOPAMINE_OFFSET + 9;
  ROW_DEVALUE_REWARD_BY_TIMES_GAINED_BY = DOPAMINE_OFFSET + 10;
//  ROW_DOPAMINE_DURATION = DOPAMINE_OFFSET + 3;
  TEXT_DOPAMINE_BACKGROUND = 'Background dopamine level';
  TEXT_DOPAMINE_HIGH = 'Dopamine pulse level';
  TEXT_DOPAMINE_LOW = 'Dopamine trough level';
  TEXT_DOPAMINE_DURATION = 'Dopamine time constant (ms)';
  TEXT_DELAY_TO_REWARD = 'Delay to reward (ms)';
  TEXT_REWARD_TIMING = 'Reward timing';
  arRewardTimings: array[0..2] of string = ('First correct door', 'Exit door', ' Every correct door');
  TEXT_DEVALUE_REWARD_BY_ROOM = 'Devalue reward by room';
  TEXT_DEVALUE_REWARD_BY_ROOM_BY = 'Devalue by (%)';
  TEXT_DEVALUE_REWARD_BY_TIMES_GAINED = 'Devalue reward by times gained';
  TEXT_DEVALUE_REWARD_BY_TIMES_GAINED_BY = 'Devalue by (%)';

  LEARNING_OFFSET = ROW_DEVALUE_REWARD_BY_TIMES_GAINED_BY + 1;
  ROW_LTP_POTENTIATION = LEARNING_OFFSET + 1;
  ROW_LTP_TIME_CONSTANT = LEARNING_OFFSET + 2;
  ROW_LTP_METHOD = LEARNING_OFFSET + 3;
  ROW_MAX_SYNAPTIC_WEIGHT = LEARNING_OFFSET + 4;
  ROW_LTD_DEPRESSION = LEARNING_OFFSET + 5;
  ROW_LTD_TIME_CONSTANT = LEARNING_OFFSET + 6;
  ROW_LTD_METHOD = LEARNING_OFFSET + 7;
  ROW_CONTENTION_METHOD = LEARNING_OFFSET + 8;
  ROW_REWARD_DURING_ACQUISITION = LEARNING_OFFSET + 9;
  ROW_REWARD_DURING_PR0BE= LEARNING_OFFSET + 10;
  ROW_PUNISHMENT_DURING_ACQUISITION = LEARNING_OFFSET + 11;
  ROW_PUNISHMENT_DURING_PROBE = LEARNING_OFFSET + 12;
  ROW_DISAPPOINTMENT_METHOD = LEARNING_OFFSET + 13;
//  ROW_DISAPPOINTMENT_FACTOR = LEARNING_OFFSET + 11;
//  ROW_DISAPPOINTMENT_TAU = LEARNING_OFFSET + 12;
  TEXT_LTP_POTENTIATION = 'Proportional potentiation from one pairing';
  TEXT_LTP_TIME_CONSTANT = 'Synaptic input time constant';
  TEXT_LTP_METHOD = 'LTP selection method';
  TEXT_MAX_SYNAPTIC_WEIGHT = 'Maximum synaptic weight';
  TEXT_LTD_DEPRESSION = 'Proportional depression from one pairing';
//  TEXT_LTD_TIME_CONSTANT = 'LTD time constant';
  TEXT_LTD_METHOD = 'LTD selection method';
  TEXT_CONTENTION_METHOD = 'Contention selection method';
  TEXT_REWARD_DURING_ACQUISITION = 'Reward during acquisition';
  TEXT_REWARD_DURING_PROBE = 'Reward during probe';
  TEXT_PUNISHMENT_DURING_ACQUISITION = 'Punishment during acquisition';
  TEXT_PUNISHMENT_DURING_PROBE = 'Punishment during probe';
  TEXT_DISAPPOINTMENT_METHOD ='Disappointment method';
//  TEXT_DISAPPOINTMENT_FACTOR ='Proportional depression from one disappointment';
//  TEXT_DISAPPOINTMENT_TAU = 'Disappointment time constant';
  arLtpMethods: array[0..2] of string = (
    'Dopamine level only','Dopamine + time since last fired','Dopamine + time since up state start');
  arLtdMethods: array[0..2] of string = (
    'No LTD','Any synapse with input in last 50 ms','Proportional to time of last input');
  arContentionMethods: array[0..3] of string = (
    'Do move for spiny which fired first','Do not move','Perform all moves','Perform one move randomly selected');
  arDisappointmentMethods: array[0..1] of string = ('Neuron causing illegal move',
    'All neurons');


implementation

end.
