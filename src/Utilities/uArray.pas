unit uArray;

interface

uses Math, SysUtils;

type
  TDynamicIntegerArray = array of integer;

procedure RemoveItem(var AnArray: TDynamicIntegerArray; ItemIndex: integer);
procedure CreateSequentialArray(var AnArray: TDynamicIntegerArray; ItemCount: integer); overload;
procedure CreateSequentialArray(var AnArray: TDynamicIntegerArray; ItemCount, Min: integer); overload;
procedure CreateRandomArray(var AnArray: TDynamicIntegerArray; ItemCount, Min: integer); overload;
procedure CreateRandomArray(var AnArray: TDynamicIntegerArray; ItemCount, Min, Max: integer); overload;
function GetItem(AnArray: TDynamicIntegerArray): integer;

implementation

{
This procedure removes an item from an array, moves all the subsequent items
up one position and shortens the array by one.
}
procedure RemoveItem(var AnArray: TDynamicIntegerArray; ItemIndex: integer);
var iPosition: integer;
    iNewLength: integer;
begin
  for iPosition := ItemIndex to (length(AnArray) - 2) do begin
    if iPosition > length(AnArray) then begin
      raise Exception.Create('Trying to remove item that does not exist from an array');
    end;
    if  iPosition < 0 then begin
      raise Exception.Create('Trying to remove item less than zero from an array');
    end;
    AnArray[iPosition] := AnArray[iPosition + 1];
  end;
  iNewLength := length(AnArray) - 1;
  SetLength(AnArray, iNewLength);
end;

{
This procedure creates an array of size itemcount and populates it with
a sequence of numbers starting from zero
}
procedure CreateSequentialArray(var AnArray: TDynamicIntegerArray; ItemCount: integer); overload;
var i: integer;
begin
  SetLength(AnArray, ItemCount);
  for i := 0 to pred(ItemCount) do
    AnArray[i] := i;
end;

{
This procedure creates an array of size itemcount and populates it with
a sequence of numbers starting from Min
}
procedure CreateSequentialArray(var AnArray: TDynamicIntegerArray; ItemCount, Min: integer); overload;
var i: integer;
begin
  SetLength(AnArray, ItemCount);
  for i := 0 to pred(ItemCount) do
    AnArray[i] := i + Min;
end;

{
This procedure creates an array of size itemcount and populates it with
a sequence of numbers starting from min, in random order
}
procedure CreateRandomArray(var AnArray: TDynamicIntegerArray; ItemCount, Min: integer); overload;
var i,
    iRandom,
    iLength: integer;
    DummyArray: TDynamicIntegerArray;
begin
  CreateSequentialArray(DummyArray, ItemCount, Min);
  SetLength(AnArray, ItemCount);
  iLength := ItemCount;
  for i := 0 to pred(ItemCount) do begin
    iRandom := RandomRange(0, iLength);
    AnArray[i] := DummyArray[iRandom];
    RemoveItem(DummyArray,iRandom);
    dec(iLength);
  end;
end;

{
This procedure creates an array of size itemcount and populates it with
ItemCount numbers drawn from the range [Min..Max]
}
procedure CreateRandomArray(var AnArray: TDynamicIntegerArray; ItemCount, Min, Max: integer); overload;
var i,
    iRandom,
    iLength: integer;
    DummyArray: TDynamicIntegerArray;
begin
  CreateSequentialArray(DummyArray, Max-Min, Min);
  SetLength(AnArray, ItemCount);
  iLength := Max-Min;
  for i := 0 to pred(ItemCount) do begin
    iRandom := RandomRange(0, iLength);
    AnArray[i] := DummyArray[iRandom];
    RemoveItem(DummyArray,iRandom);
    dec(iLength);
  end;
end;

function GetItem(AnArray: TDynamicIntegerArray): integer;
begin
 result := trunc(Random(length(AnArray)));
end;

end.
