unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, XStringGrid, Grids, TB97Ctls, TB97, TB97Tlbr, ExtCtrls,
  ImgList, TeEngine, Series, TeeProcs, Chart, IniFiles, ComCtrls, ComObj, ActiveX,
  viArrow, Arrow, Spin, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdTCPServer, uBoltzmann, Buttons, TeeURL, TeeSeriesTextEd,
  ExtDlgs, IdAntiFreezeBase, IdAntiFreeze;

type
  TTarget = record
    Direction: integer;
    Salience,
    ExpectedValue: real;  //calculated trial to trial using  the PE and the learning rate. PE = Reward - ExpectedValue
    Use,
    Show: boolean;
    Correct: boolean;
    ChosenAsTarget: boolean;
    ChosenAsDirection: boolean;
    RewardProbability: real;
    PresentedCount,
    PickedCount,
    RewardedCount: integer;
    EffectiveProbability: real;
  end;

  TSimulation = record
    Successful,
    Optimum,
    Rewarded: integer;
  end;

  TTrial = record
    Successful,
    Optimum,
    Rewarded: boolean;
    Target1,
    Target2,
    Direction1,
    Direction2: integer;
    Target: array[0..1] of TTarget;
    TargetPicked: integer;
    ErrorCode: integer;
  end;

  TStructureDefaults = record
//    TonicFiring: integer;
    IgnoreTonic: boolean;
    NoisePercent: double;
    NoiseProportion: double;  //Noise is added to the external inputs
    Tau,
    Threshold: real;
  end;

  TConnection = record
//    ExpectedValue,
    Real,
    Normalized: real;
  end;

//  TCxConnection =  array[1..4,1..4] of real;

  TContext = record
    ChosenCount: integer;
    ExpectedValue: real;
  end;

  //Used in a 6 by x array where x is number of runs in a simulation / 6 to keep track of the outcome
  //of the presentation of each context
  TOutcome = record
    Successful,
    Optimum,
    Rewarded: integer;
    Value,          //keeps the value of the context after the current trial so that the evolution of these valued can be seen in the spreadsheet
    PE: real;
  end;

  TIntegrator = record
    YScale,
    Eps,
    Error,
    StepSize: real;
  end;

  {
  This array contains the activations for each ensemble in a structure
  It is always 5 x 5, although not all structures have an associative area.
  Position [0,0] is never used
  }
  TStructureArray = array [0..4,0..4] of double;

  TParamStep = record
    Start,
    Stop,
    Step: real;
  end;

  TfrmMain = class(TForm)
    Dock971: TDock97;
    Toolbar971: TToolbar97;
    btnSaveParameters: TToolbarButton97;
    btnTarget: TToolbarButton97;
    btnDecisionPhase: TToolbarButton97;
    btnMovmentPhase: TToolbarButton97;
    ilTargets: TImageList;
    Timer1: TTimer;
    ToolbarSep971: TToolbarSep97;
    dlgSave: TSaveDialog;
    btnOpen: TToolbarButton97;
    dlgOpen: TOpenDialog;
    pc1: TPageControl;
    tsSim: TTabSheet;
    tsConfig: TTabSheet;
    Panel2: TPanel;
    shpGpi: TShape;
    shpCx: TShape;
    shpTh: TShape;
    shpStn: TShape;
    shpStr: TShape;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    imT1: TImage;
    imT2: TImage;
    grdCxCog: TXStringGrid;
    grdThMotor: TXStringGrid;
    grdStnMotor: TXStringGrid;
    grdCxMotor: TXStringGrid;
    grdStnCog: TXStringGrid;
    grdThCog: TXStringGrid;
    grdStrCog: TXStringGrid;
    grdGpiCog: TXStringGrid;
    grdStrAss: TXStringGrid;
    grdStrMotor: TXStringGrid;
    grdGPiMotor: TXStringGrid;
    Panel3: TPanel;
    Panel5: TPanel;
    grdCxAss: TXStringGrid;
    Label44: TLabel;
    imT3: TImage;
    imT4: TImage;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    shpCxAss: TShape;
    Label49: TLabel;
    GroupBox6: TGroupBox;
    GroupBox8: TGroupBox;
    imD1: TImage;
    imD2: TImage;
    imD3: TImage;
    imD4: TImage;
    Shape1: TShape;
    im1: TImage;
    im4: TImage;
    im3: TImage;
    im2: TImage;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label6: TLabel;
    btnReinitialize: TToolbarButton97;
    ToolbarSep972: TToolbarSep97;
    btnOneTrial: TToolbarButton97;
    GroupBox7: TGroupBox;
    GroupBox2: TGroupBox;
    Label35: TLabel;
    Label38: TLabel;
    edStnMotorToGpiMotor: TEdit;
    edStnCogToGpiCog: TEdit;
    GroupBox3: TGroupBox;
    Label39: TLabel;
    edCogStrToGpi: TEdit;
    GroupBox1: TGroupBox;
    GroupBox10: TGroupBox;
    Label43: TLabel;
    Label58: TLabel;
    edCxMotorToStrAss: TEdit;
    edCxCogToStrAss: TEdit;
    GroupBox12: TGroupBox;
    Label59: TLabel;
    Label62: TLabel;
    edThMotorToCxMotor: TEdit;
    edThCogToCxCog: TEdit;
    Label64: TLabel;
    Label65: TLabel;
    edCxMotorToStrMotor: TEdit;
    edCxCogToStrCog: TEdit;
    GroupBox14: TGroupBox;
    Label68: TLabel;
    Label69: TLabel;
    edCxMotorToStnMotor: TEdit;
    edCxCogToStnCog: TEdit;
    Arrow1: TArrow;
    viArrow1: TviArrow;
    Arrow2: TArrow;
    Arrow3: TArrow;
    arrowThCx: TviArrow;
    viArrow3: TviArrow;
    viArrow4: TviArrow;
    Label51: TLabel;
    edCxAssToStrAss: TEdit;
    tsStats: TTabSheet;
    grdAnovaCxT: TStringGrid;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label67: TLabel;
    Label70: TLabel;
    grdAnovaCxM: TStringGrid;
    grdAnovaStrT: TStringGrid;
    grdAnovaStrM: TStringGrid;
    grdAnovaStnT: TStringGrid;
    grdAnovaStnM: TStringGrid;
    grdAnovaGpiT: TStringGrid;
    grdAnovaGpiM: TStringGrid;
    grdAnovaThT: TStringGrid;
    grdanovaThM: TStringGrid;
    tsConfigTeach: TTabSheet;
    Label72: TLabel;
    Edit1: TEdit;
    Label73: TLabel;
    lblCurrentRun2: TLabel;
    Label75: TLabel;
    edDopamineLevel: TEdit;
    sbDopamineLevel: TSpinButton;
    IdTCPServer1: TIdTCPServer;
    btnOneStepDecision: TToolbarButton97;
    GroupBox13: TGroupBox;
    imConfigT1: TImage;
    imConfigT4: TImage;
    imConfigT3: TImage;
    imConfigT2: TImage;
    Label61: TLabel;
    Label74: TLabel;
    edTarget1Value: TEdit;
    edTarget2Value: TEdit;
    edTarget3Value: TEdit;
    edTarget4Value: TEdit;
    edTarget4Reward: TEdit;
    edTarget3Reward: TEdit;
    edTarget2Reward: TEdit;
    edTarget1Reward: TEdit;
    Label66: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    edBaselineMotorLevel: TEdit;
    edBaselineAssociationLevel: TEdit;
    edBaselineCognitiveLevel: TEdit;
    cbShowWeights: TCheckBox;
    GroupBox16: TGroupBox;
    Label78: TLabel;
    edValueLearningRate: TEdit;
    imSad: TImage;
    imHappy: TImage;
    cbLearning: TCheckBox;
    grdStrRow: TXStringGrid;
    Label80: TLabel;
    grdStrCol: TXStringGrid;
    Label81: TLabel;
    Label84: TLabel;
    edMotorGpiToMotorTh: TEdit;
    edCogGpiToCogTh: TEdit;
    Label85: TLabel;
    ToolbarSep974: TToolbarSep97;
    rgLearningBasedOn: TRadioGroup;
    GroupBox17: TGroupBox;
    Label11: TLabel;
    edRunsPerSimulation: TEdit;
    cbSendEveryTrialToExcel: TCheckBox;
    cbRobotAttached: TCheckBox;
    GroupBox9: TGroupBox;
    Label41: TLabel;
    Label40: TLabel;
    Label42: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    GroupBox5: TGroupBox;
    edCxNoisePercent: TEdit;
    edStrNoisePercent: TEdit;
    edStnNoisePercent: TEdit;
    edGpiNoisePercent: TEdit;
    edThNoisePercent: TEdit;
    GroupBox18: TGroupBox;
    lblWInitial: TLabel;
    edWInitial: TEdit;
    cbWeightsRandom: TCheckBox;
    Label86: TLabel;
    edSdWInitial: TEdit;
    Label10: TLabel;
    Label88: TLabel;
    edMotorStrToGpi: TEdit;
    edAssStrToGpi: TEdit;
    GroupBox11: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    cbUseThreshold: TCheckBox;
    edCorticalThresholdLevel: TEdit;
    edMaxLoops: TEdit;
    GroupBox19: TGroupBox;
    Label18: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    edMaxStrActivation: TEdit;
    edStrVh: TEdit;
    edStrVc: TEdit;
    cbTransferFunction: TCheckBox;
    Label36: TLabel;
    edMinStrActivation: TEdit;
    GroupBox20: TGroupBox;
    cbGpiAss: TCheckBox;
    cbAutoSave: TCheckBox;
    Label37: TLabel;
    edSimsPerExperiment: TEdit;
    Label63: TLabel;
    edSaveDirectory: TEdit;
    Label79: TLabel;
    edFileName: TEdit;
    cbAddDate: TCheckBox;
    btnPause: TToolbarButton97;
    btnStop: TToolbarButton97;
    grdGpiCol: TXStringGrid;
    lblGpiCol: TLabel;
    lblGpiAss: TLabel;
    grdGpiAss: TXStringGrid;
    grdGpiRow: TXStringGrid;
    lblGpiRow: TLabel;
    Label50: TLabel;
    edAssGpiToMotorTh: TEdit;
    edAssGpiToCogTh: TEdit;
    Label71: TLabel;
    Label90: TLabel;
    edStnMotorToAssGpi: TEdit;
    edStnCogToAssGpi: TEdit;
    Label91: TLabel;
    pc2: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label82: TLabel;
    Label83: TLabel;
    Label87: TLabel;
    chrtStr: TChart;
    srsStrCol1: TFastLineSeries;
    srsStrCol2: TFastLineSeries;
    srsStrCol3: TFastLineSeries;
    srsStrCol4: TFastLineSeries;
    srsStrRow1: TFastLineSeries;
    srsStrRow2: TFastLineSeries;
    srsStrRow3: TFastLineSeries;
    srsStrRow4: TFastLineSeries;
    chrtCx: TChart;
    srsCxCog1: TFastLineSeries;
    srsCxCog2: TFastLineSeries;
    srsCxCog3: TFastLineSeries;
    srsCxCog4: TFastLineSeries;
    srsCxMotor1: TFastLineSeries;
    srsCxMotor2: TFastLineSeries;
    srsCxMotor3: TFastLineSeries;
    srsCxMotor4: TFastLineSeries;
    chrtGpi: TChart;
    srsGpiTotal: TFastLineSeries;
    Memo1: TMemo;
    Label92: TLabel;
    cbUseTarget4: TCheckBox;
    cbUseTarget3: TCheckBox;
    cbUseTarget2: TCheckBox;
    cbUseTarget1: TCheckBox;
    btnMultipleTrials: TToolbarButton97;
    GroupBox22: TGroupBox;
    edTauGpi: TEdit;
    edTauTh: TEdit;
    edTauCx: TEdit;
    edTauStr: TEdit;
    edTauStn: TEdit;
    GroupBox23: TGroupBox;
    edThresholdGpi: TEdit;
    edThresholdTh: TEdit;
    edThresholdCx: TEdit;
    edThresholdStr: TEdit;
    edThresholdStn: TEdit;
    shpCog4: TShape;
    shpCog3: TShape;
    shpCog2: TShape;
    shpCog1: TShape;
    Label94: TLabel;
    edLoopsBeforeTargets: TEdit;
    GroupBox24: TGroupBox;
    Label95: TLabel;
    edCxToTh: TEdit;
    cbCxThalamusActive: TCheckBox;
    shpMotor1: TShape;
    shpMotor2: TShape;
    shpMotor3: TShape;
    shpMotor4: TShape;
    Label96: TLabel;
    edMaxWeight: TEdit;
    Label97: TLabel;
    edMinWeight: TEdit;
    Label98: TLabel;
    edLtp: TEdit;
    rgLearningType: TRadioGroup;
    Label57: TLabel;
    Label109: TLabel;
    edLtd: TEdit;
    TabSheet3: TTabSheet;
    grdCogWeights: TXStringGrid;
    grdMotorWeights: TXStringGrid;
    Shape10: TShape;
    Label110: TLabel;
    grdAssWeights: TXStringGrid;
    rgAssWeights: TRadioGroup;
    Label111: TLabel;
    lblPE: TLabel;
    grdCogDeltaW: TXStringGrid;
    grdMotorDeltaW: TXStringGrid;
    Shape11: TShape;
    Label112: TLabel;
    grdAssDeltaW: TXStringGrid;
    cbMotorLearning: TCheckBox;
    rgWeightsDisplay: TRadioGroup;
    sbIncGains: TSpeedButton;
    sbDecGains: TSpeedButton;
    GroupBox15: TGroupBox;
    sbCxActivationToImage: TSpeedButton;
    sbStrActivationToImage: TSpeedButton;
    sbGpiActivationToImage: TSpeedButton;
    sbSaveAllImages: TSpeedButton;
    Label126: TLabel;
    Label128: TLabel;
    Label130: TLabel;
    Label131: TLabel;
    Label132: TLabel;
    edGainDirectStartValue: TEdit;
    edGainHyperStartValue: TEdit;
    edGainDirectEndValue: TEdit;
    edGainHyperEndValue: TEdit;
    edGainDirectStep: TEdit;
    edGainHyperStep: TEdit;
    sbPathwayParams: TSpeedButton;
    Label133: TLabel;
    edAssociativeRatio: TEdit;
    Label134: TLabel;
    GroupBox4: TGroupBox;
    Label135: TLabel;
    Label136: TLabel;
    lblDirectGain: TLabel;
    lblHyperGain: TLabel;
    dlgSaveImage1: TSavePictureDialog;
    TabSheet4: TTabSheet;
    Label89: TLabel;
    lblCurrentSim: TLabel;
    Label60: TLabel;
    lblCurrentRun1: TLabel;
    Label14: TLabel;
    lblLoops: TLabel;
    Panel4: TPanel;
    chrtCx2: TChart;
    srsCxCog2_1: TFastLineSeries;
    srsCxCog2_2: TFastLineSeries;
    srsCxCog2_3: TFastLineSeries;
    srsCxCog2_4: TFastLineSeries;
    srsCxMotor2_1: TFastLineSeries;
    srsCxMotor2_2: TFastLineSeries;
    srsCxMotor2_3: TFastLineSeries;
    srsCxMotor2_4: TFastLineSeries;
    chrtTh2: TChart;
    srsThCog2_1: TFastLineSeries;
    srsThCog2_2: TFastLineSeries;
    srsThCog2_3: TFastLineSeries;
    srsThCog2_4: TFastLineSeries;
    srsThMotor2_1: TFastLineSeries;
    srsThMotor2_2: TFastLineSeries;
    srsThMotor2_3: TFastLineSeries;
    srsThMotor2_4: TFastLineSeries;
    chrtStr2: TChart;
    srsStrCog2_1: TFastLineSeries;
    srsStrCog2_2: TFastLineSeries;
    srsStrCog2_3: TFastLineSeries;
    srsStrCog2_4: TFastLineSeries;
    srsStrMotor2_1: TFastLineSeries;
    srsStrMotor2_2: TFastLineSeries;
    srsStrMotor2_3: TFastLineSeries;
    srsStrMotor2_4: TFastLineSeries;
    chrtGpi2: TChart;
    srsGpiCog2_1: TFastLineSeries;
    srsGpiCog2_2: TFastLineSeries;
    srsGpiCog2_3: TFastLineSeries;
    srsGpiCog2_4: TFastLineSeries;
    srsGpiMotor2_1: TFastLineSeries;
    srsGpiMotor2_2: TFastLineSeries;
    srsGpiMotor2_3: TFastLineSeries;
    srsGpiMotor2_4: TFastLineSeries;
    chrtStn2: TChart;
    srsStnCog2_1: TFastLineSeries;
    srsStnCog2_2: TFastLineSeries;
    srsStnCog2_3: TFastLineSeries;
    srsStnCog2_4: TFastLineSeries;
    srsStnMotor2_1: TFastLineSeries;
    srsStnMotor2_2: TFastLineSeries;
    srsStnMotor2_3: TFastLineSeries;
    srsStnMotor2_4: TFastLineSeries;
    arrowThCx2: TviArrow;
    Arrow4: TArrow;
    viArrow5: TviArrow;
    viArrow6: TviArrow;
    Arrow5: TArrow;
    viArrow7: TviArrow;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label93: TLabel;
    Label137: TLabel;
    Panel6: TPanel;
    GroupBox25: TGroupBox;
    Label100: TLabel;
    Label101: TLabel;
    Label102: TLabel;
    Label103: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    Label106: TLabel;
    ImC1T1: TImage;
    Label107: TLabel;
    Label108: TLabel;
    imC2T1: TImage;
    imC1T2: TImage;
    imC3T2: TImage;
    imC3T1: TImage;
    imC4T2: TImage;
    imC4T1: TImage;
    imC5T2: TImage;
    imC5T1: TImage;
    imC6T2: TImage;
    imC6T1: TImage;
    imC2T2: TImage;
    Label113: TLabel;
    Label114: TLabel;
    Label115: TLabel;
    Label116: TLabel;
    Label117: TLabel;
    Label118: TLabel;
    Label119: TLabel;
    grdContextValues: TXStringGrid;
    GroupBox26: TGroupBox;
    Label99: TLabel;
    imTV1: TImage;
    imTv2: TImage;
    imTv3: TImage;
    imTV4: TImage;
    Label120: TLabel;
    Label121: TLabel;
    Label122: TLabel;
    Label123: TLabel;
    Label124: TLabel;
    grdTargetValue: TXStringGrid;
    Panel7: TPanel;
    GroupBox21: TGroupBox;
    shpColorCog1: TShape;
    shpColorCog2: TShape;
    shpColorCog3: TShape;
    shpColorCog4: TShape;
    Shape2: TShape;
    im2_1: TImage;
    im2_4: TImage;
    im2_3: TImage;
    im2_2: TImage;
    Label146: TLabel;
    Label147: TLabel;
    Label148: TLabel;
    Label149: TLabel;
    Label150: TLabel;
    imSad2: TImage;
    imHappy2: TImage;
    shpChosen1: TShape;
    shpChosen2: TShape;
    shpLegendCog1: TShape;
    shpLegendCog2: TShape;
    shpLegendCog3: TShape;
    shpLegendCog4: TShape;
    imT2_1: TImage;
    imT2_2: TImage;
    imT2_3: TImage;
    imT2_4: TImage;
    GroupBox28: TGroupBox;
    Label142: TLabel;
    Label143: TLabel;
    Label144: TLabel;
    Label145: TLabel;
    shpColorMotor1: TShape;
    shpColorMotor2: TShape;
    shpColorMotor3: TShape;
    shpColorMotor4: TShape;
    shpLegendMotor1: TShape;
    shpLegendMotor2: TShape;
    shpLegendMotor3: TShape;
    shpLegendMotor4: TShape;
    cbShowCog: TCheckBox;
    cbShowMotor: TCheckBox;
    cbShowGraphs: TCheckBox;
    btnBoltzmann: TSpeedButton;
    GroupBox27: TGroupBox;
    Label125: TLabel;
    Label127: TLabel;
    Label129: TLabel;
    Label138: TLabel;
    edLearningTFMax: TEdit;
    edLearningTFVh: TEdit;
    edLearningTFVc: TEdit;
    edLearningTFMin: TEdit;
    cbLearningTransferFunction: TCheckBox;
    btnWeightBoltzmann: TSpeedButton;
    cbShowActivations: TCheckBox;
    cbShowTargets: TCheckBox;
    ToolbarSep973: TToolbarSep97;
    btnRobot: TToolbarButton97;
    IdAntiFreeze1: TIdAntiFreeze;
    procedure btnTargetClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnDecisionPhaseClick(Sender: TObject);
    procedure btnMovmentPhaseClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure grdCxMotorDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure btnSaveParametersClick(Sender: TObject);
    procedure edStnMotorToGpiMotorChange(Sender: TObject);
    procedure edStnCogToGpiCogChange(Sender: TObject);
    procedure edCogStrToGpiChange(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure edCxNoisePercentChange(Sender: TObject);
    procedure edStrNoisePercentChange(Sender: TObject);
    procedure edStnNoisePercentChange(Sender: TObject);
    procedure edGpiNoisePercentChange(Sender: TObject);
    procedure edThNoisePercentChange(Sender: TObject);
    procedure edMSNThresholdChange(Sender: TObject);
    procedure btnMultipleTrialsClick(Sender: TObject);
    procedure edRunsPerSimulationChange(Sender: TObject);
    procedure btnOneTrialClick(Sender: TObject);
    procedure edCorticalThresholdLevelChange(Sender: TObject);
    procedure edMaxLoopsChange(Sender: TObject);
    procedure cbUseThresholdClick(Sender: TObject);
    procedure edCxMotorToStrAssChange(Sender: TObject);
    procedure edCxCogToStrAssChange(Sender: TObject);
    procedure cbIgnoreTonicStnClick(Sender: TObject);
    procedure cbIgnoreTonicGPiClick(Sender: TObject);
    procedure cbIgnoreTonicThClick(Sender: TObject);
    procedure edThMotorToCxMotorChange(Sender: TObject);
    procedure edThCogToCxCogChange(Sender: TObject);
    procedure edTarget2ValueChange(Sender: TObject);
    procedure edTarget1ValueChange(Sender: TObject);
    procedure edBaselineMotorLevelChange(Sender: TObject);
    procedure edCxMotorToStrMotorChange(Sender: TObject);
    procedure edCxCogToStrCogChange(Sender: TObject);
    procedure edCxCogToStnCogChange(Sender: TObject);
    procedure edCxMotorToStnMotorChange(Sender: TObject);
    procedure grdCxCogCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure grdStnMotorCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure grdGPiMotorCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure grdThMotorCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure edBaselineAssociationLevelChange(Sender: TObject);
    procedure edCxAssToStrAssChange(Sender: TObject);
    procedure grdCxAssCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure grdCxMotorCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure grdStrMotorCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure grdStrAssCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure grdStrCogCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbDopamineLevelDownClick(Sender: TObject);
    procedure sbDopamineLevelUpClick(Sender: TObject);
    procedure IdTCPServer1Execute(AThread: TIdPeerThread);
    procedure cbRobotAttachedClick(Sender: TObject);
    procedure edTarget1RewardChange(Sender: TObject);
    procedure edTarget2RewardChange(Sender: TObject);
    procedure edTarget3RewardChange(Sender: TObject);
    procedure edTarget4RewardChange(Sender: TObject);
    procedure edTarget3ValueChange(Sender: TObject);
    procedure edTarget4ValueChange(Sender: TObject);
    procedure edValueLearningRateChange(Sender: TObject);
    procedure edBaselineCognitiveLevelChange(Sender: TObject);
    procedure IdTCPServer1Connect(AThread: TIdPeerThread);
    procedure IdTCPServer1Disconnect(AThread: TIdPeerThread);
    procedure cbLearningClick(Sender: TObject);
    procedure btnOneStepDecisionClick(Sender: TObject);
    procedure cbStnTargetSaliencyClick(Sender: TObject);
    procedure edMotorGpiToMotorThChange(Sender: TObject);
    procedure edCogGpiToCogThChange(Sender: TObject);
    procedure rgLearningBasedOnClick(Sender: TObject);
    procedure edWInitialChange(Sender: TObject);
    procedure edSdWInitialChange(Sender: TObject);
    procedure cbWeightsRandomClick(Sender: TObject);
    procedure edMotorStrToGpiChange(Sender: TObject);
    procedure btnBoltzmannClick(Sender: TObject);
    procedure cbTransferFunctionClick(Sender: TObject);
    procedure edMaxStrActivationChange(Sender: TObject);
    procedure edStrVhChange(Sender: TObject);
    procedure edStrVcChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edMinStrActivationChange(Sender: TObject);
    procedure cbShowWeightsClick(Sender: TObject);
    procedure cbGpiAssClick(Sender: TObject);
    procedure edSimsPerExperimentChange(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure btnPauseClick(Sender: TObject);
    procedure edAssStrToGpiChange(Sender: TObject);
    procedure edAssGpiToMotorThChange(Sender: TObject);
    procedure edAssGpiToCogThChange(Sender: TObject);
    procedure edStnMotorToAssGpiChange(Sender: TObject);
    procedure edStnCogToAssGpiChange(Sender: TObject);
    procedure cbAutoSaveClick(Sender: TObject);
    procedure cbUseTarget1Click(Sender: TObject);
    procedure cbUseTarget2Click(Sender: TObject);
    procedure cbUseTarget3Click(Sender: TObject);
    procedure cbUseTarget4Click(Sender: TObject);
    procedure edTauCxChange(Sender: TObject);
    procedure edTauStrChange(Sender: TObject);
    procedure edTauStnChange(Sender: TObject);
    procedure edTauGpiChange(Sender: TObject);
    procedure edTauThChange(Sender: TObject);
    procedure edThresholdCxChange(Sender: TObject);
    procedure edThresholdStrChange(Sender: TObject);
    procedure edThresholdStnChange(Sender: TObject);
    procedure edThresholdGpiChange(Sender: TObject);
    procedure edThresholdThChange(Sender: TObject);
    procedure edLoopsBeforeTargetsChange(Sender: TObject);
    procedure edCxToThChange(Sender: TObject);
    procedure cbCxThalamusActiveClick(Sender: TObject);
    procedure edMaxWeightChange(Sender: TObject);
    procedure edMinWeightChange(Sender: TObject);
    procedure edLtpChange(Sender: TObject);
    procedure rgLearningTypeClick(Sender: TObject);
    procedure edLtdChange(Sender: TObject);
    procedure rgAssWeightsClick(Sender: TObject);
    procedure grdAssWeightsCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure grdMotorWeightsCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure grdCogWeightsCellProps(Sender: TObject; Canvas: TCanvas;
      var Alignment: TAlignment; var CellText: String;
      AState: TGridDrawState; Row, Col: Integer);
    procedure rgWeightsDisplayClick(Sender: TObject);
    procedure sbIncGainsClick(Sender: TObject);
    procedure sbDecGainsClick(Sender: TObject);
    procedure sbCxActivationToImageClick(Sender: TObject);
    procedure sbSaveAllImagesClick(Sender: TObject);
    procedure cbShowGraphsClick(Sender: TObject);
    procedure sbPathwayParamsClick(Sender: TObject);
    procedure edAssociativeRatioChange(Sender: TObject);
    procedure btnReinitializeClick(Sender: TObject);
    procedure pc1Change(Sender: TObject);
    procedure cbShowCogClick(Sender: TObject);
    procedure cbShowMotorClick(Sender: TObject);
    procedure btnWeightBoltzmannClick(Sender: TObject);
    procedure cbLearningTransferFunctionClick(Sender: TObject);
    procedure pc1DrawTab(Control: TCustomTabControl; TabIndex: Integer;
      const Rect: TRect; Active: Boolean);
    procedure edLearningTFMinChange(Sender: TObject);
    procedure edLearningTFMaxChange(Sender: TObject);
    procedure edLearningTFVhChange(Sender: TObject);
    procedure edLearningTFVcChange(Sender: TObject);
    procedure btnRobotClick(Sender: TObject);
    // Two function to deal with beginings and ends of remote sessions
    function startServer(): Boolean;
    function terminateServer(): Boolean;
  private
    { Private declarations }
    FTeachingVersion: boolean;
    //Store each structure as a 5 x 5 array. Position [0,0] is empty. The rest of
    //column 0 is the cognitive section, the rest of row 0 is the motor
    // section. The remaining 4 x 4 is for the associative (in those structures
    //that also have an associative structure)
    arCx: TStructureArray;
    arStr: TStructureArray;
    arStn: TStructureArray;
    arGpi: TStructureArray;
    arTh: TStructureArray;
//    FCurrentCell: double;

    //keep count of the number of failed runs and how they .
    //0 = too many loops on target selection
    //1 = too many loops on direction selection
    //2 = incorrect target chosen
    //3 = incorrect direction chosen
    //4 = all cortical activations less than zero
    arFail: array[0..4] of integer;
    arTarget: array[1..4] of TTarget;
    //only the corticostriatal connections have weights at present and each cortical cell can project
    //to each striatal cell
    arConnection: array[0..4, 0..4] of TConnection;
    //First index is the row
    arCogConnection: array[1..4, 1..4] of TConnection;
    //First index is the column
    arMotorConnection: array[1..4, 1..4] of TConnection;

    arDeltaW: array[0..4, 0..4] of real;
    arCogDeltaW: array[1..4, 1..4] of real;
    arMotorDeltaW: array[1..4, 1..4] of real;

    arDirectionShown: array[1..4] of boolean;
    arTrial: array of TTrial;
    arSimulation: array of TSimulation;
    arXlWorkbooks: array of variant;

    {
    arContext[1] = T1 & T2
    arContext[2] = T1 & T3
    arContext[3] = T1 & T4
    arContext[4] = T2 & T3
    arContext[5] = T2 & T4
    arContext[6] = T3 & T4
    The order of the targets is not important, so there are 6 possible combinations
    arContext keeps track of how many times each pairing has been presented
    arPairChoices holds a list of pair choices 1,2,3,4,5,6,1,2,3.....4,5,6,1,2
    arContextOutcome holds the totals of outcomes (success, optimum, reward) for
    each pair on each trial over all the runs of a simulation
    }
    arContext: array[1..6] of TContext;
    arPairChoices: array of integer;
    arContextOutcome: array of array of TOutcome; //6 by x array where x is number of runs in a simulation / 6

    psDirect: TParamStep;
    psHyper: TParamStep;

    FStage,
    iDirectionChosen,
    iTargetChosen,
    iTargetByDirection,
    iFlash,
    iFlashReward,
    FSimsPerExperiment,
    FRunsPerSimulation,
    FCurrentSimulation,
    FCurrentRun,
    FiMaxLoops,
    FiCogLoops,
    FiMotorLoops,
    FiLoops,
    FiLoopsBeforeTargets,
    FiLearningType,
    FTotalRewarded,
    FTotalOptimum,
    FCurrentContext,    //which of the 6 target pairs is being used in the current trial
    FiTargetCount,
    FiNumberOfPairs,
    FiPairAppearances       //how many times to use each of the 6 target pairs in a simulatiion
    : integer;

    eBaselineCogLevel,
    eBaselineMotorLevel,
    eBaselineAssociationLevel,
    eCxMotorToStrMotor,
    eCxMotorToStrAss,
    eCxCogToStrCog,
    eCxCogToStrAss,
    eCxAssToStrAss,
    eCxCogToStnCog,
    eCxMotorToStnMotor,
    eCxToTh,
    eStnMotorToGpiMotor,
    eStnMotorToGpiAss,
    eStnCogToGpiCog,
    eStnCogToGpiAss,
    eMotorGpiToMotorTh,
    eCogGpiToCogTh,
    eAssGPiToCogTh,
    eAssGPiToMotorTh,
    eCogStrToGpi,
    eMotorStrToGpi,
    eStrAssToGpi,
    eThMotorToCxMotor,
    eThCogToCxCog,
    eAssociativeRatio,
    eMSNThresholdLevel,
    FeSelectionThreshold,
    eValueLearningRate,
    eLTP,
    eLTD,
    eInitialWeight,
    eSdInitialWeight,
    eMinWeight,
    eMaxWeight,
    ePredictionError,
    eContextPE: double;

    bStop,
    bPause,
    bAutoSave,
    // Session started with a remote client
    bSessionStarted,
    bRobotConnected,
    bDirectionSent,
    bChosenMessage,
{
    bPersistentCorticalActivation,
    bPersistentStriatalActivation,
    bPersistentStnActivation,
}
    bUseCorticalThreshold,
{
    bIgnoreTonicStn,
    bIgnoreTonicGpi,
    bIgnoreTonicTh,
}
    FLoading,
//    bCorrectTargetChosen,
//    bCorrectDirectionChosen,
    bRobotAttached,
    bRewardGiven,
    FLearning,
    bShowWeights,
    FByTargetChosen,
    FStnTargetSaliency,
    bWInitialRandom,
    bUseTransferFuction,
    bUseLearningTransferFuction,
    bUseGpiAss,
    bCxToTh,
    bMultipleRuns,
    bShowGraphs: boolean;

    FXLApp,
    FXLWorkbook,
    FXLAveragesWorkbook,
    FXLGainsWorkbook,
    FXLWorksheet: OLEVariant;
//    FExcelFileName,
    FIniFileName,
    FExcelWorksheet,
    sSaveLocation: string;

    FBoltzmann,
    FWeightBoltzmann: TBoltzmann;

    FFormatSettings: TFormatSettings;

    CxDefaults: TStructureDefaults;
    StrDefaults: TStructureDefaults;
    StnDefaults: TStructureDefaults;
    GpiDefaults: TStructureDefaults;
    ThDefaults: TStructureDefaults;

    procedure ZeroArrays;
    procedure ZeroGridCells;
    procedure InitializeWeights;
    procedure DisplayWeights;
    procedure ClearGraphs;

    procedure GetParameters;
    procedure ShowTargetScreen;
    procedure MakeImagesVisible;
    procedure DisplayValues(Stage: integer);

    procedure DoALoop;
    procedure SetCognitiveStriatalActivation;
    procedure SetMotorStriatalActivation;
    procedure SetAssociativeStriatalActivation;

    procedure SetCognitiveStnActivation;
    procedure SetMotorStnActivation;

    function GetCognitiveStnActivation: double;
    function GetMotorStnActivation: double;

    procedure SetCognitiveGPiActivation;
    procedure SetMotorGpiActivation;
    procedure SetAssociativeGPiActivation;

    procedure SetCognitiveThalamusActivation;
    procedure SetMotorThalamusActivation;

    procedure SetCognitiveCortexActivation;
    procedure SetMotorCortexActivation;
    procedure SetAssociationCortexActivation;
    function CheckCorticalActivation(WhichPhase: integer): integer;

    function GetNoise(Mean, SD: double): double;

    procedure SetupPairings;
    procedure InitializeSimulation;
    procedure InitializeRun;

    //procedures for interacting with robot
    procedure SetTargets(AString: string);

    function ComputeContext(target1,target2:integer):integer;
    procedure GetTargets;
    function SelectTarget(delayDisplay: boolean = false): integer;
    function SelectDirection(delayDisplay: boolean = false): integer;
    function VerifyDirection: boolean;

    function StartExcel: boolean;
    procedure CreateAveragesWorkbook;
    procedure CreateDirectVHyperWorkbook;
    procedure SaveDirectVHyperWorkbook;
    procedure CreateExcelWorkbook;
    procedure SetSaveLocation;
    function DateAsString: string;
    procedure SaveExcelWorkbook;
    procedure SendPairingProbabilitiesToExcel;
    function AddWorksheet(RunNumber: integer): boolean;
    procedure WriteXLCell(AWorksheet: OleVariant; Row, Col: integer; Data: string); overload;
    procedure WriteXLCell(AWorksheet: OleVariant; Row, Col: integer; Data: integer); overload;
    procedure WriteXLCell(AWorksheet: OleVariant; Row, Col: integer; Data: real); overload;
    procedure WriteXLCell(AWorksheet: OleVariant; Row, Col: integer; Data: boolean); overload;
//    function ReadXLCellString(AWorksheet: OleVariant; Row, Col: integer): string;
    function ReadXLCellInteger(AWorksheet: OleVariant; Row, Col: integer): integer;
    function ReadXLCellReal(AWorksheet: OleVariant; Row, Col: integer): real;
//    function ReadXLCellBoolean(AWorksheet: OleVariant; Row, Col: integer): boolean;
    procedure SendSetupToExcel(IsAverages: boolean);
    procedure SendWeightsSummaryToExcel;
    procedure SendTrialsSummaryToExcel;
    procedure SendPairingsSummaryToExcel;
    procedure SendAveragesSummaryToExcel;
    procedure SendAverageLoopsFormulaeToExcel(AWorksheet: OleVariant);
    procedure SendAveragePairingsToExcel;
    procedure SendTrialBatchResultToExcel(SendProbability: boolean);
    procedure SendTrialResultToExcel;
    procedure SendTrialToExcel(SheetName: string;
                                      WhichArray: TStructureArray;
                                      SendAssociation: boolean);
{
    procedure SendStageDataToExcel(SheetName: string;
                                      WhichArray: TStructureArray;
                                      SendAssociation: boolean);
}                                      
    procedure SendTrialResultToTrialsWorksheet(SendProbability: boolean);
    procedure SendTrialResultToEffectiveProbabilityWorksheet;
    procedure SendTrialResultToPairingsWorksheet;
    procedure SendTrialResultToAveragesWorkbook;
    procedure SendWeightsToExcel;
    procedure SendDeltaWeightsToExcel;
    procedure WriteDirectGainToExcel(Column: integer; Gain: real);
    procedure WriteHyperGainToExcel(Row: integer; Gain: real);
    procedure WriteGainDataToExcel(Row, Column: integer);

    procedure CheckOptimum;
    procedure UpdateWeights(RewardValue: integer; Method: integer);

    function CheckChoices: integer;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses Math, uShowBoltzmann, IdThreadMgr;

{$R *.dfm}

const
  NO_YES: array[0..1] of string = ('No','Yes');

  ROW_MOTOR = 0;
  ROW_ASSOCIATIVE1 = 1;
  ROW_ASSOCIATIVE2 = 2;
  ROW_ASSOCIATIVE3 = 3;
  ROW_ASSOCIATIVE4 = 4;
  COL_COGNITIVE = 0;
  COL_ASSOCIATIVE1 = 1;
  COL_ASSOCIATIVE2 = 2;
  COL_ASSOCIATIVE3 = 3;
  COL_ASSOCIATIVE4 = 4;

  CX_FR_TO_STR_COG = 0;
  CX_FR_TO_STR_ASS = 1;
  CX_FR_TO_STN_COG = 2;
  CX_FR_TO_STN_ASS = 3;
  CX_MOTOR_TO_STR_MOTOR = 4;
  CX_MOTOR_TO_STR_ASS = 5;
  CX_MOTOR_TO_STN_MOTOR = 6;
  CX_MOTOR_TO_STN_ASS = 7;

  xlWBATWorksheet = -4167;
  // Emplacements of template excel files, relative to program path
  // No need for the first backslash (see ExtractFilePath doc)
  xlTEMPLATE = 'Results\Template1.xls';
  xlTEMPLATE_AVERAGES = 'Results\Template_Averages1.xls';
  xlTEMPLATE_DIRECT_V_HYPER = 'Results\Template_DirectVHyperdirect.xls';

  NEW_FILE = '<New file>';
  NEW_SHEET = '<New sheet>';
  SETUP = 'Setup';
  ROW_HEADER = 1;

  //constants for trial failures
  TARGET_LOOPS = 0;
  DIRECTION_LOOPS = 1;
  TARGET_CHOSEN = 2;
  DIRECTION_CHOSEN = 3;
  ACTIVATIONS = 4;

  NO_TARGET = 0;
  NO_DIRECTION = 0;

  CharSet: array [1..104] of string = ('A','B','C','D','E','F','G','H','I','J','K','L',
                                     'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                                     'AA','AB','AC','AD','AE','AF','AG','AH','AI',
                                     'AJ','AK','AL','AM','AN','AO','AP','AQ','AR',
                                     'AS','AT','AU','AV','AW','AX','AY','AZ',
                                     'BA','BB','BC','BD','BE','BF','BG','BH','BI',
                                     'BJ','BK','BL','BM','BN','BO','BP','BQ','BR',
                                     'BS','BT','BU','BV','BW','BX','BY','BZ',
                                     'CA','CB','CC','CD','CE','CF','CG','CH','CI',
                                     'CJ','CK','CL','CM','CN','CO','CP','CQ','CR',
                                     'CS','CT','CU','CV','CW','CX','CY','CZ');

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  FBoltzmann := TBoltzmann.Create;
  FBoltzmann.OutputPositiveOnly := true;
  FWeightBoltzmann := TBoltzmann.Create;
  FWeightBoltzmann.OutputPositiveOnly := true;
end;

procedure TfrmMain.FormShow(Sender: TObject);
var i: integer;
begin
  // CoInitialize(nil);
  Randomize;
  GetLocaleFormatSettings(0, FFormatSettings);
  FFormatSettings.DecimalSeparator := '.';
  bRobotconnected := false;
{
  try
    IdTCPServer1.Active := true;
    bSessionStarted := false;
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
}
  FIniFileName := ExtractFilePath(Application.ExeName)+'default.ini';
  GetParameters;

  //show the bitmaps next to the cogitive cortical cells
  for i := 1 to 4 do begin
    ilTargets.GetBitmap(i-1, TImage(FindComponent('imT' + IntToStr(i))).Picture.Bitmap);
    TImage(FindComponent('imT' + IntToStr(i))).Visible := false;
    ilTargets.GetBitmap(i-1, TImage(FindComponent('imConfigT' + IntToStr(i))).Picture.Bitmap);
    ilTargets.GetBitmap(i-1, TImage(FindComponent('imTV' + IntToStr(i))).Picture.Bitmap);
    ilTargets.GetBitmap(i-1, TImage(FindComponent('imT2_' + IntToStr(i))).Picture.Bitmap);
  end;

  {
  show the bitmaps for each context
  For 4 targets:
  arContext[1] = T1 & T2
  arContext[2] = T1 & T3
  arContext[3] = T1 & T4
  arContext[4] = T2 & T3
  arContext[5] = T2 & T4
  arContext[6] = T3 & T4
  }
  ilTargets.GetBitmap(0, imC1T1.Picture.Bitmap);
  ilTargets.GetBitmap(1, imC1T2.Picture.Bitmap);
  ilTargets.GetBitmap(0, imC2T1.Picture.Bitmap);
  ilTargets.GetBitmap(2, imC2T2.Picture.Bitmap);
  ilTargets.GetBitmap(0, imC3T1.Picture.Bitmap);
  ilTargets.GetBitmap(3, imC3T2.Picture.Bitmap);
  ilTargets.GetBitmap(1, imC4T1.Picture.Bitmap);
  ilTargets.GetBitmap(2, imC4T2.Picture.Bitmap);
  ilTargets.GetBitmap(1, imC5T1.Picture.Bitmap);
  ilTargets.GetBitmap(3, imC5T2.Picture.Bitmap);
  ilTargets.GetBitmap(2, imC6T1.Picture.Bitmap);
  ilTargets.GetBitmap(3, imC6T2.Picture.Bitmap);

  InitializeWeights;
  ZeroGridCells;

  bChosenMessage := false;
  pc1.ActivePage := tsSim;
  bMultipleRuns := false;

  pc1.OwnerDraw := true;
  pc2.OwnerDraw := true;

  //show the shapes round the target directions the same colors as in the graphs for ease of identification
  shpMotor1.Pen.Color := srsCxMotor1.Pen.Color;
  shpMotor2.Pen.Color := srsCxMotor2.Pen.Color;
  shpMotor3.Pen.Color := srsCxMotor3.Pen.Color;
  shpMotor4.Pen.Color := srsCxMotor4.Pen.Color;

  shpColorCog1.Brush.Color := srsCxCog1.Pen.Color;
  shpColorCog2.Brush.Color := srsCxCog2.Pen.Color;
  shpColorCog3.Brush.Color := srsCxCog3.Pen.Color;
  shpColorCog4.Brush.Color := srsCxCog4.Pen.Color;
  shpColorMotor1.Brush.Color := srsCxMotor1.Pen.Color;
  shpColorMotor2.Brush.Color := srsCxMotor2.Pen.Color;
  shpColorMotor3.Brush.Color := srsCxMotor3.Pen.Color;
  shpColorMotor4.Brush.Color := srsCxMotor4.Pen.Color;
end;

procedure TfrmMain.pc1DrawTab(Control: TCustomTabControl;
  TabIndex: Integer; const Rect: TRect; Active: Boolean);
begin
  Control.Canvas.TextOut(Rect.left+5,Rect.top+3,TPageControl(Control).Pages[TabIndex].Caption);
  pc1.Pages[TabIndex].Brush.Color := clWhite;
end;

procedure TfrmMain.ZeroArrays;
var i,j: integer;
begin
  for i := 0 to 4 do begin
    for j := 0 to 4 do begin
      {
      Calculate the noise as a gaussian with mean = 0  and SD a proportion of tonic firing rate.
      Add 1 to the SD for when the tonic firing rate = 0
      }
      arCx[i,j] := Abs(GetNoise(0, CxDefaults.NoiseProportion));
      arStr[i,j] := Abs(GetNoise(0, StrDefaults.NoiseProportion));
      arStn[i,j] := Abs(GetNoise(0, StnDefaults.NoiseProportion));
      arGpi[i,j] := Abs(GetNoise(0, GpiDefaults.NoiseProportion));
      arTh[i,j] := Abs(GetNoise(0, ThDefaults.NoiseProportion));
    end;
  end;
end;

procedure TfrmMain.ZeroGridCells;
var i,j: integer;
    eActivation: real;
begin
  for i := 0 to 3 do begin
    if CxDefaults.Threshold > 0 then
      eActivation := 0
    else
      eActivation := -1 * CxDefaults.Threshold;
    grdCxMotor.Cells[i,0] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);
    grdCxCog.Cells[0,i] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);
    for j := 0 to 3 do
      grdCxAss.Cells[i,j] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);
    grdCogWeights.Cells[0,i] := FloatToStrF(arConnection[COL_COGNITIVE, i+1].Normalized, ffFixed, 3, 2, FFormatSettings);
    grdMotorWeights.Cells[i,0] := FloatToStrF(arConnection[i+1, ROW_MOTOR].Normalized, ffFixed, 3, 2, FFormatSettings);
    grdCogDeltaW.Cells[0,i] := FloatToStrF(arDeltaW[COL_COGNITIVE, i+1], ffFixed, 3, 2, FFormatSettings);
    grdMotorDeltaW.Cells[i,0] := FloatToStrF(arDeltaW[i+1, ROW_MOTOR], ffFixed, 3, 2, FFormatSettings);

    grdTargetValue.Cells[0,i] := FloatToStrF(arTarget[i+1].ExpectedValue, ffFixed, 3, 2, FFormatSettings);
    if StrDefaults.Threshold > 0 then
      eActivation := 0
    else
      eActivation := -1 * StrDefaults.Threshold;
    grdStrMotor.Cells[i,0] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);
    grdStrCog.Cells[0,i] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);
    for j := 0 to 3 do begin
      grdStrAss.Cells[i,j] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);
      grdStrRow.Cells[0,i] := FloatToStrF(5 * eActivation, ffFixed, 3, 2, FFormatSettings);
      grdStrCol.Cells[i,0] := FloatToStrF(5 * eActivation, ffFixed, 3, 2, FFormatSettings);
    end;

    if StnDefaults.Threshold > 0 then
      eActivation := 0
    else
      eActivation := -1 * StnDefaults.Threshold;
    grdStnMotor.Cells[i,0] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);
    grdStnCog.Cells[0,i] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);

    if GpiDefaults.Threshold > 0 then
      eActivation := 0
    else
      eActivation := -1 * GpiDefaults.Threshold;
    grdGpiMotor.Cells[i,0] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);
    grdGpiCog.Cells[0,i] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);
    for j := 0 to 3 do begin
      grdGpiAss.Cells[i,j] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);
      grdGpiRow.Cells[0,i] := FloatToStrF(5 * eActivation, ffFixed, 3, 2, FFormatSettings);
      grdGpiCol.Cells[i,0] := FloatToStrF(5 * eActivation, ffFixed, 3, 2, FFormatSettings);
    end;

    if ThDefaults.Threshold > 0 then
      eActivation := 0
    else
      eActivation := -1 * ThDefaults.Threshold;
    grdThMotor.Cells[i,0] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);
    grdThCog.Cells[0,i] := FloatToStrF(eActivation, ffFixed, 3, 2, FFormatSettings);

    for j := 0 to 3 do begin
      case rgAssWeights.ItemIndex of
        0: begin
          grdAssWeights.Cells[i,j] := FloatToStrF(arCogConnection[i+1, j+1].Normalized, ffFixed, 3, 2, FFormatSettings);
          grdAssDeltaW.Cells[i,j] := FloatToStrF(arCogDeltaW[i+1, j+1], ffFixed, 3, 2, FFormatSettings);
        end;
        1: begin
          grdAssWeights.Cells[i,j] := FloatToStrF(arMotorConnection[i+1, j+1].Normalized, ffFixed, 3, 2, FFormatSettings);
          grdAssDeltaW.Cells[i,j] := FloatToStrF(arMotorDeltaW[i+1, j+1], ffFixed, 3, 2, FFormatSettings);
        end;
        2: begin
          grdAssWeights.Cells[i,j] := FloatToStrF(arConnection[i+1, j+1].Normalized, ffFixed, 3, 2, FFormatSettings);
          grdAssDeltaW.Cells[i,j] := FloatToStrF(arDeltaW[i+1, j+1], ffFixed, 3, 2, FFormatSettings);
        end;
      end;
    end;
  end;
  for i := 0 to 5 do
    grdContextValues.Cells[0,i] := FloatToStr(arContext[i+1].ExpectedValue, FFormatSettings);
end;

{
Corticostriatal weights.
All weights start at an initial weight (usually 0.5) and can increase to a maximum of
eMaxWeight or decrease to a minimum of eMinWeight during learning
}
procedure TfrmMain.InitializeWeights;
var i,j: integer;
begin
  for i := 1 to 4 do begin
    //cognitive and motor weights
    if bWInitialRandom then begin
      arConnection[COL_COGNITIVE, i].Real := GetNoise(eInitialWeight, eSdInitialWeight);
      arConnection[COL_COGNITIVE, i].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arConnection[COL_COGNITIVE, i].Real);
      arConnection[i, ROW_MOTOR].Real := GetNoise(eInitialWeight, eSdInitialWeight);
      arConnection[i, ROW_MOTOR].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arConnection[i, ROW_MOTOR].Real);
    end;
    arDeltaW[COL_COGNITIVE, i] := 0;
    arDeltaW[i, ROW_MOTOR] := 0;
  end;
  for i := 1 to 4 do begin
    for j := 1 to 4 do begin
      //weights to associtative striatum from cognitive, motor and associational cortex
      if bWInitialRandom then begin
        arConnection[i, j].Real := GetNoise(eInitialWeight, eSdInitialWeight);
        arConnection[i, j].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arConnection[i, j].Real);
        arCogConnection[i, j].Real := GetNoise(eInitialWeight, eSdInitialWeight);
        arCogConnection[i, j].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arCogConnection[i, j].Real);
        arMotorConnection[i, j].Real := GetNoise(eInitialWeight, eSdInitialWeight);
        arMotorConnection[i, j].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arMotorConnection[i, j].Real);
      end
      else begin
        arConnection[i, j].Real := eInitialWeight;
        arConnection[i, j].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arConnection[i, j].Real);
        arCogConnection[i, j].Real := eInitialWeight;
        arCogConnection[i, j].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arCogConnection[i, j].Real);
        arMotorConnection[i, j].Real := eInitialWeight;
        arMotorConnection[i, j].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arMotorConnection[i, j].Real);
      end;
      arDeltaW[i,j] := 0;
      arCogDeltaW[i,j] := 0;
      arMotorDeltaW[i,j] := 0;
    end;
  end;
  lblPE.Caption := '0';

  for i := 0 to 4 do
    arFail[i] := 0;

  //reset the counters that keep a running tally of the the actual target
  //reward probability based on how many times it has been picked and rewarded
  for i := 1 to 4 do begin
    arTarget[i].PresentedCount := 0;
    arTarget[i].PickedCount := 0;
    arTarget[i].RewardedCount := 0;
    arTarget[i].EffectiveProbability := 0;
    arTarget[i].ExpectedValue := 0.5;
  end;

  //reset the counters
  FTotalOptimum := 0;
  FTotalRewarded := 0;
  FCurrentRun := 1;
  SetLength(arSimulation, FRunsPerSimulation);
  SetLength(arTrial, FRunsPerSimulation);
  for i := 1 to FiNumberOfPairs do begin
    //arContext is 1 based
    arContext[i].ChosenCount := 0;
    arContext[i].ExpectedValue := 0.5;
  end;
end;

{
FIXME: step by step is broken, SelectTarget is not used anymore for example.
  Have to look carefully at the code for each of these buttons.
FIXME: for instance "InitializeRun" seems to fit here, instead of just FiLoops := 0

First off just put values into 2 cells of the cognitive column and
4 associative cells of coritcal matrix
}
procedure TfrmMain.btnTargetClick(Sender: TObject);
begin
  bMultipleRuns := false;
  memo1.Lines.Add('-----------------');
  GetTargets;
  ShowTargetScreen;
  ClearGraphs;
  FiLoops := 0;
  DisplayValues(1);
  iFlash := 0;
  FStage := 1;
  lblLoops.Caption := '0';
  Timer1.Enabled := true;
end;

procedure TfrmMain.btnDecisionPhaseClick(Sender: TObject);
begin
  MakeImagesVisible;
  SelectTarget;
  iFlash := 0;
  FStage := 2;
  Timer1.Enabled := true;
end;

procedure TfrmMain.btnMovmentPhaseClick(Sender: TObject);
begin
  MakeImagesVisible;
  SelectDirection;
  iFlash := 0;
  FStage := 3;
  Timer1.Enabled := true;
end;

//Perform all the steps of one trial
procedure TfrmMain.btnOneTrialClick(Sender: TObject);
var iReward: integer;
begin
  memo1.Lines.Clear;
  bMultipleRuns := false;
  InitializeRun;
  GetTargets;
  DisplayValues(1);
  ShowTargetScreen;
  if SelectDirection = 0 then begin;
    CheckOptimum;
    iReward := CheckChoices;
    UpdateWeights(iReward,1);

    iFlash := 0;
    FStage := 3;
    Timer1.Enabled := true;
  end;
  DisplayValues(3);
end;

//Perform a set of simulations and send the results to Excel
procedure TfrmMain.btnMultipleTrialsClick(Sender: TObject);
var iRun,
    iSim,
    iReward: integer;
begin
  if edSaveDirectory.Text = '' then begin
    MessageDlg('Set save directory before starting simulations', mtError,[mbOK],0);
    EXIT;
  end;
  if edFileName.Text = '' then begin
    MessageDlg('Set base file name before starting simulations', mtError,[mbOK],0);
    EXIT;
  end;
  bMultipleRuns := true;
  FCurrentSimulation := 0;
  InitializeSimulation;
  bStop := false;
  bPause := false;
  if StartExcel then try
    SetSaveLocation;
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    CreateAveragesWorkbook;
    for iSim := 1 to FSimsPerExperiment do begin
      SetupPairings;
      inc(FCurrentSimulation);
      lblCurrentSim.Caption := IntToStr(FCurrentSimulation);
      CreateExcelWorkbook;
      InitializeWeights;
      for iRun := 1 to FRunsPerSimulation do begin
        FCurrentRun := iRun;
        InitializeRun;
        if AddWorkSheet(FCurrentRun) then begin
          GetTargets;
          ShowTargetScreen;
          if SelectDirection = 0 then begin;
            if VerifyDirection then begin
              CheckOptimum;
              iReward := CheckChoices;
              UpdateWeights(iReward,1);
            end;
          end;
          SendTrialBatchResultToExcel(true);
        end;
        DisplayValues(3);
        if bStop then
          BREAK;
        while bPause do
          Application.ProcessMessages;
      end;
      SendSetupToExcel(false);
      SendTrialsSummaryToExcel;
      SendWeightsSummaryToExcel;
      SendPairingsSummaryToExcel;
      lblCurrentRun2.Caption := 'Finished';
      if bStop then
        BREAK;
      SaveExcelWorkbook;
    end;
    if FSimsPerExperiment > 1 then begin
      SendSetupToExcel(true);
      SendAveragesSummaryToExcel;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  bMultipleRuns := false;
end;

{
FIXME: factorize with btnMultipleTrialsClick

Do multiple steps of gain in direct and hyperdirect pathways
}
procedure TfrmMain.sbPathwayParamsClick(Sender: TObject);
var iRun,
    iReward,
    iRow,
    iCol: integer;
    eOldDirectGain,
    eOldHyperGain: real;
    iOldSimsPerExperiment: integer;
    sOldSaveLocation,
    sDir,
    sCurrentStep: string;
begin
  if edSaveDirectory.Text = '' then begin
    MessageDlg('Set save directory before starting simulations', mtError,[mbOK],0);
    EXIT;
  end;

  // init variables
  eOldDirectGain := -1;
  eOldHyperGain := -1;
  iOldSimsPerExperiment := -1;

  psDirect.Start := StrToFloat(edGainDirectStartValue.Text);
  psDirect.Stop := StrToFloat(edGainDirectEndValue.Text);
  psDirect.Step := StrToFloat(edGainDirectStep.Text);
  psHyper.Start := StrToFloat(edGainHyperStartValue.Text);
  psHyper.Stop := StrToFloat(edGainHyperEndValue.Text);
  psHyper.Step := StrToFloat(edGainHyperStep.Text);

  //start columns and rows in DirectVHyperdirect.xls to save data to
  iCol := 2;

  bStop := false;
  bPause := false;
  bMultipleRuns := true;
  if StartExcel then try
    eOldDirectGain := eCxMotorToStrMotor;
    eOldHyperGain := eCxMotorToStnMotor;
    iOldSimsPerExperiment := FSimsPerExperiment;
    FSimsPerExperiment := 1;
    sOldSaveLocation := sSaveLocation;
    sDir := ExtractFilePath(Application.ExeName)+'Results\'+edSaveDirectory.Text + '\';
    sSaveLocation := sDir;
    CreateDirectVHyperWorkbook;

    eCxMotorToStrMotor := psDirect.Start;
    eCxCogToStrCog := psDirect.Start;
    eCxAssToStrAss := psDirect.Start;
    eCxMotorToStrAss := psDirect.Start * eAssociativeRatio;
    eCxCogToStrAss := psDirect.Start * eAssociativeRatio;
    repeat
      WriteDirectGainToExcel(iCol, eCxCogToStrCog);
      eCxMotorToStnMotor := psHyper.Start;
      eCxCogToStnCog := psHyper.Start;
      lblDirectGain.Caption := FloatToStrF(eCxCogToStrCog, ffFixed,3,1);
      iRow := 3;
      repeat
        lblHyperGain.Caption := FloatToStrF(eCxCogToStnCog, ffFixed,3,1);
        if eCxMotorToStrMotor = psDirect.Start then
          //only do it the first time round
          WriteHyperGainToExcel(iRow, eCxCogToStnCog);
        sCurrentStep := 'Direct' + FloatToStrF(eCxCogToStrCog, ffFixed,3,1) + '_Hyper' + FloatToStrF(eCxCogToStnCog, ffFixed,3,1);
        sSaveLocation := sDir + sCurrentStep;
        FCurrentSimulation := 0;
        InitializeSimulation;
        SetupPairings;
        inc(FCurrentSimulation);
        lblCurrentSim.Caption := IntToStr(FCurrentSimulation);
        CreateExcelWorkbook;
        InitializeWeights;
        for iRun := 1 to FRunsPerSimulation do begin
          FCurrentRun := iRun;
          InitializeRun;
          GetTargets;
          ShowTargetScreen;
          if SelectDirection = 0 then begin;
            if VerifyDirection then begin
              CheckOptimum;
              iReward := CheckChoices;
              UpdateWeights(iReward,1);
            end;
          end;
          // TODO: use SendTrialBatchResultToExcel if SendTrialResultToAveragesWorkbook can apply here
          SendTrialResultToExcel;
          SendTrialResultToTrialsWorksheet(true);
          SendTrialResultToEffectiveProbabilityWorksheet;
          SendTrialResultToPairingsWorksheet;
          SendWeightsToExcel;
          SendDeltaWeightsToExcel;
        end;
        DisplayValues(3);
        SendSetupToExcel(false);
        SendTrialsSummaryToExcel;
        SendWeightsSummaryToExcel;
        SendPairingsSummaryToExcel;
        WriteGainDataToExcel(iRow, iCol);
        SaveExcelWorkbook;
        eCxMotorToStnMotor := eCxMotorToStnMotor + psHyper.Step;
        eCxCogToStnCog := eCxCogToStnCog + psHyper.Step;
        inc(iRow);
      until eCxMotorToStnMotor > (psHyper.Stop + 0.01);
      eCxMotorToStrMotor := eCxMotorToStrMotor + psDirect.Step;
      eCxCogToStrCog := eCxCogToStrCog + psDirect.Step;
      eCxAssToStrAss := eCxAssToStrAss + psDirect.Step;
      eCxMotorToStrAss := eCxMotorToStrMotor * eAssociativeRatio;
      eCxCogToStrAss := eCxCogToStrCog * eAssociativeRatio;
      inc(iCol);
    until eCxMotorToStrMotor > (psDirect.Stop + 0.01);
  finally
    Screen.Cursor := crDefault;
    eCxMotorToStrMotor := eOldDirectGain;
    eCxMotorToStnMotor := eOldHyperGain;
    eCxCogToStrCog := eOldDirectGain;
    eCxCogToStnCog := eOldHyperGain;
    eCxAssToStrAss := eOldDirectGain;
    FSimsPerExperiment := iOldSimsPerExperiment;
    sSaveLocation := sOldSaveLocation;

    eCxMotorToStrAss := eCxMotorToStrMotor * eAssociativeRatio;
    eCxCogToStrAss := eCxCogToStrCog * eAssociativeRatio;
    SaveDirectVHyperWorkbook;
    lblCurrentRun2.Caption := 'Finished';
  end;
  bMultipleRuns := false;
end;

{
At the start of each parameter exploration of the hyperdirect pathway, put the value of the direct
pathway gain in the correct Excel column
}
procedure TfrmMain.WriteDirectGainToExcel(Column: integer; Gain: real);
begin
  FXLWorksheet := FXLGainsWorkbook.Worksheets['Successful'];
  WriteXLCell(FXLWorksheet,2,Column,Gain);
  FXLWorksheet := FXLGainsWorkbook.Worksheets['Loops'];
  WriteXLCell(FXLWorksheet,2,Column,Gain);
end;

{
For each new gain value of the hyperdirect pathway, put the gain in the correct Excel row
}
procedure TfrmMain.WriteHyperGainToExcel(Row: integer; Gain: real);
begin
  FXLWorksheet := FXLGainsWorkbook.Worksheets['Successful'];
  WriteXLCell(FXLWorksheet,Row,1,Gain);
  FXLWorksheet := FXLGainsWorkbook.Worksheets['Loops'];
  WriteXLCell(FXLWorksheet,Row,1,Gain);
end;

{
when doing the parameter exploration of direct vs hyperdirect pathway, write the number of successful
trials in a simulation and the average number of loops to the DirectVHyperdirect workbook
}
procedure TfrmMain.WriteGainDataToExcel(Row, Column: integer);
var eSuccessful,
    eLoops: real;
begin
  FXLWorksheet := FXLWorkbook.Worksheets['Trials'];
  eSuccessful := ReadXLCellReal(FXLWorksheet, FRunsPerSimulation + 3, 11);
  eLoops := ReadXLCellReal(FXLWorksheet, FRunsPerSimulation + 3, 14);

  FXLWorksheet := FXLGainsWorkbook.Worksheets['Successful'];
  WriteXLCell(FXLWorksheet,Row,Column,eSuccessful);

  FXLWorksheet := FXLGainsWorkbook.Worksheets['Loops'];
  WriteXLCell(FXLWorksheet,Row,Column,eLoops);
end;

{
A pair of targets is considered as a context in this task
The number of possible pairs of targets depends on the number of targets to use in the simulations!
Set up an array of pairs of targets so that information about learning of the context can be saved
to Excel
}
procedure TfrmMain.SetupPairings;
var i: integer;
begin
  //each pairing appears 1/6th of the time if there are 4 targets. Set the array 1 longer for run
  //counts that are not divisible by 6
  FiTargetCount := 0;
  for i := 1 to 4 do begin
    if arTarget[i].Use then
      inc(FiTargetCount);
  end;
  case FiTargetCount of
    2: FiNumberOfPairs := 1;
    3: FiNumberOfPairs := 3;
    4: FiNumberOfPairs := 6;
  end;

  {
  create a list of pair choices 1,2,3,4,5,6,1,2,3.....4,5,6,1,2
  these are chosen randomly to pick targets and assure that each pair gets used
  about the same number of times
  }
  SetLength(arPairChoices, FRunsPerSimulation);
  for i := 0 to pred(FRunsPerSimulation) do
    arPairChoices[i] := (i mod FiNumberOfPairs) + 1;
  for i := 1 to FiNumberOfPairs do
    //arContext is 1 based
    //set the number of times that this context has been used in this simulaton to zero
    arContext[i].ChosenCount := 0;
end;

procedure TfrmMain.InitializeSimulation;
var i,j: integer;
begin
  for i := 0 to pred(FRunsPerSimulation) do begin
    arSimulation[i].Successful := 0;
    arSimulation[i].Optimum := 0;
    arSimulation[i].Rewarded := 0;
  end;
  //one XL workbook for each simulation
  SetLength(arXlWorkbooks, FSimsPerExperiment);

  FiPairAppearances := (FRunsPerSimulation div FiNumberOfPairs) + 1;
  SetLength(arContextOutcome, FiNumberOfPairs, FiPairAppearances);

  //Reset the number of times each target pairing has been seen
  for i := 1 to FiNumberOfPairs do begin
    for j := 0 to pred(FiPairAppearances) do begin
      arContextOutcome[i-1,j].Successful := 0;
      arContextOutcome[i-1,j].Optimum := 0;
      arContextOutcome[i-1,j].Rewarded := 0;
      arContextOutcome[i-1,j].Value :=0;
      arContextOutcome[i-1,j].PE := 0;
    end;
  end;
end;

procedure TfrmMain.InitializeRun;
var i: integer;
begin
  ClearGraphs;
  lblCurrentRun1.Caption := IntToStr(FCurrentRun);
  lblCurrentRun2.Caption := IntToStr(FCurrentRun);
  Application.ProcessMessages;
  arTrial[FCurrentRun-1].Successful := false;
  arTrial[FCurrentRun-1].Optimum := false;
  arTrial[FCurrentRun-1].Rewarded := false;
  iTargetChosen := NO_TARGET; //global variable
  iDirectionChosen := NO_DIRECTION;  //global variable. set this here in case no target is ever chosen
  iTargetByDirection := NO_TARGET;
  FiLoops := 0;
  FiCogLoops := 0;
  FiMotorLoops := 0;
  lblLoops.Caption := '0';
  imHappy.Visible := false;
  imSad.Visible := false;
  imHappy2.Visible := false;
  imSad2.Visible := false;
  shpChosen1.Visible := false;
  shpChosen2.Visible := false;
  for i := 1 to 4 do begin
    TShape(FindComponent('shpLegendCog' + IntToStr(i))).Visible := false;
    TShape(FindComponent('shpLegendMotor' + IntToStr(i))).Visible := false;
  end;
  bPause := false;
  bStop := false;
end;

{
  Return which context is corresponding to theese two targets
  (used in GetTargets and SetTargets to set FCurrentContext)

    For 4 targets:
    arContext[1] = T1 & T2
    arContext[2] = T1 & T3
    arContext[3] = T1 & T4
    arContext[4] = T2 & T3
    arContext[5] = T2 & T4
    arContext[6] = T3 & T4
}
function TfrmMain.ComputeContext(target1,target2:integer):integer;
begin
  result := -1;
  case target1 of
   1: begin
      case target2 of
        2: result := 1;
        3: result := 2;
        4: result := 3;
      end;
    end;
    2: begin
      case target2 of
        1: result := 1;
        3: result := 4;
        4: result := 5;
      end;
    end;
    3: begin
      case target2 of
        1: result := 2;
        2: result := 4;
        4: result := 6;
      end;
    end;
    4: begin
      case target2 of
        1: result := 3;
        2: result := 5;
        3: result := 6;
      end;
    end;
  end;
end;

//Choose two targets in two positions and display them along with their values
procedure TfrmMain.GetTargets;
var i, j,
    iPair,
    iChoicesLeft,
    iTarget1,
    iTarget2: integer;
    arTargetNumbers: array of integer;
begin
  // init variables
  iTarget1 := -1;
  iTarget2 := -1;

  ZeroArrays;
  ZeroGridCells;

  SetLength(arTargetNumbers, FiTargetCount);
  j := 0;
  //arTarget is a 1 based array
  for i := 1 to 4 do begin
    arTarget[i].Direction := -1;
    arTarget[i].Show := false;
    arTarget[i].Correct := false;
    arTarget[i].ChosenAsDirection := false;
    arTarget[i].ChosenAsTarget := false;
    if arTarget[i].Use then begin
      //We need to work out which targets have been selected as this changes which pairs
      //are used when there are less than 4 targets
      arTargetNumbers[j] := i;
      inc(j);
    end;
    arDirectionShown[i] := false;
    TImage(FindComponent('imT' + IntToStr(i))).Visible := false;
    TShape(FindComponent('shpCog' + IntToStr(i))).Visible := false;
    TShape(FindComponent('shpMotor' + IntToStr(i))).Visible := false;
  end;

  if bMultipleRuns then begin
    {
    For 4 targets:
    arContext[1] = T1 & T2
    arContext[2] = T1 & T3
    arContext[3] = T1 & T4
    arContext[4] = T2 & T3
    arContext[5] = T2 & T4
    arContext[6] = T3 & T4
    The order of the targets is not important, so with 4 targets there are 6 possible combinations
    Work out which is the current pairing so the outcome for that pairing can be
    shown in the correct columns in the pairings sheet in Excel

    arPairChoices holds all the possible pair combinations left to be chosen from
    }
    iChoicesLeft := length(arPairChoices);
    iPair := RandomRange(0, iChoicesLeft);
    FCurrentContext := arPairChoices[iPair];
    arContext[FCurrentContext].ChosenCount := arContext[FCurrentContext].ChosenCount + 1;
    case FiNumberOfPairs of
      1: begin
        //with 2 targets, only one pair
        iTarget1 := arTargetNumbers[0];
        iTarget2 := arTargetNumbers[1];
      end;
      3: begin
        //with 3 targets, 3 pairs
        case FCurrentContext of
          1: begin
            iTarget1 := arTargetNumbers[0];
            iTarget2 := arTargetNumbers[1];
          end;
          2: begin
            iTarget1 := arTargetNumbers[0];
            iTarget2 := arTargetNumbers[2];
          end;
          3: begin
            iTarget1 := arTargetNumbers[1];
            iTarget2 := arTargetNumbers[2];
          end;
        end;
      end;
      6: begin
        //with 4 targets, 6 pairs
          case FCurrentContext of
            1: begin
              iTarget1 := arTargetNumbers[0];
              iTarget2 := arTargetNumbers[1];
            end;
            2: begin
              iTarget1 := arTargetNumbers[0];
              iTarget2 := arTargetNumbers[2];
            end;
            3: begin
              iTarget1 := arTargetNumbers[0];
              iTarget2 := arTargetNumbers[3];
            end;
            4: begin
              iTarget1 := arTargetNumbers[1];
              iTarget2 := arTargetNumbers[2];
            end;
            5: begin
              iTarget1 := arTargetNumbers[1];
              iTarget2 := arTargetNumbers[3];
            end;
            6: begin
              iTarget1 := arTargetNumbers[2];
              iTarget2 := arTargetNumbers[3];
            end;
          end;
        end;
      end;

    //then remove that pair from the list
    for i := iPair to iChoicesLeft - 2 do begin
      arPairChoices[i] := arPairChoices[i+1];
    end;
    SetLength(arPairChoices, iChoicesLeft - 1);
  end
  else begin
    {
    Just doing one run, so no need to draw a target pairing from the arPairChoices array
    to balance the number of pairs seen.
    Two of the targets are displayed and the probablility of the target is shown
    in the corresponding cognitive cortical cell, the other two cells remaining 0
    cognitive cortex is column 0 of the array, rows 1 to 4
    iTarget is global as it is used to flash the chosen target. Value = 1 to 4
    }
    iTarget1 := RandomRange(1,FiTargetCount+1);
    repeat
      iTarget2 := RandomRange(1,FiTargetCount+1);
    until iTarget1 <> iTarget2;
    
    //work out the current context in case we are using learning by context
    FCurrentContext := ComputeContext(iTarget1,iTarget2);
  end;
  arTarget[iTarget1].Show := true;
  arTarget[iTarget2].Show := true;

  if cbShowTargets.Checked then begin
    //show the two targets next to the activations in the cortex ensemble box surrounded by the colo
    //of the line that will be used in the graph for that target
    TImage(FindComponent('imT' + IntToStr(iTarget1))).Visible := true;
    TImage(FindComponent('imT' + IntToStr(iTarget2))).Visible := true;
    TShape(FindComponent('shpCog' + IntToStr(iTarget1))).Visible := true;
    TShape(FindComponent('shpCog' + IntToStr(iTarget2))).Visible := true;
    TShape(FindComponent('shpLegendCog' + IntToStr(iTarget1))).Visible := true;
    TShape(FindComponent('shpLegendCog' + IntToStr(iTarget2))).Visible := true;
  end;

  //Need to work out the two directions here. Even though they do not appear in
  //motor cortex until the movements phase, they are used in association cortex
  //in the decision phase.
  //Motor cortex is row 0 of the array, columns 1 to 4
  arTarget[iTarget1].Direction := RandomRange(1,5);
  repeat
    arTarget[iTarget2].Direction := RandomRange(1,5);
  until arTarget[iTarget1].Direction <> arTarget[iTarget2].Direction;

  arDirectionShown[arTarget[iTarget1].Direction] := true;
  arDirectionShown[arTarget[iTarget2].Direction] := true;

  SetCognitiveCortexActivation;
  SetMotorCortexActivation;
  SetAssociationCortexActivation;

  arTrial[FCurrentRun-1].Target1 := iTarget1;
  arTrial[FCurrentRun-1].Target[0] := arTarget[iTarget1];
  arTrial[FCurrentRun-1].Target2 := iTarget2;
  arTrial[FCurrentRun-1].Target[1] := arTarget[iTarget2];
  arTrial[FCurrentRun-1].Direction1 := arTarget[iTarget1].Direction;
  arTrial[FCurrentRun-1].Direction2 := arTarget[iTarget2].Direction;

  arTarget[iTarget1].PresentedCount := arTarget[iTarget1].PresentedCount + 1;
  arTarget[iTarget2].PresentedCount := arTarget[iTarget2].PresentedCount + 1;
  TShape(FindComponent('shpLegendMotor' + IntToStr(arTarget[iTarget1].Direction))).Visible := true;
  TShape(FindComponent('shpLegendMotor' + IntToStr(arTarget[iTarget2].Direction))).Visible := true;
end;

{
Select the target with the highest reward probability (hopefully) by going around the
cognitive loop, possibly multiple times

delayDisplay : do or do not update display during computation
  (trick to avoid thread conflicts when called from IdTCPServer1Execute)
  Side effect: 12 times faster when true for both Target and Direction

}
function TfrmMain.SelectTarget(delayDisplay : Boolean = false) : integer;
var iTarget: integer;
begin
//  FiCogLoops := 0;  //You always do at least one loop
//  FiMotorLoops := -1;  //set this here in case target selection fails
  FiLoops := -1;
//  ClearGraphs;
  repeat
    result := 0;
    DoALoop;
    iTarget := CheckCorticalActivation(1);
    inc(FiLoops);
    // Acualize GUI at each loop in normal condition
    if not delayDisplay then
     DisplayValues(2);
    if iTarget = -1 then begin
      //All cognitive cortical activations zero or less
      inc(arFail[ACTIVATIONS]);
      result := 2;  //so that the direction selection won't be done
      BREAK;
    end;
    if FiLoops >= FiMaxLoops then begin
      result := 3;
      inc(arFail[TARGET_LOOPS]);
      BREAK;
    end;
  until iTarget = 1;
  // When called from indy thread
  if delayDisplay then
     DisplayValues(2);
end;

{
Select the direction of the target chosen in the previous step
The result says if the direction was successfully chosen or not
WhichDirection says the direction that was chosen

delayDisplay : do or do not update display during computation
  (trick to avoid thread conflicts when called from IdTCPServer1Execute)
  Side effect: 12 times faster when true for both Target and Direction
}
function TfrmMain.SelectDirection(delayDisplay: boolean = false): integer;
var iDirection: integer;
begin
  iDirection := 0;
  repeat
    result := 0;
    DoALoop;
    if FiLoops >= FiLoopsBeforeTargets then begin
      if iDirectionChosen = NO_DIRECTION then begin
        if iTargetChosen <> NO_TARGET then
          //a target has already been chosen, so count the motor loops
          inc(FiMotorLoops);
        iDirection := CheckCorticalActivation(2);
      end;

      if iTargetChosen = NO_TARGET then begin
        inc(FiCogLoops);
        CheckCorticalActivation(1);
      end;
    end;

    inc(FiLoops);
    // Acualize GUI at each loop in normal condition
    if not delayDisplay then
      DisplayValues(3);
    if FiLoops >= FiMaxLoops then begin
      result := 3;
      inc(arFail[DIRECTION_LOOPS]);
      BREAK;
    end;
    while bPause do
      Application.ProcessMessages;
    if bStop then begin
      result := 3;
      inc(arFail[DIRECTION_LOOPS]);
      BREAK;
    end;
  until iDirection = 1;
  // When called from indy thread
  if delayDisplay then
     DisplayValues(3);
end;

//Check whether the cortical activation exceeds the threshold
//WhichPhase specifies whether to check activations in cognitive (1) or motor
//cortex (2).
function TfrmMain.CheckCorticalActivation(WhichPhase: integer): integer;
var eTop,
    eSecond: double;
    i,
    iNotChosen: integer;
begin
  result := 0;
  case WhichPhase of
    1: begin //selecting target
      //The cognitive cortical cell with the highest value selects the corresponding
      //frontal cortex cell and suppresses all others
      eTop := 0;
      for i := 1 to 4 do begin
        //need to reset them all as this may not be the first loop and the
        //wrong target may have been chosen on an earlier loop
        arTarget[i].ChosenAsTarget := false;
        if arCx[COL_COGNITIVE, i] > eTop then begin
          eTop := arCx[COL_COGNITIVE, i];
          iTargetChosen := i;
        end;
      end;
      if iTargetChosen = NO_TARGET then begin
        //no cognitive cortex cells are activated. Once this happens, no cells will ever become
        //activated in cognitive cortex, so get out of  the target selection
        //before motor values go through the roof.
        result := -1;
        EXIT;
      end;

      //Get the cognitive cortical cell with the second highest activation to do
      //threshold detection
      eSecond := 0;
      for i := 1 to 4 do begin
        if i <> iTargetChosen then begin
          if arCx[COL_COGNITIVE, i] > eSecond then begin
            eSecond := arCx[COL_COGNITIVE, i];
          end;
        end;
      end;
      if eTop > eSecond + FeSelectionThreshold then begin
        //a cortical cell has been selected as the threshold has been passed
        arTarget[iTargetChosen].ChosenAsTarget := true;
        //The target picked is the target not necessarily the one that the cursor will be moved to
        //Sometimes a different direction is picked from that of the
        //target that was chosen
//        arTarget[iTargetChosen].PickedCount := arTarget[iTargetChosen].PickedCount + 1;
        Memo1.Lines.Add('Target ' + IntToStr(iTargetChosen) + ' selected');
        //send the activations in all structures at this point to Excel
//        if bMultipleRuns then
//          SendStageResultToExcel(2);
        result := 1;
      end
      else
        iTargetChosen := NO_TARGET; //reset this in case it is the last time round the loop
    end;
    2: begin //selecting direction
      //The motor cortical cell with the highest activation gives the direction
      //chosen
      eTop := 0;
      for i := 1 to 4 do begin
        if arCx[i, ROW_MOTOR] > eTop then begin
          eTop := arCx[i, ROW_MOTOR];
          iDirectionChosen := i;
        end;
      end;
      if iDirectionChosen = NO_DIRECTION then begin
        //No cells are activated, but that is quite common on the motor loop
        //as the activations are suppressed by STN during target selection. So,
        //unlike target selection, we can keep trying as the motor cortical
        //activations will eventually rise due to the cognitive cortical activations.
        result := 0;
        EXIT;
      end
      else begin
      //If at least one cell is activated, count how many of the cortical cells
      //have an activation less than the threshold. If the other three cells
      //are not activated, a selection has been made
        iNotChosen := 0;
        for i := 1 to 4 do begin
          if i <> iDirectionChosen then begin
            if arCx[i, ROW_MOTOR] < FeSelectionThreshold then begin
              inc(iNotChosen);
            end;
          end;
        end;
      end;

      //Get the motor cortical cell with the second highest activation to do
      //threshold detection
      eSecond := 0;
      for i := 1 to 4 do begin
        if i <> iDirectionChosen then begin
          if arCx[i, ROW_MOTOR] > eSecond then begin
            eSecond := arCx[i, ROW_MOTOR];
          end;
        end;
      end;
      if eTop > eSecond + FeSelectionThreshold then begin
        //a cortical cell has been selected
        result := 1;
        if (iDirectionChosen <> NO_DIRECTION) then begin
          for i := 1 to 4 do begin
            arTarget[i].ChosenAsDirection := arTarget[i].Direction = iDirectionChosen;
            if arTarget[i].Direction = iDirectionChosen then begin
              iTargetByDirection := i;
            end;
          end;
          Memo1.Lines.Add('Direction ' + IntToStr(iDirectionChosen) + ' selected');
//          if bMultipleRuns then
//            SendTrialResultToExcel;
          if iNotChosen = 3 then
            result := 1;
        end;
      end
      else
        iDirectionChosen := NO_DIRECTION;  //in case this is the last time round the loop
    end;
  end;
end;

{
this procedure has been put in to check when the direction chosen is not one of
the directions in which a target is present. In that case send reward = 0 to the
model to decrease the weights to that direction
}
function TfrmMain.VerifyDirection: boolean;
var i: integer;
begin
  result := false;
  for i := 1 to 4 do begin
    if arTarget[i].Show then begin
      if arTarget[i].Direction = iDirectionChosen then begin
        result := true;
        EXIT;
      end;
    end;
  end;
  //If we reach here the direction chosen was not the direction of one of the displayed targets
  UpdateWeights(0,1);
end;

// TODO: prevented a bug when btnTarget is clicked, but have to check consistency with icon highlighting when a new set of targets is chose
procedure TfrmMain.Timer1Timer(Sender: TObject);
var AImage,
    BImage: TImage;
    i: integer;
    bAFound: boolean;
begin
  if cbShowTargets.Checked then begin
    AImage := nil;
    BImage := nil;
    case FStage of
      1: begin
        bAFound := false;
        for i := 1 to 4 do begin
          if arTarget[i].Show then begin
            if not bAFound then begin
              AImage := TImage(FindComponent('imT' + IntToStr(i)));
              bAFound := true;
            end
            else
              BImage := TImage(FindComponent('imT' + IntToStr(i)));
          end;
        end;
      end;
      2: begin
        //doing decision phase
        if iTargetChosen = NO_TARGET then
          EXIT;
        AImage := TImage(FindComponent('imT' + IntToStr(iTargetChosen)));
        BImage := nil;
      end;
      3: begin
        //doing motor phase
        if iDirectionChosen = NO_DIRECTION then
          EXIT;
        AImage := TImage(FindComponent('im' + IntToStr(iDirectionChosen)));
        BImage := TImage(FindComponent('imD' + IntToStr(iDirectionChosen)));
      end;
    end;

    //If we get to here, a direction has been chosen
    {
    ...err...not necessarily, will raise an an "Access Violoation" in the next
    few lines as soon as we click on "btnTarget", have to check a direction has
    been chosen.
    }
    if (AImage <> nil) and (iDirectionChosen <> NO_DIRECTION ) then begin
      AImage.Visible := not AImage.Visible;
      TImage(FindComponent('im2_' + IntToStr(iDirectionChosen))).Visible := AImage.Visible;
      shpChosen1.Visible := true;
      shpChosen2.Visible := true;
      shpChosen1.Top := TImage(FindComponent('im' + IntToStr(iDirectionChosen))).Top;
      shpChosen2.Top := TImage(FindComponent('im2_' + IntToStr(iDirectionChosen))).Top;
      shpChosen1.Left := TImage(FindComponent('im' + IntToStr(iDirectionChosen))).Left;
      shpChosen2.Left := TImage(FindComponent('im2_' + IntToStr(iDirectionChosen))).Left;
    end;
    if BImage <> nil then
      BImage.Visible := not BImage.Visible;

    //sutomatically stop flashing the chosen target after 5 appearances
    inc(iFlash);
    if iFlash = 10 then begin
      Timer1.Enabled := false;
      if AImage <> nil then
        AImage.Visible := true;
      if BImage <> nil then
        BImage.Visible := true;
    end;
  end
  else
    Timer1.Enabled := false;
end;

procedure TfrmMain.grdCxMotorDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if Timer1.Enabled then begin
{    if ARow = iChosen then begin
      if Canvas.Pen.Color = clBlack then
        Canvas.Pen.Color := clRed
      else
        Canvas.Brush.Color := clBlack;
    end;
}
  end;
end;

procedure TfrmMain.MakeImagesVisible;
var i: integer;
    AImage: TImage;
begin
  if cbShowTargets.Checked then
    for i := 1 to 4 do begin
      AImage := TImage(FindComponent('im' + IntToStr(i)));
      AImage.Visible := true;
      TImage(FindComponent('im2_' + IntToStr(i))).Visible := true;
      AImage := TImage(FindComponent('imConfigT' + IntToStr(i)));
      AImage.Visible := true;
    end;
end;

procedure TfrmMain.btnSaveParametersClick(Sender: TObject);
var FIni: TIniFile;
begin
  dlgSave.Filename := FIniFileName;
  if dlgSave.Execute then begin
    FIniFileName := dlgSave.FileName;
    FIni := TIniFile.Create(dlgSave.FileName);
    with FIni do try
{
      WriteInteger('Parameters','Tonic cortex level',CxDefaults.TonicFiring);
      WriteInteger('Parameters','Tonic striatum level',StrDefaults.TonicFiring);
      WriteInteger('Parameters','Tonic STN level',StnDefaults.TonicFiring);
      WriteInteger('Parameters','Tonic GPi level',GpiDefaults.TonicFiring);
      WriteInteger('Parameters','Tonic thalamus level',ThDefaults.TonicFiring);
}

      WriteFloat('Parameters','Cortical noise percentage',CxDefaults.NoisePercent);
      WriteFloat('Parameters','Striatal noise percentage',StrDefaults.NoisePercent);
      WriteFloat('Parameters','STN noise percentage',StnDefaults.NoisePercent);
      WriteFloat('Parameters','GPi noise percentage',GpiDefaults.NoisePercent);
      WriteFloat('Parameters','Thalamic noise percentage',ThDefaults.NoisePercent);

      WriteFloat('Parameters','Cortical activation decay time constant',CxDefaults.Tau);
      WriteFloat('Parameters','Striatal activation decay time constant',StrDefaults.Tau);
      WriteFloat('Parameters','STN activation decay time constant',StnDefaults.Tau);
      WriteFloat('Parameters','GPi activation decay time constant',GpiDefaults.Tau);
      WriteFloat('Parameters','Thalamic activation decay time constant',ThDefaults.Tau);

      WriteFloat('Parameters','Cortical activation threshold',CxDefaults.Threshold);
      WriteFloat('Parameters','Striatal activation threshold',StrDefaults.Threshold);
      WriteFloat('Parameters','STN activation threshold',StnDefaults.Threshold);
      WriteFloat('Parameters','GPi activation threshold',GpiDefaults.Threshold);
      WriteFloat('Parameters','Thalamic activation threshold',ThDefaults.Threshold);

      WriteInteger('Parameters','Ignore STN tonic firing',Integer(StnDefaults.IgnoreTonic));
      WriteInteger('Parameters','Ignore GPi tonic firing',Integer(GpiDefaults.IgnoreTonic));
      WriteInteger('Parameters','Ignore Thalamic tonic firing',Integer(ThDefaults.IgnoreTonic));

      WriteFloat('Parameters','Connectivity - Motor Cortex to Motor Striatum',eCxMotorToStrMotor);
      WriteFloat('Parameters','Connectivity - Motor Cortex to Associative Striatum',eCxMotorToStrAss);
      WriteFloat('Parameters','Connectivity - Cognitive Cortex to Cognitive Striatum',eCxCogToStrCog);
      WriteFloat('Parameters','Connectivity - Cognitive Cortex to Associative Striatum',eCxCogToStrAss);
      WriteFloat('Parameters','Connectivity - Associative Cortex to Associative Striatum',eCxAssToStrAss);
      WriteFloat('Parameters','Connectivity - Associative Ratio',eAssociativeRatio);

      WriteFloat('Parameters','Connectivity - Motor Cortex to Motor STN',eCxMotorToStnMotor);
      WriteFloat('Parameters','Connectivity - Cognitive Cortex to Cognitive STN',eCxCogToStnCog);

      WriteFloat('Parameters','Connectivity - Cortex to Thalamus',eCxToTh);
      WriteInteger('Parameters','Bidirectional Cortex to Thalamus',Integer(bCxToTh));

      WriteFloat('Parameters','Connectivity - Motor STN to Motor GPi',eStnMotorToGpiMotor);
      WriteFloat('Parameters','Connectivity - Motor STN to Associative GPi',eStnMotorToGpiAss);
      WriteFloat('Parameters','Connectivity - Cognitive STN to Cognitive GPi',eStnCogToGpiCog);
      WriteFloat('Parameters','Connectivity - Cognitive STN to Associative GPi',eStnCogToGpiAss);

      WriteFloat('Parameters','Connectivity - Motor GPi to Motor Thalamus',eMotorGPiToMotorTh);
      WriteFloat('Parameters','Connectivity - Cognitive GPi to Cognitive Thalamus',eCogGPiToCogTh);
      WriteFloat('Parameters','Connectivity - Associative GPi to Cognitive Thalamus',eAssGPiToCogTh);
      WriteFloat('Parameters','Connectivity - Associative GPi to Motor Thalamus',eAssGPiToMotorTh);
      WriteFloat('Parameters','Connectivity - Cognitive Striatum to GPi',eCogStrToGpi);
      WriteFloat('Parameters','Connectivity - Motor Striatum to GPi',eMotorStrToGpi);
      WriteFloat('Parameters','Connectivity - associative Striatum to GPi',eStrAssToGpi);

      WriteFloat('Parameters','Connectivity - Motor Thalamus to Motor Cortex',eThMotorToCxMotor);
      WriteFloat('Parameters','Connectivity - Cognitive Thalamus to Cognitive Cortex',eThCogToCxCog);

      WriteFloat('Parameters','Baseline Cognitive Level',eBaselineCogLevel);
      WriteFloat('Parameters','Baseline Motor Level',eBaselineMotorLevel);
      WriteFloat('Parameters','Baseline Association Level',eBaselineAssociationLevel);
//      WriteInteger('Parameters','Persistent Cortical Activation',Integer(bPersistentCorticalActivation));
//      WriteFloat('Parameters','Minimum Cognitive Target Value',eMinCogTargetValue);
//      WriteFloat('Parameters','Maximum Cognitive Target Value',eMaxCogTargetValue);
      WriteInteger('Parameters','Use Target 1',Integer(arTarget[1].Use));
      WriteInteger('Parameters','Use Target 2',Integer(arTarget[2].Use));
      WriteInteger('Parameters','Use Target 3',Integer(arTarget[3].Use));
      WriteInteger('Parameters','Use Target 4',Integer(arTarget[4].Use));
      WriteInteger('Parameters','Target 1 Value',StrToInt(edTarget1Value.Text));
      WriteInteger('Parameters','Target 2 Value',StrToInt(edTarget2Value.Text));
      WriteInteger('Parameters','Target 3 Value',StrToInt(edTarget3Value.Text));
      WriteInteger('Parameters','Target 4 Value',StrToInt(edTarget4Value.Text));
      WriteFloat('Parameters','Target 1 Reward Probability', arTarget[1].RewardProbability);
      WriteFloat('Parameters','Target 2 Reward Probability', arTarget[2].RewardProbability);
      WriteFloat('Parameters','Target 3 Reward Probability', arTarget[3].RewardProbability);
      WriteFloat('Parameters','Target 4 Reward Probability', arTarget[4].RewardProbability);
      WriteInteger('Parameters','Loops before targets shown',FiLoopsBeforeTargets);

      WriteInteger('Parameters','Auto save results',Integer(bAutoSave));
      WriteInteger('Parameters','Show graphs',Integer(bShowGraphs));

      WriteFloat('Parameters','Value learning rate', eValueLearningRate);
      WriteFloat('Parameters','Action learning rate - LTP', eLTP);
      WriteFloat('Parameters','Action learning rate - LTD', eLTD);
      WriteInteger('Parameters','Learning on',Integer(FLearning));
      WriteInteger('Parameters','Learning type',FiLearningType);
      WriteInteger('Parameters','Use learning transfer function',Integer(bUseLearningTransferFuction));
      WriteFloat('Parameters','Minimum Learning activation',FWeightBoltzmann.MinOutput);
      WriteFloat('Parameters','Maximum Learning activation',FWeightBoltzmann.MaxOutput);
      WriteFloat('Parameters','Learning half activation',FWeightBoltzmann.Vh);
      WriteFloat('Parameters','Learning activation slope',FWeightBoltzmann.Vc);
      WriteFloat('Parameters','Initial weight', eInitialWeight);
      WriteFloat('Parameters','SD Initial weight', eSdInitialWeight);
      WriteFloat('Parameters','Minimum weight', eMinWeight);
      WriteFloat('Parameters','Maximum weight', eMaxWeight);
      WriteInteger('Parameters','Random initial weights',Integer(bWInitialRandom));
      WriteInteger('Parameters','Show weights',Integer(bShowWeights));
      WriteInteger('Parameters','By target chosen',Integer(FByTargetChosen));

      WriteInteger('Parameters','Use cortical threshold',Integer(bUseCorticalThreshold));
      WriteFloat('Parameters','Cortical Threshold level',FeSelectionThreshold);
      WriteFloat('Parameters','MSN Threshold level',eMSNThresholdLevel);
      WriteInteger('Parameters','STN target saliency',Integer(FStnTargetSaliency));

      WriteInteger('Parameters','Use striatal transfer function',Integer(bUseTransferFuction));
      WriteFloat('Parameters','Minimum striatal activation',FBoltzmann.MinOutput);
      WriteFloat('Parameters','Maximum striatal activation',FBoltzmann.MaxOutput);
      WriteFloat('Parameters','Striatal half activation',FBoltzmann.Vh);
      WriteFloat('Parameters','Striatal activation slope',FBoltzmann.Vc);

      WriteInteger('Parameters','Use associative GPi',Integer(bUseGpiAss));

      WriteInteger('Parameters','Maximum loops for activation', FiMaxLoops);
      WriteInteger('Parameters','Simulations per experiment', FSimsPerExperiment);
      WriteInteger('Parameters','Runs per simulation', FRunsPerSimulation);
    finally
      Free;
    end;
  end;
end;

procedure TfrmMain.btnOpenClick(Sender: TObject);
begin
  dlgOpen.InitialDir := ExtractFilePath(Application.ExeName);
  if dlgOpen.Execute then begin
    FIniFileName := dlgOpen.FileName;
    GetParameters;
  end;
end;

procedure TfrmMain.GetParameters;
var FIni: TIniFile;
begin
  FIni := TIniFile.Create(FIniFileName);
  with FIni do try
    try
      FTeachingVersion := boolean(ReadInteger('Setup','Teaching',0));

{
      CxDefaults.TonicFiring := ReadInteger('Parameters','Tonic cortex level',0);
      StrDefaults.TonicFiring := ReadInteger('Parameters','Tonic striatum level',0);
      StnDefaults.TonicFiring := ReadInteger('Parameters','Tonic STN level',5);
      GpiDefaults.TonicFiring := ReadInteger('Parameters','Tonic GPi level',5);
      ThDefaults.TonicFiring := ReadInteger('Parameters','Tonic thalamus level',40);
}

      CxDefaults.NoisePercent := ReadFloat('Parameters','Cortical noise percentage',5);
      StrDefaults.NoisePercent := ReadFloat('Parameters','Striatal noise percentage',5);
      StnDefaults.NoisePercent := ReadFloat('Parameters','STN noise percentage',5);
      GpiDefaults.NoisePercent := ReadFloat('Parameters','GPi noise percentage',5);
      ThDefaults.NoisePercent := ReadFloat('Parameters','Thalamic noise percentage',5);

      CxDefaults.Tau := ReadFloat('Parameters','Cortical activation decay time constant',10);
      StrDefaults.Tau := ReadFloat('Parameters','Striatal activation decay time constant',10);
      StnDefaults.Tau := ReadFloat('Parameters','STN activation decay time constant',10);
      GpiDefaults.Tau := ReadFloat('Parameters','GPi activation decay time constant',10);
      ThDefaults.Tau := ReadFloat('Parameters','Thalamic activation decay time constant',10);

      CxDefaults.Threshold := ReadFloat('Parameters','Cortical activation threshold',0);
      StrDefaults.Threshold := ReadFloat('Parameters','Striatal activation threshold',0);
      StnDefaults.Threshold := ReadFloat('Parameters','STN activation threshold',0);
      GpiDefaults.Threshold := ReadFloat('Parameters','GPi activation threshold',0);
      ThDefaults.Threshold := ReadFloat('Parameters','Thalamic activation threshold',0);

      StnDefaults.IgnoreTonic := boolean(ReadInteger('Parameters','Ignore STN tonic firing',0));
      GpiDefaults.IgnoreTonic := boolean(ReadInteger('Parameters','Ignore GPi tonic firing',0));
      ThDefaults.IgnoreTonic := boolean(ReadInteger('Parameters','Ignore Thalamic tonic firing',0));

      eCxMotorToStrMotor := ReadFloat('Parameters','Connectivity - Motor Cortex to Motor Striatum',1);
      eCxMotorToStrAss := ReadFloat('Parameters','Connectivity - Motor Cortex to Associative Striatum',0.5);
      eCxCogToStrCog := ReadFloat('Parameters','Connectivity - Cognitive Cortex to Cognitive Striatum',1);
      eCxCogToStrAss := ReadFloat('Parameters','Connectivity - Cognitive Cortex to Associative Striatum',0.5);
      eCxAssToStrAss := ReadFloat('Parameters','Connectivity - Associative Cortex to Associative Striatum',0.5);
      eAssociativeRatio := ReadFloat('Parameters','Connectivity - Associative Ratio',0.25);

      eCxMotorToStnMotor := ReadFloat('Parameters','Connectivity - Motor Cortex to Motor STN',1);
      eCxCogToStnCog := ReadFloat('Parameters','Connectivity - Cognitive Cortex to Cognitive STN',1);

      eCxToTh := ReadFloat('Parameters','Connectivity - Cortex to Thalamus',1);
      bCxToTh := boolean(ReadInteger('Parameters','Bidirectional Cortex to Thalamus',1));

      eStnMotorToGpiMotor := ReadFloat('Parameters','Connectivity - Motor STN to Motor GPi',2);
      eStnMotorToGpiAss := ReadFloat('Parameters','Connectivity - Motor STN to Associative GPi',2);
      eStnCogToGpiCog := ReadFloat('Parameters','Connectivity - Cognitive STN to Cognitive GPi',2);
      eStnCogToGpiAss := ReadFloat('Parameters','Connectivity - Cognitive STN to Associative GPi',2);

      eCogGPiToCogTh := ReadFloat('Parameters','Connectivity - Cognitive GPi to Cognitive Thalamus',0.5);
      eMotorGPiToMotorTh := ReadFloat('Parameters','Connectivity - Motor GPi to Motor Thalamus',0.5);
      eAssGPiToCogTh := ReadFloat('Parameters','Connectivity - Associative GPi to Cognitive Thalamus',0.5);
      eAssGPiToMotorTh := ReadFloat('Parameters','Connectivity - Associative GPi to Motor Thalamus',0.5);
      eCogStrToGpi := ReadFloat('Parameters','Connectivity - Cognitive Striatum to GPi',1);
      eMotorStrToGpi := ReadFloat('Parameters','Connectivity - Motor Striatum to GPi',1);
      eStrAssToGpi := ReadFloat('Parameters','Connectivity - Associative Striatum to GPi',1);

      eThMotorToCxMotor := ReadFloat('Parameters','Connectivity - Motor Thalamus to Motor Cortex',2);
      eThCogToCxCog := ReadFloat('Parameters','Connectivity - Cognitive Thalamus to Cognitive Cortex',2);

      eBaselineCogLevel := ReadFloat('Parameters','Baseline Cognitive Level',1);
      eBaselineMotorLevel := ReadFloat('Parameters','Baseline Motor Level',1);
      eBaselineAssociationLevel := ReadFloat('Parameters','Baseline Association Level',1);
//      bPersistentCorticalActivation := boolean(ReadInteger('Parameters','Persistent Cortical Activation',0));
      arTarget[1].Use := boolean(ReadInteger('Parameters','Use Target 1',1));
      arTarget[2].Use := boolean(ReadInteger('Parameters','Use Target 2',1));
      arTarget[3].Use := boolean(ReadInteger('Parameters','Use Target 3',1));
      arTarget[4].Use := boolean(ReadInteger('Parameters','Use Target 4',1));
      edTarget1Value.Text := IntToStr(ReadInteger('Parameters','Target 1 Value',1));
      edTarget2Value.Text := IntToStr(ReadInteger('Parameters','Target 2 Value',2));
      edTarget3Value.Text := IntToStr(ReadInteger('Parameters','Target 3 Value',3));
      edTarget4Value.Text := IntToStr(ReadInteger('Parameters','Target 4 Value',4));
      arTarget[1].RewardProbability := ReadFloat('Parameters','Target 1 Reward Probability',0);
      arTarget[2].RewardProbability := ReadFloat('Parameters','Target 2 Reward Probability',0.33);
      arTarget[3].RewardProbability := ReadFloat('Parameters','Target 3 Reward Probability',0.66);
      arTarget[4].RewardProbability := ReadFloat('Parameters','Target 4 Reward Probability',1);
      FiLoopsBeforeTargets := ReadInteger('Parameters','Loops before targets shown',50);

      bAutoSave := boolean(ReadInteger('Parameters','Auto save results',1));
      bShowGraphs := boolean(ReadInteger('Parameters','Show graphs',0));

      eValueLearningRate := ReadFloat('Parameters','Value learning rate',0.1);
      eLTP := ReadFloat('Parameters','Action learning rate - LTP',0.1);
      eLTD := ReadFloat('Parameters','Action learning rate - LTD',0.1);
      FLearning := boolean(ReadInteger('Parameters','Learning on',1));
      FiLearningType := ReadInteger('Parameters','Learning type',1);
      bUseLearningTransferFuction := boolean(ReadInteger('Parameters','Use learning transfer function',1));
      FWeightBoltzmann.MinOutput := ReadFloat('Parameters','Minimum Learning activation',-0.2);
      FWeightBoltzmann.MaxOutput := ReadFloat('Parameters','Maximum Learning activation',1);
      FWeightBoltzmann.Vh := ReadFloat('Parameters','Learning half activation',0.5);
      FWeightBoltzmann.Vc := ReadFloat('Parameters','Learning activation slope',0.2);
      eInitialWeight := ReadFloat('Parameters','Initiial weight',0.5);
      eSdInitialWeight := ReadFloat('Parameters','SD Initial weight',0.05);
      eMinWeight := ReadFloat('Parameters','Minimum weight',0.3);
      eMaxWeight := ReadFloat('Parameters','Maximum weight',0.7);
      bWInitialRandom := boolean(ReadInteger('Parameters','Random initial weights',1));
      bShowWeights := boolean(ReadInteger('Parameters','Show weights',1));
      FByTargetChosen := boolean(ReadInteger('Parameters','By target chosen',1));

      bUseCorticalThreshold := boolean(ReadInteger('Parameters','Use cortical threshold',1));
      FeSelectionThreshold := ReadFloat('Parameters','Cortical Threshold level',0.2);
      eMSNThresholdLevel := ReadFloat('Parameters','MSN Threshold level',2);
      FStnTargetSaliency := boolean(ReadInteger('Parameters','STN target saliency',1));

      bUseTransferFuction := boolean(ReadInteger('Parameters','Use striatal transfer function',1));
      FBoltzmann.MinOutput := ReadFloat('Parameters','Minimum striatal activation',0);
      FBoltzmann.MaxOutput := ReadFloat('Parameters','Maximum striatal activation',10);
      FBoltzmann.Vh := ReadFloat('Parameters','Striatal half activation',10);
      FBoltzmann.Vc := ReadFloat('Parameters','Striatal activation slope',10);

      bUseGpiAss := boolean(ReadInteger('Parameters','Use associative GPi',0));

      FiMaxLoops := ReadInteger('Parameters','Maximum loops for activation',10);
      FSimsPerExperiment := ReadInteger('Parameters','Simulations per experiment',1);
      FRunsPerSimulation := ReadInteger('Parameters','Runs per simulation',100);
    except
      ShowMessage('Problem reading values from ini file');
      Abort
    end;
  finally
    Free;
  end;

  FLoading := true;
  try
    tsConfigTeach.TabVisible := FTeachingVersion;
    tsConfig.TabVisible := not FTeachingVersion;
    btnTarget.Visible := not FTeachingVersion;
    btnDecisionPhase.Visible := not FTeachingVersion;
    btnMovmentPhase.Visible := not FTeachingVersion;
//    btnClearGrids.Visible := not FTeachingVersion;
    btnSaveParameters.Visible := not FTeachingVersion;
    btnOpen.Visible := not FTeachingVersion;
{
    edTonicCx.Text := IntToStr(CxDefaults.TonicFiring);
    edTonicStr.Text := IntToStr(StrDefaults.TonicFiring);
    edTonicStn.Text := IntToStr(StnDefaults.TonicFiring);
    edTonicGpi.Text := IntToStr(GpiDefaults.TonicFiring);
    edTonicTh.Text := IntToStr(ThDefaults.TonicFiring);
}
    edCxNoisePercent.Text := FloatToStr(CxDefaults.NoisePercent, FFormatSettings);
    edStrNoisePercent.Text := FloatToStr(StrDefaults.NoisePercent, FFormatSettings);
    edStnNoisePercent.Text := FloatToStr(StnDefaults.NoisePercent, FFormatSettings);
    edGpiNoisePercent.Text := FloatToStr(GpiDefaults.NoisePercent, FFormatSettings);
    edThNoisePercent.Text := FloatToStr(ThDefaults.NoisePercent, FFormatSettings);

    CxDefaults.NoiseProportion := CxDefaults.NoisePercent / 100;
    StrDefaults.NoiseProportion := StrDefaults.NoisePercent / 100;
    StnDefaults.NoiseProportion := StnDefaults.NoisePercent / 100;
    GpiDefaults.NoiseProportion := GpiDefaults.NoisePercent / 100;
    ThDefaults.NoiseProportion := ThDefaults.NoisePercent / 100;

    edTauCx.Text := FloatToStr(CxDefaults.Tau, FFormatSettings);
    edTauStr.Text := FloatToStr(StrDefaults.Tau, FFormatSettings);
    edTauStn.Text := FloatToStr(StnDefaults.Tau, FFormatSettings);
    edTauGpi.Text := FloatToStr(GpiDefaults.Tau, FFormatSettings);
    edTauTh.Text := FloatToStr(ThDefaults.Tau, FFormatSettings);

    edThresholdCx.Text := FloatToStr(CxDefaults.Threshold, FFormatSettings);
    edThresholdStr.Text := FloatToStr(StrDefaults.Threshold, FFormatSettings);
    edThresholdStn.Text := FloatToStr(StnDefaults.Threshold, FFormatSettings);
    edThresholdGpi.Text := FloatToStr(GpiDefaults.Threshold, FFormatSettings);
    edThresholdTh.Text := FloatToStr(ThDefaults.Threshold, FFormatSettings);

    edCxMotorToStrMotor.Text := FloatToStr(eCxMotorToStrMotor, FFormatSettings);
    edCxMotorToStrAss.Text := FloatToStr(eCxMotorToStrAss, FFormatSettings);
    edCxCogToStrCog.Text := FloatToStr(eCxCogToStrCog, FFormatSettings);
    edCxCogToStrAss.Text := FloatToStr(eCxCogToStrAss, FFormatSettings);
    edCxAssToStrAss.Text := FloatToStr(eCxAssToStrAss, FFormatSettings);
    edAssociativeRatio.Text := FloatToStr(eAssociativeRatio, FFormatSettings);

    edCxMotorToStnMotor.Text := FloatToStr(eCxMotorToStnMotor, FFormatSettings);
    edCxCogToStnCog.Text := FloatToStr(eCxCogToStnCog, FFormatSettings);

    edCxToTh.Text := FloatToStr(eCxToTh, FFormatSettings);
    cbCxThalamusActive.Checked := bCxToTh;
    if bCxToTh then
      arrowThCx.ArrowPosition := apBoth
    else
      arrowThCx.ArrowPosition := apRight;

//    cbIgnoreTonicStn.Checked := StnDefaults.IgnoreTonic;
    edStnMotorToGpiMotor.Text := FloatToStr(eStnMotorToGpiMotor, FFormatSettings);
    edStnMotorToAssGpi.Text := FloatToStr(eStnMotorToGpiAss, FFormatSettings);
    edStnCogToGpiCog.Text := FloatToStr(eStnCogToGpiCog, FFormatSettings);
    edStnCogToAssGpi.Text := FloatToStr(eStnCogToGpiAss, FFormatSettings);

//    cbIgnoreTonicGpi.Checked := GpiDefaults.IgnoreTonic;
    edCogGpiToCogTh.Text := FloatToStr(eCogGPiToCogTh, FFormatSettings);
    edMotorGpiToMotorTh.Text := FloatToStr(eMotorGPiToMotorTh, FFormatSettings);
    edAssGpiToCogTh.Text := FloatToStr(eAssGPiToCogTh, FFormatSettings);
    edAssGpiToMotorTh.Text := FloatToStr(eAssGPiToMotorTh, FFormatSettings);
    edCogStrToGpi.Text := FloatToStr(eCogStrToGpi, FFormatSettings);
    edMotorStrToGpi.Text := FloatToStr(eMotorStrToGpi, FFormatSettings);
    edAssStrToGpi.Text := FloatToStr(eStrAssToGpi, FFormatSettings);

//    cbIgnoreTonicTh.Checked := ThDefaults.IgnoreTonic;
    edThMotorToCxMotor.Text := FloatToStr(eThMotorToCxMotor, FFormatSettings);
    edThCogToCxCog.Text := FloatToStr(eThCogToCxCog, FFormatSettings);

    cbUseTarget1.Checked := arTarget[1].Use;
    cbUseTarget2.Checked := arTarget[2].Use;
    cbUseTarget3.Checked := arTarget[3].Use;
    cbUseTarget4.Checked := arTarget[4].Use;
    SetupPairings;
    edTarget1Value.Text := FloatToStr(arTarget[1].Salience, FFormatSettings);
    edTarget2Value.Text := FloatToStr(arTarget[2].Salience, FFormatSettings);
    edTarget3Value.Text := FloatToStr(arTarget[3].Salience, FFormatSettings);
    edTarget4Value.Text := FloatToStr(arTarget[4].Salience, FFormatSettings);
    edTarget1Reward.Text := FloatToStr(arTarget[1].RewardProbability, FFormatSettings);
    edTarget2Reward.Text := FloatToStr(arTarget[2].RewardProbability, FFormatSettings);
    edTarget3Reward.Text := FloatToStr(arTarget[3].RewardProbability, FFormatSettings);
    edTarget4Reward.Text := FloatToStr(arTarget[4].RewardProbability, FFormatSettings);
    edLoopsBeforeTargets.Text := IntToStr(FiLoopsBeforeTargets);

    edBaselineCognitiveLevel.Text := FloatToStr(eBaselineCogLevel, FFormatSettings);
    edBaselineMotorLevel.Text := FloatToStr(eBaselineMotorLevel, FFormatSettings);
    edBaselineAssociationLevel.Text := FloatToStr(eBaselineAssociationLevel, FFormatSettings);
//    chkPersistentCxActivation.Checked := bPersistentCorticalActivation;

    cbTransferFunction.Checked := bUseTransferFuction;
    edMinStrActivation.Text := FloatToStr(FBoltzmann.MinOutput, FFormatSettings);
    edMaxStrActivation.Text := FloatToStr(FBoltzmann.MaxOutput, FFormatSettings);
    edStrVh.Text := FloatToStr(FBoltzmann.Vh, FFormatSettings);
    edStrVc.Text := FloatToStr(FBoltzmann.Vc, FFormatSettings);

    cbGpiAss.Checked := bUseGpiAss;
    grdGpiAss.Visible := bUseGpiAss;
    grdGpiRow.Visible := bUseGpiAss;
    grdGpiCol.Visible := bUseGpiAss;
    lblGpiAss.Visible := bUseGpiAss;
    lblGpiRow.Visible := bUseGpiAss;
    lblGpiCol.Visible := bUseGpiAss;

    cbAutoSave.Checked := bAutoSave;
    cbShowGraphs.Checked := bShowGraphs;

    edValueLearningRate.Text := FloatToStr(eValueLearningRate, FFormatSettings);
    edLtp.Text := FloatToStr(eLTP, FFormatSettings);
    edLtd.Text := FloatToStr(eLTD, FFormatSettings);
    cbLearning.Checked := FLearning;
    cbLearningTransferFunction.Checked := bUselearningTransferFuction;
    edLearningTFMin.Text := FloatToStr(FWeightBoltzmann.MinOutput, FFormatSettings);
    edLearningTFMax.Text := FloatToStr(FWeightBoltzmann.MaxOutput, FFormatSettings);
    edLearningTFVc.Text := FloatToStr(FWeightBoltzmann.Vc, FFormatSettings);
    edLearningTFVh.Text := FloatToStr(FWeightBoltzmann.Vh, FFormatSettings);
    rgLearningType.ItemIndex := FiLearningType;
    edWInitial.Text := FloatToStr(eInitialWeight, FFormatSettings);
    edSdWInitial.Text := FloatToStr(eSdInitialWeight, FFormatSettings);
    edMinWeight.Text := FloatToStr(eMinWeight, FFormatSettings);
    edMaxWeight.Text := FloatToStr(eMaxWeight, FFormatSettings);
    cbWeightsRandom.checked := bWInitialRandom;
    rgLearningBasedOn.ItemIndex := integer(FByTargetChosen);
    cbShowWeights.Checked := bShowWeights;

    cbUseThreshold.Checked := bUseCorticalThreshold;
    edCorticalThresholdLevel.Text := FloatToStr(FeSelectionThreshold, FFormatSettings);
    edMaxLoops.Text := IntToStr(FiMaxLoops);
    edSimsPerExperiment.Text := IntToStr(FSimsPerExperiment);
    edRunsPerSimulation.Text := IntToStr(FRunsPerSimulation);
  except
    on E: Exception do begin
      ShowMessage(E.Message + #10 + #13 + 'Writing ini file values to components');
      Abort;
    end;
  end;
  FLoading := false;
end;

procedure TfrmMain.edTarget1ValueChange(Sender: TObject);
begin
  if not FLoading then
    arTarget[1].Salience:= StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edTarget2ValueChange(Sender: TObject);
begin
  if not FLoading then
    arTarget[2].Salience:= StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edTarget3ValueChange(Sender: TObject);
begin
  if not FLoading then
    arTarget[3].Salience:= StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edTarget4ValueChange(Sender: TObject);
begin
  if not FLoading then
    arTarget[4].Salience:= StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edTarget1RewardChange(Sender: TObject);
begin
  if not FLoading then
    arTarget[1].RewardProbability := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edTarget2RewardChange(Sender: TObject);
begin
  if not FLoading then
    arTarget[2].RewardProbability := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edTarget3RewardChange(Sender: TObject);
begin
  if not FLoading then
    arTarget[3].RewardProbability := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edTarget4RewardChange(Sender: TObject);
begin
  if not FLoading then
    arTarget[4].RewardProbability := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.cbUseTarget1Click(Sender: TObject);
begin
  if not FLoading then begin
    arTarget[1].Use := TCheckbox(Sender).Checked;
    SetupPairings;
  end;
end;

procedure TfrmMain.cbUseTarget2Click(Sender: TObject);
begin
  if not FLoading then begin
    arTarget[2].Use := TCheckbox(Sender).Checked;
    SetupPairings;
  end;
end;

procedure TfrmMain.cbUseTarget3Click(Sender: TObject);
begin
  if not FLoading then begin
    arTarget[3].Use := TCheckbox(Sender).Checked;
    SetupPairings;
  end;
end;

procedure TfrmMain.cbUseTarget4Click(Sender: TObject);
begin
  if not FLoading then begin
    arTarget[4].Use := TCheckbox(Sender).Checked;
    SetupPairings;
  end;
end;

procedure TfrmMain.edLoopsBeforeTargetsChange(Sender: TObject);
begin
  if not FLoading then
    FiLoopsBeforeTargets := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edValueLearningRateChange(Sender: TObject);
begin
  if not FLoading then
    eValueLearningRate := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edLtpChange(Sender: TObject);
begin
  if not FLoading then
    eLTP := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edLtdChange(Sender: TObject);
begin
  if not FLoading then
    eLTD:= StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edWInitialChange(Sender: TObject);
begin
  if  not FLoading then
    eInitialWeight := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.cbWeightsRandomClick(Sender: TObject);
begin
  if not FLoading then
    bWInitialRandom := boolean(cbWeightsRandom.Checked);
end;

procedure TfrmMain.edSdWInitialChange(Sender: TObject);
begin
  if  not FLoading then
    eSdInitialWeight := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edMaxWeightChange(Sender: TObject);
begin
  if not FLoading then
    eMaxWeight := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edMinWeightChange(Sender: TObject);
begin
  if not FLoading then
    eMinWeight := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.rgLearningBasedOnClick(Sender: TObject);
begin
  if not FLoading then
    FByTargetChosen := boolean(rgLearningBasedOn.ItemIndex);
end;

procedure TfrmMain.edBaselineCognitiveLevelChange(Sender: TObject);
begin
  if not FLoading then
    eBaselineCogLevel := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edBaselineMotorLevelChange(Sender: TObject);
begin
  if not FLoading then
    eBaselineMotorLevel := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edBaselineAssociationLevelChange(Sender: TObject);
begin
  if not FLoading then
    eBaselineAssociationLevel := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.cbShowWeightsClick(Sender: TObject);
begin
  if  not FLoading then
    bShowWeights := cbShowWeights.Checked;
end;

procedure TfrmMain.cbUseThresholdClick(Sender: TObject);
begin
  if not FLoading then
    bUseCorticalThreshold := cbUseThreshold.Checked;
end;

procedure TfrmMain.edCorticalThresholdLevelChange(Sender: TObject);
begin
  if not FLoading then
    FeSelectionThreshold := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edMaxLoopsChange(Sender: TObject);
begin
  if not FLoading then
    FiMaxLoops := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edMSNThresholdChange(Sender: TObject);
begin
  if not FLoading then
    eMSNThresholdLevel := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.cbTransferFunctionClick(Sender: TObject);
begin
  if not FLoading then
    bUseTransferFuction := TCheckbox(Sender).Checked;
end;

procedure TfrmMain.edMinStrActivationChange(Sender: TObject);
begin
  if not FLoading then
    FBoltzmann.MinOutput := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edMaxStrActivationChange(Sender: TObject);
begin
  if not FLoading then
    FBoltzmann.MaxOutput := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edStrVhChange(Sender: TObject);
begin
  if not FLoading then
    FBoltzmann.Vh := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edStrVcChange(Sender: TObject);
begin
  if not FLoading then
    FBoltzmann.Vc := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.cbGpiAssClick(Sender: TObject);
begin
  if not FLoading then
    bUseGpiAss := TCheckbox(Sender).Checked;
  grdGpiAss.Visible := bUseGpiAss;
  grdGpiRow.Visible := bUseGpiAss;
  grdGpiCol.Visible := bUseGpiAss;
  lblGpiAss.Visible := bUseGpiAss;
  lblGpiRow.Visible := bUseGpiAss;
  lblGpiCol.Visible := bUseGpiAss;
end;

procedure TfrmMain.cbStnTargetSaliencyClick(Sender: TObject);
begin
  if not FLoading then
    FStnTargetSaliency := TCheckbox(Sender).Checked;
end;

procedure TfrmMain.edCxNoisePercentChange(Sender: TObject);
begin
  if not FLoading then begin
    CxDefaults.NoisePercent := StrToFloat(TEdit(Sender).Text, FFormatSettings);
    CxDefaults.NoiseProportion := CxDefaults.NoisePercent / 100;
  end;
end;

procedure TfrmMain.edStrNoisePercentChange(Sender: TObject);
begin
  if not FLoading then begin
    StrDefaults.NoisePercent := StrToFloat(TEdit(Sender).Text, FFormatSettings);
    StrDefaults.NoiseProportion := StrDefaults.NoisePercent / 100;
  end;
end;

procedure TfrmMain.edStnNoisePercentChange(Sender: TObject);
begin
  if not FLoading then begin
    StnDefaults.NoisePercent := StrToFloat(TEdit(Sender).Text, FFormatSettings);
    StnDefaults.NoiseProportion := StnDefaults.NoisePercent / 100;
  end;
end;

procedure TfrmMain.edGpiNoisePercentChange(Sender: TObject);
begin
  if not FLoading then begin
    GpiDefaults.NoisePercent := StrToFloat(TEdit(Sender).Text, FFormatSettings);
    GpiDefaults.NoiseProportion := GpiDefaults.NoisePercent / 100;
  end;
end;

procedure TfrmMain.edThNoisePercentChange(Sender: TObject);
begin
  if not FLoading then begin
    ThDefaults.NoisePercent := StrToFloat(TEdit(Sender).Text, FFormatSettings);
    ThDefaults.NoiseProportion := ThDefaults.NoisePercent / 100;
  end;
end;

procedure TfrmMain.edTauCxChange(Sender: TObject);
begin
  if not FLoading then
    CxDefaults.Tau := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edTauStrChange(Sender: TObject);
begin
  if not FLoading then
    StrDefaults.Tau := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edTauStnChange(Sender: TObject);
begin
  if not FLoading then
    StnDefaults.Tau := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edTauGpiChange(Sender: TObject);
begin
  if not FLoading then
    GpiDefaults.Tau := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edTauThChange(Sender: TObject);
begin
  if not FLoading then
    ThDefaults.Tau := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edThresholdCxChange(Sender: TObject);
begin
  if not FLoading then
    CxDefaults.Threshold := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edThresholdStrChange(Sender: TObject);
begin
  if not FLoading then
    StrDefaults.Threshold := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edThresholdStnChange(Sender: TObject);
begin
  if not FLoading then
    StnDefaults.Threshold := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edThresholdGpiChange(Sender: TObject);
begin
  if not FLoading then
    GpiDefaults.Threshold := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edThresholdThChange(Sender: TObject);
begin
  if not FLoading then
    ThDefaults.Threshold := StrToInt(TEdit(Sender).Text);
end;

procedure TfrmMain.edCxMotorToStrMotorChange(Sender: TObject);
begin
  if not FLoading then
    eCxMotorToStrMotor := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edAssociativeRatioChange(Sender: TObject);
begin
  if not FLoading then
    eAssociativeRatio := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edCxMotorToStrAssChange(Sender: TObject);
begin
  if not FLoading then
    eCxMotorToStrAss := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edCxCogToStrCogChange(Sender: TObject);
begin
  if not FLoading then
    eCxCogToStrCog := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edCxCogToStrAssChange(Sender: TObject);
begin
  if not FLoading then
    eCxCogToStrAss := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edCxAssToStrAssChange(Sender: TObject);
begin
  if not FLoading then
    eCxAssToStrAss := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edCxMotorToStnMotorChange(Sender: TObject);
begin
  if not FLoading then
    eCxMotorToStnMotor := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edCxCogToStnCogChange(Sender: TObject);
begin
  if not FLoading then
    eCxCogToStnCog := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edCxToThChange(Sender: TObject);
begin
  if not FLoading then
    eCxToTh := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.cbCxThalamusActiveClick(Sender: TObject);
begin
  if not FLoading then begin
    bCxToTh := TCheckBox(Sender).Checked;
    if bCxToTh then begin
      arrowThCx.ArrowPosition := apBoth;
      arrowThCx2.ArrowPosition := apBoth;
    end
    else begin
      arrowThCx.ArrowPosition := apRight;
      arrowThCx2.ArrowPosition := apRight;
    end;
  end;
end;

procedure TfrmMain.cbIgnoreTonicStnClick(Sender: TObject);
begin
  if not FLoading then
    StnDefaults.IgnoreTonic := TCheckBox(Sender).Checked;
end;

procedure TfrmMain.edStnMotorToGpiMotorChange(Sender: TObject);
begin
  if not FLoading then
    eStnMotorToGpiMotor :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edStnMotorToAssGpiChange(Sender: TObject);
begin
  if not FLoading then
    eStnMotorToGpiAss :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edStnCogToGpiCogChange(Sender: TObject);
begin
  if not FLoading then
    eStnCogToGpiCog :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edStnCogToAssGpiChange(Sender: TObject);
begin
  if not FLoading then
    eStnCogToGpiAss :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edCogStrToGpiChange(Sender: TObject);
begin
  if not FLoading then
    eCogStrToGpi :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edMotorStrToGpiChange(Sender: TObject);
begin
  if not FLoading then
    eMotorStrToGpi :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edAssStrToGpiChange(Sender: TObject);
begin
  if not FLoading then
    eStrAssToGpi :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.cbIgnoreTonicGPiClick(Sender: TObject);
begin
  if not FLoading then
    GpiDefaults.IgnoreTonic := TCheckBox(Sender).Checked;
end;

procedure TfrmMain.edCogGpiToCogThChange(Sender: TObject);
begin
  if not FLoading then
    eCogGPiToCogTh :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edAssGpiToCogThChange(Sender: TObject);
begin
  if not FLoading then
    eAssGPiToCogTh :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edMotorGpiToMotorThChange(Sender: TObject);
begin
  if not FLoading then
    eMotorGPiToMotorTh :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edAssGpiToMotorThChange(Sender: TObject);
begin
  if not FLoading then
    eAssGPiToMotorTh :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.cbIgnoreTonicThClick(Sender: TObject);
begin
  if not FLoading then
    ThDefaults.IgnoreTonic := TCheckBox(Sender).Checked;
end;

procedure TfrmMain.edThMotorToCxMotorChange(Sender: TObject);
begin
  if not FLoading then
    eThMotorToCxMotor :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edThCogToCxCogChange(Sender: TObject);
begin
  if not FLoading then
    eThCogToCxCog :=  StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edSimsPerExperimentChange(Sender: TObject);
begin
  if not FLoading then
    FSimsPerExperiment := StrToInt(TEdit(Sender).Text);
  SetLength(arXlWorkbooks, FSimsPerExperiment);
end;

procedure TfrmMain.edRunsPerSimulationChange(Sender: TObject);
begin
  if not FLoading then
    FRunsPerSimulation := StrToInt(TEdit(Sender).Text);
  SetLength(arTrial, FRunsPerSimulation);
  SetLength(arPairChoices, FRunsPerSimulation);
  SetupPairings;
end;

procedure TfrmMain.cbRobotAttachedClick(Sender: TObject);
begin
  bRobotAttached := cbRobotAttached.Checked;
end;

procedure TfrmMain.cbAutoSaveClick(Sender: TObject);
begin
  if not FLoading then
    bAutoSave := TCheckBox(Sender).Checked;
end;

procedure TfrmMain.ShowTargetScreen;
var i: integer;
    AImage: TImage;
begin
  if cbShowTargets.Checked then begin
    //Show the bitmaps of the targets
    for i := 1 to 4 do begin
      AImage := TImage(FindComponent('im' + IntToStr(i)));
      AImage.Picture.Bitmap := nil;
      TImage(FindComponent('im2_' + IntToStr(i))).Picture.Bitmap := nil;
      AImage := TImage(FindComponent('imD' + IntToStr(i)));
      AImage.Picture.Bitmap := nil;
    end;
    for i := 1 to 4 do begin
      if arTarget[i].Show then begin
        AImage := TImage(FindComponent('im' + IntToStr(arTarget[i].Direction)));
        //image list array is zero based
        ilTargets.GetBitmap(i-1, AImage.Picture.Bitmap);
        ilTargets.GetBitmap(i-1, TImage(FindComponent('im2_' + IntToStr(arTarget[i].Direction))).Picture.Bitmap);
        AImage := TImage(FindComponent('imD' + IntToStr(arTarget[i].Direction)));
        ilTargets.GetBitmap(i-1, AImage.Picture.Bitmap);
        TShape(FindComponent('shpMotor' + IntToStr(arTarget[i].Direction))).Visible := true;
      end;
    end;
    MakeImagesVisible;
  end;
  Application.ProcessMessages;
end;

procedure TfrmMain.DisplayValues(Stage: integer);
var i,j: integer;
    eCxCogActivation,
    eCxMotorActivation,
    eCxAssActivation: real;
    eGpiRow,
    eGpiCol,
    eGpiTotal,
    eStrRow,
    eStrCol: real;
    ASeries: TFastLineSeries;
begin
  lblLoops.Caption := IntToStr(FiLoops);
  if cbShowGraphs.Checked then begin
    chrtStr.BottomAxis.Maximum := FiLoops;
    chrtCx.BottomAxis.Maximum := FiLoops;
  end;

  if cbShowActivations.Checked then begin
    //turn off the display of activations to speed up long simulations 
    for i := 0 to 3 do begin
      if bShowWeights then begin
        //show the weighted cortical activation. i.e how much excitation will actually go to striatum
        eCxCogActivation := arCx[COL_COGNITIVE, i+1] * arConnection[COL_COGNITIVE, i+1].Normalized;
        eCxMotorActivation := arCx[i+1, ROW_MOTOR] * arConnection[i+1, ROW_MOTOR].Normalized;
      end
      else begin
        eCxCogActivation := arCx[COL_COGNITIVE, i+1];
        eCxMotorActivation := arCx[i+1, ROW_MOTOR];
      end;
      grdCxCog.Cells[0,i] := FloatToStrF(eCxCogActivation, ffFixed, 4, 2, FFormatSettings);
      grdCxMotor.Cells[i,0] := FloatToStrF(eCxMotorActivation, ffFixed, 4, 2, FFormatSettings);
      if cbShowGraphs.Checked then begin
        //The 5 graphs on the graphs tab
        ASeries := TFastLineSeries(FindComponent('srsCxCog2_'+IntToStr(i+1)));
        ASeries.AddXY(FiLoops, arCx[COL_COGNITIVE,i+1]);
        ASeries := TFastLineSeries(FindComponent('srsCxMotor2_'+IntToStr(i+1)));
        ASeries.AddXY(FiLoops, arCx[i+1,ROW_MOTOR]);
        ASeries := TFastLineSeries(FindComponent('srsStrCog2_'+IntToStr(i+1)));
        ASeries.AddXY(FiLoops, arStr[COL_COGNITIVE,i+1]);
        ASeries := TFastLineSeries(FindComponent('srsStrMotor2_'+IntToStr(i+1)));
        ASeries.AddXY(FiLoops, arStr[i+1,ROW_MOTOR]);
        ASeries := TFastLineSeries(FindComponent('srsStnCog2_'+IntToStr(i+1)));
        ASeries.AddXY(FiLoops, arStn[COL_COGNITIVE,i+1]);
        ASeries := TFastLineSeries(FindComponent('srsStnMotor2_'+IntToStr(i+1)));
        ASeries.AddXY(FiLoops, arStn[i+1,ROW_MOTOR]);
        ASeries := TFastLineSeries(FindComponent('srsGpiCog2_'+IntToStr(i+1)));
        ASeries.AddXY(FiLoops, arGpi[COL_COGNITIVE,i+1]);
        ASeries := TFastLineSeries(FindComponent('srsGpiMotor2_'+IntToStr(i+1)));
        ASeries.AddXY(FiLoops, arGpi[i+1,ROW_MOTOR]);
        ASeries := TFastLineSeries(FindComponent('srsThCog2_'+IntToStr(i+1)));
        ASeries.AddXY(FiLoops, arTh[COL_COGNITIVE,i+1]);
        ASeries := TFastLineSeries(FindComponent('srsThMotor2_'+IntToStr(i+1)));
        ASeries.AddXY(FiLoops, arTh[i+1,ROW_MOTOR]);
        //only show graph lines for the target and directions shown to cut down on the clutter
        if arTarget[i+1].Show then begin
          ASeries := TFastLineSeries(FindComponent('srsCxCog'+IntToStr(i+1)));
          ASeries.AddXY(FiLoops, eCxCogActivation);
        end;
        if arDirectionShown[i+1] then begin
          ASeries := TFastLineSeries(FindComponent('srsCxMotor'+IntToStr(i+1)));
          ASeries.AddXY(FiLoops, eCxMotorActivation);
        end;
      end;

      //show the current target values as estimated by the number of times that they have been rewarded
      grdTargetValue.Cells[0,i] := FloatToStr(arTarget[i+1].ExpectedValue, FFormatSettings);

      //show the cognitive and motor activations in each structure
      grdStrMotor.Cells[i,0] := FloatToStrF(arStr[i+1, ROW_MOTOR], ffFixed, 4, 2, FFormatSettings);
      grdStrCog.Cells[0,i] := FloatToStrF(arStr[COL_COGNITIVE, i+1], ffFixed, 4, 2, FFormatSettings);
      grdStnMotor.Cells[i,0] := FloatToStrF(arStn[i+1, ROW_MOTOR], ffFixed, 4, 2, FFormatSettings);
      grdStnCog.Cells[0,i] := FloatToStrF(arStn[COL_COGNITIVE, i+1], ffFixed, 4, 2, FFormatSettings);
      grdGpiMotor.Cells[i,0] := FloatToStrF(arGpi[i+1, ROW_MOTOR], ffFixed, 4, 2, FFormatSettings);
      grdGpiCog.Cells[0,i] := FloatToStrF(arGpi[COL_COGNITIVE, i+1], ffFixed, 4, 2, FFormatSettings);
      grdThMotor.Cells[i,0] := FloatToStrF(arTh[i+1, ROW_MOTOR], ffFixed, 4, 2, FFormatSettings);
      grdThCog.Cells[0,i] := FloatToStrF(arTh[COL_COGNITIVE, i+1], ffFixed, 4, 2, FFormatSettings);


      for j := 0 to 3 do begin
        if bShowWeights then
          eCxAssActivation := arCx[i+1, j+1] * arConnection[i+1, j+1].Normalized
        else
          eCxAssActivation := arCx[i+1, j+1];
        //show associative activations in those structures that have an associative area
        grdCxAss.Cells[i,j] := FloatToStrF(eCxAssActivation, ffFixed, 4, 2, FFormatSettings);
        grdStrAss.Cells[i,j] := FloatToStrF(arStr[i+1, j+1], ffFixed, 4, 2, FFormatSettings);
        grdGpiAss.Cells[i,j] := FloatToStrF(arGpi[i+1, j+1], ffFixed, 4, 2, FFormatSettings);
      end;
    end;

    //Display the row and column totals for the striatum as these are what feeds back to the cortex
    for i := 1 to 4 do begin
      eStrRow := 0;
      eStrCol := 0;
      for j := 0 to 4 do begin
        eStrRow := eStrRow + arStr[j,i];
        eStrCol := eStrCol + arStr[i,j];
      end;
      grdStrRow.Cells[0,i-1] := FloatToStrF(eStrRow, ffFixed, 4, 2, FFormatSettings);
      grdStrCol.Cells[i-1,0] := FloatToStrF(eStrCol, ffFixed, 4, 2, FFormatSettings);
      if cbShowGraphs.Checked then begin
        if arTarget[i].Show then begin
          ASeries := TFastLineSeries(FindComponent('srsStrRow'+IntToStr(i)));
          ASeries.AddXY(FiLoops, eStrRow);
        end;
        if arDirectionShown[i] then begin
          ASeries := TFastLineSeries(FindComponent('srsStrCol'+IntToStr(i)));
          ASeries.AddXY(FiLoops, eStrCol);
        end;
      end;
    end;

    for i := 1 to 4 do begin
      eGpiRow := 0;
      eGpiCol := 0;
      for j := 0 to 4 do begin
        eGpiRow := eGpiRow + arGpi[j,i];
        eGPiCol := eGpiCol + arGpi[i,j];
      end;
      grdGpiRow.Cells[0,i-1] := FloatToStrF(eGpiRow, ffFixed, 4, 2, FFormatSettings);
      grdGpiCol.Cells[i-1,0] := FloatToStrF(eGpiCol, ffFixed, 4, 2, FFormatSettings);
    end;

    if cbShowGraphs.Checked then begin
      //show the total GPi activation
      eGpiTotal := 0;
      if bUseGpiAss then begin
        for i := 0 to 4 do begin
          for j := 0 to 4 do begin
            if not ((i = 0) and (j = 0)) then begin
              eGpiTotal := eGpiTotal + arGPi[i,j];
            end;
          end;
        end;
      end
      else begin
        for i := 1 to 4 do begin
          eGpiTotal := eGpiTotal + arGPi[COL_COGNITIVE,i];
          eGpiTotal := eGpiTotal + arGPi[i,ROW_MOTOR];
        end;
      end;
      srsGpiTotal.AddXY(FiLoops, eGPiTotal);
    end;

    for i := 0 to 5 do
      grdContextValues.Cells[0,i] := FloatToStr(arContext[i+1].ExpectedValue, FFormatSettings);

    DisplayWeights;
  end;

  Application.ProcessMessages;
end;

procedure TfrmMain.DisplayWeights;
var i, j: integer;
begin
  for i := 0 to 3 do begin
    //show the current cognitive and motor corticostriatal weights and how much they changed on the
    //most recent trial
    if rgWeightsDisplay.ItemIndex = 0 then begin
      grdCogWeights.Cells[0,i] := FloatToStrF(arConnection[COL_COGNITIVE, i+1].Real, ffFixed, 5, 2, FFormatSettings);
      grdMotorWeights.Cells[i,0] := FloatToStrF(arConnection[i+1, ROW_MOTOR].Real, ffFixed, 5, 2, FFormatSettings);
    end
    else begin
      grdCogWeights.Cells[0,i] := FloatToStrF(arConnection[COL_COGNITIVE, i+1].Normalized, ffFixed, 5, 2, FFormatSettings);
      grdMotorWeights.Cells[i,0] := FloatToStrF(arConnection[i+1, ROW_MOTOR].Normalized, ffFixed, 5, 2, FFormatSettings);
    end;
    grdMotorDeltaW.Cells[i,0] := FloatToStrF(arDeltaW[i+1, ROW_MOTOR], ffFixed, 3,2,FFormatSettings);
    grdCogDeltaW.Cells[0,i] := FloatToStrF(arDeltaW[COL_COGNITIVE, i+1],  ffFixed, 3,2,FFormatSettings);
    for j := 0 to 3 do begin
      case rgAssWeights.ItemIndex of
        0: begin
          if rgWeightsDisplay.ItemIndex = 0 then
            grdAssWeights.Cells[i,j] := FloatToStrF(arCogConnection[i+1, j+1].Real, ffFixed, 5, 2, FFormatSettings)
          else
            grdAssWeights.Cells[i,j] := FloatToStrF(arCogConnection[i+1, j+1].Normalized, ffFixed, 5, 2, FFormatSettings);
          grdAssDeltaW.Cells[i,j] := FloatToStrF(arCogDeltaW[i+1, j+1], ffFixed, 3,2, FFormatSettings);
        end;
        1: begin
          if rgWeightsDisplay.ItemIndex = 0 then
            grdAssWeights.Cells[i,j] := FloatToStrF(arMotorConnection[i+1, j+1].Real, ffFixed, 5, 2, FFormatSettings)
          else
            grdAssWeights.Cells[i,j] := FloatToStrF(arMotorConnection[i+1, j+1].Normalized, ffFixed, 5, 2, FFormatSettings);
          grdAssDeltaW.Cells[i,j] := FloatToStrF(arMotorDeltaW[i+1, j+1], ffFixed, 3,2, FFormatSettings);
        end;
        2: begin
          if rgWeightsDisplay.ItemIndex = 0 then
            grdAssWeights.Cells[i,j] := FloatToStrF(arConnection[i+1, j+1].Real, ffFixed, 5, 2, FFormatSettings)
          else
            grdAssWeights.Cells[i,j] := FloatToStrF(arConnection[i+1, j+1].Normalized, ffFixed, 5, 2, FFormatSettings);
          grdAssDeltaW.Cells[i,j] := FloatToStrF(arDeltaW[i+1, j+1], ffFixed, 3,2, FFormatSettings);
        end;
      end;
    end;
  end;
end;

{
Go round the loop and set the activations for each structure
}
procedure TfrmMain.DoALoop;
begin
  SetCognitiveStriatalActivation;
  SetMotorStriatalActivation;
  SetAssociativeStriatalActivation;

  SetCognitiveStnActivation;
  SetMotorStnActivation;

  SetCognitiveGpiActivation;
  SetMotorGPiActivation;
  if bUseGpiAss then
    SetAssociativeGPiActivation;

  SetCognitiveThalamusActivation;
  SetMotorThalamusActivation;

  SetCognitiveCortexActivation;
  SetMotorCortexActivation;
  SetAssociationCortexActivation;
end;

{
Frontal cortex projects cell to cell to cognitive striatum
In the movement loop, the striatum has persisitent activation
The striatum is the only place where the synapse strengths
are changed during learning. Therefore it is the only
place that the weights are used in calculation of the activation

In version 4 we start to use a sigmoidal transfer function from cortex to striatum
to mimic the non-linear transfer function of the MSN
}
procedure TfrmMain.SetCognitiveStriatalActivation;
var i: integer;
    eM,
    eDeltaM,
    eISyn: real;
begin
  for i := 1 to 4 do begin
    eM := arStr[COL_COGNITIVE, i];
    eISyn := arCx[COL_COGNITIVE, i] * eCxCogToStrCog * arConnection[COL_COGNITIVE, i].Normalized;
    if bUseTransferFuction then
      eISyn := FBoltzmann.GetOutput(eISyn);
    eISyn := eISyn + GetNoise(0, eISyn * StrDefaults.NoiseProportion);
    eDeltaM := ((-1 * eM) + eISyn - StrDefaults.Threshold) / StrDefaults.Tau;
    arStr[COL_COGNITIVE, i] := eM + eDeltaM;
    if arStr[COL_COGNITIVE, i] < 0 then
      arStr[COL_COGNITIVE, i] := 0;
  end;
end;

{
Motor Striatum has the same activation as motor cortex
In the movement phase associative striatum has persistent
values from activation in the previous loop. The motor cortex activation is
added to this only in the cells that had the persistent activation
}
procedure TfrmMain.SetMotorStriatalActivation;
var i: integer;
    eM,
    eDeltaM,
    eISyn: real;
begin
  for i := 1 to 4 do begin
    eM := arStr[i, ROW_MOTOR];
    eISyn := arCx[i, ROW_MOTOR] * eCxMotorToStrMotor * arConnection[i, ROW_MOTOR].Normalized;
    if bUseTransferFuction then
      eISyn := FBoltzmann.GetOutput(eISyn);
    eISyn := eISyn + GetNoise(0, eISyn * StrDefaults.NoiseProportion);
    eDeltaM := ((-1 * eM) + eISyn - StrDefaults.Threshold) / StrDefaults.Tau;
    arStr[i, ROW_MOTOR] := eM + eDeltaM;
    if arStr[i, ROW_MOTOR] < 0 then
      arStr[i, ROW_MOTOR] := 0;
  end;
end;

{
Associative striatum is the sum of each corresponding associative cortex cell
plus the frontal cortex activation of the corresponding row plus the
motor cortex activation of the corresponding column (this is zero in the
decision phase)plus the persistent associative striatal activation from the
decision phase (this is also zero in the decision phase)
}
procedure TfrmMain.SetAssociativeStriatalActivation;
var i,j: integer;
    eM,
    eDeltaM,
    eISyn: real;
begin
  for i := ROW_ASSOCIATIVE1 to ROW_ASSOCIATIVE4 do begin
    for j := COL_ASSOCIATIVE1 to COL_ASSOCIATIVE4 do begin
      eM := arStr[i, j];
      eISyn := (arCx[COL_COGNITIVE, j] * eCxCogToStrAss * arCogConnection[i, j].Normalized)
                + (arCx[i,ROW_MOTOR] * eCxMotorToStrAss * arMotorConnection[i, j].Normalized)
                + (arCx[i,j] * eCxAssToStrAss * arConnection[i, j].Normalized);
      if bUseTransferFuction then
        eISyn := FBoltzmann.GetOutput(eISyn);
      eISyn := eISyn + GetNoise(0, eISyn * StrDefaults.NoiseProportion);
      eDeltaM := ((-1 * eM) + eISyn - StrDefaults.Threshold) / StrDefaults.Tau;
      arStr[i, j] := eM + eDeltaM;
      if arStr[i,j] < 0 then
        arStr[i,j] := 0;
    end;
  end;

end;

//Cognitive STN has a one to one connection to frontal cortex
procedure TfrmMain.SetCognitiveStnActivation;
var i: integer;
    eM,
    eDeltaM,
    eISyn: real;
begin
  for i := 1 to 4 do begin
    eM := arStn[COL_COGNITIVE, i];
    eISyn := arCx[COL_COGNITIVE, i] * eCxCogToStnCog ;
    eISyn := eISyn + GetNoise(0, eISyn * StnDefaults.NoiseProportion);
    eDeltaM := ((-1 * eM) + eISyn - StnDefaults.Threshold) / StnDefaults.Tau;
    arStn[COL_COGNITIVE, i] := eM + eDeltaM;
  end;
end;

//Motor STN has same activation as motor cortex
procedure TfrmMain.SetMotorStnActivation;
var i: integer;
    eM,
    eDeltaM,
    eISyn: real;
begin
  for i := 1 to 4 do begin
    eM := arStn[i, ROW_MOTOR];
    eISyn := arCx[i, ROW_MOTOR] * eCxMotorToStnMotor ;
    eISyn := eISyn + GetNoise(0, eISyn * StnDefaults.NoiseProportion);
    eDeltaM := ((-1 * eM) + eISyn - StnDefaults.Threshold) / StnDefaults.Tau;
    arStn[i, ROW_MOTOR] := eM + eDeltaM;
  end;
end;

{
Cognitive STN is summed and the summed value is excitatory to each cognitive
GPi cell and each associative GPi row
}
function TfrmMain.GetCognitiveStnActivation: double;
var i: integer;
begin
  result := 0;
  for i := 1 to 4 do
    result := result + arStn[COL_COGNITIVE, i];
end;

{
Motor STN is summed and the summed value is excitatory to each motor
GPi cell and each associative GPi column
}
function TfrmMain.GetMotorStnActivation: double;
var i: integer;
begin
  result := 0;
  for i := 1 to 4 do
    result := result +  arStn[i, ROW_MOTOR];
end;

{
Str to GPi is inhibitory, cell to cell. STN to GPi is excitatory.
Cognitive GPi gets excitation from all cognitive STN and inhibition from a
corresponding cognitive striatal cell plus a row of associative striatum
}
procedure TfrmMain.SetCognitiveGPiActivation;
var i, j: integer;
    eAssStrInhibition,
    eCogStnExcitation: double;
    eM,
    eDeltaM,
    eISyn: real;
begin
  eCogStnExcitation := GetCognitiveStnActivation * eStnCogToGPiCog;
  for i := 1 to 4 do begin
    eAssStrInhibition := 0;
    for j := 1 to 4 do begin
      //add the cell in the congitive striatum to those in the same associative striatal row
      eAssStrInhibition := eAssStrInhibition + arStr[j,i];
    end;
    eAssStrInhibition := eAssStrInhibition * eStrAssToGpi;

    eM := arGpi[COL_COGNITIVE, i];
    eISyn := eCogStnExcitation - (arStr[COL_COGNITIVE, i] * eCogStrToGpi) - eAssStrInhibition;
    eISyn := eISyn + GetNoise(0, eISyn * GpiDefaults.NoiseProportion);
    eDeltaM := ((-1 * eM) + eISyn - GpiDefaults.Threshold) / GpiDefaults.Tau;
    arGpi[COL_COGNITIVE, i] := eM + eDeltaM;
    if arGpi[COL_COGNITIVE, i] < 0 then
      arGpi[COL_COGNITIVE, i] := 0;
  end;
end;

{
Motor STN -> Motor GPi. Each motor STN cell innervates all motor STN cells, so all motor GPi gets
the same input from STN
Keep motor GPi at tonic level during the decision phase
Motor Str is inhibitory to motor GPi and the only input
}
procedure TfrmMain.SetMotorGpiActivation;
var i, j: integer;
    eAssStrInhibition,
    eMotorStnExcitation: double;
    eM,
    eDeltaM,
    eISyn: real;
begin
    eMotorStnExcitation :=  GetMotorStnActivation * eStnMotorToGpiMotor;
  for i := 1 to 4 do begin
    eAssStrInhibition := 0;
    for j := 1 to 4 do begin
      //add the cells in the associative striatal column
      eAssStrInhibition := eAssStrInhibition + arStr[i,j];
    end;
    eAssStrInhibition := eAssStrInhibition * eStrAssToGpi;

    eM := arGpi[i, ROW_MOTOR];
    eISyn := eMotorStnExcitation - (arStr[i,ROW_MOTOR] * eMotorStrToGpi) - eAssStrInhibition;
    eISyn := eISyn + GetNoise(0, eISyn * GpiDefaults.NoiseProportion);
    eDeltaM := ((-1 * eM) + eISyn - GpiDefaults.Threshold) / GpiDefaults.Tau;
    arGpi[i, ROW_MOTOR] := eM + eDeltaM;
    if arGpi[i, ROW_MOTOR] < 0 then
      arGpi[i, ROW_MOTOR] := 0;
  end;
end;

{
Associative GPi gets excitation from all cognitive and motor STN and inhibition
from corresponding associative Striatum cell
}
procedure TfrmMain.SetAssociativeGPiActivation;
var i,j: integer;
    eStnCogExcitation,
    eMotorStnExcitation: double;
    eM,
    eDeltaM,
    eISyn: real;
begin
  eStnCogExcitation := GetCognitiveStnActivation * eStnCogToGpiAss;
  eMotorStnExcitation :=  GetMotorStnActivation * eStnMotorToGpiAss;
  for i := 1 to 4 do begin
    for j := 1 to 4 do begin
      eM := arGpi[i, j];
      eISyn := eStnCogExcitation + eMotorStnExcitation - (arStr[i,j] * eStrAssToGpi);
      eISyn := eISyn + GetNoise(0, eISyn * GpiDefaults.NoiseProportion);
      eDeltaM := ((-1 * eM) + eISyn - GpiDefaults.Threshold) / GpiDefaults.Tau;
      arGpi[i, j] := eM + eDeltaM;
      if arGPi[i,j] < 0 then
        arGPi[i,j] := 0;
    end;
  end;
end;

{
Cognitive thalamus is inhibited by correspondig cognitive GPi cell plus
a proportion of the corresponding associative GPi row - if there is an associative GPi
}
procedure TfrmMain.SetCognitiveThalamusActivation;
var i,j: integer;
    eAssGpiInhibition,
    eTotalGpiInhibition,
    eCxExcitation: double;
    eM,
    eDeltaM,
    eISyn: real;
begin
  for i := 1 to 4 do begin
    eTotalGpiInhibition := arGpi[COL_COGNITIVE, i]* eCogGPiToCogTh;
    if bUseGpiAss then begin
      eAssGpiInhibition := 0;
      for j := 1 to 4 do begin
        //add the cells in the associative GPi row
        eAssGpiInhibition := eAssGpiInhibition + arGpi[j,i];
      end;
      eAssGpiInhibition := eAssGpiInhibition * eAssGPiToCogTh;
      eTotalGpiInhibition := eTotalGpiInhibition + eAssGpiInhibition;
    end;

    eCxExcitation := 0;
    if bCxToTh then
      eCxExcitation := arCx[COL_COGNITIVE, i] * eCxToTh;

    eM := arTh[COL_COGNITIVE, i];
    eISyn := eCxExcitation - eTotalGpiInhibition;
    eISyn := eISyn + GetNoise(0, eISyn * ThDefaults.NoiseProportion);
    eDeltaM := ((-1 * eM) + eISyn - ThDefaults.Threshold) / ThDefaults.Tau;
    arTh[COL_COGNITIVE, i] := eM + eDeltaM;
    if arTh[COL_COGNITIVE, i] < 0 then
      arTh[COL_COGNITIVE, i] := 0;
  end;
end;

{
Associative GPi to motor thalamus is inhibition column to cell
There is no input from motor GPi during the decision phase
No associative GPi in this version, so just a 1:1 inhibition
}
procedure TfrmMain.SetMotorThalamusActivation;
var i,j: integer;
    eAssGpiInhibition,
    eTotalGpiInhibition,
    eCxExcitation: double;
    eM,
    eDeltaM,
    eISyn: real;
begin
  for i := 1 to 4 do begin
    eTotalGpiInhibition := arGpi[i, ROW_MOTOR] * eMotorGPiToMotorTh;
    if bUseGpiAss then begin
      eAssGpiInhibition := 0;
      for j := 1 to 4 do begin
        //add the cells in the associative GPi column
        eAssGpiInhibition := eAssGpiInhibition + arGpi[i,j];
      end;
      eAssGpiInhibition := eAssGpiInhibition * eAssGPiToMotorTh;
      eTotalGpiInhibition := eTotalGpiInhibition + eAssGpiInhibition;
    end;

    eCxExcitation := 0;
    if bCxToTh then
      eCxExcitation := arCx[i, ROW_MOTOR] * eCxToTh;

    eM := arTh[i, ROW_MOTOR];
    eISyn := eCxExcitation - eTotalGpiInhibition;
    eISyn := eISyn + GetNoise(0, eISyn * ThDefaults.NoiseProportion);
    eDeltaM := ((-1 * eM) + eISyn - ThDefaults.Threshold) / ThDefaults.Tau;
    arTh[i, ROW_MOTOR] := eM + eDeltaM;
    if arTh[i, ROW_MOTOR] < 0 then
      arTh[i, ROW_MOTOR] := 0;
  end;
end;

{
Cognitive cortex gets excitatory input from the cognitive thalamus.
If the target for a cell is showing, the baseline activation level is added to
the thalamic input along with the value of the target.
The target value represents its saliency.
The baseline level and the target value are separated as there is parameter to
say whether the target saliency should be passed on to the STN
}
procedure TfrmMain.SetCognitiveCortexActivation;
var i: integer;
    eM,
    eDeltaM,
    eISyn,
    eIExt: real;
begin
  for i := 1 to 4 do begin
    eM := arCx[COL_COGNITIVE, i];
    eISyn := arTh[COL_COGNITIVE, i] * eThCogToCxCog;
    eISyn := eISyn + GetNoise(0, eISyn * CxDefaults.NoiseProportion);

    eIExt := 0;
    if arTarget[i].Show then
      //Don't show the targets until a set number of loops have been done
      //This gives the system time to settle to m(infinity)
      if FiLoops >= FiLoopsBeforeTargets then
        eIExt := eBaselineCogLevel + arTarget[i].Salience;
    //Noise is added to the external inputs
    eIExt := eIExt + GetNoise(0, eIExt * CxDefaults.NoiseProportion);

    eDeltaM := ((-1 * eM) + eISyn + eIExt - CxDefaults.Threshold) / CxDefaults.Tau;
    arCx[COL_COGNITIVE, i] := eM + eDeltaM;// + GetNoise(0, eM * CxDefaults.NoiseProportion);
    if arCx[COL_COGNITIVE, i] < 0 then
      arCx[COL_COGNITIVE, i] := 0;
  end;
end;

{
Motor cortex gets input from motor thalamus.
In addition, if a target is shown in a certain direction, that cell also gets
the baseline motor excitation added.
}
procedure TfrmMain.SetMotorCortexActivation;
var i: integer;
    eM,
    eDeltaM,
    eISyn,
    eIExt: real;
begin
  for i := 1 to 4 do begin
    eM := arCx[i, ROW_MOTOR];
    eISyn := arTh[i, ROW_MOTOR] * eThCogToCxCog;
    eISyn := eISyn + GetNoise(0, eISyn * CxDefaults.NoiseProportion);

    eIExt := 0;
    if arDirectionShown[i] then
      //for directions that have targets shown, put in the baseline motor level
      if FiLoops >= FiLoopsBeforeTargets then
        eIExt := eBaselineMotorLevel;
    eIExt := eIExt + GetNoise(0, eIExt * CxDefaults.NoiseProportion);

    eDeltaM := ((-1 * eM) + eISyn + eIExt - CxDefaults.Threshold) / CxDefaults.Tau;
    arCx[i, ROW_MOTOR] := eM + eDeltaM;// + GetNoise(0, eM * CxDefaults.NoiseProportion);
    if arCx[i, ROW_MOTOR] < 0 then
      arCx[i, ROW_MOTOR] := 0;
  end;
end;

{
Association cortex gets no feedback from corticostriatal loops, so the baseline
level put in stays there for the whole trial. Add noise to that baseline level
to get some variation
Only the 2 cells that are a conflation of the targets shown and their directions
are activated
}
procedure TfrmMain.SetAssociationCortexActivation;
var i,j: integer;
    eM,
    eDeltaM,
    eISyn,
    eIExt: real;
begin
  for i := ROW_ASSOCIATIVE1 to ROW_ASSOCIATIVE4 do begin
    for j := COL_ASSOCIATIVE1 to COL_ASSOCIATIVE4 do begin
      eM := arCx[i,j];
      eISyn := 0; //no synaptic input as the association loop is open
      eIExt := 0;
      if arTarget[j].Show and (arTarget[j].Direction = i) then
        if FiLoops >= FiLoopsBeforeTargets then
          eIExt := eBaselineAssociationLevel;
      eDeltaM := ((-1 * eM) + eISyn + eIExt - CxDefaults.Threshold) / CxDefaults.Tau;
      eDeltaM := eDeltaM + GetNoise(0, eDeltaM * CxDefaults.NoiseProportion);
      arCx[i,j] := eM + eDeltaM + GetNoise(0, eM * CxDefaults.NoiseProportion);
      if arCx[i,j] < 0 then
        arCx[i,j] := 0;
    end;
  end;
end;

//Noise is added as a Gaussian distribution
function TfrmMain.GetNoise(Mean, SD: double): double;
begin
 result := RandG(Mean, SD);
end;

{
Set all weights back to the starting values and display the starting weights in the grids
}
procedure TfrmMain.btnReinitializeClick(Sender: TObject);
begin
  InitializeWeights;
  ZeroGridCells;
  DisplayValues(1);
end;

{
Only need to start Excel once, although it does not cause a problem calling this
when Excel is already open
}
function TfrmMain.StartExcel: boolean;
begin
  result := true;

  // By using GetActiveOleObject, you use an instance of Excel that's already running,
  // if there is one.
  try
    FXLApp := GetActiveOleObject('Excel.Application');
  except
    try
      // If no instance of Excel is running, try to Create a new Excel Object
      FXLApp := CreateOleObject('Excel.Application');
    except
      ShowMessage('Cannot start Excel/Excel not installed ?');
      result := false;
      Exit;
    end;
  end;
  FXLApp.Visible := true;
end;

{
For each simulation, the basic file directory and name remains the same, with just
the number of the simulation added to the file name. So set the basic save location
(directory and basic part of file name) at the start in sSaveLocation
}
procedure TfrmMain.SetSaveLocation;
var sDir,
    sFileName,
    sDate: string;
begin
  sDir := ExtractFilePath(Application.ExeName)+'Results\'+edSaveDirectory.Text;
  if not DirectoryExists(sDir) then try
  //Doesn't work if the new directory is more than one level below results
  //but I can't be bothered to code for this
    CreateDir(sDir);
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
  if sDir[length(sDir)] <> '\' then
    sDir := sDir + '\';

//  sDate := DateToStr(Now, FFormatSettings);
  if cbAddDate.Checked then
    sDate := DateAsString;
  sFileName := sDate + edFileName.Text + '_';

  sSaveLocation := sDir + sFileName;
end;

{
To make analysis easier when multiple simulattions are run, keep a running record
of the averages and SDs of all the variables.

FIXME: not "under 50 trials"-proof !
}
procedure TfrmMain.CreateAveragesWorkbook;
var bSent: boolean;
    i,j,
    iCountPairings: integer;
begin
  //Don't bother with an averages workbook when you are only doing a single simulation
  if FSimsPerExperiment > 1 then begin
    Screen.Cursor := crHourglass;
    Application.ProcessMessages;
    try
      FXLApp.Workbooks.Add(ExtractFilePath(Application.ExeName)+xlTEMPLATE_AVERAGES);
      FXLAveragesWorkbook := FXLApp.ActiveWorkbook;
      if cbAutoSave.Checked then try
        FXLAveragesWorkbook.SaveAs(sSaveLocation + 'Averages.xls');
      except
        on E: Exception do begin
          ShowMessage(E.Message);
          EXIT;
        end;
      end;

      bSent := false;
      while not bSent do try
        //put the trial numbers on each worksheet. Can't do this in the template as the number of trials
        //changes from simulation to simulationi
        FExcelWorksheet := 'Cognitive loops';
        FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
        for i := 1 to FRunsPerSimulation do
          FXLWorksheet.Cells[i + 1, 1] := i;
        for i := 1 to FSimsPerExperiment do
          FXLWorksheet.Cells[1,i+1] := 'Run' + IntToStr(i);
        FXLWorksheet.Cells[1,FSimsPerExperiment+2] := 'Average';
        FXLWorksheet.Cells[1,FSimsPerExperiment+3] := 'SD';
        FXLWorksheet.Cells[1,FSimsPerExperiment+4] := 'SErr';

        FExcelWorksheet := 'Motor loops';
        FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
        for i := 1 to FRunsPerSimulation do
          FXLWorksheet.Cells[i + 1, 1] := i;
        for i := 1 to FSimsPerExperiment do
          FXLWorksheet.Cells[1,i+1] := 'Run' + IntToStr(i);
        FXLWorksheet.Cells[1,FSimsPerExperiment+2] := 'Average';
        FXLWorksheet.Cells[1,FSimsPerExperiment+3] := 'SD';
        FXLWorksheet.Cells[1,FSimsPerExperiment+4] := 'SErr';

        FExcelWorksheet := 'Total loops';
        FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
        for i := 1 to FRunsPerSimulation do
          FXLWorksheet.Cells[i + 1, 1] := i;
        for i := 1 to FSimsPerExperiment do
          FXLWorksheet.Cells[1,i+1] := 'Run' + IntToStr(i);
        FXLWorksheet.Cells[1,FSimsPerExperiment+2] := 'Average';
        FXLWorksheet.Cells[1,FSimsPerExperiment+3] := 'SD';
        FXLWorksheet.Cells[1,FSimsPerExperiment+4] := 'SErr';

        FXLWorksheet := FXLAveragesWorkbook.Worksheets['Outcomes'];
        for i := 1 to FRunsPerSimulation do
          WriteXLCell(FXLWorksheet, i + 1, 1, i);
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+3,1,'Average');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+4,1,'Last 50');
        //Averages at the bottoms of the columns for successful, optimum and rewarded
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+3,2,'=AVERAGE(B2:B' + IntToStr(FRunsPerSimulation+1) + ')');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+3,3,'=AVERAGE(C2:C' + IntToStr(FRunsPerSimulation+1) + ')');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+3,4,'=AVERAGE(D2:D' + IntToStr(FRunsPerSimulation+1) + ')');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+3,5,'=AVERAGE(E2:E' + IntToStr(FRunsPerSimulation+1) + ')');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+3,6,'=AVERAGE(F2:F' + IntToStr(FRunsPerSimulation+1) + ')');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+3,7,'=AVERAGE(G2:G' + IntToStr(FRunsPerSimulation+1) + ')');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+4,2,'=AVERAGE(B' + IntToStr(FRunsPerSimulation-48) + ':B' + IntToStr(FRunsPerSimulation+1) + ')');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+4,3,'=AVERAGE(C' + IntToStr(FRunsPerSimulation-48) + ':C' + IntToStr(FRunsPerSimulation+1) + ')');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+4,4,'=AVERAGE(D' + IntToStr(FRunsPerSimulation-48) + ':D' + IntToStr(FRunsPerSimulation+1) + ')');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+4,5,'=AVERAGE(E' + IntToStr(FRunsPerSimulation-48) + ':E' + IntToStr(FRunsPerSimulation+1) + ')');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+4,6,'=AVERAGE(F' + IntToStr(FRunsPerSimulation-48) + ':F' + IntToStr(FRunsPerSimulation+1) + ')');
        WriteXlCell(FXLWorksheet, FRunsPerSimulation+4,7,'=AVERAGE(G' + IntToStr(FRunsPerSimulation-48) + ':G' + IntToStr(FRunsPerSimulation+1) + ')');

        WriteXLCell(FXLWorksheet, FSimsPerExperiment+4, 9, 'Last 50');
        for i := 1 to FSimsPerExperiment do begin
          WriteXLCell(FXLWorksheet, i+1, 9, i);
          WriteXLCell(FXLWorksheet, i+FSimsPerExperiment+4, 9, i);
        end;
        //Averages at the bottom of the columns for how often target and direction matched, how often
        //each target and each direction were chosen
        WriteXLCell(FXLWorksheet, FSimsPerExperiment + 3, 10, '=AVERAGE(J2:J' + IntToStr(FSimsPerExperiment+1) + ')');
        WriteXLCell(FXLWorksheet, FSimsPerExperiment + 3, 11, '=AVERAGE(K2:K' + IntToStr(FSimsPerExperiment+1) + ')');
        WriteXLCell(FXLWorksheet, FSimsPerExperiment + 3, 12, '=AVERAGE(L2:L' + IntToStr(FSimsPerExperiment+1) + ')');
        WriteXLCell(FXLWorksheet, FSimsPerExperiment + 3, 13, '=AVERAGE(M2:M' + IntToStr(FSimsPerExperiment+1) + ')');
        WriteXLCell(FXLWorksheet, FSimsPerExperiment + 3, 14, '=AVERAGE(N2:N' + IntToStr(FSimsPerExperiment+1) + ')');
        WriteXLCell(FXLWorksheet, FSimsPerExperiment + 3, 15, '=AVERAGE(O2:O' + IntToStr(FSimsPerExperiment+1) + ')');
        WriteXLCell(FXLWorksheet, FSimsPerExperiment + 3, 16, '=AVERAGE(P2:P' + IntToStr(FSimsPerExperiment+1) + ')');
        WriteXLCell(FXLWorksheet, FSimsPerExperiment + 3, 17, '=AVERAGE(Q2:Q' + IntToStr(FSimsPerExperiment+1) + ')');
        WriteXLCell(FXLWorksheet, FSimsPerExperiment + 3, 18, '=AVERAGE(R2:R' + IntToStr(FSimsPerExperiment+1) + ')');
        WriteXLCell(FXLWorksheet, FSimsPerExperiment + 3, 19, '=AVERAGE(S2:S' + IntToStr(FSimsPerExperiment+1) + ')');
        WriteXLCell(FXLWorksheet, FSimsPerExperiment + 3, 20, '=AVERAGE(T2:T' + IntToStr(FSimsPerExperiment+1) + ')');

        //Amd then the same for the last 50 trials of each run
        WriteXLCell(FXLWorksheet, (2 * FSimsPerExperiment) + 6, 10, '=AVERAGE(J' + IntToStr(FSimsPerExperiment+5) + ':J' + IntToStr((2 * FSimsPerExperiment)+4) + ')');
        WriteXLCell(FXLWorksheet, (2 * FSimsPerExperiment) + 6, 11, '=AVERAGE(K' + IntToStr(FSimsPerExperiment+5) + ':K' + IntToStr((2 * FSimsPerExperiment)+4) + ')');
        WriteXLCell(FXLWorksheet, (2 * FSimsPerExperiment) + 6, 12, '=AVERAGE(L' + IntToStr(FSimsPerExperiment+5) + ':L' + IntToStr((2 * FSimsPerExperiment)+4) + ')');
        WriteXLCell(FXLWorksheet, (2 * FSimsPerExperiment) + 6, 13, '=AVERAGE(M' + IntToStr(FSimsPerExperiment+5) + ':M' + IntToStr((2 * FSimsPerExperiment)+4) + ')');
        WriteXLCell(FXLWorksheet, (2 * FSimsPerExperiment) + 6, 14, '=AVERAGE(N' + IntToStr(FSimsPerExperiment+5) + ':N' + IntToStr((2 * FSimsPerExperiment)+4) + ')');
        WriteXLCell(FXLWorksheet, (2 * FSimsPerExperiment) + 6, 15, '=AVERAGE(O' + IntToStr(FSimsPerExperiment+5) + ':O' + IntToStr((2 * FSimsPerExperiment)+4) + ')');
        WriteXLCell(FXLWorksheet, (2 * FSimsPerExperiment) + 6, 16, '=AVERAGE(P' + IntToStr(FSimsPerExperiment+5) + ':P' + IntToStr((2 * FSimsPerExperiment)+4) + ')');
        WriteXLCell(FXLWorksheet, (2 * FSimsPerExperiment) + 6, 17, '=AVERAGE(Q' + IntToStr(FSimsPerExperiment+5) + ':Q' + IntToStr((2 * FSimsPerExperiment)+4) + ')');
        WriteXLCell(FXLWorksheet, (2 * FSimsPerExperiment) + 6, 18, '=AVERAGE(R' + IntToStr(FSimsPerExperiment+5) + ':R' + IntToStr((2 * FSimsPerExperiment)+4) + ')');
        WriteXLCell(FXLWorksheet, (2 * FSimsPerExperiment) + 6, 19, '=AVERAGE(S' + IntToStr(FSimsPerExperiment+5) + ':S' + IntToStr((2 * FSimsPerExperiment)+4) + ')');
        WriteXLCell(FXLWorksheet, (2 * FSimsPerExperiment) + 6, 20, '=AVERAGE(T' + IntToStr(FSimsPerExperiment+5) + ':T' + IntToStr((2 * FSimsPerExperiment)+4) + ')');

        FExcelWorksheet := 'Average weights';
        FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
        for i := 1 to FRunsPerSimulation do
          FXLWorksheet.Cells[i + 1, 1] := i;

        FExcelWorksheet := 'SD weights';
        FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
        for i := 1 to FRunsPerSimulation do
          FXLWorksheet.Cells[i + 1, 1] := i;

        FExcelWorksheet := 'SErr weights';
        FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
        for i := 1 to FRunsPerSimulation do
          FXLWorksheet.Cells[i + 1, 1] := i;

        for j := 1 to 4 do begin
          FExcelWorksheet := 'WT' + IntToStr(j);
          FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
          for i := 1 to FRunsPerSimulation do
            FXLWorksheet.Cells[i + 1, 1] := i;
          for i := 1 to FSimsPerExperiment do
            FXLWorksheet.Cells[1,i+1] := 'Run' + IntToStr(i);
          FXLWorksheet.Cells[1,FSimsPerExperiment+2] := 'Average';
          FXLWorksheet.Cells[1,FSimsPerExperiment+3] := 'SD';
          FXLWorksheet.Cells[1,FSimsPerExperiment+4] := 'SErr';

          FExcelWorksheet := 'WD' + IntToStr(j);
          FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
          for i := 1 to FRunsPerSimulation do
            FXLWorksheet.Cells[i + 1, 1] := i;
          for i := 1 to FSimsPerExperiment do
            FXLWorksheet.Cells[1,i+1] := 'Run' + IntToStr(i);
          FXLWorksheet.Cells[1,FSimsPerExperiment+2] := 'Average';
          FXLWorksheet.Cells[1,FSimsPerExperiment+3] := 'SD';
          FXLWorksheet.Cells[1,FSimsPerExperiment+4] := 'SErr';
        end;
        for j := 1 to 16 do begin
          FExcelWorksheet := 'WA' + IntToStr(j);
          FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
          for i := 1 to FRunsPerSimulation do
            FXLWorksheet.Cells[i + 1, 1] := i;
          for i := 1 to FSimsPerExperiment do
            FXLWorksheet.Cells[1,i+1] := 'Run' + IntToStr(i);
          FXLWorksheet.Cells[1,FSimsPerExperiment+2] := 'Average';
          FXLWorksheet.Cells[1,FSimsPerExperiment+3] := 'SD';
          FXLWorksheet.Cells[1,FSimsPerExperiment+4] := 'SErr';
        end;

        FExcelWorksheet := 'Contexts';
        FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
        //Each pairing is shown only 1/6th of the time
        //Work out how many times each is shown to be put in column A
        iCountPairings := FRunsPerSimulation div 6;
        if (FRunsPerSimulation mod 6) > 0 then
          inc(iCountPairings);
        for i := 1 to iCountPairings do
          FXLWorksheet.Cells[i + 2, 1] := i;
        //Add the reward probabilities for the two targets of each pairing in the
        //page header
        FXLWorksheet.Cells[ROW_HEADER,3].Value := 'T1 & T2 (' + FloatToStr(arTarget[1].RewardProbability) + ' & ' + FloatToStr(arTarget[2].RewardProbability) + ')';
        FXLWorksheet.Cells[ROW_HEADER,8].Value := 'T1 & T3 (' + FloatToStr(arTarget[1].RewardProbability) + ' & ' + FloatToStr(arTarget[3].RewardProbability) + ')';
        FXLWorksheet.Cells[ROW_HEADER,13].Value := 'T1 & T4 (' + FloatToStr(arTarget[1].RewardProbability) + ' & ' + FloatToStr(arTarget[4].RewardProbability) + ')';
        FXLWorksheet.Cells[ROW_HEADER,18].Value := 'T2 & T3 (' + FloatToStr(arTarget[2].RewardProbability) + ' & ' + FloatToStr(arTarget[3].RewardProbability) + ')';
        FXLWorksheet.Cells[ROW_HEADER,23].Value := 'T2 & T4 (' + FloatToStr(arTarget[2].RewardProbability) + ' & ' + FloatToStr(arTarget[4].RewardProbability) + ')';
        FXLWorksheet.Cells[ROW_HEADER,28].Value := 'T3 & T4 (' + FloatToStr(arTarget[3].RewardProbability) + ' & ' + FloatToStr(arTarget[4].RewardProbability) + ')';

        bSent := true;
      except
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TfrmMain.CreateDirectVHyperWorkbook;
begin
  try
    FXLApp.Workbooks.Add(ExtractFilePath(Application.ExeName)+xlTEMPLATE_DIRECT_V_HYPER);
    FXLGainsWorkbook := FXLApp.ActiveWorkbook;
    FXLGainsWorkbook.SaveAs(sSaveLocation + 'DirectVHyperdirect.xls');
  except
    on E: Exception do begin
      ShowMessage(E.Message);
      EXIT;
    end;
  end;
end;

procedure TfrmMain.SaveDirectVHyperWorkbook;
begin
  try
    FXLGainsWorkbook.Save;
  except
    on E: Exception do begin
      ShowMessage(E.Message);
      EXIT;
    end;
  end;
end;

procedure TfrmMain.CreateExcelWorkbook;
begin
  FXLApp.Workbooks.Add(ExtractFilePath(Application.ExeName)+xlTEMPLATE);
  FXLWorkbook := FXLApp.Activeworkbook;
  arXlWorkbooks[FCurrentSimulation-1] := FXLWorkbook;
  // TODO: not right if used remotely, without knowing target probabilities
  SendPairingProbabilitiesToExcel;
  Application.ProcessMessages;
end;

{
At the end of each simulation (set of FRunsPerSimulation runs) of a set of
simulations (set of FSimsPerExperiment simulations), save the Excel workbook
automatically
}
procedure TfrmMain.SaveExcelWorkbook;
var sFileName: string;
begin
  if cbAutoSave.Checked then try
    if FSimsPerExperiment > 1 then
      sFileName := sSaveLocation + IntToStr(FCurrentSimulation) + '.xls'
    else
      sFileName := sSaveLocation + '.xls';
    FXLWorkbook.SaveAs(sFileName);
  except
    on E: Exception do begin
      ShowMessage(E.Message);
    end;
  end;
end;

{
The pairings worksheet holds details on the outcomes for each time one of the 6
combinations of targets are presented
The probabilities for the pairs of targets are put into the sheet header at the
start of each simulation when the workbook is created
}
procedure TfrmMain.SendPairingProbabilitiesToExcel;
var bSent: boolean;
begin
  bSent := false;
  while not bSent do try
    FXLWorksheet := FXLWorkbook.Worksheets['Contexts'];
    FXLWorksheet.Cells[ROW_HEADER,3].Value := 'T1 & T2 (' + FloatToStr(arTarget[1].RewardProbability) + ' & ' + FloatToStr(arTarget[2].RewardProbability) + ')';
    FXLWorksheet.Cells[ROW_HEADER,8].Value := 'T1 & T3 (' + FloatToStr(arTarget[1].RewardProbability) + ' & ' + FloatToStr(arTarget[3].RewardProbability) + ')';
    FXLWorksheet.Cells[ROW_HEADER,13].Value := 'T1 & T4 (' + FloatToStr(arTarget[1].RewardProbability) + ' & ' + FloatToStr(arTarget[4].RewardProbability) + ')';
    FXLWorksheet.Cells[ROW_HEADER,18].Value := 'T2 & T3 (' + FloatToStr(arTarget[2].RewardProbability) + ' & ' + FloatToStr(arTarget[3].RewardProbability) + ')';
    FXLWorksheet.Cells[ROW_HEADER,23].Value := 'T2 & T4 (' + FloatToStr(arTarget[2].RewardProbability) + ' & ' + FloatToStr(arTarget[4].RewardProbability) + ')';
    FXLWorksheet.Cells[ROW_HEADER,28].Value := 'T3 & T4 (' + FloatToStr(arTarget[3].RewardProbability) + ' & ' + FloatToStr(arTarget[4].RewardProbability) + ')';

    bSent := true;
  except
    on E: Exception do begin
//      Abort
//      ShowMessage(E.Message);
    end;
  end;
end;

{
Add a worksheet for every run so that the activations of all units can be
stored at the end of each stage, along with the targets and directions shown, correct and
incorrect targets, target chosen, direction chosen and whether these were correct
This only happens if cbSendEveryTrialToExcel is checked. Not generally done for large simulations
}
function TfrmMain.AddWorksheet(RunNumber: integer): boolean;
var bSent: boolean;
begin
  FCurrentRun := RunNumber;
  if cbSendEveryTrialToExcel.Checked then begin
    bSent := false;
    result := false;
    while not bSent do try
      FExcelWorksheet := IntToStr(RunNumber);
      FXLWorkbook.Worksheets.Add;
      FXLWorkbook.Activesheet.Name := FExcelWorksheet;
      FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];

      //show the headers above each matrix on the stage sheet
      FXLWorksheet.Cells[ROW_HEADER, 3].Value := 'Cortex';
      FXLWorksheet.Cells[ROW_HEADER, 9].Value := 'Striatum';
      FXLWorksheet.Cells[ROW_HEADER, 15].Value := 'STN';
      FXLWorksheet.Cells[ROW_HEADER, 21].Value := 'GPi';
      FXLWorksheet.Cells[ROW_HEADER, 27].Value := 'Thalamus';
      result := true;

      bSent := true;
    except
      on E: Exception do begin
//      Abort
//        ShowMessage(E.Message);
      end;
    end;
  end
  else begin
    //this will allow a much greater number of trials in a simulation as if a
    //worksheet is created for each trial we are limited to 255 worksheets by
    //Excel
    result := true;
  end;
end;

procedure TfrmMain.WriteXLCell(AWorksheet: OleVariant; Row, Col: integer; Data: string);
var bSent: boolean;
    iTries: integer;
begin
  iTries := 1;
  bSent := false;
  while not bSent do try
    AWorksheet.Cells[Row, Col] := Data;
    bSent := true;
  except
    inc(iTries);
    if iTries > 10 then
      EXIT;
  end;
end;

procedure TfrmMain.WriteXLCell(AWorksheet: OleVariant; Row, Col: integer; Data: integer);
begin
  WriteXLCell(AWorksheet, Row, Col, IntToStr(Data));
end;

procedure TfrmMain.WriteXLCell(AWorksheet: OleVariant; Row, Col: integer; Data: real);
begin
  WriteXLCell(AWorksheet, Row, Col, FloatToStr(Data));
end;

procedure TfrmMain.WriteXLCell(AWorksheet: OleVariant; Row, Col: integer; Data: boolean);
begin
  WriteXLCell(AWorksheet, Row, Col, NO_YES[integer(Data)]);
end;

{ function TfrmMain.ReadXLCellString(AWorksheet: OleVariant; Row, Col: integer): string;
var bGot: boolean;
    iTries: integer;
begin
  iTries := 1;
  bGot := false;
  while not bGot do try
    result := AWorksheet.Cells[Row, Col];
    bGot := true;
  except
    inc(iTries);
    if iTries > 10 then
      EXIT;
  end;
end;}

{function TfrmMain.ReadXLCellBoolean(AWorksheet: OleVariant; Row, Col: integer): boolean;
var bGot: boolean;
    iTries: integer;
begin
  result := false;
  iTries := 1;
  bGot := false;
  while not bGot do try
    result := AWorksheet.Cells[Row, Col];
    bGot := true;
  except
    inc(iTries);
    if iTries > 10 then
      EXIT;
  end;
end;}

function TfrmMain.ReadXLCellInteger(AWorksheet: OleVariant; Row, Col: integer): integer;
var bGot: boolean;
    iTries: integer;
begin
  result := 0;
  iTries := 1;
  bGot := false;
  while not bGot do try
    result := AWorksheet.Cells[Row, Col];
    bGot := true;
  except
    inc(iTries);
    if iTries > 10 then
      EXIT;
  end;
end;

function TfrmMain.ReadXLCellReal(AWorksheet: OleVariant; Row, Col: integer): real;
var bGot: boolean;
    iTries: integer;
begin
  result := 0;
  iTries := 1;
  bGot := false;
  while not bGot do try
    result := AWorksheet.Cells[Row, Col];
    bGot := true;
  except
    inc(iTries);
    if iTries > 10 then begin
      result := 0;
      Abort;
      EXIT;
    end;
  end;
end;

{
Send data to the Trials worksheet after each trial showing which targets and direction were chosen
}
procedure TfrmMain.SendTrialResultToTrialsWorksheet(SendProbability: boolean);
var bSent: boolean;
begin
  bSent := false;
  while not bSent do try
    FExcelWorksheet := 'Trials';
    FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];
    FXLWorksheet.Cells[FCurrentRun + 1, 1] := FCurrentRun;
    FXLWorksheet.Cells[FCurrentRun + 1, 2] := arTrial[FCurrentRun-1].Target1;
    FXLWorksheet.Cells[FCurrentRun + 1, 3] := arTrial[FCurrentRun-1].Target2;
    FXLWorksheet.Cells[FCurrentRun + 1, 4] := arTrial[FCurrentRun-1].Direction1;
    FXLWorksheet.Cells[FCurrentRun + 1, 5] := arTrial[FCurrentRun-1].Direction2;
    if SendProbability then begin
      FXLWorksheet.Cells[FCurrentRun + 1, 6] := arTrial[FCurrentRun-1].Target[0].RewardProbability;
      FXLWorksheet.Cells[FCurrentRun + 1, 7] := arTrial[FCurrentRun-1].Target[1].RewardProbability;
    end
    else begin
      FXLWorksheet.Cells[FCurrentRun + 1, 6] := '';
      FXLWorksheet.Cells[FCurrentRun + 1, 7] := '';
    end;
    FXLWorksheet.Cells[FCurrentRun + 1, 8] := iTargetChosen;
    FXLWorksheet.Cells[FCurrentRun + 1, 9] := iDirectionChosen;
    FXLWorksheet.Cells[FCurrentRun + 1, 10] := iTargetByDirection;
    //check if the target chosen and the target by direction are the same
    FXLWorksheet.Cells[FCurrentRun + 1, 11] := '=H'+ IntToStr(FCurrentRun+1) + '=J' + IntToStr(FCurrentRun+1);
//    if (iTargetChosen = NO_TARGET) or (iDirectionChosen = NO_DIRECTION) then begin
    if iDirectionChosen = NO_DIRECTION then begin
      //Can't have had a reward signal of any kind if no direction was sent back to the user/robot
      //Also can't be successful or pick optimum
      FXLWorksheet.Cells[FCurrentRun + 1, 12] := '?';
      FXLWorksheet.Cells[FCurrentRun + 1, 13] := '?';
      FXLWorksheet.Cells[FCurrentRun + 1, 14] := '?';
    end
    else begin
      FXLWorksheet.Cells[FCurrentRun + 1, 12] := NO_YES[integer(arTrial[FCurrentRun-1].Successful)];
      FXLWorksheet.Cells[FCurrentRun + 1, 13] := NO_YES[integer(arTrial[FCurrentRun-1].Optimum)];
      FXLWorksheet.Cells[FCurrentRun + 1, 14] := NO_YES[integer(arTrial[FCurrentRun-1].Rewarded)];
    end;
    FXLWorksheet.Cells[FCurrentRun + 1, 15] := FiLoops - FiLoopsBeforeTargets;
    FXLWorksheet.Cells[FCurrentRun + 1, 16] := FiCogLoops;
    FXLWorksheet.Cells[FCurrentRun + 1, 17] := FiMotorLoops;
    if FCurrentRun > 9 then begin
      //Moving averages
      FXLWorksheet.Cells[FCurrentRun + 1, 18] := '=COUNTIF(L' + IntToStr(FCurrentRun - 8) + ':L' + IntToStr(FCurrentRun + 1) + ',"Yes")';
      FXLWorksheet.Cells[FCurrentRun + 1, 19] := '=COUNTIF(M' + IntToStr(FCurrentRun - 8) + ':M' + IntToStr(FCurrentRun + 1) + ',"Yes")';
      FXLWorksheet.Cells[FCurrentRun + 1, 20] := '=COUNTIF(N' + IntToStr(FCurrentRun - 8) + ':N' + IntToStr(FCurrentRun + 1) + ',"Yes")';
    end;

    bSent := true;
  except
    on E: Exception do begin
    end;
  end;
end;

procedure TfrmMain.SendTrialResultToAveragesWorkbook;
var bSent: boolean;
    i, j,
    iWorksheet: integer;
begin
  if FSimsPerExperiment > 1 then begin
    bSent := false;
    while not bSent do try
      FExcelWorksheet := 'Cognitive loops';
      FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
      FXLWorksheet.Cells[FCurrentRun + 1, FCurrentSimulation + 1] := FiCogLoops;
      FExcelWorksheet := 'Motor loops';
      FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
      FXLWorksheet.Cells[FCurrentRun + 1, FCurrentSimulation + 1] := FiMotorLoops;
      FExcelWorksheet := 'Total loops';
      FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
      //subtract the number of loops before the target is presented from the total loops
      FXLWorksheet.Cells[FCurrentRun + 1, FCurrentSimulation + 1] := FiLoops - FiLoopsBeforeTargets;
      //save the current weights of all corticostriatal connections
      for i := 1 to 4 do begin
        FExcelWorksheet := 'WT' + IntToStr(i);
        FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
        FXLWorksheet.Cells[FCurrentRun + 1, FCurrentSimulation + 1] := arConnection[COL_COGNITIVE, i].Real;
        FExcelWorksheet := 'WD' + IntToStr(i);
        FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
        FXLWorksheet.Cells[FCurrentRun + 1, FCurrentSimulation + 1] := arConnection[i, ROW_MOTOR].Real;
      end;
      for i := 1 to 4 do begin
        for j := 1 to 4 do begin
          iWorksheet := ((j-1) * 4) + i;
          FExcelWorksheet := 'WA' + IntToStr(iWorksheet);
          FXLWorksheet := FXLAveragesWorkbook.Worksheets[FExcelWorksheet];
          FXLWorksheet.Cells[FCurrentRun+1,FCurrentSimulation + 1] := arConnection[j, i].Real;
        end;
      end;

      bSent := true;
    except
      on E: Exception do begin
      end;
    end;
  end;
end;

procedure TfrmMain.SendTrialResultToEffectiveProbabilityWorksheet;
var bSent: boolean;
    i: integer;
begin
  bSent := false;
  while not bSent do try
    FExcelWorksheet := 'Targets';
    FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];
    FXLWorksheet.Cells[FCurrentRun + 1, 1] := FCurrentRun;
    for i := 1 to 4 do begin
      //Expected value of the reward
      FXLWorksheet.Cells[FCurrentRun + 1, i + 1] := arTarget[i].ExpectedValue;
      //Effective probability. Calculated from how many times the target has actually been rewarded
      FXLWorksheet.Cells[FCurrentRun + 1, i + 11] := arTarget[i].EffectiveProbability;
    end;
    FXLWorksheet.Cells[FCurrentRun + 1, 6] := iTargetChosen;
    FXLWorksheet.Cells[FCurrentRun + 1, 7] := iDirectionChosen;
    FXLWorksheet.Cells[FCurrentRun + 1, 8] := iTargetByDirection;
    FXLWorksheet.Cells[FCurrentRun + 1, 9] := NO_YES[integer(arTrial[FCurrentRun-1].Successful)];
    FXLWorksheet.Cells[FCurrentRun + 1, 10] := NO_YES[integer(arTrial[FCurrentRun-1].Optimum)];
    FXLWorksheet.Cells[FCurrentRun + 1, 11] := NO_YES[integer(arTrial[FCurrentRun-1].Rewarded)];

    bSent := true;
  except
    on E: Exception do begin
    end;
  end;
end;

procedure TfrmMain.SendTrialResultToPairingsWorksheet;
const COLS_PER_PAIRING = 5;
var bSent: boolean;
    iRow,
    iBaseCol: integer;
begin
  bSent := false;
  iRow := arContext[FCurrentContext].ChosenCount + 2;
  iBaseCol := COLS_PER_PAIRING * (FCurrentContext - 1) + 2;
  while not bSent do try
    FExcelWorksheet := 'Contexts';
    FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];
//    FXLWorksheet.Cells[iRow, 1] := arContext[FCurrentContext].ChosenCount;
    // TODO: control iRow and iBaseCol, raise "ole error 800A03EC" otherwise
    // (eg: FCurrentContext previously not set with robot)
    FXLWorksheet.Cells[iRow, iBaseCol] := NO_YES[integer(arTrial[FCurrentRun-1].Successful)];
    FXLWorksheet.Cells[iRow, iBaseCol + 1] := NO_YES[integer(arTrial[FCurrentRun-1].Optimum)];
    FXLWorksheet.Cells[iRow, iBaseCol + 2] := NO_YES[integer(arTrial[FCurrentRun-1].Rewarded)];
    FXLWorksheet.Cells[iRow, iBaseCol + 3] := arContext[FCurrentContext].ExpectedValue;
    FXLWorksheet.Cells[iRow, iBaseCol + 4] := eContextPE;

    bSent := true;
  except
    on E: Exception do begin
    end;
  end;
end;

{
At the end of each trial send the current status of each array
}
procedure TfrmMain.SendTrialResultToExcel;
begin
  SendTrialToExcel('Cortex', arCx, true);
  SendTrialToExcel('Striatum', arStr, true);
  SendTrialToExcel('STN', arStn, false);
  SendTrialToExcel('GPi', arGpi, false);
  SendTrialToExcel('Thalamus', arTh, false);
end;

{
There is one page of stage summary for each structure
This sends the outcomes of the trial to Excel
}
procedure TfrmMain.SendTrialToExcel(SheetName: string;
  WhichArray: TStructureArray; SendAssociation: boolean);
const COG_START = 14;
      MOTOR_START = COG_START + 4;
      ASS_START = MOTOR_START + 4;
var i,j: integer;
    bSent: boolean;
begin
  bSent := false;
  while not bSent do try
    FXLWorksheet := FXLWorkbook.Worksheets[SheetName];
    FXLWorksheet.Cells[FCurrentRun + 1, 1].Value := FCurrentRun;
    FXLWorksheet.Cells[FCurrentRun + 1, 1] := FCurrentRun;
    FXLWorksheet.Cells[FCurrentRun + 1, 2] := arTrial[FCurrentRun-1].Target1;
    FXLWorksheet.Cells[FCurrentRun + 1, 3] := arTrial[FCurrentRun-1].Target2;
    FXLWorksheet.Cells[FCurrentRun + 1, 4] := arTrial[FCurrentRun-1].Direction1;
    FXLWorksheet.Cells[FCurrentRun + 1, 5] := arTrial[FCurrentRun-1].Direction2;
    FXLWorksheet.Cells[FCurrentRun + 1, 6].Value := iTargetChosen;
    FXLWorksheet.Cells[FCurrentRun + 1, 7].Value := iDirectionChosen;
    FXLWorksheet.Cells[FCurrentRun + 1, 8].Value := iTargetByDirection;
    FXLWorksheet.Cells[FCurrentRun + 1, 9] := NO_YES[integer(arTrial[FCurrentRun-1].Successful)];
    FXLWorksheet.Cells[FCurrentRun + 1, 10] := NO_YES[integer(arTrial[FCurrentRun-1].Optimum)];
    FXLWorksheet.Cells[FCurrentRun + 1, 11] := NO_YES[integer(arTrial[FCurrentRun-1].Rewarded)];
    FXLWorksheet.Cells[FCurrentRun + 1, 12].Value := FiLoops;
    FXLWorksheet.Cells[FCurrentRun + 1, 13].Value := FiCogLoops;
    FXLWorksheet.Cells[FCurrentRun + 1, 14].Value := FiMotorLoops;

    for i := 1 to 4 do begin
      FXLWorksheet.Cells[FCurrentRun+1,i+COG_START].Value := WhichArray[COL_COGNITIVE,i];
        FXLWorksheet.Cells[FCurrentRun+1,i+MOTOR_START].Value := WhichArray[i,ROW_MOTOR];
    end;
    if SendAssociation then
      //only some structures have association cortex
      for i := 1 to 4 do
        for j := 1 to 4 do
          FXLWorksheet.Cells[FCurrentRun+1,((i-1) * 4) + j + ASS_START].Value := WhichArray[i,j];

    bSent := true;
  except
    on E: Exception do begin
//      Abort
//      ShowMessage(E.Message);
    end;
  end;
end;

{
Sends the activations of each structure at the end of the trial to Excel
}
{
procedure TfrmMain.SendStageDataToExcel(SheetName: string;
  WhichArray: TStructureArray; SendAssociation: boolean);
const COG_START = 14;
      MOTOR_START = COG_START + 4;
      ASS_START = MOTOR_START + 4;
var i,j: integer;
    bSent: boolean;
begin
  bSent := false;
  while not bSent do try
    FXLWorksheet := FXLWorkbook.Worksheets[SheetName];
    for i := 1 to 4 do begin
      FXLWorksheet.Cells[FCurrentRun+1,i+COG_START].Value := WhichArray[COL_COGNITIVE,i];
        FXLWorksheet.Cells[FCurrentRun+1,i+MOTOR_START].Value := WhichArray[i,ROW_MOTOR];
    end;
    if SendAssociation then
      //only some structures have association cortex
      for i := 1 to 4 do
        for j := 1 to 4 do
          FXLWorksheet.Cells[FCurrentRun+1,((i-1) * 4) + j + ASS_START].Value := WhichArray[i,j];

    bSent := true;
  except
    on E: Exception do begin
//      Abort
//      ShowMessage(E.Message);
    end;
  end;
end;
}
{
At the end of each trial, all the corticostriatal weights are put on one line
along with information about the targets for that trial
}
procedure TfrmMain.SendWeightsToExcel;
var i, j: integer;
    bSent: boolean;
begin
  bSent := false;
  while not bSent do try
    FXLWorksheet := FXLWorkbook.Worksheets['Weights'];
    FXLWorksheet.Cells[FCurrentRun+1,1].Value := FCurrentRun;
    FXLWorksheet.Cells[FCurrentRun+1,6].Value := iTargetChosen;
    FXLWorksheet.Cells[FCurrentRun+1,7].Value := iDirectionChosen;
    FXLWorksheet.Cells[FCurrentRun+1,8] := iTargetByDirection;
    if iDirectionChosen = NO_DIRECTION then begin
      //If no direction was chosen, cannot have a reward or a prediction error
      FXLWorksheet.Cells[FCurrentRun+1,10].Value := '?';
      FXLWorksheet.Cells[FCurrentRun+1,9].Value := '?';
    end
    else begin
      FXLWorksheet.Cells[FCurrentRun+1,9].Value := ePredictionError; //this could be the ValuePE or the ContextPE depending on the method chosen in the configurations sheet
      FXLWorksheet.Cells[FCurrentRun+1,10].Value := NO_YES[integer(arTrial[FCurrentRun-1].Successful)];
      FXLWorksheet.Cells[FCurrentRun+1,11].Value := NO_YES[integer(arTrial[FCurrentRun-1].Optimum)];
      FXLWorksheet.Cells[FCurrentRun+1,12].Value := NO_YES[integer(arTrial[FCurrentRun-1].Rewarded)];
    end;
    j := 0;
    for i := 1 to 4 do begin
      if arTarget[i].Show then begin
        FXLWorksheet.Cells[FCurrentRun+1,2+j].Value := i;
        //Direction of the target
        FXLWorksheet.Cells[FCurrentRun+1,4+j].Value := arTarget[i].Direction;
        inc(j);
      end;
      FXLWorksheet.Cells[FCurrentRun+1,i+12].Value := arConnection[COL_COGNITIVE, i].Real;
      FXLWorksheet.Cells[FCurrentRun+1,i+16].Value := arConnection[i, ROW_MOTOR].Real;
    end;
    for i := 1 to 4 do begin
      for j := 1 to 4 do begin
        FXLWorksheet.Cells[FCurrentRun+1,(i+20) + ((j -1) * 4)].Value := arConnection[j, i].Real;
      end;
    end;

    bSent := true;
  except
    on E: Exception do begin
//      Abort
//      ShowMessage(E.Message);
    end;
  end;
end;

{
At the end of each trial, all the corticostriatal weights are put on one line
along with information about the targets for that trial
}
procedure TfrmMain.SendDeltaWeightsToExcel;
var i, j: integer;
    bSent: boolean;
begin
  bSent := false;
  while not bSent do try
    FXLWorksheet := FXLWorkbook.Worksheets['DeltaW'];
    FXLWorksheet.Cells[FCurrentRun+1,1].Value := FCurrentRun;
    FXLWorksheet.Cells[FCurrentRun+1,6].Value := iTargetChosen;
    FXLWorksheet.Cells[FCurrentRun+1,7].Value := iDirectionChosen;
    FXLWorksheet.Cells[FCurrentRun+1,8] := iTargetByDirection;
    if iDirectionChosen = NO_DIRECTION then begin
      //If no direction was chosen, cannot have a reward or a prediction error
      FXLWorksheet.Cells[FCurrentRun+1,10].Value := '?';
      FXLWorksheet.Cells[FCurrentRun+1,9].Value := '?';
    end
    else begin
      FXLWorksheet.Cells[FCurrentRun+1,9].Value := ePredictionError;
      FXLWorksheet.Cells[FCurrentRun+1,10].Value := NO_YES[integer(arTrial[FCurrentRun-1].Successful)];
      FXLWorksheet.Cells[FCurrentRun+1,11].Value := NO_YES[integer(arTrial[FCurrentRun-1].Optimum)];
      FXLWorksheet.Cells[FCurrentRun+1,12].Value := NO_YES[integer(arTrial[FCurrentRun-1].Rewarded)];
    end;
    j := 0;
    for i := 1 to 4 do begin
      if arTarget[i].Show then begin
        FXLWorksheet.Cells[FCurrentRun+1,2+j].Value := i;
        //Direction of the target
        FXLWorksheet.Cells[FCurrentRun+1,4+j].Value := arTarget[i].Direction;
        inc(j);
      end;
      FXLWorksheet.Cells[FCurrentRun+1,i+12].Value := arDeltaW[COL_COGNITIVE, i];
      FXLWorksheet.Cells[FCurrentRun+1,i+16].Value := arDeltaW[i, ROW_MOTOR];
    end;
    for i := 1 to 4 do begin
      for j := 1 to 4 do begin
        FXLWorksheet.Cells[FCurrentRun+1,(i+20) + ((j -1) * 4)].Value := arDeltaW[j, i];
        FXLWorksheet.Cells[FCurrentRun+1,(i+36) + ((j -1) * 4)].Value := arCogDeltaW[j, i];
        FXLWorksheet.Cells[FCurrentRun+1,(i+52) + ((j -1) * 4)].Value := arMotorDeltaW[j, i];
      end;
    end;

    bSent := true;
  except
    on E: Exception do begin
    end;
  end;
end;

procedure TfrmMain.SendSetupToExcel(IsAverages: boolean);
const COL_CONNECTIVITY = 11;
      ROW_CONNECTIVITY = 15;
      ROW_DEFAULTS = 24;
      ROW_TARGET_VALUES = 1;
      ROW_OUTCOMES = 31;
      COL_LEARNING = 2;
      ROW_LEARNING = 12;
      COL_LEARNING_TF =  5;
      ROW_LEARNING_TF = 13;
var bSent: boolean;
    i,
    iTotalFailed: integer;
    //AWorkbook: OleVariant;
begin
  FExcelWorksheet := 'Setup';
  if IsAverages then
    FXLWorksheet :=  FXLAveragesWorkbook.Worksheets[FExcelWorksheet]
  else
    FXLWorksheet :=  arXlWorkbooks[FCurrentSimulation-1].Worksheets[FExcelWorksheet];
//  FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];
  bSent := false;
  while not bSent do try

    FXLWorksheet.Cells[2,COL_CONNECTIVITY].Value := NO_YES[integer(bUseCorticalThreshold)];
    FXLWorksheet.Cells[3,COL_CONNECTIVITY].Value := FeSelectionThreshold;
    FXLWorksheet.Cells[4,COL_CONNECTIVITY].Value := FiMaxLoops;
    FXLWorksheet.Cells[8,COL_CONNECTIVITY].Value := eMSNThresholdLevel;
    FXLWorksheet.Cells[9,COL_CONNECTIVITY].Value := NO_YES[integer(bUseTransferFuction)];
    FXLWorksheet.Cells[10,COL_CONNECTIVITY].Value := FBoltzmann.MinOutput;
    FXLWorksheet.Cells[11,COL_CONNECTIVITY].Value := FBoltzmann.MaxOutput;
    FXLWorksheet.Cells[12,COL_CONNECTIVITY].Value := FBoltzmann.Vh;
    FXLWorksheet.Cells[13,COL_CONNECTIVITY].Value := FBoltzmann.Vc;

    FXLWorksheet.Cells[ROW_CONNECTIVITY + 1,COL_CONNECTIVITY].Value := eCxMotorToStrMotor;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 2,COL_CONNECTIVITY].Value := eCxMotorToStrAss;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 3,COL_CONNECTIVITY].Value := eCxCogToStrCog;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 4,COL_CONNECTIVITY].Value := eCxCogToStrAss;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 5,COL_CONNECTIVITY].Value := eCxAssToStrAss;

    FXLWorksheet.Cells[ROW_CONNECTIVITY + 6,COL_CONNECTIVITY].Value := eCxMotorToStnMotor;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 7,COL_CONNECTIVITY].Value := eCxCogToStnCog;

    FXLWorksheet.Cells[ROW_CONNECTIVITY + 8,COL_CONNECTIVITY].Value := NO_YES[integer(StnDefaults.IgnoreTonic)];
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 9,COL_CONNECTIVITY].Value := eStnMotorToGpiMotor;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 10,COL_CONNECTIVITY].Value := eStnMotorToGpiAss;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 11,COL_CONNECTIVITY].Value := eStnCogToGpiCog;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 12,COL_CONNECTIVITY].Value := eStnCogToGpiAss;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 13,COL_CONNECTIVITY].Value := eCogStrToGpi;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 14,COL_CONNECTIVITY].Value := eMotorStrToGpi;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 15,COL_CONNECTIVITY].Value := eStrAssToGpi;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 16,COL_CONNECTIVITY].Value := NO_YES[integer(GPiDefaults.IgnoreTonic)];
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 17,COL_CONNECTIVITY].Value := eCogGPiToCogTh;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 18,COL_CONNECTIVITY].Value := eMotorGPiToMotorTh;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 19,COL_CONNECTIVITY].Value := eAssGPiToCogTh;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 20,COL_CONNECTIVITY].Value := eAssGPiToMotorTh;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 21,COL_CONNECTIVITY].Value := NO_YES[integer(ThDefaults.IgnoreTonic)];
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 22,COL_CONNECTIVITY].Value := eThMotorToCxMotor;
    FXLWorksheet.Cells[ROW_CONNECTIVITY + 23,COL_CONNECTIVITY].Value := eThCogToCxCog;

    FXLWorksheet.Cells[ROW_LEARNING + 1,COL_LEARNING].Value := NO_YES[integer(FLearning)];
    if FiLearningType = 0 then
      FXLWorksheet.Cells[ROW_LEARNING + 2,COL_LEARNING].Value := 'Value of target'
    else
      FXLWorksheet.Cells[ROW_LEARNING + 2,COL_LEARNING].Value := 'Value of context';
    FXLWorksheet.Cells[ROW_LEARNING + 3,COL_LEARNING].Value := eValueLearningRate;
    FXLWorksheet.Cells[ROW_LEARNING + 4,COL_LEARNING].Value := eLTP;
    FXLWorksheet.Cells[ROW_LEARNING + 5,COL_LEARNING].Value := NO_YES[integer(bWInitialRandom)];
    FXLWorksheet.Cells[ROW_LEARNING + 6,COL_LEARNING].Value := eInitialWeight;
    FXLWorksheet.Cells[ROW_LEARNING + 7,COL_LEARNING].Value := eSdInitialWeight;
    FXLWorksheet.Cells[ROW_LEARNING + 8,COL_LEARNING].Value := eMinWeight;
    FXLWorksheet.Cells[ROW_LEARNING + 9,COL_LEARNING].Value := eMaxWeight;
    if FByTargetChosen then
      FXLWorksheet.Cells[ROW_LEARNING + 10,COL_LEARNING].Value := 'Target chosen'
    else
      FXLWorksheet.Cells[ROW_LEARNING + 10,COL_LEARNING].Value := 'Direction chosen';

    FXLWorksheet.Cells[ROW_LEARNING_TF,COL_LEARNING_TF].Value := NO_YES[integer(bUseLearningTransferFuction)];
    FXLWorksheet.Cells[ROW_LEARNING_TF+1,COL_LEARNING_TF].Value := FWeightBoltzmann.MinOutput;
    FXLWorksheet.Cells[ROW_LEARNING_TF+1,COL_LEARNING_TF].Value := FWeightBoltzmann.MaxOutput;
    FXLWorksheet.Cells[ROW_LEARNING_TF+1,COL_LEARNING_TF].Value := FWeightBoltzmann.Vh;
    FXLWorksheet.Cells[ROW_LEARNING_TF+1,COL_LEARNING_TF].Value := FWeightBoltzmann.Vc;

//    FXLWorksheet.Cells[ROW_DEFAULTS + 1,2].Value := CxDefaults.TonicFiring;
    FXLWorksheet.Cells[ROW_DEFAULTS + 1,2].Value := CxDefaults.NoisePercent;
    FXLWorksheet.Cells[ROW_DEFAULTS + 1,3].Value := CxDefaults.NoiseProportion;
    FXLWorksheet.Cells[ROW_DEFAULTS + 1,4].Value := CxDefaults.Tau;
    FXLWorksheet.Cells[ROW_DEFAULTS + 1,5].Value := CxDefaults.Threshold;
//    FXLWorksheet.Cells[ROW_DEFAULTS + 2,2].Value := StrDefaults.TonicFiring;
    FXLWorksheet.Cells[ROW_DEFAULTS + 2,2].Value := StrDefaults.NoisePercent;
    FXLWorksheet.Cells[ROW_DEFAULTS + 2,3].Value := StrDefaults.NoiseProportion;
    FXLWorksheet.Cells[ROW_DEFAULTS + 2,4].Value := StrDefaults.Tau;
    FXLWorksheet.Cells[ROW_DEFAULTS + 2,5].Value := StrDefaults.Threshold;
//    FXLWorksheet.Cells[ROW_DEFAULTS + 3,2].Value := StnDefaults.TonicFiring;
    FXLWorksheet.Cells[ROW_DEFAULTS + 3,2].Value := StnDefaults.NoisePercent;
    FXLWorksheet.Cells[ROW_DEFAULTS + 3,3].Value := StnDefaults.NoiseProportion;
    FXLWorksheet.Cells[ROW_DEFAULTS + 3,4].Value := StnDefaults.Tau;
    FXLWorksheet.Cells[ROW_DEFAULTS + 3,5].Value := StnDefaults.Threshold;
//    FXLWorksheet.Cells[ROW_DEFAULTS + 4,2].Value := GpiDefaults.TonicFiring;
    FXLWorksheet.Cells[ROW_DEFAULTS + 4,2].Value := GpiDefaults.NoisePercent;
    FXLWorksheet.Cells[ROW_DEFAULTS + 4,3].Value := GpiDefaults.NoiseProportion;
    FXLWorksheet.Cells[ROW_DEFAULTS + 4,4].Value := GpiDefaults.Tau;
    FXLWorksheet.Cells[ROW_DEFAULTS + 4,5].Value := GpiDefaults.Threshold;
//    FXLWorksheet.Cells[ROW_DEFAULTS + 5,2].Value := ThDefaults.TonicFiring;
    FXLWorksheet.Cells[ROW_DEFAULTS + 5,2].Value := ThDefaults.NoisePercent;
    FXLWorksheet.Cells[ROW_DEFAULTS + 5,3].Value := ThDefaults.NoiseProportion;
    FXLWorksheet.Cells[ROW_DEFAULTS + 5,4].Value := ThDefaults.Tau;
    FXLWorksheet.Cells[ROW_DEFAULTS + 5,5].Value := ThDefaults.Threshold;

    for i := 1 to 4 do begin
      FXLWorksheet.Cells[ROW_TARGET_VALUES + i,1].Value := 'Target ' + IntToStr(i);
      FXLWorksheet.Cells[ROW_TARGET_VALUES + i,2].Value := arTarget[i].Salience;
      FXLWorksheet.Cells[ROW_TARGET_VALUES + i,3].Value := arTarget[i].RewardProbability;
      if not IsAverages then begin
        FXLWorksheet.Cells[ROW_TARGET_VALUES + i,4].Value := arTarget[i].EffectiveProbability;
        FXLWorksheet.Cells[ROW_TARGET_VALUES + i,5].Value := arTarget[i].PresentedCount;
        FXLWorksheet.Cells[ROW_TARGET_VALUES + i,6].Value := arTarget[i].PickedCount;
        FXLWorksheet.Cells[ROW_TARGET_VALUES + i,7].Value := arTarget[i].RewardedCount;
        if arTarget[i].PickedCount > 0 then
          FXLWorksheet.Cells[ROW_TARGET_VALUES + i,8].Value := arTarget[i].RewardedCount / arTarget[i].PickedCount
        else
          FXLWorksheet.Cells[ROW_TARGET_VALUES + i,8].Value := 0;
      end;
    end;

    FXLWorksheet.Cells[ROW_TARGET_VALUES + 6,2].Value := eBaselineCogLevel;
    FXLWorksheet.Cells[ROW_TARGET_VALUES + 7,2].Value := eBaselineMotorLevel;
    FXLWorksheet.Cells[ROW_TARGET_VALUES + 8,2].Value := eBaselineAssociationLevel;
    FXLWorksheet.Cells[ROW_TARGET_VALUES + 9,2].Value := FiLoopsBeforeTargets;

    if not IsAverages then begin
      //Show a summary of the outcomes of the runs. How many runs failed and
      //what the failure was
      iTotalFailed := arFail[TARGET_LOOPS] + arFail[TARGET_CHOSEN] + arFail[DIRECTION_LOOPS]
                    + arFail[DIRECTION_CHOSEN] + arFail[ACTIVATIONS];
      FXLWorksheet.Cells[ROW_OUTCOMES + 1,2].Value := FRunsPerSimulation - iTotalFailed; //successful runs
      FXLWorksheet.Cells[ROW_OUTCOMES + 2,2].Value := iTotalFailed;
      FXLWorksheet.Cells[ROW_OUTCOMES + 3,2].Value := arFail[TARGET_CHOSEN];  //wrong target
      FXLWorksheet.Cells[ROW_OUTCOMES + 4,2].Value := arFail[TARGET_LOOPS]+ arFail[ACTIVATIONS];  //unable to choose target
      FXLWorksheet.Cells[ROW_OUTCOMES + 5,2].Value := arFail[DIRECTION_CHOSEN]; //wrong direction
      FXLWorksheet.Cells[ROW_OUTCOMES + 6,2].Value := arFail[DIRECTION_LOOPS];   //unable to choose direction
      FXLWorksheet.Cells[ROW_OUTCOMES + 7,2].Value := FTotalRewarded;
      FXLWorksheet.Cells[ROW_OUTCOMES + 8,2].Value := FTotalOptimum;

      FXLWorksheet.Cells[ROW_OUTCOMES + 1,3].Value := (FRunsPerSimulation - iTotalFailed) / FRunsPerSimulation;
      FXLWorksheet.Cells[ROW_OUTCOMES + 2,3].Value := iTotalFailed / FRunsPerSimulation;
      FXLWorksheet.Cells[ROW_OUTCOMES + 3,3].Value := arFail[TARGET_CHOSEN] / FRunsPerSimulation;
      FXLWorksheet.Cells[ROW_OUTCOMES + 4,3].Value := (arFail[TARGET_LOOPS] + arFail[ACTIVATIONS])/ FRunsPerSimulation;
      FXLWorksheet.Cells[ROW_OUTCOMES + 5,3].Value := arFail[DIRECTION_CHOSEN] / FRunsPerSimulation;
      FXLWorksheet.Cells[ROW_OUTCOMES + 6,3].Value := arFail[DIRECTION_LOOPS] / FRunsPerSimulation;
      FXLWorksheet.Cells[ROW_OUTCOMES + 7,3].Value := FTotalRewarded / FRunsPerSimulation;
      FXLWorksheet.Cells[ROW_OUTCOMES + 8,3].Value := FTotalOptimum / FRunsPerSimulation;
    end;

    FXLWorksheet.Columns[1].ColumnWidth := 40;
    for i := 2 to 7 do
      FXLWorksheet.Columns[i].ColumnWidth := 15;
    FXLWorksheet.Columns[COL_CONNECTIVITY].ColumnWidth := 36;

    bSent := true;
  except
    on E: Exception do begin
//      Abort
//        ShowMessage(E.Message);
    end;
  end;
end;

procedure TfrmMain.SendPairingsSummaryToExcel;
var bSent: boolean;
    i,
    iMax: integer;
begin
  iMax := 0;
  for i := 1 to 6 do begin
    if arContext[i].ChosenCount > iMax then
      iMax := arContext[i].ChosenCount;
  end;

  bSent := false;
  while not bSent do try
    FExcelWorksheet := 'Contexts';
    FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];
    FXLWorksheet.Cells[iMax + 5, 1] := 'Count';
    FXLWorksheet.Cells[iMax + 5, 2] := '=COUNTIF(B3:B' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 3] := '=COUNTIF(C3:C' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 4] := '=COUNTIF(D3:D' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 7] := '=COUNTIF(G3:G' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 8] := '=COUNTIF(H3:H' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 9] := '=COUNTIF(I3:I' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 12] := '=COUNTIF(L3:L' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 13] := '=COUNTIF(M3:M' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 14] := '=COUNTIF(N3:N' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 17] := '=COUNTIF(Q3:Q' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 18] := '=COUNTIF(R3:R' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 19] := '=COUNTIF(S3:S' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 22] := '=COUNTIF(V3:V' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 23] := '=COUNTIF(W3:W' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 24] := '=COUNTIF(X3:X' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 27] := '=COUNTIF(AA3:AA' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 28] := '=COUNTIF(AB3:AB' + IntToStr(iMax + 2) +',"Yes")';
    FXLWorksheet.Cells[iMax + 5, 29] := '=COUNTIF(AC3:AC' + IntToStr(iMax + 2) +',"Yes")';

    FXLWorksheet.Cells[iMax + 6, 1] := 'Proportion';
    FXLWorksheet.Cells[iMax + 6, 2] := '=B' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[1].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 3] := '=C' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[1].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 4] := '=D' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[1].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 7] := '=G' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[2].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 8] := '=H' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[2].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 9] := '=I' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[2].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 12] := '=L' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[3].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 13] := '=M' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[3].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 14] := '=N' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[3].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 17] := '=Q' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[4].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 18] := '=R' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[4].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 19] := '=S' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[4].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 22] := '=V' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[5].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 23] := '=W' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[5].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 24] := '=X' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[5].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 27] := '=AA' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[6].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 28] := '=AB' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[6].ChosenCount);
    FXLWorksheet.Cells[iMax + 6, 29] := '=AC' + IntToStr(iMax + 5) +'/' + IntToStr(arContext[6].ChosenCount);

    bSent := true;
  except
    on E: Exception do begin
//      Abort
//      ShowMessage(E.Message);
    end;
  end;
end;

{
This is called after every simulation is finished to calculate averages etc. for the trials of the
simulation and put them in the correct worksheet of the workbook for that simulation
}
procedure TfrmMain.SendTrialsSummaryToExcel;
var bSent: boolean;
    i: integer;
    iTEqualsD,
    iTEqualsDLast50: integer;
    XLws: OLEVariant;
    arTargetChosen: array[0..4] of integer;
    arDirectionChosen: array[0..4] of integer;
    // At which row starts the last 50 set of trials
    last50RowStart: integer;
    // How many trials to compute the average ?
    nbAverageTrials: integer;
begin

  // if less than 50 runs occured, we have to set the first row (otherwise it'll cross table boundary)
  // (change 'Last 50' text in that case)
  if  FCurrentRun < 50 then
    begin
    last50RowStart := 2;
    nbAverageTrials := FCurrentRun;
    end
  else
    begin
    last50RowStart := FCurrentRun - 48;
    nbAverageTrials := 50;
    end;
  // (rendez-moi mon op�rateur ternaire !)

  // TODO: improve readability with for example : trialsRowStop := FCurrentRun + 1;

  bSent := false;
  while not bSent do try
    FXLWorksheet := FXLWorkbook.Worksheets['Trials'];
    FXLWorksheet.Cells[FCurrentRun + 3,1] := 'Appearances';
    FXLWorksheet.Cells[FCurrentRun + 3,4] := 'Sum';
    FXLWorksheet.Cells[FCurrentRun + 9,1] := 'Sum';
    FXLWorksheet.Cells[FCurrentRun + 16,1] := 'Sum';
    for i := 1 to 4 do begin
      //show how many times each target was shown to check there is no bias
      FXLWorksheet.Cells[FCurrentRun + 3 + i,1] := 'T' + IntToStr(i);
      FXLWorksheet.Cells[FCurrentRun + 3 + i,2] := '=COUNTIF(B2:B' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      FXLWorksheet.Cells[FCurrentRun + 3 + i,3] := '=COUNTIF(C2:C' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      //Sum of target appearances as either first or second target. Should be roughly trials / 2
      FXLWorksheet.Cells[FCurrentRun + 3 + i,4] := '=SUM(B' + IntToStr(FCurrentRun + 3 + i) + ':C' + IntToStr(FCurrentRun + 3 + i) + ')';
      //then do the same for the directions
      FXLWorksheet.Cells[FCurrentRun + 10 + i,1] := 'D' + IntToStr(i);
      FXLWorksheet.Cells[FCurrentRun + 10 + i,2] := '=COUNTIF(D2:D' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      FXLWorksheet.Cells[FCurrentRun + 10 + i,3] := '=COUNTIF(E2:E' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      //Sum of direction appearances as either first or second target. Should be roughly trials / 2
      FXLWorksheet.Cells[FCurrentRun + 10 + i,4] := '=SUM(B' + IntToStr(FCurrentRun + 10 + i) + ':C' + IntToStr(FCurrentRun + 10 + i) + ')';
    end;

    //Sum of appearances by all targets. Should equal the number of trials
    FXLWorksheet.Cells[FCurrentRun + 9, 2] := '=SUM(B' + IntToStr(FCurrentRun + 4) + ':B' + IntToStr(FCurrentRun + 7) + ')';
    FXLWorksheet.Cells[FCurrentRun + 9, 3] := '=SUM(C' + IntToStr(FCurrentRun + 4) + ':C' + IntToStr(FCurrentRun + 7) + ')';
    //Sum of appearances by all directions. Should equal the number of trials
    FXLWorksheet.Cells[FCurrentRun + 16, 2] := '=SUM(B' + IntToStr(FCurrentRun + 11) + ':B' + IntToStr(FCurrentRun + 14) + ')';
    FXLWorksheet.Cells[FCurrentRun + 16, 3] := '=SUM(C' + IntToStr(FCurrentRun + 11) + ':C' + IntToStr(FCurrentRun + 14) + ')';

    FXLWorksheet.Cells[FCurrentRun + 18,1] := 'Chosen';
    FXLWorksheet.Cells[FCurrentRun + 18,2] := 'Count';
    FXLWorksheet.Cells[FCurrentRun + 18,3] := 'Proportion';
    FXLWorksheet.Cells[FCurrentRun + 18,4] := 'Last ' + IntToStr(nbAverageTrials);
    FXLWorksheet.Cells[FCurrentRun + 18,5] := 'Proportion';
    FXLWorksheet.Cells[FCurrentRun + 25,1] := 'Sum';
    FXLWorksheet.Cells[FCurrentRun + 33,1] := 'Sum';
    for i := 0 to 4 do begin
      //show how many times each target was chosen
      if i = 0 then
        FXLWorksheet.Cells[FCurrentRun + 19 + i,1] := 'No target chosen'
      else
        FXLWorksheet.Cells[FCurrentRun + 19 + i,1] := 'T' + IntToStr(i);
      FXLWorksheet.Cells[FCurrentRun + 19 + i,2] := '=COUNTIF(H2:H' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      FXLWorksheet.Cells[FCurrentRun + 19 + i,3] := '=COUNTIF(H2:H' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ') / ' + IntToStr(FCurrentRun);
      FXLWorksheet.Cells[FCurrentRun + 19 + i,4] := '=COUNTIF(H'+ IntToStr(last50RowStart) + ':H' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      FXLWorksheet.Cells[FCurrentRun + 19 + i,5] := '=COUNTIF(H'+ IntToStr(last50RowStart) + ':H' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ') / ' + IntToStr(nbAverageTrials);
      //then how many times each direction was chosen
      if i = 0 then
        FXLWorksheet.Cells[FCurrentRun + 27 + i,1] := 'No direction chosen'
      else
        FXLWorksheet.Cells[FCurrentRun + 27 + i,1] := 'D' + IntToStr(i);
      FXLWorksheet.Cells[FCurrentRun + 27 + i,2] := '=COUNTIF(I2:I' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      FXLWorksheet.Cells[FCurrentRun + 27 + i,3] := '=COUNTIF(I2:I' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ') / ' + IntToStr(FCurrentRun);
      FXLWorksheet.Cells[FCurrentRun + 27 + i,4] := '=COUNTIF(I'+ IntToStr(last50RowStart) + ':I' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      FXLWorksheet.Cells[FCurrentRun + 27 + i,5] := '=COUNTIF(I'+ IntToStr(last50RowStart) + ':I' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ') / ' + IntToStr(nbAverageTrials);
    end;
    //Sum of targets chosen. Should equal the number of trials
    FXLWorksheet.Cells[FCurrentRun + 25, 2] := '=SUM(B' + IntToStr(FCurrentRun + 19) + ':B' + IntToStr(FCurrentRun + 23) + ')';
    //Sum of directions chosen. Should equal the number of trials
    FXLWorksheet.Cells[FCurrentRun + 33, 2] := '=SUM(B' + IntToStr(FCurrentRun + 27) + ':B' + IntToStr(FCurrentRun + 31) + ')';

    //Number of trials where the target chosen and the target by direction are the same
    FXLWorksheet.Cells[FCurrentRun + 9,10] := 'T=T by D';
    FXLWorksheet.Cells[FCurrentRun + 10,10] := 'Last ' + IntToStr(nbAverageTrials);
    FXLWorksheet.Cells[FCurrentRun + 8,11] := 'Count';
    FXLWorksheet.Cells[FCurrentRun + 8,12] := 'Proportion';
    //Number of trials where the direction chosen was the correct direction for the target chosen
    FXLWorksheet.Cells[FCurrentRun + 9,11] := '=COUNTIF(K2:K' + IntToStr(FCurrentRun + 1) + ',TRUE)';
    //Number of trials where the direction chosen was the correct direction for the target chosen in the last 50 trials
    FXLWorksheet.Cells[FCurrentRun + 10,11] := '=COUNTIF(K'+ IntToStr(last50RowStart) + ':K' + IntToStr(FCurrentRun + 1) + ',TRUE)';
    //Proportion of trials where the direction chosen was the correct direction for the target chosen
    FXLWorksheet.Cells[FCurrentRun + 9,12] := '=COUNTIF(K2:K' + IntToStr(FCurrentRun + 1) + ',TRUE) / ' + IntToStr(FCurrentRun);
    //Proportion of trials where the direction chosen was the correct direction for the target chosen in the last 50 trials
    FXLWorksheet.Cells[FCurrentRun + 10,12] := '=COUNTIF(K'+ IntToStr(last50RowStart) + ':K' + IntToStr(FCurrentRun + 1) + ',TRUE) / ' + IntToStr(nbAverageTrials);

    FXLWorksheet.Cells[FCurrentRun + 3,11] := 'Outcomes';
    FXLWorksheet.Cells[FCurrentRun + 4,11] := 'Proportion';
    FXLWorksheet.Cells[FCurrentRun + 5,11] := 'Last ' + IntToStr(nbAverageTrials);
    FXLWorksheet.Cells[FCurrentRun + 2,12] := 'Successful?';
    FXLWorksheet.Cells[FCurrentRun + 2,13] := 'Optimum?';
    FXLWorksheet.Cells[FCurrentRun + 2,14] := 'Rewarded?';
    //Number of successful trials
    FXLWorksheet.Cells[FCurrentRun + 3,12] := '=COUNTIF(L2:L' + IntToStr(FCurrentRun + 1) + ',"Yes")';
    //number of optimum trials
    FXLWorksheet.Cells[FCurrentRun + 3,13] := '=COUNTIF(M2:M' + IntToStr(FCurrentRun + 1) + ',"Yes")';
    //Number of trials rewarded
    FXLWorksheet.Cells[FCurrentRun + 3,14] := '=COUNTIF(N2:N' + IntToStr(FCurrentRun + 1) + ',"Yes")';
    //Proportion of trials successful
    FXLWorksheet.Cells[FCurrentRun + 4,12] := '=L' + IntToStr(FCurrentRun + 3) +  '/L' + IntToStr(FCurrentRun + 3);
    //Proportion of successful trials optimum
    FXLWorksheet.Cells[FCurrentRun + 4,13] := '=M' + IntToStr(FCurrentRun + 3) +  '/L' + IntToStr(FCurrentRun + 3);
    //Proportion of successful trials rewarded
    FXLWorksheet.Cells[FCurrentRun + 4,14] := '=N' + IntToStr(FCurrentRun + 3) +  '/L' + IntToStr(FCurrentRun + 3);

    FXLWorksheet.Cells[FCurrentRun + 2,15] := 'Average loops';
    //aAverage number of loops over the trials. Mainly useful for when learning is off.
    FXLWorksheet.Cells[FCurrentRun + 3,15] := '=AVERAGE(O2:O' + IntToStr(FCurrentRun + 1) + ')';

    if FRunsPerSimulation >= 50 then begin
      FXLWorksheet.Cells[FCurrentRun + 5,12] := '=COUNTIF(L'+ IntToStr(last50RowStart) + ':L' + IntToStr(FCurrentRun + 1) + ',"Yes") / ' + IntToStr(nbAverageTrials);
      FXLWorksheet.Cells[FCurrentRun + 5,13] := '=COUNTIF(M'+ IntToStr(last50RowStart) + ':M' + IntToStr(FCurrentRun + 1) + ',"Yes") / ' + IntToStr(nbAverageTrials);
      FXLWorksheet.Cells[FCurrentRun + 5,14] := '=COUNTIF(N'+ IntToStr(last50RowStart) + ':N' + IntToStr(FCurrentRun + 1) + ',"Yes") / ' + IntToStr(nbAverageTrials);
    end;

    {
    For every run read the trials worksheet to get:
      Number of times that the direction chosen matched target chosen
      Number of times that the direction chosen matched target chosen in the last 50 trials
      Number of times each target chosen (including no target chosen)
      Number of times no direction chosen (including no direction chosen, although this should be zero if all trials were successful)
      Then do the same for the last 50 trials of each run
    Then send these to one row on the outcomes sheet of the averages workbook
    }
    if FSimsPerExperiment > 1 then begin
      XLws := FXLAveragesWorkbook.Worksheets['Outcomes'];
      iTEqualsD := ReadXLCellInteger(FXLWorksheet, FRunsPerSimulation+9, 11);
      iTEqualsDLast50 := ReadXLCellInteger(FXLWorksheet, FRunsPerSimulation+10, 11);
      WriteXLCell(XLws, FCurrentSimulation+1, 10, iTEqualsD);
      WriteXLCell(XLws, FSimsPerExperiment + FCurrentSimulation+4, 10 , iTEqualsDLast50);
      for i := 0 to 4 do begin
        arTargetChosen[i] := ReadXLCellInteger(FXLWorksheet, FRunsPerSimulation+19+i, 2);
        arDirectionChosen[i] := ReadXLCellInteger(FXLWorksheet, FRunsPerSimulation+27+i, 2);
        WriteXLCell(XLws, FCurrentSimulation+1, 11+i, arTargetChosen[i]);
        WriteXLCell(XLws, FCurrentSimulation+1, 16+i, arDirectionChosen[i]);
        //then transfer the data for the targets and directions chosen during the last 50 trials
        arTargetChosen[i] := ReadXLCellInteger(FXLWorksheet, FRunsPerSimulation+19+i, 4);
        arDirectionChosen[i] := ReadXLCellInteger(FXLWorksheet, FRunsPerSimulation+27+i, 4);
        WriteXLCell(XLws, FSimsPerExperiment + FCurrentSimulation+4, 11+i, arTargetChosen[i]);
        WriteXLCell(XLws, FSimsPerExperiment + FCurrentSimulation+4, 16+i, arDirectionChosen[i]);
      end;
    end;

    bSent := true;
  except
    on E: Exception do begin
//      Abort
//      ShowMessage(E.Message);
    end;
  end;
end;

procedure TfrmMain.SendWeightsSummaryToExcel;
var bSent: boolean;
    i: integer;
begin
  bSent := false;
  while not bSent do try
    FExcelWorksheet := 'Weights';
    FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];
    FXLWorksheet.Cells[FCurrentRun + 3,1] := 'Appearances';
    FXLWorksheet.Cells[FCurrentRun + 3,4] := 'Sum';
    FXLWorksheet.Cells[FCurrentRun + 9,1] := 'Sum';
    FXLWorksheet.Cells[FCurrentRun + 11,4] := 'Sum';
    FXLWorksheet.Cells[FCurrentRun + 16,1] := 'Sum';
    for i := 1 to 4 do begin
      //show how many times each target was shown to check there is no bias
      FXLWorksheet.Cells[FCurrentRun + 3 + i,1] := 'T' + IntToStr(i);
      FXLWorksheet.Cells[FCurrentRun + 3 + i,2] := '=COUNTIF(B2:B' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      FXLWorksheet.Cells[FCurrentRun + 3 + i,3] := '=COUNTIF(C2:C' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      FXLWorksheet.Cells[FCurrentRun + 3 + i,4] := '=SUM(B' + IntToStr(FCurrentRun + 3 + i) + ':C' + IntToStr(FCurrentRun + 3 + i) + ')';
      //then do the same for the directions
      FXLWorksheet.Cells[FCurrentRun + 10 + i,1] := 'D' + IntToStr(i);
      FXLWorksheet.Cells[FCurrentRun + 10 + i,2] := '=COUNTIF(D2:D' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      FXLWorksheet.Cells[FCurrentRun + 10 + i,3] := '=COUNTIF(E2:E' + IntToStr(FCurrentRun + 1) + ',' + IntToStr(i) + ')';
      FXLWorksheet.Cells[FCurrentRun + 10 + i,4] := '=SUM(B' + IntToStr(FCurrentRun + 10 + i) + ':C' + IntToStr(FCurrentRun + 10 + i) + ')';
    end;
    FXLWorksheet.Cells[FCurrentRun + 9, 2] := '=SUM(B' + IntToStr(FCurrentRun + 4) + ':B' + IntToStr(FCurrentRun + 7) + ')';
    FXLWorksheet.Cells[FCurrentRun + 9, 3] := '=SUM(C' + IntToStr(FCurrentRun + 4) + ':C' + IntToStr(FCurrentRun + 7) + ')';
    FXLWorksheet.Cells[FCurrentRun + 16, 2] := '=SUM(B' + IntToStr(FCurrentRun + 11) + ':B' + IntToStr(FCurrentRun + 14) + ')';
    FXLWorksheet.Cells[FCurrentRun + 16, 3] := '=SUM(C' + IntToStr(FCurrentRun + 11) + ':C' + IntToStr(FCurrentRun + 14) + ')';

    FXLWorksheet.Cells[FCurrentRun + 3,9] := 'Outcomes';
    FXLWorksheet.Cells[FCurrentRun + 4,9] := 'Proportion';
    FXLWorksheet.Cells[FCurrentRun + 2,10] := 'Successful?';
    FXLWorksheet.Cells[FCurrentRun + 2,11] := 'Optimum?';
    FXLWorksheet.Cells[FCurrentRun + 2,12] := 'Rewarded?';
    FXLWorksheet.Cells[FCurrentRun + 3,10] := '=COUNTIF(J2:J' + IntToStr(FCurrentRun + 1) + ',"Yes")';
    FXLWorksheet.Cells[FCurrentRun + 3,11] := '=COUNTIF(K2:K' + IntToStr(FCurrentRun + 1) + ',"Yes")';
    FXLWorksheet.Cells[FCurrentRun + 3,12] := '=COUNTIF(L2:L' + IntToStr(FCurrentRun + 1) + ',"Yes")';
    FXLWorksheet.Cells[FCurrentRun + 4,10] := '=J' + IntToStr(FCurrentRun + 3) +  '/J' + IntToStr(FCurrentRun + 3);
    FXLWorksheet.Cells[FCurrentRun + 4,11] := '=K' + IntToStr(FCurrentRun + 3) +  '/J' + IntToStr(FCurrentRun + 3);
    FXLWorksheet.Cells[FCurrentRun + 4,12] := '=L' + IntToStr(FCurrentRun + 3) +  '/J' + IntToStr(FCurrentRun + 3);

    bSent := true;
  except
    on E: Exception do begin
//      Abort
//      ShowMessage(E.Message);
    end;
  end;
end;

{
At the end of all the simulations send the outomes to one worksheet and put in the
calculations for the averages and SDs on all the other worksheets
}
procedure TfrmMain.SendAveragesSummaryToExcel;
var i, j, k: integer;
    wsAverages,
    wsAverageWeights,
    wsSdWeights,
    wsSErrWeights: OLEVariant;
    eWeight: real;
begin
  if FSimsPerExperiment > 1 then begin
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    try
      SendAverageLoopsFormulaeToExcel(FXLAveragesWorkbook.Worksheets['Cognitive loops']);
      SendAverageLoopsFormulaeToExcel(FXLAveragesWorkbook.Worksheets['Motor loops']);
      SendAverageLoopsFormulaeToExcel(FXLAveragesWorkbook.Worksheets['Total loops']);

      wsAverages := FXLAveragesWorkbook.Worksheets['Outcomes'];
      for i := 1 to FRunsPerSimulation do begin
        WriteXLCell(wsAverages, i+1, 2, arSimulation[i-1].Successful);
        WriteXLCell(wsAverages, i+1, 3, arSimulation[i-1].Optimum);
        WriteXLCell(wsAverages, i+1, 4, arSimulation[i-1].Rewarded);
        WriteXLCell(wsAverages, i+1, 5, arSimulation[i-1].Successful / FSimsPerExperiment);
        WriteXLCell(wsAverages, i+1, 6, arSimulation[i-1].Optimum / FSimsPerExperiment);
        WriteXLCell(wsAverages, i+1, 7, arSimulation[i-1].Rewarded / FSimsPerExperiment);
      end;
//      WriteXLCell(wsAverages, 1, 9, 'Total simulations: '+ IntToStr(FSimsPerExperiment));

      //Then do the averages, SDs and SErrs
      for j := 1 to 4 do begin
        wsAverages := FXLAveragesWorkbook.Worksheets['WT'+ IntToStr(j)];
        for k := 1 to FRunsPerSimulation do begin
          WriteXLCell(wsAverages,k+1,FSimsPerExperiment + 2,'=AVERAGE(B' + IntToStr(k+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(k+1) + ')');
          WriteXLCell(wsAverages,k+1,FSimsPerExperiment + 3,'=STDEV(B' + IntToStr(k+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(k+1) + ')');
          WriteXLCell(wsAverages,k+1,FSimsPerExperiment + 4,'=STDEV(B' + IntToStr(k+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(k+1) + ')/SQRT(' + IntToStr(FSimsPerExperiment) + ')');
        end;
      end;
      for j := 1 to 4 do begin
        wsAverages := FXLAveragesWorkbook.Worksheets['WD'+ IntToStr(j)];
        for k := 1 to FRunsPerSimulation do begin
          WriteXLCell(wsAverages,k+1,FSimsPerExperiment + 2,'=AVERAGE(B' + IntToStr(k+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(k+1) + ')');
          WriteXLCell(wsAverages,k+1,FSimsPerExperiment + 3,'=STDEV(B' + IntToStr(k+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(k+1) + ')');
          WriteXLCell(wsAverages,k+1,FSimsPerExperiment + 4,'=STDEV(B' + IntToStr(k+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(k+1) + ')/SQRT(' + IntToStr(FSimsPerExperiment) + ')');
        end;
      end;
      for j := 1 to 16 do begin
        wsAverages := FXLAveragesWorkbook.Worksheets['WA'+ IntToStr(j)];
        for k := 1 to FRunsPerSimulation do begin
          WriteXLCell(wsAverages,k+1,FSimsPerExperiment + 2,'=AVERAGE(B' + IntToStr(k+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(k+1) + ')');
          WriteXLCell(wsAverages,k+1,FSimsPerExperiment + 3,'=STDEV(B' + IntToStr(k+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(k+1) + ')');
          WriteXLCell(wsAverages,k+1,FSimsPerExperiment + 4,'=STDEV(B' + IntToStr(k+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(k+1) + ')/SQRT(' + IntToStr(FSimsPerExperiment) + ')');
        end;
      end;

      //Finally, transfer the average, sd and serr of the weights to the weights
      //summary pages
      wsAverageWeights := FXLAveragesWorkbook.Worksheets['Average weights'];
      wsSdWeights := FXLAveragesWorkbook.Worksheets['SD weights'];
      wsSErrWeights := FXLAveragesWorkbook.Worksheets['SErr weights'];
      for i := 1 to 4 do begin
        wsAverages := FXLAveragesWorkbook.Worksheets['WT'+ IntToStr(i)];
        for j := 1 to FRunsPerSimulation do begin
          eWeight := ReadXlCellReal(wsAverages, j + 1, FSimsPerExperiment + 2);
          WriteXLCell(wsAverageWeights, j+1, i+1, eWeight);
          //If there is only one simulation, then the SD and SErr will be #DIV/0!, so cannot be transferred
          if FSimsPerExperiment > 1 then begin
            eWeight := ReadXlCellReal(wsAverages, j + 1, FSimsPerExperiment + 3);
            WriteXLCell(wsSdWeights, j+1, i+1, eWeight);
            eWeight := ReadXlCellReal(wsAverages, j + 1, FSimsPerExperiment + 4);
            WriteXLCell(wsSErrWeights, j+1, i+1, eWeight);
          end;
        end;
      end;
      for i := 1 to 4 do begin
        wsAverages := FXLAveragesWorkbook.Worksheets['WD'+ IntToStr(i)];
        for j := 1 to FRunsPerSimulation do begin
          eWeight := ReadXlCellReal(wsAverages, j + 1, FSimsPerExperiment + 2);
          WriteXLCell(wsAverageWeights, j+1, i+5, eWeight);
          if FSimsPerExperiment > 1 then begin
            eWeight := ReadXlCellReal(wsAverages, j + 1, FSimsPerExperiment + 3);
            WriteXLCell(wsSdWeights, j+1, i+5, eWeight);
            eWeight := ReadXlCellReal(wsAverages, j + 1, FSimsPerExperiment + 4);
            WriteXLCell(wsSErrWeights, j+1, i+5, eWeight);
          end;
        end;
      end;
      for i := 1 to 16 do begin
        wsAverages := FXLAveragesWorkbook.Worksheets['WA'+ IntToStr(i)];
        for j := 1 to FRunsPerSimulation do begin
          eWeight := ReadXlCellReal(wsAverages, j + 1, FSimsPerExperiment + 2);
          WriteXLCell(wsAverageWeights, j+1, i+9, eWeight);
          if FSimsPerExperiment > 1 then begin
            eWeight := ReadXlCellReal(wsAverages, j + 1, FSimsPerExperiment + 3);
            WriteXLCell(wsSdWeights, j+1, i+9, eWeight);
            eWeight := ReadXlCellReal(wsAverages, j + 1, FSimsPerExperiment + 4);
            WriteXLCell(wsSErrWeights, j+1, i+9, eWeight);
          end;
        end;
      end;

      SendAveragePairingsToExcel;

      FXLAveragesWorkbook.Save;

    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TfrmMain.SendAverageLoopsFormulaeToExcel(AWorksheet: OleVariant);
var i: integer;
begin
  for i := 1 to FRunsPerSimulation do begin
    WriteXLCell(AWorksheet, i + 1, FSimsPerExperiment + 2, '=AVERAGE(B' + IntToStr(i+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(i+1) + ')');
    WriteXLCell(AWorksheet, i + 1, FSimsPerExperiment + 3, '=STDEV(B' + IntToStr(i+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(i+1) + ')');
    WriteXLCell(AWorksheet, i + 1, FSimsPerExperiment + 4, '=STDEV(B' + IntToStr(i+1) + ':'+ CharSet[FSimsPerExperiment + 1] + IntToStr(i+1) + ')/SQRT(' + IntToStr(FSimsPerExperiment) + ')');
  end;
end;

procedure TfrmMain.SendAveragePairingsToExcel;
var i, j,
    iRunCount: integer;
const COLS_PER_PAIRING = 5;
begin
  if FSimsPerExperiment > 1 then begin
    FXLWorksheet := FXLAveragesWorkbook.Worksheets['Contexts'];
    iRunCount := 0;
    for i := 0 to pred(FiPairAppearances) do begin
      for j := 0 to pred(FiNumberOfPairs) do begin
        inc(iRunCount);
        if iRunCount > FRunsPerSimulation then
          BREAK;
        WriteXLCell(FXLWorksheet, i+3, (j*COLS_PER_PAIRING)+2, arContextOutcome[j,i].Successful);
        WriteXLCell(FXLWorksheet, i+3, (j*COLS_PER_PAIRING)+3, arContextOutcome[j,i].Optimum);
        WriteXLCell(FXLWorksheet, i+3, (j*COLS_PER_PAIRING)+4, arContextOutcome[j,i].Rewarded);
        WriteXLCell(FXLWorksheet, i+3, (j*COLS_PER_PAIRING)+5, arContextOutcome[j,i].Value / FSimsPerExperiment);
        WriteXLCell(FXLWorksheet, i+3, (j*COLS_PER_PAIRING)+6, arContextOutcome[j,i].PE / FSimsPerExperiment);
      end;
    end;
  end;
end;

procedure TfrmMain.grdCxCogCellProps(Sender: TObject; Canvas: TCanvas;
  var Alignment: TAlignment; var CellText: String; AState: TGridDrawState;
  Row, Col: Integer);
begin
  if StrToFloat(TXStringGrid(Sender).Cells[Col, Row], FFormatSettings) > FeSelectionThreshold then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

procedure TfrmMain.grdCxMotorCellProps(Sender: TObject; Canvas: TCanvas;
  var Alignment: TAlignment; var CellText: String; AState: TGridDrawState;
  Row, Col: Integer);
begin
  if StrToFloat(TXStringGrid(Sender).Cells[Col, Row], FFormatSettings) > (eBaselineMotorLevel / 2) then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

procedure TfrmMain.grdCxAssCellProps(Sender: TObject; Canvas: TCanvas;
  var Alignment: TAlignment; var CellText: String; AState: TGridDrawState;
  Row, Col: Integer);
var eDivisor: real;
begin
  if bShowWeights then
    eDivisor := (eBaselineAssociationLevel * arConnection[Col,Row].Real)/ 2 
  else
    eDivisor := eBaselineAssociationLevel / 2;
  if StrToFloat(TXStringGrid(Sender).Cells[Col, Row], FFormatSettings) > eDivisor then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

procedure TfrmMain.grdStrMotorCellProps(Sender: TObject; Canvas: TCanvas;
  var Alignment: TAlignment; var CellText: String; AState: TGridDrawState;
  Row, Col: Integer);
begin
  if StrToFloat(TXStringGrid(Sender).Cells[Col, Row], FFormatSettings) > eMSNThresholdLevel then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

procedure TfrmMain.grdStrCogCellProps(Sender: TObject; Canvas: TCanvas;
  var Alignment: TAlignment; var CellText: String; AState: TGridDrawState;
  Row, Col: Integer);
begin
  if StrToFloat(TXStringGrid(Sender).Cells[Col, Row], FFormatSettings) > eMSNThresholdLevel then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

procedure TfrmMain.grdStrAssCellProps(Sender: TObject; Canvas: TCanvas;
  var Alignment: TAlignment; var CellText: String; AState: TGridDrawState;
  Row, Col: Integer);
var eThreshold: real;
begin
  if StrDefaults.Threshold > 0 then
    eThreshold := 0
  else
    eThreshold := -1 * StrDefaults.Threshold;
//  if StrToFloat(TXStringGrid(Sender).Cells[Col, Row], FFormatSettings) > eMSNThresholdLevel then
  if StrToFloat(TXStringGrid(Sender).Cells[Col, Row], FFormatSettings) > eThreshold then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

procedure TfrmMain.grdStnMotorCellProps(Sender: TObject; Canvas: TCanvas;
  var Alignment: TAlignment; var CellText: String; AState: TGridDrawState;
  Row, Col: Integer);
begin
  if StrToFloat(TXStringGrid(Sender).Cells[Col, Row], FFormatSettings) > StnDefaults.Threshold + (StnDefaults.Threshold * StnDefaults.NoiseProportion) then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

procedure TfrmMain.grdGPiMotorCellProps(Sender: TObject; Canvas: TCanvas;
  var Alignment: TAlignment; var CellText: String; AState: TGridDrawState;
  Row, Col: Integer);
begin
  if StrToFloat(TXStringGrid(Sender).Cells[Col, Row], FFormatSettings) < GpiDefaults.Threshold - (GpiDefaults.Threshold * GpiDefaults.NoiseProportion) then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

procedure TfrmMain.grdThMotorCellProps(Sender: TObject; Canvas: TCanvas;
  var Alignment: TAlignment; var CellText: String; AState: TGridDrawState;
  Row, Col: Integer);
begin
  if StrToFloat(TXStringGrid(Sender).Cells[Col, Row], FFormatSettings) > ThDefaults.Threshold + (ThDefaults.Threshold * ThDefaults.NoiseProportion  ) then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var hProc:THandle;
begin
  terminateServer;
  hProc := OpenProcess(PROCESS_TERMINATE, False, GetCurrentProcessId);
  TerminateProcess(hProc, 0);
  if not VarIsEmpty(FXLApp) then begin
    FXLApp.DisplayAlerts := false;
    FXLApp.Quit;
    // CoUninitialize;
  end;
end;

{
For teaching, the students can only vary the striatum to GPi connectivity. This
is presented to them as the dopamine level.
A dopamine level of 1 is eStrToGpi = 1.8
A dopamine level of 0.2 is eStrToGpi = 0.6
Therefore a dopamine level of 0 is eStrToGpi = 0.3
}
procedure TfrmMain.sbDopamineLevelDownClick(Sender: TObject);
var eDopamineLevel: real;
begin
  eDopamineLevel := StrToFloat(edDopamineLevel.Text, FFormatSettings);
  if eDopamineLevel > 0.1 then begin
    eDopamineLevel := eDopamineLevel - 0.1;
    edDopamineLevel.Text := FloatToStr(eDopamineLevel, FFormatSettings);
    eCogStrToGpi := 0.3 + (eDopamineLevel * 1.5);
    eMotorStrToGpi := 0.3 + (eDopamineLevel * 1.5);
    eStrAssToGpi := 0.3 + (eDopamineLevel * 1.5);
  end;
end;

procedure TfrmMain.sbDopamineLevelUpClick(Sender: TObject);
var eDopamineLevel: real;
begin
  eDopamineLevel := StrToFloat(edDopamineLevel.Text, FFormatSettings);
  if eDopamineLevel < 1.2 then begin
    eDopamineLevel := eDopamineLevel + 0.1;
    edDopamineLevel.Text := FloatToStr(eDopamineLevel, FFormatSettings);
    eCogStrToGpi := 0.3 + (eDopamineLevel * 1.5);
    eMotorStrToGpi := 0.3 + (eDopamineLevel * 1.5);
    eStrAssToGpi := 0.3 + (eDopamineLevel * 1.5);
  end;
end;

{
  Regroup all procedures used after each trial to send data to Excel.

  SendProbability: if the model knows real probabilities of targets, ask SendTrialResultToTrialsWorksheet to send them as well.
}
procedure TfrmMain.SendTrialBatchResultToExcel(SendProbability: boolean);
begin
  SendTrialResultToExcel;
  SendTrialResultToTrialsWorksheet(SendProbability);
  SendTrialResultToEffectiveProbabilityWorksheet;
  SendTrialResultToPairingsWorksheet;
  SendWeightsToExcel;
  SendDeltaWeightsToExcel;
  SendTrialResultToAveragesWorkbook;
end;

{
  FIXME: factorize code with btnMultipleTrialsClick
  
  FIXME: Use Synchronize to properly update every GUI component. Should do for almost every operation in fact.
   (had a bad time with memory violation due to XStringGrid accessing in-use variables)

  TODO: set FRunsPerSimulation before the beginning of a session ? (a new command sent by BT ?)
  TODO: can erase previous simulation, or be erased by a futur one (InitializeSimulation a little weak)
  TODO: no AverageWorkBook right now (useful ?)
}
procedure TfrmMain.IdTCPServer1Execute(AThread: TIdPeerThread);
var sRec,
    sDirection: string;
    iReward: integer;
begin
  {
    An exception class EIdClosedSocket (Disconnected) will raise here when running inside debugger
    if a client is still connected and we try to stop the server. No big deal from what I read.
  }
  sRec := AThread.Connection.ReadLn;
  Memo1.Lines.Add(AThread.Connection.Socket.Binding.IP + ' sends : ' + sRec) ;
  if (sRec = 'S') then
  begin
    // A clever client shouldn't trigger this
    if bSessionStarted then
      AThread.Connection.WriteLn('Error: a session is aleady running.')
    else
      begin
      //Start message for the session
      bSessionStarted := true;
      AThread.Connection.WriteLn('OK');
      Memo1.Lines.Add(AThread.Connection.Socket.LocalName + ' sends: OK');
      // Ne pas oublier d'initialiser variables et tableaux pour r�sultats
      FCurrentSimulation := 0;
      InitializeSimulation;
      // TODO: mimic btnMultipleTrialsClick but I don't think it's necessary
      SetupPairings;
      // Je suppose que rentre dans le cadre d'une nouvelle simulation
      // Sinon CreateExcelWorkbook va avoir la bonne id�e de taper dans l'indice -1 d'un tableau autrement
      inc(FCurrentSimulation);

      // Prevent "EOleSysError, CoInitialize has not been called"
      // (here we are in a different thread than the main program)
      // TODO: won't work with several clients I guess, each one should have a unique reference to excel
      CoInitialize(nil);

      if StartExcel then
        // FIXME: and savelocation ? (used in multiple trials)
        CreateExcelWorkbook;

      InitializeWeights;
      FCurrentRun := 0;
      Application.ProcessMessages;
      end;
  end
  else if bSessionStarted then begin
    if sRec = 'End' then begin
      //message to end the session, all the trials have been done
      bSessionStarted := false;
      // no use anymore
      // bDirectionSent := true; //get out of the loop
      // Let's say for Excel's sake that we reach our aim
      FRunsPerSimulation := FCurrentRun;
      SendSetupToExcel(false);
      SendTrialsSummaryToExcel;
      SendWeightsSummaryToExcel;
      SendPairingsSummaryToExcel;
      lblCurrentRun2.Caption := 'Finished';
      // Close corresponding CoInitialize, set previously in (sRec = 'S')
      CoUninitialize;
      EXIT;
    end
    // FIXME: send error to client if two consecutive targets
//    if not bDirectionSent then begin
    else if sRec[1] = 'T' then begin
      inc(FCurrentRun);
      //must have just been sent the targets to start a trial
      AThread.Connection.WriteLn('Targets received');
      Memo1.Lines.Add(AThread.Connection.Socket.LocalName + ' sends: OK');
      Memo1.Lines.Add(' -- Iteration n�' + IntToStr(FCurrentRun));
      InitializeRun;
      if AddWorkSheet(FCurrentRun) then begin
        MakeImagesVisible;
        SetTargets(sRec);
        ShowTargetScreen;
        if SelectDirection(true) = 0 then begin
          if VerifyDirection then begin
            //iDirectionChosen is a global variable that is set in CheckCorticalActivation...called in SelectDirection
            sDirection := IntToStr(iDirectionChosen);
            {
            After a decision on the direction to move is made, send that to the task server.
            0 = no direction chosen
            1 = 0 degrees (i.e. right), 4 = 360 degrees (i.e. down)
            the user or the robot will move the joystick to the correct target (hopefully)
            based on visual feedback from the information sent to the
            behavioural task program
            }
            AThread.Connection.WriteLn(sDirection);
            Memo1.Lines.Add(AThread.Connection.Socket.LocalName + ' sends direction = ' + sDirection);
            bDirectionSent := true;
            iFlash := 0;
            FStage := 3;
            Timer1.Enabled := true;
            // As in btnMultipleTrialsClick
            CheckOptimum;
          end
          else begin
            //did not get a valid direction
            AThread.Connection.WriteLn('0');
            Memo1.Lines.Add(AThread.Connection.Socket.LocalName + ' sends direction = 0 (no valid direction)');
            SendTrialBatchResultToExcel(false); //don't send the probabilities as the model does not know them!
          end;
        end
        else begin
          //did not get a direction
          AThread.Connection.WriteLn('0');
          Memo1.Lines.Add(AThread.Connection.Socket.LocalName + ' sends direction = 0 (no direction)');
          SendTrialBatchResultToExcel(false); //don't send the probabilities as the model does not know them!
        end;
        Application.ProcessMessages;
      end;
    end
    // FIXME: flag if two consecutive rewards
    else if sRec[1] = 'R' then begin
      // If no direction has been chosen, a 0 reward has already been sent internaly in VerifyDirection
      if bDirectionSent then begin
        bDirectionSent := false;
        //Reward value sent
        iReward := StrToInt(sRec[2]);
        UpdateWeights(iReward,1);
        DisplayValues(3);
        // Didn't wait for a reward if no direction chosen when targets were presented
        SendTrialBatchResultToExcel(false); //don't send the probabilities as the model does not know them!
      end;
      Application.ProcessMessages;
    end
    else if sRec[1] = 'F' then begin
      //Fail. Probably did not move to any target in the time allowed
      SendTrialBatchResultToExcel(false); //don't send the probabilities as the model does not know them!
      //get ready for the next trial
      bDirectionSent := false;
      Application.ProcessMessages;
    end;
  end;
end;

{
For communication with the robot. When the robot sends the string of what targets
are being shown and where, decode it and put the correct values into arTarget.
Communication is done indirectly via the task server program
AString[2] = First target (0..3)
AString[3] = First target direction (0..3)
AString[4] = Second target (0..3)
AString[5] = Second target direction (0..3)
}
procedure TfrmMain.SetTargets(AString: string);
var i,
    iTarget1,
    iTarget2: integer;
begin
  ZeroArrays;
  ZeroGridCells;

  //arTarget is a 1 based array
  for i := 1 to 4 do begin
    arTarget[i].Direction := -1;
    arTarget[i].Show := false;
    arTarget[i].Correct := false;
    arTarget[i].ChosenAsDirection := false;
    arTarget[i].ChosenAsTarget := false;
    arDirectionShown[i] := false;
  end;

  iTarget1 := StrToInt(AString[2]);
  arTarget[iTarget1].Direction := StrToInt(AString[3]);
  arTarget[iTarget1].Show := true;

  iTarget2 := StrToInt(AString[4]);
  arTarget[iTarget2].Direction := StrToInt(AString[5]);
  arTarget[iTarget2].Show := true;

  arDirectionShown[arTarget[iTarget1].Direction] := true;
  arDirectionShown[arTarget[iTarget2].Direction] := true;

  SetCognitiveCortexActivation;
  SetMotorCortexActivation;
  SetAssociationCortexActivation;

  arTrial[FCurrentRun-1].Target1 := iTarget1;
  arTrial[FCurrentRun-1].Target[0] := arTarget[iTarget1];
  arTrial[FCurrentRun-1].Target2 := iTarget2;
  arTrial[FCurrentRun-1].Target[1] := arTarget[iTarget2];
  arTrial[FCurrentRun-1].Direction1 := arTarget[iTarget1].Direction;
  arTrial[FCurrentRun-1].Direction2 := arTarget[iTarget2].Direction;

  arTarget[iTarget1].PresentedCount := arTarget[iTarget1].PresentedCount + 1;
  arTarget[iTarget2].PresentedCount := arTarget[iTarget2].PresentedCount + 1;

  // To which context belong those 2 targets ?
  FCurrentContext := ComputeContext(iTarget1,iTarget2);

  // A context has been recognized, increase its counter ("Contexts" sheet)
  inc(arContext[FCurrentContext].ChosenCount);
end;

{
Check if the target with the highest reward probability was chosen in the
current trial
}
procedure TfrmMain.CheckOptimum;
var iNonChosen,
    iTarget: integer;
    bOptimum: boolean;
begin
  if FByTargetChosen then
    iTarget := iTargetChosen
  else
    iTarget := iTargetByDirection;

  if iTarget = arTrial[FCurrentRun-1].Target1 then
    iNonChosen := arTrial[FCurrentRun-1].Target2
  else
    iNonChosen := arTrial[FCurrentRun-1].Target1;
  bOptimum := arTarget[iTarget].RewardProbability
           >= arTarget[iNonChosen].RewardProbability;
  arTrial[FCurrentRun-1].Optimum := bOptimum;
  if bOptimum then begin
    FTotalOptimum := FTotalOptimum + 1;
    if bMultipleRuns then begin
      inc(arSimulation[FCurrentRun-1].Optimum);
      inc(arContextOutcome[FCurrentContext-1, arContext[FCurrentContext].ChosenCount-1].Optimum);
    end;
    memo1.Lines.Add('Optimum choice');
  end
  else
    memo1.Lines.Add('Non-optimum choice');
end;

function TfrmMain.CheckChoices: integer;
var i: integer;
begin
  result := 0; //set this to false in case no target was chosen
  for i := 1 to 4 do begin
    if arTarget[i].Show then begin
      if arTarget[i].ChosenAsDirection then begin
        //do the reward based on the actual direction moved, not the target chosen
        bRewardGiven := arTarget[i].RewardProbability > Random; //need this as well as iReward as it is sent to Excel with the results
        result := integer(bRewardGiven); //either 1 or 0
      end;
    end;
  end;
end;

{
At the end of each trial the strenght of connectivity between cortical and striatal units is updated
The direction of update depends on whether a reward is received or not.
The value of the reward is 0 or 1
}
procedure TfrmMain.UpdateWeights(RewardValue: integer; Method: integer);
var iTarget: integer;
//    eTargetPredictionError,
    eDirectionPredictionError,
    eValuePE,
    eAssPredictionError: real;
//    eActivation: real;
    i,j: integer;
begin
//  if (iTargetChosen > NO_TARGET) and (iDirectionChosen > NO_DIRECTION) then begin
      //Good run, both a target and a direction were correctly chosen
      //We no longer worry about this as, in early trials, it is possible for a direction to be chosen
      //before a target
  if (iDirectionChosen > NO_DIRECTION) then begin
    arTrial[FCurrentRun-1].Successful := true;
    Memo1.Lines.Add('Trial successful');
    if bMultipleRuns then begin
      inc(arSimulation[FCurrentRun-1].Successful);
      inc(arContextOutcome[FCurrentContext-1, arContext[FCurrentContext].ChosenCount-1].Successful);
    end;

    {
    The probability of reward is based on the target that the cursor was moved to,
    not the target chosen in the first stage. Usually these are the same, but
    sometimes one target is chosen and then the direction for the other target
    is picked. This may be due to biases in the motor cortex from early trials
    and should become less common as the session goes on.
    However, which target has its weights updated is dependant on the FByTargetChosen
    variable. If this is true, then the target that was actually chosen is
    updated. If it is false, the target associated with the direction moved
    is updated.
    }
    //wirk out which target to reward based on the target that was moved to or
    //the target actually chosen (which may differ)
    if FByTargetChosen then
      iTarget := iTargetChosen
    else
      iTarget := iTargetByDirection;
    if iTarget > NO_TARGET then
      arTarget[iTarget].PickedCount := arTarget[iTarget].PickedCount + 1;

    if (RewardValue = 1) then begin
      if bMultipleRuns then begin
        FTotalRewarded := FTotalRewarded + 1;
        arTrial[FCurrentRun-1].Rewarded := true;
        inc(arSimulation[FCurrentRun-1].Rewarded);
        inc(arContextOutcome[FCurrentContext-1, arContext[FCurrentContext].ChosenCount-1].Rewarded);
        if iTarget > NO_TARGET then
          arTarget[iTarget].RewardedCount := arTarget[iTarget].RewardedCount + 1;
      end;
      memo1.Lines.Add('Trial rewarded');
      imHappy.Visible := true;
      imSad.Visible := false;
      imHappy2.Visible := true;
      imSad2.Visible := false;
    end
    else begin
      memo1.Lines.Add('Trial not rewarded');
      imHappy.Visible := false;
      imSad.Visible := true;
      imHappy2.Visible := false;
      imSad2.Visible := true;
    end;
    iFlashReward := 0;
    //recalculate the effective reward probability
    if iTarget > NO_TARGET then
      if arTarget[iTarget].PickedCount > 0 then
        arTarget[iTarget].EffectiveProbability := arTarget[iTarget].RewardedCount / arTarget[iTarget].PickedCount;

    if FLearning then
    case Method of
    0: begin //no critic. Update the weights based on difference between received reward and weights
      if iTarget > NO_TARGET then begin
        ePredictionError := RewardValue - arConnection[COL_COGNITIVE, iTarget].Real;

        arConnection[COL_COGNITIVE, iTarget].Real := arConnection[COL_COGNITIVE, iTarget].Real
          + (ePredictionError * eValueLearningRate * arStr[COL_COGNITIVE, iTarget]);
        if arConnection[COL_COGNITIVE, iTarget].Real < 0.000001 then
          arConnection[COL_COGNITIVE, iTarget].Real := 0;
      end;

      {
      update the weights in the motor striatum for the direction chosen
      }
      eDirectionPredictionError := RewardValue - arConnection[iDirectionChosen, ROW_MOTOR].Real;
      arConnection[iDirectionChosen, ROW_MOTOR].Real := arConnection[iDirectionChosen, ROW_MOTOR].Real
        + (eDirectionPredictionError * eLTP * arStr[iDirectionChosen, ROW_MOTOR]);

      {
      update the weights of the association striatum for the combination of
      target and direction chosen
      Using both errors may make association striatum learn more slowly
      }
      if iTarget > NO_TARGET then begin
        eAssPredictionError := RewardValue - arConnection[iDirectionChosen, iTarget].Real;
        arConnection[iDirectionChosen, iTarget].Real :=
          arConnection[iDirectionChosen, iTarget].Real
          + (eAssPredictionError * eLTP * arStr[iDirectionChosen, iTarget]);
      end;

    end;
    1: begin //critic. Update weights based on difference between critic expected value and weights
        {
        Update the expected value of the target based on the prediction error
        This is the critic learning effectively
        }
        eValuePE := RewardValue - arTarget[iTarget].ExpectedValue;
        arTarget[iTarget].ExpectedValue := arTarget[iTarget].ExpectedValue + (eValuePE * eValueLearningRate);
        {
        Also update the expected value of the context, based on its own prediction error. This will
        allow comparison of the two methods of learning, context and target value only
        }
        eContextPE := RewardValue - arContext[FCurrentContext].ExpectedValue;
        arContext[FCurrentContext].ExpectedValue := arContext[FCurrentContext].ExpectedValue + (eContextPE * eValueLearningRate);
        if FiLearningType = 0 then
          //learn by target value prediction error
          ePredictionError := eValuePE
        else
          //learn by context value prediction error
          ePredictionError := eContextPE;
        //keep a running total of the value for the context on the current presentation.
        //Divide it by the number of runs before sending it to Excel
        if bMultipleRuns then begin
          arContextOutcome[FCurrentContext-1, arContext[FCurrentContext].ChosenCount-1].Value :=
            arContextOutcome[FCurrentContext-1, arContext[FCurrentContext].ChosenCount-1].Value + arContext[FCurrentContext].ExpectedValue;
          //keep a running total of the PEs for the context on the current presentation.
          //Divide it by the number of runs before sending it to Excel
          arContextOutcome[FCurrentContext-1, arContext[FCurrentContext].ChosenCount-1].PE :=
            arContextOutcome[FCurrentContext-1, arContext[FCurrentContext].ChosenCount-1].PE + eContextPE;
        end;

        lblPE.Caption := FloatToStrF(ePredictionError, ffFixed, 3,2, FFormatSettings);

        //Update the weights to all corticostriatal connections.
        //Only those with non-zero activation will change, so might as well go through all cells
        for i := 1 to 4 do begin
          //Cognitive cortex to cognitive striatum
//          eActivation := FBoltzmann.GetOuput(arStr[COL_COGNITIVE, i]);
//          eActivation := arStr[COL_COGNITIVE, i];
          if ePredictionError > 0 then
            //Do LTP - the learning rates for LTP and LTD can be different
            arDeltaW[COL_COGNITIVE, i] := ePredictionError * eLTP * arStr[COL_COGNITIVE, i]
          else
            //Do LTD
            arDeltaW[COL_COGNITIVE, i] := ePredictionError * eLTD * arStr[COL_COGNITIVE, i];
          if bUseLearningTransferFuction then
            //Sigmoidal transfer function slows learning as the weight gets closer to 0 or 1 and puts an upper boundary at ~ 1.1
            arConnection[COL_COGNITIVE, i].Real := FWeightBoltzmann.GetDifference(arConnection[COL_COGNITIVE, i].Real, arDeltaW[COL_COGNITIVE, i])
          else
            arConnection[COL_COGNITIVE, i].Real := arConnection[COL_COGNITIVE, i].Real  + arDeltaW[COL_COGNITIVE, i];
          arConnection[COL_COGNITIVE, i].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arConnection[COL_COGNITIVE, i].Real);


          //Motor cortex to motor striatum
          if cbMotorLearning.Checked then begin
//            eActivation := FBoltzmann.GetOuput(arStr[i, ROW_MOTOR]);
//            eActivation := arStr[i, ROW_MOTOR];
            if ePredictionError > 0 then
              arDeltaW[i, ROW_MOTOR] := ePredictionError * eLTP * arStr[i, ROW_MOTOR]
            else
              arDeltaW[i, ROW_MOTOR] := ePredictionError * eLTD * arStr[i, ROW_MOTOR];
            if bUseLearningTransferFuction then
              arConnection[i, ROW_MOTOR].Real := FWeightBoltzmann.GetDifference(arConnection[i, ROW_MOTOR].Real, arDeltaW[i, ROW_MOTOR])
            else
              arConnection[i, ROW_MOTOR].Real := arConnection[i, ROW_MOTOR].Real + arDeltaW[i, ROW_MOTOR];
            arConnection[i, ROW_MOTOR].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arConnection[i, ROW_MOTOR].Real);
          end;
        end;

        for i := 1 to 4 do begin
          for j := 1 to 4 do begin
            //Associative cortex to associative striatum
//            eActivation := FBoltzmann.GetOuput(arStr[i, j]);
//            eActivation := arStr[i, j];
            if ePredictionError > 0 then
              arDeltaW[i, j] := ePredictionError * eLTP * arStr[i, j]
            else
              arDeltaW[i, j] := ePredictionError * eLTD * arStr[i, j];
            if bUseLearningTransferFuction then
              arConnection[i, j].Real := FWeightBoltzmann.GetDifference(arConnection[i, j].Real, arDeltaW[i, j])
            else
              arConnection[i, j].Real := arConnection[i, j].Real+ arDeltaW[i, j];
            arConnection[i, j].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arConnection[i, j].Real);

            //Cognitive cortex to associative striatum
            if ePredictionError > 0 then
              arCogDeltaW[i, j] := ePredictionError * eLTP * arStr[i, j]
            else
              arCogDeltaW[i, j] := ePredictionError * eLTD * arStr[i, j];
            if bUseLearningTransferFuction then
              arCogConnection[i, j].Real := FWeightBoltzmann.GetDifference(arCogConnection[i, j].Real, arCogDeltaW[i, j])
            else
              arCogConnection[i, j].Real := arCogConnection[i, j].Real + arCogDeltaW[i, j];
            arCogConnection[i, j].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arCogConnection[i, j].Real);

            //Motor cortex to associative striatum
            if cbMotorLearning.Checked then begin
              if ePredictionError > 0 then
                arMotorDeltaW[i, j] := ePredictionError * eLTP * arStr[i, j]
              else
                arMotorDeltaW[i, j] := ePredictionError * eLTD * arStr[i, j];
              if bUseLearningTransferFuction then
                arMotorConnection[i, j].Real := FWeightBoltzmann.GetDifference(arMotorConnection[i, j].Real, arMotorDeltaW[i, j])
              else
                arMotorConnection[i, j].Real := arMotorConnection[i, j].Real + arMotorDeltaW[i, j];
              arMotorConnection[i, j].Normalized := eMinWeight + ((eMaxWeight - eMinWeight) * arMotorConnection[i, j].Real);
            end;
          end;
        end;
      end;
    end;
  end
  else
    Memo1.Lines.Add('Trial unsuccessful');
end;

// If a second client try to connect, send it an error message and then disconnect.
procedure TfrmMain.IdTCPServer1Connect(AThread: TIdPeerThread);
begin
  // Instead of closing the door to a second connection, inform the client that all seats are taken
  // (also prevent "raised exception class EThread with message 'Thread Error: The handle is invalid (6).'")
  if IdTCPServer1.Threads.LockList.Count >= IdTCPServer1.MaxConnections then
    begin
    Memo1.Lines.Add(AThread.Connection.Socket.Binding.IP + ' attempted to connect but maximum number of clients reached.');
    AThread.Connection.Writeln('Error: maximum number of clients reached.');
    Athread.Connection.Disconnect;
    end
  else
    Memo1.Lines.Add('Connected to ' + AThread.Connection.Socket.Binding.IP);
  // release the lock put when counting
  IdTCPServer1.Threads.UnlockList;
  // CoInitialize(nil);
end;

procedure TfrmMain.IdTCPServer1Disconnect(AThread: TIdPeerThread);
begin
  // Unfortunately, can't access anymore to IP when we trigger disconnect in IdTCPServer1Connect
  //Memo1.Lines.Add('Disconnected from ' + AThread.Connection.Socket.Binding.IP);
  Memo1.Lines.Add('Client disconnected.');
  // CoUninitialize;
end;

procedure TfrmMain.cbLearningClick(Sender: TObject);
begin
  if not FLoading then
    FLearning := cbLearning.Checked;
end;

procedure TfrmMain.cbLearningTransferFunctionClick(Sender: TObject);
begin
  if not FLoading then
    bUseLearningTransferFuction := TCheckbox(Sender).Checked;
end;

procedure TfrmMain.edLearningTFMinChange(Sender: TObject);
begin
  if not FLoading then
    FWeightBoltzmann.MinOutput := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edLearningTFMaxChange(Sender: TObject);
begin
  if not FLoading then
    FWeightBoltzmann.MaxOutput := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edLearningTFVhChange(Sender: TObject);
begin
  if not FLoading then
    FWeightBoltzmann.Vh := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.edLearningTFVcChange(Sender: TObject);
begin
  if not FLoading then
    FWeightBoltzmann.Vc := StrToFloat(TEdit(Sender).Text, FFormatSettings);
end;

procedure TfrmMain.rgLearningTypeClick(Sender: TObject);
begin
  if not FLoading then
    FiLearningType := TRadioGroup(Sender).ItemIndex;
end;

procedure TfrmMain.ClearGraphs;
var iSeries: integer;
begin
  for iSeries := 1 to 4 do begin
    TFastLineSeries(FindComponent('srsStrCol'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsStrRow'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsCxCog'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsCxMotor'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsCxCog2_'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsCxMotor2_'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsStrCog2_'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsStrMotor2_'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsStnCog2_'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsStnMotor2_'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsGpiCog2_'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsGpiMotor2_'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsThCog2_'+IntToStr(iSeries))).Clear;
    TFastLineSeries(FindComponent('srsThMotor2_'+IntToStr(iSeries))).Clear;
 end;
  TFastLineSeries(FindComponent('srsGpiTotal')).Clear;
end;

procedure TfrmMain.btnOneStepDecisionClick(Sender: TObject);
begin
  MakeImagesVisible;
  DoALoop;
  CheckCorticalActivation(1);
  CheckCorticalActivation(2);
  inc(FiLoops);
  DisplayValues(2);
end;

procedure TfrmMain.btnBoltzmannClick(Sender: TObject);
begin
  if frmBoltzmann = nil then
    frmBoltzmann := TfrmBoltzmann.Create(nil);
  with frmBoltzmann do try
    FormatSettings := FFormatSettings;
    Title := 'Corticostriatal transfer function';
    XLabel := 'Cortical Activation';
    YLabel := 'Striatal Activation';
    oBoltzmann := FBoltzmann;
    ShowModal;
    FLoading := true;
    edMinStrActivation.Text := FloatToStr(FBoltzmann.MinOutput, FFormatSettings);
    edMaxStrActivation.Text := FloatToStr(FBoltzmann.MaxOutput, FFormatSettings);
    edStrVh.Text := FloatToStr(FBoltzmann.Vh, FFormatSettings);
    edStrVc.Text := FloatToStr(FBoltzmann.Vc, FFormatSettings);
    FLoading := false;
  finally
    Free;
    frmBoltzmann := nil;
  end;
end;

procedure TfrmMain.btnWeightBoltzmannClick(Sender: TObject);
begin
  if frmBoltzmann = nil then
    frmBoltzmann := TfrmBoltzmann.Create(nil);
  with frmBoltzmann do try
    FormatSettings := FFormatSettings;
    Title := 'Learning transfer function';
    XLabel := 'Weight change';
    YLabel := 'Actual weight';
    oBoltzmann := FWeightBoltzmann;
    ShowModal;
    FLoading := true;
    edLearningTFMin.Text := FloatToStr(FWeightBoltzmann.MinOutput, FFormatSettings);
    edLearningTFMax.Text := FloatToStr(FWeightBoltzmann.MaxOutput, FFormatSettings);
    edLearningTFVh.Text := FloatToStr(FWeightBoltzmann.Vh, FFormatSettings);
    edLearningTFVc.Text := FloatToStr(FWeightBoltzmann.Vc, FFormatSettings);
    FLoading := false;
  finally
    Free;
    frmBoltzmann := nil;
  end;
end;

procedure TfrmMain.btnStopClick(Sender: TObject);
begin
  bStop := true;
end;

procedure TfrmMain.btnPauseClick(Sender: TObject);
begin
  bPause := not bPause;
end;

procedure TfrmMain.rgAssWeightsClick(Sender: TObject);
begin
  if not FLoading then
    DisplayWeights;
end;

procedure TfrmMain.rgWeightsDisplayClick(Sender: TObject);
begin
  if not FLoading then
    DisplayWeights;
end;

procedure TfrmMain.grdAssWeightsCellProps(Sender: TObject; Canvas: TCanvas;
  var Alignment: TAlignment; var CellText: String; AState: TGridDrawState;
  Row, Col: Integer);
begin
  if arTarget[Row+1].Show and (arTarget[Row+1].Direction = Col + 1) then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

procedure TfrmMain.grdMotorWeightsCellProps(Sender: TObject;
  Canvas: TCanvas; var Alignment: TAlignment; var CellText: String;
  AState: TGridDrawState; Row, Col: Integer);
begin
  if arDirectionShown[Col+1] then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

procedure TfrmMain.grdCogWeightsCellProps(Sender: TObject; Canvas: TCanvas;
  var Alignment: TAlignment; var CellText: String; AState: TGridDrawState;
  Row, Col: Integer);
begin
  if arTarget[Row+1].Show then
    Canvas.Font.Color := clBlue
  else
    Canvas.Font.Color := clBlack;
end;

{
Increment the gain from cortex to striatum by 0.1
}
procedure TfrmMain.sbIncGainsClick(Sender: TObject);
var e: real;
begin
  e := StrToFloat(edCxMotorToStrMotor.Text, FFormatSettings) + 0.1;
  edCxMotorToStrMotor.Text := FloatToStrF(e, ffFixed, 3, 2, FFormatSettings);
  edCxCogToStrCog.Text := FloatToStrF(e, ffFixed, 3, 2, FFormatSettings);
  edCxAssToStrAss.Text := FloatToStrF(e, ffFixed, 3, 2, FFormatSettings);
  //Associative striatum receives a proportion of the cognitive and motor cortex output
  e := e * eAssociativeRatio;
  edCxMotorToStrAss.Text := FloatToStrF(e, ffFixed, 3, 2, FFormatSettings);
  edCxCogToStrAss.Text := FloatToStrF(e, ffFixed, 3, 2, FFormatSettings);
end;

{
Decrement the gain from cortex to striatum by 0.1
}
procedure TfrmMain.sbDecGainsClick(Sender: TObject);
var e: real;
begin
  e := StrToFloat(edCxMotorToStrMotor.Text, FFormatSettings) - 0.1;
  edCxMotorToStrMotor.Text := FloatToStrF(e, ffFixed, 3, 2, FFormatSettings);
  edCxCogToStrCog.Text := FloatToStrF(e, ffFixed, 3, 2, FFormatSettings);
  edCxAssToStrAss.Text := FloatToStrF(e, ffFixed, 3, 2, FFormatSettings);
  //Associative striatum receives a proportion of the cognitive and motor cortex output
  e := e * eAssociativeRatio;
  edCxMotorToStrAss.Text := FloatToStrF(e, ffFixed, 3, 2, FFormatSettings);
  edCxCogToStrAss.Text := FloatToStrF(e, ffFixed, 3, 2, FFormatSettings);
end;

procedure TfrmMain.sbCxActivationToImageClick(Sender: TObject);
var sFileName: string;
begin
  if TSpeedButton(Sender) = sbCxActivationToImage then
    sFileName := 'Cx - '
  else if TSpeedButton(Sender) = sbStrActivationToImage then
    sFileName := 'Str - '
  else if TSpeedButton(Sender) = sbGpiActivationToImage then
    sFileName := 'Gpi - ';
  sFileName := sFileName + 'Hyper_'+ FloatToStr(eCxMotorToStnMotor) + ' Direct_'+ FloatToStr(eCxMotorToStrMotor) + '.bmp';
  dlgSaveImage1.FileName := sFileName;
  if dlgSaveImage1.Execute then begin
    if TSpeedButton(Sender) = sbCxActivationToImage then
      chrtCx.SaveToBitmapFile(dlgSaveImage1.FileName)
    else if TSpeedButton(Sender) = sbStrActivationToImage then
      chrtStr.SaveToBitmapFile(dlgSaveImage1.FileName)
    else if TSpeedButton(Sender) = sbGpiActivationToImage then
      chrtGpi.SaveToBitmapFile(dlgSaveImage1.FileName);
  end;
end;

{
Save the three graphs of cortex, striatum and GPi activity all at once
}
procedure TfrmMain.sbSaveAllImagesClick(Sender: TObject);
var sFileName,
    sDate,
    sPrefix,
    sDir: string;
begin
  sDir := ExtractFilePath(Application.ExeName) + 'Screen caps\';
  //Add the date so that I can save multiple copies
  sDate := DateAsString;
  sFileName := 'Hyper_'+ FloatToStr(eCxMotorToStnMotor) + ' Direct_'+ FloatToStr(eCxMotorToStrMotor) + '.bmp';
  sPrefix := 'Cx - ';
  chrtCx.SaveToBitmapFile(sDir + sDate + sPrefix + sFileName);

  sPrefix := 'Str - ';
  chrtStr.SaveToBitmapFile(sDir + sDate + sPrefix + sFileName);

  sPrefix := 'Gpi - ';
  chrtGpi.SaveToBitmapFile(sDir + sDate + sPrefix + sFileName);
end;

{
Construct a string of the date with _ as the seperator
}
function TfrmMain.DateAsString: string;
var D, M, Y: word;
    sM, sD: string;
begin
  DecodeDate(Now, Y, M, D);
  sM := IntToStr(M);
  if length(sM) < 2 then
    sM := '0' + sM;
  sD := IntToStr(D);
  if length(sD) < 2 then
    sD := '0' + sD;
  result := IntToStr(Y) + '_' + sM + '_' + sD + '_';
end;

procedure TfrmMain.cbShowGraphsClick(Sender: TObject);
begin
  if not FLoading then
    bShowGraphs := TCheckBox(Sender).Checked;
end;

procedure TfrmMain.pc1Change(Sender: TObject);
//var i: integer;
begin
{
  for i := 0 to pc1.PageCount - 1 do
    pc1.Pages[i].Font.Style := [];
  pc1.ActivePage.Font.Style := [fsBold];
}  
end;

procedure TfrmMain.cbShowCogClick(Sender: TObject);
var i: integer;
    bShow: boolean;
begin
  bShow := cbShowCog.Checked;
  for i := 1 to 4 do begin
    TFastLineSeries(FindComponent('srsCxCog2_' + IntToStr(i))).Visible := bShow;
    TFastLineSeries(FindComponent('srsStrCog2_' + IntToStr(i))).Visible := bShow;
    TFastLineSeries(FindComponent('srsStnCog2_' + IntToStr(i))).Visible := bShow;
    TFastLineSeries(FindComponent('srsGpiCog2_' + IntToStr(i))).Visible := bShow;
    TFastLineSeries(FindComponent('srsThCog2_' + IntToStr(i))).Visible := bShow;
  end;
end;

procedure TfrmMain.cbShowMotorClick(Sender: TObject);
var i: integer;
    bShow: boolean;
begin
  bShow := cbShowMotor.Checked;
  for i := 1 to 4 do begin
    TFastLineSeries(FindComponent('srsCxMotor2_' + IntToStr(i))).Visible := bShow;
    TFastLineSeries(FindComponent('srsStrMotor2_' + IntToStr(i))).Visible := bShow;
    TFastLineSeries(FindComponent('srsStnMotor2_' + IntToStr(i))).Visible := bShow;
    TFastLineSeries(FindComponent('srsGpiMotor2_' + IntToStr(i))).Visible := bShow;
    TFastLineSeries(FindComponent('srsThMotor2_' + IntToStr(i))).Visible := bShow;
  end;
end;

procedure TfrmMain.btnRobotClick(Sender: TObject);
var
  // Have we succeeded in changing connection status ?
  connectionStatusChanged: boolean;
begin
  connectionStatusChanged := false;
  // Has server been disconnected during this lazy evaluation ?
  if bRobotConnected and terminateServer then
    begin
    // End of the journey
    bMultipleRuns := false;
    connectionStatusChanged := true;
    Memo1.Lines.Add('Server stopped.');
    end;
  // User wanted to start server
  if (not bRobotConnected) and startServer then
    begin
    // try to mimic results from multiple trials
    bMultipleRuns := true;
    connectionStatusChanged := true;
    Memo1.Lines.Add('Server started.');
    end;
  // User gets what it wanted
  if connectionStatusChanged then
    begin
    // TODO: reinitialize data like in IdTCPServer1Execute
    bSessionStarted := false;
    // enable / disable other buttons accordingly
    btnTarget.Enabled := bRobotConnected;
    btnOneStepDecision.Enabled := bRobotConnected;
    btnDecisionPhase.Enabled := bRobotConnected;
    btnMovmentPhase.Enabled := bRobotConnected;
    btnOneTrial.Enabled := bRobotConnected;
    btnReinitialize.Enabled := bRobotConnected;
    btnMultipleTrials.Enabled := bRobotConnected;
    btnPause.Enabled := bRobotConnected;
    btnStop.Enabled := bRobotConnected;
    bRobotConnected := not bRobotConnected;
    end;
end;

function TfrmMain.startServer(): Boolean;
begin
  //try to connect to the robot via tcp
  try
    IdTCPServer1.Active := true;
    // CoInitialize(nil);
  except on E: Exception do
    begin
    ShowMessage(E.Message);
    end;
  end;
  result := IdTCPServer1.Active;
end;

// Disconnect politely the clients before shutting down the server.
function TfrmMain.terminateServer(): Boolean;
   var
   // list of clients still connected
   clientsList:TList;
   i:Integer;

  begin

  // Disconnect properly all remaining clients

  clientsList := IdTCPServer1.Threads.LockList;
  if clientsList.Count <>0 then
    begin
    for i := 0 to clientsList.Count-1 do
      begin
        // Capture error here in case we can't reach clients (a raising exception could prevent application from exiting)
        try
          TIdPeerThread(clientsList.Items[i]).Connection.Writeln('Warning: server is about to stop.');
          TIdPeerThread(clientsList.Items[i]).Connection.Disconnect;
        except on E: Exception do
          ShowMessage(E.Message);
        end;
      end;
  end;
  IdTCPServer1.Threads.UnlockList;

  // Give it some rest to effectively disconnect everybody
  Application.ProcessMessages;

  // Will raise a timeout if take more than TerminateWaitTime to stop
  // (won't be patient if set to 0)
  try
    IdTCPServer1.Active := false;
    except on E: Exception do
          ShowMessage(E.Message);
    end;
  Result := not(idTCPServer1.Active);
  end;

end.


