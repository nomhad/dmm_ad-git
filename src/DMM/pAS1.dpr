program pAS1;

uses
  Forms,
  uMain in 'uMain.pas' {frmMain},
  uShowBoltzmann in 'uShowBoltzmann.pas' {frmBoltzmann},
  uBoltzmann in '..\Utilities\uBoltzmann.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Decision Making Model';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
