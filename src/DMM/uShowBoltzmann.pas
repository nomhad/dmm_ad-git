unit uShowBoltzmann;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TB97Ctls, TB97, TB97Tlbr, TeEngine, Series, TeeProcs,
  Chart, ExtCtrls, uBoltzmann, Buttons, ExtDlgs;

type
  TfrmBoltzmann = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    chrt1: TChart;
    srsSigmoid: TFastLineSeries;
    Dock971: TDock97;
    Toolbar971: TToolbar97;
    btnRun: TToolbarButton97;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edMax: TEdit;
    edVh: TEdit;
    edVc: TEdit;
    btnSave: TToolbarButton97;
    btnDiscard: TToolbarButton97;
    ToolbarSep971: TToolbarSep97;
    Label4: TLabel;
    edMin: TEdit;
    cbPosOnly: TCheckBox;
    ToolbarSep972: TToolbarSep97;
    btnUndoQuit: TToolbarButton97;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    lblX: TLabel;
    edY: TEdit;
    btnCalcX: TSpeedButton;
    dlgSaveImage1: TSavePictureDialog;
    ToolbarSep973: TToolbarSep97;
    btnSaveImage: TToolbarButton97;
    procedure edMaxChange(Sender: TObject);
    procedure edVhChange(Sender: TObject);
    procedure edVcChange(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edMinChange(Sender: TObject);
    procedure cbPosOnlyClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnDiscardClick(Sender: TObject);
    procedure btnCalcXClick(Sender: TObject);
    procedure btnSaveImageClick(Sender: TObject);
  private
    { Private declarations }
    FoBoltzmann,
    FoOriginalBoltzmann: TBoltzmann;
    FLoading: boolean;
    FFormatSettings: TFormatSettings;
    FXLabel: string;
    FYLabel: string;
    FTitle: string;
    procedure SetTitle(const Value: string);
  public
    { Public declarations }
    property oBoltzmann: TBoltzmann read FoBoltzmann write FoBoltzmann;
    property FormatSettings: TFormatSettings read FFormatSettings write FFormatSettings;
    property XLabel: string read FXLabel write FXLabel;
    property YLabel: string read FYLabel write FYLabel;
    property Title: string read FTitle write SetTitle;
  end;

var
  frmBoltzmann: TfrmBoltzmann;

implementation

{$R *.dfm}

procedure TfrmBoltzmann.cbPosOnlyClick(Sender: TObject);
begin
  if not FLoading then
    FoBoltzmann.OutputPositiveOnly := cbPosOnly.Checked;
end;

procedure TfrmBoltzmann.edMinChange(Sender: TObject);
var eOffset: real;
begin
  if not FLoading then
    FoBoltzmann.MinOutput := StrToFloat(edMin.Text);

  if (FoBoltzmann.MaxOutput - FoBoltzmann.MinOutput) > 10 then
    eOffset := 1
  else
    eOffset := (FoBoltzmann.MaxOutput - FoBoltzmann.MinOutput) / 10;

  if FoBoltzmann.OutputPositiveOnly then begin
    if (FoBoltzmann.MinOutput >= 0) then
      chrt1.LeftAxis.Minimum := FoBoltzmann.MinOutput - eOffset
    else
      chrt1.LeftAxis.Minimum := -1 * eOffset;
  end
  else
    chrt1.LeftAxis.Minimum := FoBoltzmann.MinOutput - eOffset
end;

procedure TfrmBoltzmann.edMaxChange(Sender: TObject);
begin
  if not FLoading then
    FoBoltzmann.MaxOutput := StrToFloat(edMax.Text);
  chrt1.LeftAxis.Maximum := FoBoltzmann.MaxOutput;
end;

procedure TfrmBoltzmann.edVhChange(Sender: TObject);
begin
  if not FLoading then
    FoBoltzmann.Vh := StrToFloat(edVh.Text);
  chrt1.BottomAxis.Maximum := 2 * FoBoltzmann.Vh;
end;

procedure TfrmBoltzmann.edVcChange(Sender: TObject);
begin
  if not FLoading then
    FoBoltzmann.Vc := StrToFloat(edVc.Text);
end;

procedure TfrmBoltzmann.btnRunClick(Sender: TObject);
var i,
    iSteps: integer;
    eY: real;
begin
  srsSigmoid.Clear;
  i := 0;
  repeat
    eY := FoBoltzmann.GetOutput(i / 10);
    srsSigmoid.AddXY(i / 10, eY);
    inc(i)
  until eY >= (FoBoltzmann.MaxOutput * 0.99);
  chrt1.BottomAxis.Maximum := i/10;
end;

procedure TfrmBoltzmann.FormShow(Sender: TObject);
begin
  FLoading := true;
  try
    edMin.Text := FloatToStr(FoBoltzmann.MinOutput, FFormatSettings);
    edMax.Text := FloatToStr(FoBoltzmann.MaxOutput, FFormatSettings);
    edVh.Text := FloatToStr(FoBoltzmann.Vh, FFormatSettings);
    edVc.Text := FloatToStr(FoBoltzmann.Vc, FFormatSettings);
    cbPosOnly.Checked := FoBoltzmann.OutputPositiveOnly;

    //keep track of the starting values in case you want to discard any changes
    FoOriginalBoltzmann.MinOutput := FoBoltzmann.MinOutput;
    FoOriginalBoltzmann.MaxOutput := FoBoltzmann.MaxOutput;
    FoOriginalBoltzmann.Vh := FoBoltzmann.Vh;
    FoOriginalBoltzmann.Vc := FoBoltzmann.Vc;
    FoOriginalBoltzmann.OutputPositiveOnly := FoBoltzmann.OutputPositiveOnly;

    chrt1.LeftAxis.Title.Caption := FYLabel;
    chrt1.BottomAxis.Title.Caption := FXLabel;

    btnRunClick(nil);
  finally
    FLoading := false;
  end;
end;

procedure TfrmBoltzmann.FormCreate(Sender: TObject);
begin
  FoOriginalBoltzmann := TBoltzmann.Create;
end;

procedure TfrmBoltzmann.FormDestroy(Sender: TObject);
begin
  FoOriginalBoltzmann.Free;
end;

procedure TfrmBoltzmann.btnDiscardClick(Sender: TObject);
begin
  edMin.Text := FloatToStr(FoOriginalBoltzmann.MinOutput, FFormatSettings);
  edMax.Text := FloatToStr(FoOriginalBoltzmann.MaxOutput, FFormatSettings);
  edVh.Text := FloatToStr(FoOriginalBoltzmann.Vh, FFormatSettings);
  edVc.Text := FloatToStr(FoOriginalBoltzmann.Vc, FFormatSettings);
  cbPosOnly.Checked := FoOriginalBoltzmann.OutputPositiveOnly;
  btnRunClick(nil);
end;

procedure TfrmBoltzmann.btnCalcXClick(Sender: TObject);
var eY,
    eX: real;
begin
  eY := StrToFloat(edY.Text);
  eX := FoBoltzmann.GetInput(eY);
  lblX.Caption := FloatToStrF(eX, ffFixed, 3, 4);
end;

procedure TfrmBoltzmann.SetTitle(const Value: string);
begin
  FTitle := Value;
  frmBoltzmann.Caption := 'Boltzmann Equation (' + FTitle + ')';
end;

procedure TfrmBoltzmann.btnSaveImageClick(Sender: TObject);
begin
  if dlgSaveImage1.Execute then begin
    chrt1.SaveToBitmapFile(dlgSaveImage1.FileName)
  end;
end;

end.
