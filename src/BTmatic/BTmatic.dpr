program BTmatic;

{$APPTYPE CONSOLE}

{
  Ce programme a pour but de piloter DMM via TCP � des fins de test. Envoyer les
  cibles, attendre r�ponse, envoyer r�compense. Pour ce faire les fonctions
  utilis�es dans DMM pour effectuer un set de trials on �t� recopi�es ici, avec un
  minimum de modification afin de coller � la simulation.

  Dans cette optique, afin de minimiser les effets de bord, les structures ont
  �t� gard�es telles qu'elles, m�me si de nombreux champs sont inutilis�s. Seul
  compte au final GetTargets, qui renvoie maitenant une cha�ne au lieu de
  modifier l'affichage graphique, et CheckChoices qui indique r�compense ou non
  suivant proba de la cible choisie par DMM.

  Attention � bien d�finir les cibles utilis�es et le nombre d'it�ration en
  d�but de programme, sinon quelques tableaux de longueur nulle vont pr�cipiter
  le programme � sa perte.
}

uses
  SysUtils,
  IniFiles,
  Forms,
  IdTCPClient,
  Chaos in 'Chaos.pas';


const
  // a priori va pas changer, nombre de cibles qu'on va traiter
  // TODO: que varie en fait suivant nombre de cible renseign� dans fichier ini
  NB_TARGETS = 4;
  // Nom du fichier de configuration pr�sent dans r�pertoire de l'application
  CONF_FILENAME = 'default.ini';

var
  // Me fait un peu mal d'exposer ainsi une insignifiante variable
  n: integer = 0;
  {
    "Chaos" pour la notion d'al�atoire, "Coco" car c'est le seul nom qui me
    soit venu � l'esprit pour la classe principale, et tout naturellement le
    surnom m'am�ne � "Syl". Esprit tortueux.
  }
  Syl:Coco;
  // Connexion vers DMM
  client: TIdTCPClient;
  // cibles actuelles
  targets : String;
  // direction actuelle
  direction : Integer = 0;

{
  Charge les param�tres des cibles et retourne un Coco bien initialis�.

  FIXME: meilleure gestion des param�tres par d�faut
   (utiliser constructeur sans param�tres de Coco et en extra�re les valeurs)
}
function GenCoco:Coco;
var
  // ces indig�nes sont partout !
  i : integer;
  // Une application console certes, mais on s'enquiert un peu du monde ext�rieur
  IniFile : TIniFile;
  // Le chemin/nom du fichier concern�
  FileName : String;
  // Probas associ�es � chaque cible
  arP: array of Real;
  // Nombre d'it�rations
  nbRuns:Integer;
begin
  // Pr�pare le tableau contenant les probas des cibles
  SetLength(arP, NB_TARGETS);
  Writeln('Nombre de cibles : ' + IntToStr(Length(arP)));

  // Fichier de configuration : default.ini dans le m�me dossier que programme
  FileName := ExtractFilePath(Application.ExeName) + CONF_FILENAME;

  // On informe utilisateur si fichier pr�sent ou non mais...
  if FileExists(FileName) then
    Writeln('Fichier de configuration trouve, lecture des parametres.')
  else
    Writeln('Erreur: fichier de configuration non trouve, utilisation des valeurs par defaut.');

  // Dans les deux cas m�me traitement

  // Ouverture du fichier et lecture des valeurs
  // (0.25 par d�faut pour cibles, 18 pour nombre d'it�rations)
  // TODO: les valeurs par d�faut sont diff�rentes de Coco.Create()...et accessoirement sont pourries
  IniFile := TIniFile.Create(FileName);
  for i := 0 to NB_TARGETS - 1 do begin
    arP[i] := IniFile.ReadFloat('Parameters','Target ' + IntToStr(i+1) +' Reward Probability',0.25);
  end;
  nbRuns := IniFile.ReadInteger('Parameters','Runs per simulation',18);

  Writeln('Parametres utilises :');
    for i := 0 to NB_TARGETS -1 do begin
      Writeln('p' + IntToStr(i) + ' = ' + FloatToStr(arP[i]));
    end;
  Writeln('Nombre d''iterations : ' + IntToStr(nbRuns));
  Writeln('');

  // L'objet qui g�rera cibles/r�compenses
  // TODO: tout tourne autour de 4 cibles, mais pourrait passer tableau � Coco et avoir n'importe quel nombre
  if (NB_TARGETS = 4)then
    result := Coco.Create(arP[0],arP[1],arP[2],arP[3],nbRuns)
  else begin
    // Pour le fun, car Coco ne sait pas se d�fendre
    Writeln('Erreur: nombre de cibles non gere pour le moment, utilise generateur par defaut.');
    result := Coco.Create;
  end;

  // On a plus besoin de lui, le laisse retourner dans le monde eth�r�
  arP := nil;
  // Fini de lire !
  IniFile.Free;
end;

// Va �tablir la connexin au serveur, renvoie true si r�ussit
function Link:Boolean;
var
  IniFile : TIniFile;
  FileName : String;
begin
  result := true;
  // Initialise connexion
  client := TIdTCPClient.Create(nil);

  // FIXME: factoriser avec GenCoco
  FileName := ExtractFilePath(Application.ExeName) + CONF_FILENAME;
  if FileExists(FileName) then
    Writeln('Fichier de configuration trouve, lecture des parametres.')
  else
    Writeln('Erreur: fichier de configuration non trouve, utilisation des valeurs par defaut.');
  IniFile := TIniFile.Create(FileName);
  // Lecture param serveur
  client.Host := IniFile.ReadString('ServerInfo','Host','127.0.0.1');
  client.Port := IniFile.ReadInteger('ServerInfo','Port',6666);

  // Si tentative �choue on lib�re variable
  try
    Writeln('Connexion au serveur...');
    client.Connect;
    Writeln('Connexion reussie !');

    // lit le message d'accueil eventuel (attend un second envoi de donn�es)
    client.ReadTimeout := 1000;
    try
      Writeln('Message d''accueil : ' + client.ReadLn());
    except
    on E : Exception do
      begin
      Writeln(E.Message);
      end;
    end;
    // RAZ timeout
    client.ReadTimeout := 0;

  // Si la connexion ne se fait pas on d�truit client et renverra false
  except
  on E : Exception do
      begin
      Writeln(E.Message);
      client.Free;
      result := false;
      end;
  end;
  
  Writeln('');

  IniFile.Free;
end;

// Attend appui sur la touche entr�e avant de continuer
procedure WaitForEnter;
begin
  Writeln('[Appuyer sur enter pour quitter]');
  Readln;
end;

{
  Le programme principal r�cup�re un objet Coco et effectue autant de tour de
  boucles que de nombre d'essais. A chacun il communiquera les cibles au serveur,
  r�cup�rera la direction choisie et retournera la r�compense.

  Niveau communication : envoie �change "S"/"OK" pour d�buter, envoie "End" � la
  fin, attend "Target received" comme retour pour chaque "T????" envoy�.
}
begin

  // Si la connexion �choue on quitte le programme
  if not Link then begin
     WaitForEnter;
     Exit;
  end;

  // R�cup�re objet qui g�n�re cibles et attribue r�compenses
  Syl := GenCoco;

  // Signale au serveur qu'on d�marre la session et attend r�ponse
  Write('Demarre la session avec "S"...');
  try
    client.Writeln('S');
    // TODO: contr�ler r�ponse (doit �tre 'OK')
    Writeln(client.Readln);
  // En cas d'erreur on quitte
  except
  on E : Exception do
    begin
    Writeln(E.Message);
    WaitForEnter;
    Exit;
    end;
  end;
  Writeln;

  // Fait tourner le generateur de cibles
  while Syl.hasRunsLeft do
  begin
    inc(n);
    Writeln('  == Essai numero ' + IntToStr(n) + ' ==');

    // R�cup�re les cibles et envoie au serveur
    targets :=  Syl.ReturnNewTargets;
    Writeln('Cibles : ' + targets);
    Writeln('Envoie au serveur...');
    try
    client.Writeln(targets);
    // En cas d'erreur on quitte la boucle
    except
    on E : Exception do
      begin
      Writeln(E.Message);
      break;
      end;
    end;

    Write('Reponse :');
    try
    Writeln(client.Readln);
    // En cas d'erreur on quitte la boucle
    except
    on E : Exception do
      begin
      Writeln(E.Message);
      break;
      end;
    end;

    // Re�oit la direction choisie
    Write('Direction retournee par le serveur : ');
    try
    // Attention, va pas �tre joli si c'est pas vrament un entier qu'on retourne
    direction := StrToInt(client.Readln);
    // En cas d'erreur on fixe direction � 0
    except
    on E : Exception do
      begin
      Writeln(E.Message);
      direction := 0;
      end;
    end;
    Writeln(IntToStr(direction));

    // Indique au mod�le la direction choisie.
    Write('Donne la direction au mod�le...');
    if Syl.setChosenDirection(direction) then
      Writeln('OK')
    else
      Writeln('Erreur');

    // Envoie r�compense associ�e au serveur
    Writeln('Recompense associee : ' + Syl.ReturnReward);
    Writeln('Envoi de la recompense au serveur...');
    try
    client.Writeln(Syl.ReturnReward);
    // En cas d'erreur on quitte la boucle
    except
    on E : Exception do
      begin
      Writeln(E.Message);
      break;
      end;
    end;

    Writeln;
  end;

  // Signale au serveur qu'on a termin� la session
  Writeln('Envoi du signal "End".');
  try
    client.Writeln('End');
  // En cas d'erreur...on affiche juste le message, on prend la retraite juste en suivant alors bon...
  except
  on E : Exception do
    begin
    Writeln(E.Message);
    end;
  end;
  Writeln;

  WaitForEnter;

  // FIXME: lib�rer ici Syl ?
  // (faudrait aussi destructeur pour Coco)

end.
