unit Chaos;

{
Comporte les bout de code tir�s de DMM, va s'occuper de g�n�rer les cibles
al�atoirement et indiquer si la r�compense est pr�sente ou non.
}

interface

uses
  Math;

type
  // Une cible gambade dans des champs luxuriants
  TTarget = record
    Direction: integer;
    Salience,
    ExpectedValue: real;  //calculated trial to trial using  the PE and the learning rate. PE = Reward - ExpectedValue
    Use,
    Show: boolean;
    Correct: boolean;
    ChosenAsTarget: boolean;
    ChosenAsDirection: boolean;
    RewardProbability: real;
    PresentedCount,
    PickedCount,
    RewardedCount: integer;
    EffectiveProbability: real;
  end;

  // "TCxConnection =  array[1..4,1..4] of real;"
  TContext = record
    ChosenCount: integer;
    ExpectedValue: real;
  end;

  // Un essai
  TTrial = record
    Successful,
    Optimum,
    Rewarded: boolean;
    Target1,
    Target2,
    Direction1,
    Direction2: integer;
    Target: array[0..1] of TTarget;
    TargetPicked: integer;
    ErrorCode: integer;
  end;

  Coco = class
    public
      constructor Create(p1,p2,p3,p4:Real;nbRuns:Integer); overload;
      constructor Create; overload;
      // appelle GetTargets en interne et retourne cha�ne de caract�re
      // NB: incr�mente nombre d'it�ration, nouvelles cibles � chaque appel
      function ReturnNewTargets:String;
      // R1 pour r�compense, R0 sinon
      // NB: ici retourne m�me r�sultat tant que pas de nouvelles cibles g�n�r�es
      function ReturnReward:String;
      // Est-ce qu'il reste des essais � faire ?
      function hasRunsLeft:Boolean;
      // Quelle est la direction choisie par le mod�le ?
      function setChosenDirection(direction: integer):boolean;
    private
       // nombre de cibles
      FiTargetCount:integer;
      // nombre de paires diff�rentess (6 paires : 4 cibles)
      FiNumberOfPairs:integer;
      // Les quatre cibles en question
      arTarget: array[1..4] of TTarget;
      // sur la scelette
      arDirectionShown: array[1..4] of boolean;
      // touche le moins possible � GetTargets, r�partira les paires de cibles
      bMultipleRuns: boolean;
      {
      arContext[1] = T1 & T2
      arContext[2] = T1 & T3
      arContext[3] = T1 & T4
      arContext[4] = T2 & T3
      arContext[5] = T2 & T4
      arContext[6] = T3 & T4
      The order of the targets is not important, so there are 6 possible combinations
      arContext keeps track of how many times each pairing has been presented
      arPairChoices holds a list of pair choices 1,2,3,4,5,6,1,2,3.....4,5,6,1,2
      }
      arContext: array[1..6] of TContext;
      arPairChoices: array of integer;
      // "which of the 6 target pairs is being used in the current trial"
      FCurrentContext: integer;
      // Nombre d'it�rations
      FRunsPerSimulation: integer;
      // it�ration actuelle
      FCurrentRun: integer;
      // Garde trace des essais
      arTrial: array of TTrial;
      // Est-ce qu'une r�compense a �t� attribu�e
      // (oui, ce n'est en fait qu'une variable locale � CheckChoices � l'origine)
      bRewardGiven : boolean;
      // Est-ce qu'une r�compense a d�j� �t� calcul�e pour cibles actuelles ?
      bRewardAlreadyGiven: boolean;
      procedure SetupPairings;
      procedure InitializeWeights;
      procedure GetTargets;
      function CheckChoices:integer;
  end;


implementation

uses SysUtils;

// Constructeur clef en main pour des param�tres automatiques
constructor Coco.Create;
begin
  // Probas classique, avec chaque contexte qui revient trois fois.
  Create(0,0.33,0.66,1,18);
end;

{
  Pas tr�s joli, mais c'est avec ce constructeur param�tre l'XP.

  Quatre r�els pour d�finir les probas des 4 cibles (une probas n�gative pour ne
  pas utiliser cette cible).

  nbRuns : nombre d'it�rations de la simulation.
}
constructor Coco.Create(p1,p2,p3,p4:Real;nbRuns:integer);
begin
  Randomize;
  bMultipleRuns := true;
  bRewardAlreadyGiven := false;

  // On d�fini les cibles (yep, une petite ellipse � la Java serait plus propre + nb de cibles variables)
  arTarget[1].Use := p1 >= 0;
  arTarget[2].Use := p2 >= 0;
  arTarget[3].Use := p3 >= 0;
  arTarget[4].Use := p4 >= 0;
  // Pour le coup la Prob n�gative fait t�che � ce niveau.
  arTarget[1].RewardProbability := p1;
  arTarget[2].RewardProbability := p2;
  arTarget[3].RewardProbability := p3;
  arTarget[4].RewardProbability := p4;

  // nombre d'it�rations
  FRunsPerSimulation := nbRuns;

  // Initialisation de quelques variables globales
  InitializeWeights;
  // A partir des cibles choisies, initialise les paires et le contexte
  SetupPairings;

  // Sera incr�ment� � chaque demande de cibles
  FCurrentRun :=0;

end;

function Coco.hasRunsLeft:Boolean;
begin
  result := FCurrentRun < FRunsPerSimulation;
end;

function Coco.ReturnNewTargets:String;
var
  i:integer;
begin
  // Nouveau tour, nouvelle r�compense
  bRewardAlreadyGiven := false;
  
  result := 'T';
  // Nouvel essai
  inc(FCurrentRun);
  GetTargets;
  // Cr�e chaine de caract�re codifi�e num cible / position cible pour chaque
  // choisie par GetTargets
  for i := 1 to 4 do begin
    if arTarget[i].Show then
      result := result + IntToStr(i) + IntToStr(arTarget[i].Direction)
  end;

end;

function Coco.ReturnReward:String;
const
  // drapeau "Assignable typed constants" modifi� localement pour utiliser const comme static
  {$J+}
  // Qu'elle est cette pr�c�dente direction ?
  previousDirection : integer = -1;
  {$J-}
begin
  result := 'R';
  // R�ponse sera consistante dans le cas de deux appels successifs
  if not bRewardAlreadyGiven then
  begin
    // comme bRewardGiven n'est pas r�initialis� m�me sans bonne direction s�lectionn�e, faut garder trace de direction pr�c�dente.
    previousDirection := CheckChoices;
    bRewardAlreadyGiven := true;
  end;
  // Une indirection syst�matique pour un else �vit�
  result := result + IntToStr(previousDirection);
end;

{
  Boucle sur les cibles, marque le bon drapeau quand l'une correspond � cette direction.

  NB: ne s'arr�te pas � premi�re cible trouv�e
    (comportement analogue � CheckCorticalActivation de DMM)

  FIXME: On pourra r�cup�rer une r�compense avec une direction de 0 inject�e ici,
    mais dans DMM une r�compense 0 est d�j� donn�e

  Retourne faux si aucune cible ne correspond.
}
function Coco.setChosenDirection(direction: integer): boolean;
var
  i : integer;
begin
  result := false;
  for i := 1 to 4 do begin
    if arTarget[i].Direction = direction then begin
      arTarget[i].ChosenAsDirection := true;
      result := true;
    end;
  end;
end;

{
A pair of targets is considered as a context in this task
The number of possible pairs of targets depends on the number of targets to use in the simulations!
Set up an array of pairs of targets so that information about learning of the context can be saved
to Excel
}
procedure Coco.SetupPairings;
var i: integer;
begin
  //each pairing appears 1/6th of the time if there are 4 targets. Set the array 1 longer for run
  //counts that are not divisible by 6
  FiTargetCount := 0;
  for i := 1 to 4 do begin
    if arTarget[i].Use then
      inc(FiTargetCount);
  end;
  case FiTargetCount of
    2: FiNumberOfPairs := 1;
    3: FiNumberOfPairs := 3;
    4: FiNumberOfPairs := 6;
  end;

  {
  create a list of pair choices 1,2,3,4,5,6,1,2,3.....4,5,6,1,2
  these are chosen randomly to pick targets and assure that each pair gets used
  about the same number of times
  }
  SetLength(arPairChoices, FRunsPerSimulation);
  for i := 0 to pred(FRunsPerSimulation) do
    arPairChoices[i] := (i mod FiNumberOfPairs) + 1;
  for i := 1 to FiNumberOfPairs do
    //arContext is 1 based
    //set the number of times that this context has been used in this simulaton to zero
    arContext[i].ChosenCount := 0;
end;

//Choose two targets in two positions and display them along with their values
procedure Coco.GetTargets;
var i, j,
    iPair,
    iChoicesLeft,
    iTarget1,
    iTarget2: integer;
    arTargetNumbers: array of integer;
begin
  // Moi je fais sauter les warnings, je suis comme �a
  iTarget1 := 0;
  iTarget2 := 0;

  SetLength(arTargetNumbers, FiTargetCount);
  j := 0;
  //arTarget is a 1 based array
  for i := 1 to 4 do begin
    arTarget[i].Direction := -1;
    arTarget[i].Show := false;
    arTarget[i].Correct := false;
    arTarget[i].ChosenAsDirection := false;
    arTarget[i].ChosenAsTarget := false;
    if arTarget[i].Use then begin
      //We need to work out which targets have been selected as this changes which pairs
      //are used when there are less than 4 targets
      arTargetNumbers[j] := i;
      inc(j);
    end;
    arDirectionShown[i] := false;
    {
    TImage(FindComponent('imT' + IntToStr(i))).Visible := false;
    TShape(FindComponent('shpCog' + IntToStr(i))).Visible := false;
    TShape(FindComponent('shpMotor' + IntToStr(i))).Visible := false;
    }
  end;

  if bMultipleRuns then begin
    {
    For 4 targets:
    arContext[1] = T1 & T2
    arContext[2] = T1 & T3
    arContext[3] = T1 & T4
    arContext[4] = T2 & T3
    arContext[5] = T2 & T4
    arContext[6] = T3 & T4
    The order of the targets is not important, so with 4 targets there are 6 possible combinations
    Work out which is the current pairing so the outcome for that pairing can be
    shown in the correct columns in the pairings sheet in Excel

    arPairChoices holds all the possible pair combinations left to be chosen from
    }
    iChoicesLeft := length(arPairChoices);
    iPair := RandomRange(0, iChoicesLeft);
    FCurrentContext := arPairChoices[iPair];
    arContext[FCurrentContext].ChosenCount := arContext[FCurrentContext].ChosenCount + 1;
    case FiNumberOfPairs of
      1: begin
        //with 2 targets, only one pair
        iTarget1 := arTargetNumbers[0];
        iTarget2 := arTargetNumbers[1];
      end;
      3: begin
        //with 3 targets, 3 pairs
        case FCurrentContext of
          1: begin
            iTarget1 := arTargetNumbers[0];
            iTarget2 := arTargetNumbers[1];
          end;
          2: begin
            iTarget1 := arTargetNumbers[0];
            iTarget2 := arTargetNumbers[2];
          end;
          3: begin
            iTarget1 := arTargetNumbers[1];
            iTarget2 := arTargetNumbers[2];
          end;
        end;
      end;
      6: begin
        //with 4 targets, 6 pairs
          case FCurrentContext of
            1: begin
              iTarget1 := arTargetNumbers[0];
              iTarget2 := arTargetNumbers[1];
            end;
            2: begin
              iTarget1 := arTargetNumbers[0];
              iTarget2 := arTargetNumbers[2];
            end;
            3: begin
              iTarget1 := arTargetNumbers[0];
              iTarget2 := arTargetNumbers[3];
            end;
            4: begin
              iTarget1 := arTargetNumbers[1];
              iTarget2 := arTargetNumbers[2];
            end;
            5: begin
              iTarget1 := arTargetNumbers[1];
              iTarget2 := arTargetNumbers[3];
            end;
            6: begin
              iTarget1 := arTargetNumbers[2];
              iTarget2 := arTargetNumbers[3];
            end;
          end;
        end;
      end;

    //then remove that pair from the list
    for i := iPair to iChoicesLeft - 2 do begin
      arPairChoices[i] := arPairChoices[i+1];
    end;
    SetLength(arPairChoices, iChoicesLeft - 1);
  end
  else begin
    {
    Just doing one run, so no need to draw a target pairing from the arPairChoices array
    to balance the number of pairs seen.
    Two of the targets are displayed and the probablility of the target is shown
    in the corresponding cognitive cortical cell, the other two cells remaining 0
    cognitive cortex is column 0 of the array, rows 1 to 4
    iTarget is global as it is used to flash the chosen target. Value = 1 to 4
    }
    iTarget1 := RandomRange(1,FiTargetCount+1);
    repeat
      iTarget2 := RandomRange(1,FiTargetCount+1);
    until iTarget1 <> iTarget2;
    //work out the current context in case we are using learning by context
    {
    For 4 targets:
    arContext[1] = T1 & T2
    arContext[2] = T1 & T3
    arContext[3] = T1 & T4
    arContext[4] = T2 & T3
    arContext[5] = T2 & T4
    arContext[6] = T3 & T4
    }
    case iTarget1 of
     1: begin
        case iTarget2 of
          2: FCurrentContext := 1;
          3: FCurrentContext := 2;
          4: FCurrentContext := 3;
        end;
      end;
      2: begin
        case iTarget2 of
          1: FCurrentContext := 1;
          3: FCurrentContext := 4;
          4: FCurrentContext := 5;
        end;
      end;
      3: begin
        case iTarget2 of
          1: FCurrentContext := 2;
          2: FCurrentContext := 4;
          4: FCurrentContext := 6;
        end;
      end;
      4: begin
        case iTarget2 of
          1: FCurrentContext := 3;
          2: FCurrentContext := 5;
          3: FCurrentContext := 6;
        end;
      end;
    end;
  end;
  arTarget[iTarget1].Show := true;
  arTarget[iTarget2].Show := true;

  {
  if cbShowTargets.Checked then begin
    //show the two targets next to the activations in the cortex ensemble box surrounded by the colo
    //of the line that will be used in the graph for that target
    TImage(FindComponent('imT' + IntToStr(iTarget1))).Visible := true;
    TImage(FindComponent('imT' + IntToStr(iTarget2))).Visible := true;
    TShape(FindComponent('shpCog' + IntToStr(iTarget1))).Visible := true;
    TShape(FindComponent('shpCog' + IntToStr(iTarget2))).Visible := true;
    TShape(FindComponent('shpLegendCog' + IntToStr(iTarget1))).Visible := true;
    TShape(FindComponent('shpLegendCog' + IntToStr(iTarget2))).Visible := true;
  end;
  }

  //Need to work out the two directions here. Even though they do not appear in
  //motor cortex until the movements phase, they are used in association cortex
  //in the decision phase.
  //Motor cortex is row 0 of the array, columns 1 to 4
  arTarget[iTarget1].Direction := RandomRange(1,5);
  repeat
    arTarget[iTarget2].Direction := RandomRange(1,5);
  until arTarget[iTarget1].Direction <> arTarget[iTarget2].Direction;

  arDirectionShown[arTarget[iTarget1].Direction] := true;
  arDirectionShown[arTarget[iTarget2].Direction] := true;

  {
  SetCognitiveCortexActivation;
  SetMotorCortexActivation;
  SetAssociationCortexActivation;
  }

  arTrial[FCurrentRun-1].Target1 := iTarget1;
  arTrial[FCurrentRun-1].Target[0] := arTarget[iTarget1];
  arTrial[FCurrentRun-1].Target2 := iTarget2;
  arTrial[FCurrentRun-1].Target[1] := arTarget[iTarget2];
  arTrial[FCurrentRun-1].Direction1 := arTarget[iTarget1].Direction;
  arTrial[FCurrentRun-1].Direction2 := arTarget[iTarget2].Direction;

  arTarget[iTarget1].PresentedCount := arTarget[iTarget1].PresentedCount + 1;
  arTarget[iTarget2].PresentedCount := arTarget[iTarget2].PresentedCount + 1;
  {
  TShape(FindComponent('shpLegendMotor' + IntToStr(arTarget[iTarget1].Direction))).Visible := true;
  TShape(FindComponent('shpLegendMotor' + IntToStr(arTarget[iTarget2].Direction))).Visible := true;
  }
end;

// Ersatz de la fonction originelle, alloue espace et initialise quelques tableaux
procedure Coco.InitializeWeights;
var
  i:integer;
begin
  // (r�cup�r� depuis dans TfrmMain.InitializeWeights)

  SetLength(arTrial, FRunsPerSimulation);
  //reset the counters that keep a running tally of the the actual target
  //reward probability based on how many times it has been picked and rewarded
  for i := 1 to 4 do begin
    arTarget[i].PresentedCount := 0;
    arTarget[i].PickedCount := 0;
    arTarget[i].RewardedCount := 0;
    arTarget[i].EffectiveProbability := 0;
    arTarget[i].ExpectedValue := 0.5;
  end;
end;

function Coco.CheckChoices: integer;
var i: integer;
begin
  result := 0; //set this to false in case no target was chosen
  for i := 1 to 4 do begin
    if arTarget[i].Show then begin
      if arTarget[i].ChosenAsDirection then begin
        //do the reward based on the actual direction moved, not the target chosen
        bRewardGiven := arTarget[i].RewardProbability > Random; //need this as well as iReward as it is sent to Excel with the results
        result := integer(bRewardGiven); //either 1 or 0
      end;
    end;
  end;
end;

end.
 