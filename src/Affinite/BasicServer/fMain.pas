{ $HDR$}
{**********************************************************************}
{ Unit archived using Team Coherence                                   }
{ Team Coherence is Copyright 2002 by Quality Software Components      }
{                                                                      }
{ For further information / comments, visit our WEB site at            }
{ http://www.TeamCoherence.com                                         }
{**********************************************************************}
{}
{ $Log:  110552: fMain.pas 
{
{   Rev 1.0    25/10/2004 22:57:22  ANeillans    Version: 9.0.17
{ Verified
}
{-----------------------------------------------------------------------------
 Demo Name: fMain
 Author:    Allen O'Neill
 Copyright: Indy Pit Crew
 Purpose:   Basic TCP server demo
 History:   Created 12/July/2002
 Date:      12/07/2002 23:57:17
-----------------------------------------------------------------------------
 Notes:

 This basic demo shows the following:

 (1) How to receive and send Strings and Integers to a remote client
 (2) How to disconnect a single remote client
 (3) Basic protocol conversation between server and client, all contained in the OnExecute event

  Verified:
  Indy 9:
    D7: 25th Oct 2004 by Andy Neillans 
}


unit fMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, CheckLst, IdBaseComponent, IdComponent,
  IdTCPServer, IdResourceStrings, IdStack, IdGlobal, IdSocketHandle, fClient;

type
  TfrmMain = class(TForm)
    Label1: TLabel;
    pgeMain: TPageControl;
    tabProcesses: TTabSheet;
    tabMain: TTabSheet;
    Label2: TLabel;
    lbIPs: TCheckListBox;
    Label3: TLabel;
    IdTCPServer: TIdTCPServer;
    cboPorts: TComboBox;
    Label4: TLabel;
    edtPort: TEdit;
    btnStartServer: TButton;
    btnStopServer: TButton;
    btnExit: TButton;
    lbProcesses: TListBox;
    StatusBar: TStatusBar;
    btnClearMessages: TButton;
    edtWelcome: TEdit;
    lbWelcome: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cboPortsChange(Sender: TObject);
    procedure btnStartServerClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnStopServerClick(Sender: TObject);
    procedure btnClearMessagesClick(Sender: TObject);
    procedure IdTCPServerExecute(AThread: TIdPeerThread);
    procedure IdTCPServerConnect(AThread: TIdPeerThread);
    procedure btnExitClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  fErrors : TStringList;
  fServerRunning : boolean;
  procedure PopulateIPAddresses;
  function PortDescription(const PortNumber: integer): string;
  function StartServer:Boolean;
  function StopServer:Boolean;
  end;
const
  // nombre maximum de clients (chaqun ayant sa fen�tre, on limite)
  // (8 client, on part de 0)
  MAX_NB_CLIENTS = 7;
var
  frmMain: TfrmMain;
  // Nombre de clients connect�s
  nbClients: Integer=-1;
  // Pointera vers les clients
  // FIXME: anttention, limit� en taille
  // FIXME: impossible de me sourvenir de na n�cessit� de Dispose
  tabClients:array [0..MAX_NB_CLIENTS] of ^TIdPeerThread;
  tabFClients:array [0..MAX_NB_CLIENTS] of TClient;


implementation

{$R *.dfm}

{ TfrmMain }

// Va passer la main � la proc�dure �ponyme de la fen�tre corresondante
// FIXME: moche, doit pouvoir renseigner directement handler de chaque TIdPeerThread (avec pointeur fonction ??)
procedure TfrmMain.IdTCPServerExecute(AThread: TIdPeerThread);
var
    // si on a bien trouv� la connexion
    found : Boolean;
    i : Integer;
begin
  found := False;
  // boucle sur toutes les connexions pour trouver fen�tre se chargeant du b�b�
  for i := 0 to nbClients do
  begin
    if  tabFClients[i].refConnection^ = AThread.Connection then
      begin
      found := True;
      lbProcesses.Items.Append('Client n�' + IntToStr(i) + ' sent data.');
      tabFClients[i].Execute
      end;
  end;
  // tr�s l�ger comme traitement, si aucun ReadLn � ce niveau, va boucler ind�finiement j'en ai bien peur
  if not found then
      lbProcesses.Items.Append('Error: no match for a previous connection!');
end;


procedure TfrmMain.PopulateIPAddresses;
var
    i : integer;
begin
with lbIPs do
    begin
    Clear;
    Items := GStack.LocalAddresses;
    Items.Insert(0, '127.0.0.1');
    end;
try
  cboPorts.Items.Add(RSBindingAny);
  cboPorts.Items.BeginUpdate;
  for i := 0 to IdPorts.Count - 1 do
    cboPorts.Items.Add(PortDescription(Integer(IdPorts[i])));
finally
  cboPorts.Items.EndUpdate;
end;
end;

function TfrmMain.PortDescription(const PortNumber: integer): string;
begin
  with GStack.WSGetServByPort(PortNumber) do try
    Result := '';
    if Count > 0 then begin
      Result := Format('%d: %s', [PortNumber, CommaText]);    {Do not Localize}
    end;
  finally
    Free;
  end;
end;

procedure TfrmMain.cboPortsChange(Sender: TObject);
    function GetPort(AString:String):String;
    begin
    Result := AString;
    if pos(':',AString) > 0 then
        Result := copy(AString,1,pos(':',AString)-1);
    end;
begin
edtPort.Text :=
    GetPort(cboPorts.Items.Strings[cboPorts.ItemIndex])
end;


function TfrmMain.StartServer: Boolean;
var
    Binding : TIdSocketHandle;
    i : integer;
    SL : TStringList;
begin
SL := TStringList.Create;

if not StopServer then
    begin
    fErrors.Append('Error stopping server');
    Result := false;
    exit;
    end;

IdTCPServer.Bindings.Clear; // bindings cannot be cleared until TidTCPServer is inactive
try
try

for i := 0 to lbIPs.Count-1 do
    if lbIPs.Checked[i] then
        begin
        Binding := IdTCPServer.Bindings.Add;
        Binding.IP := lbIPs.Items.Strings[i];
        Binding.Port := StrToInt(edtPort.Text);
        SL.append('Server bound to IP ' +  Binding.IP + ' (' + GStack.WSGetHostByAddr(Binding.IP) + ') on port ' + edtPort.Text);
        end;

IdTCPServer.Active := true;
result := IdTCPServer.Active;
fServerRunning := result;
lbProcesses.Items.AddStrings(SL);
lbProcesses.Items.Append('Server started');

if result then
    StatusBar.SimpleText := 'Server running'
else StatusBar.SimpleText := 'Server stopped';

except
on E : Exception do
    begin
    lbProcesses.Items.Append('Server not started');
    fErrors.append(E.Message);
    Result := false;
    fServerRunning := result;
    end;
end;
finally
FreeAndNil(SL);
end;
end;


function TfrmMain.StopServer: Boolean;
begin
IdTCPServer.Active := false;
IdTCPServer.Bindings.Clear;
Result := not IdTCPServer.Active;
fServerRunning := result;
if result then
    begin
    StatusBar.SimpleText := 'Server stopped';
    lbProcesses.Items.Append('Server stopped');
    end
else
    begin
    StatusBar.SimpleText := 'Server running';
    lbProcesses.Items.Append('Server not stopped');
    end;
end;

procedure TfrmMain.btnStopServerClick(Sender: TObject);
begin
fErrors.Clear;
if not fServerRunning then
    begin
    ShowMessage('Server it not running - no need to stop !');
    Exit;
    end;
if not StopServer then
    ShowMessage('Error stopping server ' + #13 + #13 + fErrors.Text)
else
    ShowMessage('Server stopped successfully');
end;

procedure TfrmMain.btnStartServerClick(Sender: TObject);
var
    x,i : integer;
begin
x := 0;
for i := 0 to lbIPs.Count-1 do
    if lbIPs.Checked[i] then
      inc(x);

if x < 1 then
    begin
    ShowMessage('Cannot proceed until you select at least one IP to bind!');
    exit;
    end;

fErrors.Clear;
if not StartServer then
    ShowMessage('Error starting server' + #13 + #13 + fErrors.text)
else ShowMessage('Server started successfully!');
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
FreeAndNil(fErrors);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var i:Integer;
begin
fErrors := TStringList.Create;
PopulateIPAddresses;

// FIXME: Super moche !!!
// on cr��e tout � l'avance afin d'�vite bloqueage Thread lors attente lecture socket (???)
for i := 0 to 7 do
begin
  tabFClients[i] := TClient.Create (Self);
  tabFClients[i].Caption := 'Client' + IntToStr(i);
  // yep, dans le d�lire faut aussi afficher une fois, sinon bloquera itou par la suite
  tabFClients[i].Show;
  tabFClients[i].Hide;
end {for};


end;

procedure TfrmMain.btnClearMessagesClick(Sender: TObject);
begin
lbProcesses.Clear;
end;

// Lors de la connexion d'un client, on lui attribue une fen�tre pour dialoguer avec (sauf si a d�pass� limite).
// FIXME: d�cr�menter nombre clients lors de d�connexion
// FIXME: je doute que soit capable de g�rer deux connexion simult�nan�e (op�rations non atomiques)
procedure TfrmMain.IdTCPServerConnect(AThread: TIdPeerThread);
var
 curClient: Integer;
 // Cible la connexion en elle-m�me
 curConnection: TIdTCPServerConnection;
begin
  curConnection := AThread.Connection;
  // Trop de clients sont pass�s par l�, on l'informe gentiement que �a va pas �tre possible et on le reconduit � la porte.
  if nbClients = MAX_NB_CLIENTS then
    begin
    lbProcesses.Items.Append('New client attempts to connect, but maximum number reached');
    curConnection.WriteLn('Sorry, maximum client numbers reached.');
    curConnection.Disconnect;
    end
  else                                        
    begin
    Inc (nbClients);
    curClient := nbClients;
    lbProcesses.Items.Append('New client, n�' + IntToStr(curClient) + ', hostname: ' + GStack.WSGetHostByAddr(curConnection.Socket.Binding.PeerIP) +', IP: ' + curConnection.Socket.Binding.PeerIP);
    // Renseigne comme il faut la fen�tre d�di�e puis on l'affiche
    tabFClients[curClient].Caption := 'Client n�' + IntToStr(curClient) + ' (' + GStack.WSGetHostByAddr(curConnection.Socket.Binding.PeerIP) + ' - ' + curConnection.Socket.Binding.PeerIP + ')';
    // Fait le lien entre fen�tre et connexion
    tabFClients[curClient].refConnection:= @AThread.Connection;
    tabFClients[curClient].Show;
    // Lors de connexion envoie le message personnalis�
    AThread.Connection.WriteLn(edtWelcome.Text);
    end;
end;

procedure TfrmMain.btnExitClick(Sender: TObject);
begin
 Close;
end;

end.
