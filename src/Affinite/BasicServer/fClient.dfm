object Client: TClient
  Left = 2212
  Top = 347
  Width = 383
  Height = 410
  Caption = 'Client'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lNom: TLabel
    Left = 16
    Top = 16
    Width = 26
    Height = 13
    Caption = 'Client'
  end
  object lbMessages: TListBox
    Left = 8
    Top = 40
    Width = 353
    Height = 273
    ItemHeight = 13
    TabOrder = 0
  end
  object edtMessage: TEdit
    Left = 8
    Top = 328
    Width = 265
    Height = 21
    TabOrder = 1
    OnKeyPress = edtMessageKeyPress
  end
  object btSend: TButton
    Left = 288
    Top = 328
    Width = 75
    Height = 25
    Caption = 'Send'
    TabOrder = 2
    OnClick = btSendClick
  end
end
