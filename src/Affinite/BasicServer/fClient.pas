unit fClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdTCPServer, StdCtrls;

type
  TClient = class(TForm)
    lbMessages: TListBox;
    lNom: TLabel;
    edtMessage: TEdit;
    btSend: TButton;
    // Ecoute et r�pond autoamtiquement au client associ�
    // (d�clench� par le programme de la fen�tre principale)
    procedure Execute;
    // Messages envoy�s manuellement au client
    procedure SendResponse;
    procedure btSendClick(Sender: TObject);
    procedure edtMessageKeyPress(Sender: TObject; var Key: Char);
  public
    // Connexion associ�e � la fen�tre
    refConnection: ^TIdTCPServerConnection;
  end;

var
  Client: TClient;

implementation

uses IdTCPConnection;

{$R *.dfm}

procedure TClient.Execute;
var
    Command : String;
    response : String;
    quit : Boolean;
begin
// Tout d'abord on r�affiche la fen�tre si elle a �t� ferm�e
if Self.Showing = False then
  Self.Show;
quit := False;
Command := refConnection^.ReadLn;
lbMessages.Items.Append('R: ' + Command);
if uppercase(Command) = 'TIME' then
    response := FormatDateTime('hh:nn:ss',now)
else if uppercase(Command) = 'DATE' then
    response := FormatDateTime('dd/mmm/yyyy',date)
else if uppercase(Command) = 'TICKCOUNT' then
    response := IntToStr(GetTickCount)
else if uppercase(Command) = 'QUIT' then
    begin
    response := 'Goodbye!';
    quit := True;
    end;
// Ne r�pond rien autrement (plus qu'� utiliser le petit "send")
if response <> '' then
  begin
  refConnection^.WriteLn(response);
  lbMessages.Items.Append('S: ' + response);
  end;
  // On ne diff�re pas plus la demande  de d�conexion
  if quit then
    refConnection^.Disconnect;
end;

procedure TClient.btSendClick(Sender: TObject);
begin
  SendResponse;
end;

// Valide r�ponse si touche entr�e press�e
procedure TClient.edtMessageKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  SendResponse;
end;

// Message envoy� manuellement au client associ�
// FIXME: si on envoie message sans donn�es re�ues et que le client par la suite envoie une commande, alors c'est le message dans le buffer qui sera renvoy�, et non la r�ponse correspondante � la commande.
// FIXME: Rem�dier � ce d�calage en contr�lant �coute ou non du client ? saut de ligne ? buffer ?
procedure TClient.SendResponse;
var
  response : String;
begin
  // Si le champ idoine contient du texte, on l'envoie et on le r�initialise
  response :=  edtMessage.Text;
  if response <> '' then
    begin
    refConnection^.WriteLn(response);
    lbMessages.Items.Append('S: ' + response);
    edtMessage.Text := '';
    end;
end;

end.


