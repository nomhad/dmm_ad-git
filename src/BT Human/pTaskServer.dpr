program pTaskServer;

uses
  Forms,
  Behavioraltask in 'Behavioraltask.pas' {frmTask},
  Trials in 'Trials.pas' {frmTrials},
  Joy in 'Joy.pas' {frmJoystick},
  uJoystick in 'uJoystick.pas',
  uQuestionnaire1 in 'uQuestionnaire1.pas' {frmQ1},
  uQuestionnaire2 in 'uQuestionnaire2.pas' {frmQ2},
  uEnd in 'uEnd.pas' {frmEnd},
  uPractiseEnd in 'uPractiseEnd.pas' {frmPractiseScore},
  uSession2 in 'uSession2.pas' {frmSession2},
  uHighestScore in 'uHighestScore.pas' {frmHighestScore},
  uPractiseStart in 'uPractiseStart.pas' {frmPractise},
  uHighScore in 'uHighScore.pas' {frmHighScore},
  uArray in '..\Utilities\uArray.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Human Behavioural Task';
  Application.CreateForm(TfrmTask, frmTask);
  Application.Run;
end.
