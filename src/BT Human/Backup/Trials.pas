unit Trials;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, Mask, ImgList, Math,
  uJoystick, Buttons, ToolWin, ComObj, IniFiles;

type
  TTarget = record
    Show: boolean;
    Position: integer;
    JoystickPosition: TJoystickPosition;
    RewardProbability: real;
    Image: TImage;
    Picked: boolean;
  end;

  TResults = record
    TrialCount,
    Success,
    Reward,
    Optimal: integer;
  end;

  TTrial = record
    ReactionTime,
    ITI: integer;
    Successful,
    Rewarded: boolean;
    Target1,
    Target2: integer;
    Target: array[0..1] of TTarget;
    TargetPicked: integer;
    Optimum: boolean;
    ErrorCode: integer;
  end;

  TfrmTrials = class(TForm)
    sb1: TStatusBar;
    Panel1: TPanel;
    img3: TImage;
    img4: TImage;
    img2: TImage;
    img1: TImage;
    imgCursor: TImage;
    Label1: TLabel;
    ilTargets: TImageList;
    imgGreenCircle: TImage;
    imgRedCircle: TImage;
    imgBlackCircle: TImage;
    Shape2: TShape;
    Shape4: TShape;
    Shape3: TShape;
    Shape1: TShape;
    ToolBar1: TToolBar;
    btnGo: TToolButton;
    ilButtons: TImageList;
    Memo1: TMemo;
    lblMoveCursor: TLabel;
    btnStop: TToolButton;
    lblReward: TLabel;
    Timer1: TTimer;
    lblFail: TLabel;
    Timer2: TTimer;
    btnPractise: TToolButton;
    btnExit: TToolButton;
    btnSend2Excel: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    Label10: TLabel;
    lblError: TLabel;
    Timer3: TTimer;
    imgBlueCircle: TImage;
    lblStartTrial: TLabel;
    lblPractise: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnGoClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure btnPractiseClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnSend2ExcelClick(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
  private
    { D�clarations priv�es }
    //which 2 of the 4 targets to show
    arToShow: array[0..1] of integer;
    //what positons to show them in. These are set in PickTargets
    arTargetPosition: array[0..1] of integer;
    FResults: TResults;
    FTrial: array of TTrial;
    FCurrentTrial: integer;
    FRewardProbabilities: array[0..3] of real;
    bStop: boolean;

    FCenter,
    FCursorOffset: TPoint;

    arTarget: array[1..4] of TTarget;
    FModelIpAddress: string;
    FRobotAttached: boolean;

    FXLApp,
    FXLWorkbook,
    FXLWorksheet: OLEVariant;
    FExcelWorksheet: string;
    FSubject: string;
    FShowProgrammerAids: boolean;
    FUseButtonToStartTrials: boolean;
    FTrialCount: integer;
    FButtonToUse: integer;

    procedure PositionLabel(ALabel: TLabel);
    procedure ShowTrialStart;
    procedure ShowRedCircle;
    procedure ShowGoSignal;
    procedure ShowBlackCircle;
    procedure ShowBlueCircle;
    procedure PickTargets;// vient s�lectionner quels images � quels emplacements al�atoires
    procedure ShowTargets;
    function GetReactionTime: boolean;
    procedure HideTargets;
    function GoToTarget(var APos: TJoystickPosition): boolean;
    procedure CheckIfOptimumChosen(TargetChosen: integer);
    function WaitOnTarget(WhatPosition: TJoystickPosition): boolean;
    function GoHome: boolean;
    function WaitOnCenter: boolean;
    procedure CheckReward;
    procedure Wait(HowLong: integer);
    function GetCursorCentered: boolean;
    procedure ShowError;

    function ShowCursorPosition: TJoystickPosition;
    procedure SetImagePosition(TargetNumber: integer; Position: integer);
    procedure SetJoystickField;
    procedure InitializeTrial;
    function StartTrial: boolean;
    procedure SetTargetRewardProbabilities;
    function GetRewardProbability(index: integer): real;
    procedure SetRewardProbability(index: integer; const Value: real);

    function CreateExcelWorkbook: boolean;
    procedure SendSetupToExcel;
    procedure SendResultsToExcel;

    function GetHighScore: integer;
    procedure SaveHighScore(SubjectId: string; Score: integer);

  public
    { D�clarations publiques }
    property TrialCount: integer read FTrialCount write FTrialCount;
    property ModelIpAddress: string read FModelIpAddress write FModelIpAddress;
    property RobotAttached: boolean read FRobotAttached write FRobotAttached;
    property RewardProbabilities[index: integer]: real read GetRewardProbability write SetRewardProbability;
    property Subject: string read FSubject write FSubject;
    property ShowProgrammerAids: boolean read FShowProgrammerAids write FShowProgrammerAids;
    property UseButtonToStartTrials: boolean read FUseButtonToStartTrials write FUseButtonToStartTrials default true;
    property ButtonToUse: integer read FButtonToUse write FButtonToUse default 1;
end;

var
  frmTrials: TfrmTrials;

implementation

{$R *.dfm}

const
  //constants for the images in image list il1 so that images can be loaded into
  //imgCenter as the trial progresses
  IMG_TARGET = 0;
  IMG_GO_SIGNAL = 1;
  IMG_SUCCESS = 2;
  IMG_FAIL = 3;
  IMG_END = 4;
  IMG_WHITE = 5;

  //constants for the error messages when a trial is not completed successfully
  EC_GOOD = 0;
  EC_NO_CENTER = 1;
  EC_ITI_TOO_LONG = 2;
  EC_NO_MOVE = 3;
  EC_NO_TARGET = 4;
  EC_NO_WAIT = 5;
  EC_NO_RETURN = 6;
  EC_NO_WAIT_CENTER = 7;

  //Error messages to display at the end of a trial if all did not go well
  ErrorMessages: array[1..7] of string = ('You did not return the cursor to the center in the time allowed',
                                          'You waited too long to start the trial',
                                          'You did not move from the center within the allowed time',
                                          'You did not move to a target within the allowed time',
                                          'You did not keep the cursor on the target long enough',
                                          'You did not return to the center within the allowed time',
                                          'You did not keep the cursor on the center long enough');

  ROW_HEADER = 1;
  xlWBATWorksheet = -4167;
  NO_YES: array[0..1] of string = ('No','Yes');

function TfrmTrials.ShowCursorPosition: TJoystickPosition;
var APoint: TPoint;
begin
  Application.ProcessMessages;
  APoint := AJoystick.AdjustedCurrentPoint;
  result := AJoystick.CurrentPosition;
  imgCursor.Left := APoint.X - (imgCursor.Width div 2) + FCursorOffset.X;
  imgCursor.Top := APoint.Y - (imgCursor.Height div 2) + FCursorOffset.Y;
  sb1.Panels[0].Text := 'X Coordinate: ' + IntToStr(APoint.X);
  sb1.Panels[1].Text := 'Y Coordinate: ' + IntToStr(APoint.Y);
  sb1.Panels[2].Text := 'Position: ' + AJoystick.CurrentPositionToString(result);
  Application.ProcessMessages;
end;

procedure TfrmTrials.FormShow(Sender: TObject);
var iOffset: integer;  //how far from the center to put each target. 1/3 of screen height?
begin
  RewardProbabilities[0] := 0;
  RewardProbabilities[1] := 0.33;
  RewardProbabilities[2] := 0.66;
  RewardProbabilities[3] := 1;

  iOffset := Screen.Height div 3;

  FCenter.X := (Screen.Width - imgCursor.Width) div 2;
  FCenter.Y := (Screen.Height - imgCursor.Height) div 2;

  Img1.SetBounds(FCenter.X + iOffset,FCenter.Y,120,120);  //right shape
  Img2.SetBounds(FCenter.X, FCenter.Y - iOffset,120,120);  //top shape
  Img3.SetBounds(FCenter.X - iOffset, FCenter.Y,120,120);  //left shape
  Img4.SetBounds(FCenter.X, FCenter.Y + iOffset,120,120);  //bottom shape
  Shape1.Left := Img1.Left - 40;
  Shape1.Top :=  Img1.Top - 40;
  Shape2.Left := Img2.Left - 40;
  Shape2.Top :=  Img2.Top - 40;
  Shape3.Left := Img3.Left - 40;
  Shape3.Top :=  Img3.Top - 40;
  Shape4.Left := Img4.Left - 40;
  Shape4.Top :=  Img4.Top - 40;
  imgRedCircle.Left := FCenter.X;
  imgRedCircle.Top := FCenter.Y;
  imgGreenCircle.Left := FCenter.X;
  imgGreenCircle.Top := FCenter.Y;
  imgBlackCircle.Left := FCenter.X;
  imgBlackCircle.Top := FCenter.Y;
  imgBlueCircle.Left := FCenter.X;
  imgBlueCircle.Top := FCenter.Y;

  PositionLabel(lblMoveCursor);
  PositionLabel(lblReward);
  PositionLabel(lblStartTrial);
  PositionLabel(lblFail);
  PositionLabel(lblError);
  PositionLabel(lblPractise);

  imgCursor.Visible := false;

  arTarget[1].Image := img1;
  arTarget[2].Image := img2;
  arTarget[3].Image := img3;
  arTarget[4].Image := img4;

  Memo1.Visible := FShowProgrammerAids;

  Randomize;
end;

procedure TfrmTrials.btnGoClick(Sender: TObject);
var i,
    iHighScore: integer;
    APos: TJoystickPosition;
begin
  iHighScore := GetHighScore;
  MessageDlg('The high score to beat is '+ IntToStr(iHighScore), mtInformation, [mbOK],0);
  imgCursor.Visible := true;
  SetJoystickField;
  bStop := false;
  SetLength(FTrial, FTrialCount);
  FResults.TrialCount := FTrialCount;
  FResults.Success := 0;
  FResults.Reward := 0;
  FResults.Optimal := 0;
  SetTargetRewardProbabilities;
  HideTargets;
  imgBlackCircle.Visible := true;
  for i := 0 to pred(FTrialCount) do begin
    FCurrentTrial := i;
    if bStop then
      EXIT;
    if GetCursorCentered then begin
      InitializeTrial;
      if StartTrial then begin
        memo1.Lines.Add('Essais num�ro : ' + IntToStr(i+1));
        PickTargets;
        ShowTrialStart;
        ShowBlackCircle;
        ShowTargets;
        if GetReactionTime then begin
      //    ShowGoSignal;
          if GoToTarget(APos) then begin
            //cursor has moved from center and reached one of the displayed targets
            if WaitOnTarget(APos) then begin
              //waited long enough on the target, move back to the center
              ShowRedCircle;
              if GoHome then begin
                //got back to the center
                ShowBlueCircle;
//                if WaitOnCenter then begin
                  //trial successfully completed
                  FResults.Success := FResults.Success + 1;
                  FTrial[FCurrentTrial].Successful := true;
//                end;
              end;
            end;
          end;
        end;
      end;
    end;
    HideTargets;
    ShowError;
    CheckReward;
  end;
  memo1.Clear;
  memo1.Lines.Add('Fin de la s�rie d''essais ');
  memo1.Lines.Add('Nombre d''essais: ' + IntToStr(FResults.TrialCount));
  memo1.Lines.Add('Nombre d''essais men�s � bien: ' + IntToStr(FResults.Success));
  memo1.Lines.Add('Nombre de r�compenses re�ues: ' + IntToStr(FResults.Reward));
  MessageDlg('Vous avez termin� la t�che. Merci pour votre participation' + #13#10
             + 'You scored ' + IntToStr(FResults.Reward), mtInformation, [mbOK], 0);
  if FResults.Reward > iHighScore then begin
    MessageDlg('You have the highest score!' +  #13#10 + 'Congratulations', mtInformation, [mbOK],0);
    SaveHighScore(FSubject, iHighScore);
  end;
  bStop := true;
end;

function TfrmTrials.GetCursorCentered: boolean;
var APos: TJoystickPosition;
    iStart: integer;
begin
  result := true;
  iStart := GetTickCount;
  lblMoveCursor.Visible := true;
  APos := ShowCursorPosition;
  while APos <> jpCenter do begin
    Application.ProcessMessages;
    APos := ShowCursorPosition;
    if GetTickCount - iStart > 10000 then begin
      //Allow 10 seconds to get back to the center
      FTrial[FCurrentTrial].ErrorCode := EC_NO_CENTER;
      result := false;
      EXIT;
    end;
  end;
  lblMoveCursor.Visible := false;
end;

procedure TfrmTrials.InitializeTrial;
begin
  FTrial[FCurrentTrial].ITI := -1;
  FTrial[FCurrentTrial].Successful := false;
  FTrial[FCurrentTrial].Rewarded := false;
  FTrial[FCurrentTrial].ReactionTime := -1;
  FTrial[FCurrentTrial].TargetPicked := -1;
  FTrial[FCurrentTrial].ErrorCode := EC_GOOD;
  memo1.Clear;
end;

{
If FUseButtonToStartTrials is set, then wait for the user to press the joystick
button before starting the trial.
Use this delay to work out the intertrial interval
}
function TfrmTrials.StartTrial: boolean;
var iStart: integer;
begin
  result := true;
  if FUseButtonToStartTrials then begin
    lblStartTrial.Visible := true;
    iStart := GetTickCount;
    while not AJoystick.ButtonPressed(ButtonToUse) do begin
      Application.ProcessMessages;
      ShowCursorPosition;
      if GetTickCount - iStart > 10000 then begin
        //Let the subject have a maximum of 10 seconds to think between trials
        FTrial[FCurrentTrial].ErrorCode := EC_ITI_TOO_LONG;
        lblStartTrial.Visible := false;
        result := false;
        EXIT;
      end;
    end;
    FTrial[FCurrentTrial].ITI := GetTickCount - iStart;
    lblStartTrial.Visible := false;
    memo1.Lines.Add('Intertrial interval: ' + IntToStr(FTrial[FCurrentTrial].ITI) + 'ms');
  end;
end;

{
Display the targets in random positions for the current trial
}
procedure TfrmTrials.PickTargets;
var i,
    iTarget1,
    iTarget2: integer;
    Selected: array[1..4]of boolean;
    AnImage: TImage;
begin
  for i := 1 to 4 do begin
    AnImage := TImage(FindComponent('img' + IntToStr(i)));
    //hide all four images. show them only at the correct time during the trial
//    AnImage.Picture := nil;
    AnImage.Visible := false;
    arTarget[i].Show := false;
    arTarget[i].Picked := false;
    arTarget[i].Position := -1;
  end;

  //get which two targets to show for this trial
  iTarget1 := RandomRange(1,5);
  arTarget[iTarget1].Show := true;
//  FTrial[FCurrentTrial].Target1 := iTarget1;
  repeat
    iTarget2 := RandomRange(1,5);
  until iTarget1 <> iTarget2;
  arTarget[iTarget2].Show := true;
//  FTrial[FCurrentTrial].Target2 := iTarget2;

  //then work out which positions to show them in
  arTarget[iTarget1].Position := RandomRange(1,5);
  //joystick positions start with jpCenter, so the joystick position is 1 more than the actual position
  arTarget[iTarget1].JoystickPosition := TJoystickPosition(arTarget[iTarget1].Position);
  repeat
    arTarget[iTarget2].Position := RandomRange(1,5);
  until arTarget[iTarget1].Position <> arTarget[iTarget2].Position;
  arTarget[iTarget2].JoystickPosition := TJoystickPosition(arTarget[iTarget2].Position);

  SetImagePosition(iTarget1, arTarget[iTarget1].Position);
  SetImagePosition(iTarget2, arTarget[iTarget2].Position);

  FTrial[FCurrentTrial].Target1 := iTarget1;
  FTrial[FCurrentTrial].Target2 := iTarget2;
  FTrial[FCurrentTrial].Target[0] := arTarget[iTarget1];
  FTrial[FCurrentTrial].Target[1] := arTarget[iTarget2];

  memo1.Lines.Add('Premi�re cible: ' + IntToStr(iTarget1));
  memo1.Lines.Add('Position de la premi�re cible: ' + IntToStr(arTarget[iTarget1].Position));
  memo1.Lines.Add('P(R) premi�re cible: ' + FloatToStrF(arTarget[iTarget1].RewardProbability, ffFixed, 3, 2));
  memo1.Lines.Add('Deuxi�me cible: ' + IntToStr(iTarget2));
  memo1.Lines.Add('Position de la deuxi�me cible: ' + IntToStr(arTarget[iTarget2].Position));
  memo1.Lines.Add('P(R) deuxi�me cible: ' + FloatToStrF(arTarget[iTarget2].RewardProbability, ffFixed, 3, 2));
end;

procedure TfrmTrials.SetImagePosition(TargetNumber, Position: integer);
var AnImage: TImage;
    iOffset: integer;  //how far from the center to put each target. 1/3 of screen height?
begin
  iOffset := Screen.Height div 3;
  AnImage := arTarget[TargetNumber].Image;
  case Position of
    1: begin
      AnImage.Left := FCenter.X + iOffset;
      AnImage.Top := FCenter.Y;
    end;
    2: begin
      AnImage.Left := FCenter.X;
      AnImage.Top := FCenter.Y - iOffset;
    end;
    3: begin
      AnImage.Left := FCenter.X - iOffset;
      AnImage.Top := FCenter.Y;
    end;
    4: begin
      AnImage.Left := FCenter.X;
      AnImage.Top := FCenter.Y + iOffset;
    end;
  end;
end;

{
At the start of each trial a green circle surrounded by a black hollow circle
with the black cross cursor is shown for a random period between 1 and 1.5 seconds
}
procedure TfrmTrials.ShowTrialStart;
var iHowLong: integer;
begin
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := true;
  imgBlackCircle.Visible := true;
  imgBlueCircle.Visible := false;
  memo1.Lines.Add('D�but de l''essai');
  Application.ProcessMessages;
  iHowLong := 1000 + random(500);
  Wait(iHowLong);
end;

{
After  the solid green circle a hollow black circle, with the cursor inside is
shown for a random period between 1 and 1.5 seconds
}
procedure TfrmTrials.ShowBlackCircle;
var iHowLong: integer;
begin
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := false;
  imgBlackCircle.Visible := true;
  imgBlueCircle.Visible := false;
  Application.ProcessMessages;
  iHowLong := 1500 + random(500);
  Wait(iHowLong);
end;

procedure TfrmTrials.ShowBlueCircle;
//var iHowLong: integer;
begin
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := false;
  imgBlackCircle.Visible := false;
  imgBlueCircle.Visible := true;
  Application.ProcessMessages;
//  iHowLong := 1500 + random(500);
//  Wait(iHowLong);
end;

{
After the hollow black circle is shown for 1 to 1.5 seconds, the 2 targets
are displayed for 1.5 to 2 seconds before the go signal is given
}
procedure TfrmTrials.ShowTargets;
var i: integer;
    AnImage: TImage;
    iHowLong: integer;
begin
  for i := 1 to 4 do begin
    if arTarget[i].Show then begin
      AnImage := arTarget[i].Image;
      AnImage.Visible := true;
    end;
  end;
  memo1.Lines.Add('Presentation des cibles');
  Application.ProcessMessages;
  //We want the subjects to move as soon as the targets are shown, so no wait time
//  iHowLong := 1500 + random(500);
//  Wait(iHowLong);
end;

{
The reaction time is the time taken to move outside of the central circle  after
the targets are shown
}
function TfrmTrials.GetReactionTime: boolean;
var iStart: integer;
    APos: TJoystickPosition;
begin
  result := true;
  iStart := GetTickCount;
  APos := ShowCursorPosition;
  while APos = jpCenter do begin
    Application.ProcessMessages;
    APos := ShowCursorPosition;
    if GetTickCount - iStart > 10000 then begin
      //let the user have 10 seconds to make some sort of move away from the center
      FTrial[FCurrentTrial].ReactionTime := -1;
      FTrial[FCurrentTrial].ErrorCode := EC_NO_MOVE;
      result := false;
      EXIT;
    end;
  end;
  FTrial[FCurrentTrial].ReactionTime := GetTickCount - iStart;
  memo1.Lines.Add('Reaction time: '+ IntToStr(FTrial[FCurrentTrial].ReactionTime) + 'ms');
end;

{
The go signal is a green circle with the black cross cursor in the monkey version
No go signal is used in the human version so that the reaction time from when
the targets are shown can be measured.
Leave the code here as a reminder of how this task differs from the monkey
task
}
procedure TfrmTrials.ShowGoSignal;
begin
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := true;
  imgBlackCircle.Visible := false;
  imgBlueCircle.Visible := false;
  memo1.Lines.Add('Apparition du signal "Go"');
  Application.ProcessMessages;
end;

{
After the go signal have to move to one of the targets in a given amount of time
Show the black cross cursor moving
The APos variable is passed back to enable checking during the wait on target
period
Allow 2.5s for the movement
}
function TfrmTrials.GoToTarget(var APos: TJoystickPosition): boolean;
var i,
    iStart,
    iHowLong,
    iNow: integer;
    sPos: string;
    AShape: TShape;
    bGood: boolean;
begin
  result := false;
  iStart:= GetTickCount;
  iHowLong := 2000 + random(500);
  bGood := false;
  repeat
    iNow := GetTickCount;
    APos := ShowCursorPosition;
    case APos of
      jpUnknown: sPos := 'Unknown';
      jpCenter: sPos := 'Center';
      jpLeft: sPos := 'Left';
      jpTop: sPos := 'Top';
      jpRight: sPos := 'Right';
      jpBottom: sPos := 'Bottom';
    end;
    sb1.Panels[2].Text := 'Position: ' + sPos;
    if not (APos in [jpUnknown, jpCenter]) then begin
      //must be on one of the target positions
      for i := 1 to 4 do begin
        if arTarget[i].Show then begin
          //check whether it is one of the displayed targets
          if APos = arTarget[i].JoystickPosition then begin
            //one of the displayed targets has been picked
            result := true;
            arTarget[i].Picked := true;
            FTrial[FCurrentTrial].TargetPicked := i;
            CheckIfOptimumChosen(i);
            AShape := TShape(FindComponent('Shape'+IntToStr(arTarget[i].Position)));
            AShape.Visible := true;
            memo1.Lines.Add('Rejoint la cible  ' + sPos);
            bGood := true;
            //so get out of this procedure
            BREAK;
          end;
        end;
      end;
    end;
    if bGood then begin
      BREAK;
    end
    else begin
      //set all targets back to unpicked
      //this allows for inaccurate joystick movement which goes over 2 targets
      for i := 1 to 4 do
        arTarget[i].Picked := false;
    end;
  until iNow >= (iStart + iHowLong);
  if not bGood then
    FTrial[FCurrentTrial].ErrorCode := EC_NO_TARGET;
end;

{
After the cursor has been moved to a target, check if it is the optimum target
The optimum target is the target with the highest probability of reward
}
procedure TfrmTrials.CheckIfOptimumChosen(TargetChosen: integer);
var bOptimum: boolean;
begin
  if FTrial[FCurrentTrial].Target[0].RewardProbability > FTrial[FCurrentTrial].Target[1].RewardProbability then begin
    bOptimum := FTrial[FCurrentTrial].Target1 = TargetChosen;
  end
  else begin
    bOptimum := FTrial[FCurrentTrial].Target2 = TargetChosen;
  end;
  FTrial[FCurrentTrial].Optimum := bOptimum;
  if bOptimum then
    FResults.Optimal := FResults.Optimal + 1;
end;

{
After the circle turns green, the cursor is moved to one of the targets. It then
has to remain on that target for between 0.5 and 1s before the central
circle turns red to signal to move the cursor back to the center
}
function TfrmTrials.WaitOnTarget(WhatPosition: TJoystickPosition): boolean;
var iStart,
    iHowLong,
    i: integer;
    AShape: TShape;
    bGood: boolean;
begin
  result := false;
  iStart:= GetTickCount;
  iHowLong := 1000 + random(500);
  bGood := true;
  memo1.Lines.Add('Attend sur la cible');
  Application.ProcessMessages;
  while GetTickCount < (iStart + iHowLong) do begin
    if ShowCursorPosition <> WhatPosition then begin
      //have moved from the position
      memo1.Lines.Add('N''a pas rester sur la cible');
      FTrial[FCurrentTrial].ErrorCode := EC_NO_WAIT;
      //set all targets as unpicked so no reward will be given at the end
      for i := 1 to 4 do
        arTarget[i].Picked := false;
      bGood := false;
      BREAK;
    end;
    Application.ProcessMessages;
  end;
  for i := 1 to 4 do begin
    AShape := TShape(FindComponent('Shape'+IntToStr(i)));
    AShape.Visible := false;
  end;
  if bGood then begin
    result := true;
    memo1.Lines.Add('Reste sur la cible');
  end;
end;

{
After the cursor has been held on the target long enough, the central circle is
changed from green to red to signal the start of the go home phase
}
procedure TfrmTrials.ShowRedCircle;
begin
  imgRedCircle.Visible := true;
  imgGreenCircle.Visible := false;
  imgBlackCircle.Visible := false;
  imgBlueCircle.Visible := false;
  memo1.Lines.Add('Retour au centre');
  Application.ProcessMessages;
end;

{
After the circle turns red, the cursor has to be moved back to the central circle
within a set time
}
function TfrmTrials.GoHome: boolean;
var iStart,
//    iHowLong,
    i: integer;
//    bGood: boolean;
begin
  result := false;
  iStart:= GetTickCount;
//  iHowLong := 5000 + random(500);
//  bGood := false;
  while ShowCursorPosition <> jpCenter do begin
    Application.ProcessMessages;
    if GetTickCount - iStart > 5000 then begin
      FTrial[FCurrentTrial].ErrorCode := EC_NO_RETURN;
      for i := 1 to 4 do
        arTarget[i].Picked := false;
      EXIT;
    end;
  end;
  result := true;
  memo1.Lines.Add('Rejoint le centre ');
{
  while GetTickCount < (iStart + iHowLong) do begin
    if ShowCursorPosition = jpCenter then begin
      //have moved back to the center, so exit the procedure
      result := true;
      memo1.Lines.Add('Rejoint le centre ');
      bGood := true;
      BREAK;
    end;
  end;
  if not bGood then begin
    //set all the targets as unpicked so as not to give a reward
    for i := 1 to 4 do
      arTarget[i].Picked := false;
  end;
}
end;

{
When the cursor has been moved back to the center, it has to be held there for
0.8 to 1.2s
}
function TfrmTrials.WaitOnCenter: boolean;
var iStart,
    iHowLong,
    i: integer;
    bGood: boolean;
begin
  result := true;
  iStart:= GetTickCount;
  iHowLong := 800 + random(400);
  bGood := true;
  memo1.Lines.Add('Attend au centre');
  Application.ProcessMessages;
  while GetTickCount < (iStart + iHowLong) do begin
    if ShowCursorPosition <> jpCenter then begin
      //have moved from the center, so failed to hold long enough
      memo1.Lines.Add('Quitte le centre');
      bGood := false;
      result := false;
      BREAK;
    end;
    Application.ProcessMessages;
  end;
  if not bGood then begin
    //set all the targets as unpicked so as not to give a reward
    for i := 1 to 4 do
      arTarget[i].Picked := false;
    FTrial[FCurrentTrial].ErrorCode := EC_NO_WAIT_CENTER;
  end;
end;

{
After each trial need to hide the targets and then wait 0.8 to 1.2 seconds
}
procedure TfrmTrials.HideTargets;
var i,
    iHowLong: integer;
    AnImage: TImage;
begin
  for i := 1 to 4 do begin
    if arTarget[i].Show then begin
      AnImage := arTarget[i].Image;
      AnImage.Visible := false;
    end;
  end;
  Application.ProcessMessages;
  memo1.Lines.Add('Disparition des cibles');
  //don't wait after hiding the targets, just show the reward/fail message immediately
//  iHowLong := 800 + random(400);
//  Wait(iHowLong);
end;

{
Procedure to wait for a set number of milliseconds
}
procedure TfrmTrials.Wait(HowLong: integer);
var iStart,
    iNow: integer;
begin
  iStart:= GetTickCount;
  repeat
    ShowCursorPosition;
    iNow := GetTickCount;
    Application.ProcessMessages;
    if bStop then
      //if the stop button is pressed, get out
      EXIT;
  until iNow >= (iStart + HowLong);
end;

{
The reward probabilities are random for each session
}
procedure TfrmTrials.SetTargetRewardProbabilities;
var i, j: integer;
    iP: integer;
    bNewProbability: boolean;
begin
  for i := 1 to 4 do begin
    repeat
      bNewProbability := true;
      iP := RandomRange(0,4);
      for j := i - 1 downto 1 do begin
        if arTarget[j].RewardProbability = FRewardProbabilities[iP] then begin
          bNewProbability := false;
          break;
        end;
      end;
    until bNewProbability;
    arTarget[i].RewardProbability := FRewardProbabilities[iP];
  end;
end;

function TfrmTrials.GetRewardProbability(index: integer): real;
begin
  result := FRewardProbabilities[index];
end;

procedure TfrmTrials.SetRewardProbability(index: integer; const Value: real);
begin
  FRewardProbabilities[index] := Value;
end;

procedure TfrmTrials.btnStopClick(Sender: TObject);
begin
  bStop := true;
  memo1.Lines.Clear;
end;

{
Give a reward with the probability of the chosen target
}
procedure TfrmTrials.CheckReward;
var i: integer;
    bReward,
    bGood: boolean;
begin
  //if the trial was not completed successfully do not bother to show any
  //reward/punishment message as the error message will have been shown
  if FTrial[FCurrentTrial].ErrorCode = EC_GOOD then begin
    bGood := false;
    for i := 1 to 4 do begin
      if arTarget[i].Show then begin
        if arTarget[i].Picked then begin
          bReward := arTarget[i].RewardProbability >= Random;
          FTrial[FCurrentTrial].Rewarded := bReward;
          if bReward then begin
            //show a flashing red label as a reward. Wow!
            FResults.Reward := FResults.Reward + 1;
            lblReward.Visible := false;
            Timer1.Enabled := true;
            Wait(2000 + random(500));
            Timer1.Enabled := false;
            lblReward.Visible := false;
            bGood := true;
            memo1.Lines.Add('R�compense obtenue');
          end;
        end;
      end;
    end;
    if not bGood then begin
      //show a flashing blue label as a punishment!
      lblFail.Visible := false;
      Timer2.Enabled := true;
      Wait(2000 + random(500));
      Timer2.Enabled := false;
      lblFail.Visible := false;
      memo1.Lines.Add('Pas de r�compense');
    end;
  end;
end;

{
Let the user know what they did wrong by flashing a message on the screen
}
procedure TfrmTrials.ShowError;
begin
  if FTrial[FCurrentTrial].ErrorCode > EC_GOOD then begin
    //Don't show a message if everything went OK
    lblError.Caption := ErrorMessages[FTrial[FCurrentTrial].ErrorCode];
    lblError.Left :=  imgBlackCircle.Left + (imgBlackCircle.Width div 2)
                   - (lblError.Width  div 2);
    Timer3.Enabled := true;
    Wait(3000);
    Timer3.Enabled := false;
    lblError.Visible := false;
  end;
end;

procedure TfrmTrials.Timer1Timer(Sender: TObject);
begin
  lblReward.Visible := not lblReward.Visible;
end;

procedure TfrmTrials.Timer2Timer(Sender: TObject);
begin
  lblFail.Visible := not lblFail.Visible;
end;

procedure TfrmTrials.btnPractiseClick(Sender: TObject);
const TRIAL_COUNT = 5;
var APos: TJoystickPosition;
    i: integer;
    AShape: TShape;
begin
  MessageDlg('You will now have the chance to practise choosing targets' + #10+#13
           + 'The chance of being rewarded is not the same as in the real task!',
           mtInformation, [mbOK],0);
  imgCursor.Visible := true;
  SetJoystickField;
  bStop := false;
  SetLength(FTrial, TRIAL_COUNT);
  FResults.TrialCount := TRIAL_COUNT;
  FResults.Success := 0;
  FResults.Reward := 0;
  FResults.Optimal := 0;
  SetTargetRewardProbabilities;
  HideTargets;
  imgBlackCircle.Visible := true;
  for i := 0 to pred(TRIAL_COUNT) do begin
    FCurrentTrial := i;
    if bStop then
      EXIT;
    if GetCursorCentered then begin
      InitializeTrial;
      if StartTrial then begin
        memo1.Lines.Add('Essais num�ro : ' + IntToStr(i+1));
        PickTargets;
        ShowTrialStart;
        ShowBlackCircle;
        ShowTargets;
        lblPractise.Visible := true;
        lblPractise.Caption := 'Move joystick cursor to one of the targets';
        PositionLabel(lblPractise);
        if GetReactionTime then begin
      //    ShowGoSignal;
          if GoToTarget(APos) then begin
            //cursor has moved from center and reached one of the displayed targets
            lblPractise.Caption := 'Keep the cursor on the target';
            PositionLabel(lblPractise);
            if WaitOnTarget(APos) then begin
              //waited long enough on the target, move back to the center
              ShowRedCircle;
              lblPractise.Caption := 'Move joystick cursor back to the center';
              PositionLabel(lblPractise);
              if GoHome then begin
                lblPractise.Visible := false;
                //got back to the center
                ShowBlueCircle;
//                if WaitOnCenter then begin
                  //trial successfully completed
                  FResults.Success := FResults.Success + 1;
                  FTrial[FCurrentTrial].Successful := true;
//                end;
              end;
            end;
          end;
        end;
      end;
    end;
    lblPractise.Visible := false;
    HideTargets;
    ShowError;
    CheckReward;
  end;
  memo1.Clear;
  memo1.Lines.Add('Fin de la s�rie d''essais ');
  memo1.Lines.Add('Nombre d''essais: ' + IntToStr(FResults.TrialCount));
  memo1.Lines.Add('Nombre d''essais men�s � bien: ' + IntToStr(FResults.Success));
  memo1.Lines.Add('Nombre de r�compenses re�ues: ' + IntToStr(FResults.Reward));
  MessageDlg('Practise finished' + #13#10
             + 'You scored ' + IntToStr(FResults.Reward), mtInformation, [mbOK], 0);
  bStop := true;

{  imgCursor.Visible := true;
  imgBlueCircle.Visible := false;
  for i := 1 to 4 do
    TImage(FindComponent('img' + IntToStr(i))).Visible := true;
  SetJoystickField;
  bStop := false;
  memo1.Clear;
  memo1.Lines.Add('Entra�nement');
  while not bStop do begin
    APos := ShowCursorPosition;
    case APos of
      jpUnknown: begin
        for i := 1 to 4 do begin
          AShape := TShape(FindComponent('Shape' + IntToStr(i)));
          AShape.Visible := false;
        end;
        imgRedCircle.Visible := false;
      end;
      jpCenter: begin
        imgRedCircle.Visible := true;
      end
      else begin
        i := integer(APos);
        AShape := TShape(FindComponent('Shape' + IntToStr(i)));
        AShape.Visible := true;
      end;
    end;
    Application.ProcessMessages;
  end;
}
end;

{
Want the joystick to be able to move to just outside the targets (left, right,
top and bottom)
}
procedure TfrmTrials.SetJoystickField;
var iWidth,
    iHeight: integer;
    APoint: TPoint;
begin
  FCursorOffset.X := Shape3.Left - 50;
  FCursorOffset.Y := Shape2.Top - 50;
  iWidth := (Shape1.Left + Shape1.Width + 50) - (Shape3.Left - 50);
  iHeight := (Shape4.Top + Shape4.Height + 50) - (Shape2.Top - 50);
  AJoystick.FieldWidth := iWidth;
  AJoystick.FieldHeight := iHeight;
end;

procedure TfrmTrials.btnSend2ExcelClick(Sender: TObject);
begin
  if CreateExcelWorkbook then begin
    SendResultsToExcel;
    SendSetupToExcel;
  end;
end;

function TfrmTrials.CreateExcelWorkbook: boolean;
begin
  result := true;
  // By using GetActiveOleObject, you use an instance of Excel that's already running,
  // if there is one.
  try
    FXLApp := GetActiveOleObject('Excel.Application');
  except
    try
      // If no instance of Excel is running, try to Create a new Excel Object
      FXLApp := CreateOleObject('Excel.Application');
    except
      ShowMessage('Impossible de d�marrer Exel / le logiciel n''est peut �tre pas install�?');
      result := false;
      Exit;
    end;
  end;
  FXLApp.Workbooks.Add(xlWBatWorkSheet);
  FXLApp.Visible := true;
  FXLWorkbook := FXLApp.Activeworkbook;
  self.SetFocus;
  Application.ProcessMessages;
end;

procedure TfrmTrials.SendResultsToExcel;
var i: integer;
begin
  FXLWorkbook.Activesheet.Name := 'Results';
  FXLWorksheet := FXLWorkbook.Worksheets['Results'];
  FXLWorksheet.Cells[ROW_HEADER, 1].Value := 'Trial Number';
  FXLWorksheet.Cells[ROW_HEADER, 2].Value := 'T1';
  FXLWorksheet.Cells[ROW_HEADER, 3].Value := 'T2';
  FXLWorksheet.Cells[ROW_HEADER, 4].Value := 'P1';
  FXLWorksheet.Cells[ROW_HEADER, 5].Value := 'P2';
  FXLWorksheet.Cells[ROW_HEADER, 6].Value := 'P(R)1';
  FXLWorksheet.Cells[ROW_HEADER, 7].Value := 'P(R)2';
  FXLWorksheet.Cells[ROW_HEADER, 8].Value := 'Picked';
  FXLWorksheet.Cells[ROW_HEADER, 9].Value := 'Optimum';
  FXLWorksheet.Cells[ROW_HEADER, 10].Value := 'Rewarded';
  FXLWorksheet.Cells[ROW_HEADER, 11].Value := 'ITI';
  FXLWorksheet.Cells[ROW_HEADER, 12].Value := 'Reaction time';
  FXLWorksheet.Cells[ROW_HEADER, 13].Value := 'Error code';
  for i := 0 to FTrialCount - 1 do begin
    FXLWorksheet.Cells[i+2, 1].Value := i+1;
    FXLWorksheet.Cells[i+2, 2].Value := FTrial[i].Target1;
    FXLWorksheet.Cells[i+2, 3].Value := FTrial[i].Target2;
    FXLWorksheet.Cells[i+2, 4].Value := FTrial[i].Target[0].Position;
    FXLWorksheet.Cells[i+2, 5].Value := FTrial[i].Target[1].Position;
    FXLWorksheet.Cells[i+2, 6].Value := FTrial[i].Target[0].RewardProbability;
    FXLWorksheet.Cells[i+2, 7].Value := FTrial[i].Target[1].RewardProbability;
    FXLWorksheet.Cells[i+2, 8].Value := FTrial[i].TargetPicked;
    FXLWorksheet.Cells[i+2, 9].Value := NO_YES[integer(FTrial[i].Optimum)];
    FXLWorksheet.Cells[i+2, 10].Value := NO_YES[integer(FTrial[i].Rewarded)];
    FXLWorksheet.Cells[i+2, 11].Value := FTrial[i].ITI;
    FXLWorksheet.Cells[i+2, 12].Value := FTrial[i].ReactionTime;
    FXLWorksheet.Cells[i+2, 13].Value := FTrial[i].ErrorCode;
  end;
end;

procedure TfrmTrials.SendSetupToExcel;
var bSent: boolean;
begin
  FXLWorkbook.Worksheets.Add;
  FExcelWorksheet := 'Setup';
  FXLWorkbook.Activesheet.Name := FExcelWorksheet;
  FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];
  bSent := false;
  while not bSent do begin
    try
      FXLWorksheet.Cells[1,1].Value := 'Targets';
      FXLWorksheet.Cells[2,1].Value := '1 (cross)';
      FXLWorksheet.Cells[3,1].Value := '2 (circle)';
      FXLWorksheet.Cells[4,1].Value := '3 (triangle)';
      FXLWorksheet.Cells[5,1].Value := '4 (rhombus)';
      FXLWorksheet.Cells[1,2].Value := 'P(R)';
      FXLWorksheet.Cells[2,2].Value := arTarget[1].RewardProbability;
      FXLWorksheet.Cells[3,2].Value := arTarget[2].RewardProbability;
      FXLWorksheet.Cells[4,2].Value := arTarget[3].RewardProbability;
      FXLWorksheet.Cells[5,2].Value := arTarget[4].RewardProbability;
      FXLWorksheet.Cells[7,1].Value := 'Outcomes';
      FXLWorksheet.Cells[8,1].Value := 'Number of trials';
      FXLWorksheet.Cells[9,1].Value := 'Succeeded';
      FXLWorksheet.Cells[10,1].Value := 'Rewarded';
      FXLWorksheet.Cells[11,1].Value := 'Optimal';
      FXLWorksheet.Cells[8,2].Value := FResults.TrialCount;
      FXLWorksheet.Cells[9,2].Value := FResults.Success;
      FXLWorksheet.Cells[10,2].Value := FResults.Reward;
      FXLWorksheet.Cells[11,2].Value := FResults.Optimal;
      FXLWorksheet.Columns[1].ColumnWidth := 20;

      FXLWorksheet.Cells[1,4].Value := 'Subject ID';
      FXLWorksheet.Cells[1,5].Value := FSubject;
      FXLWorksheet.Cells[2,4].Value := 'Date of test';
      FXLWorksheet.Cells[2,5].Value := DateToStr(Now);
      bSent := true;
    except
      on E: Exception do begin
        ShowMessage(E.Message);
      end;
    end;
  end;
end;

procedure TfrmTrials.btnExitClick(Sender: TObject);
begin
  bStop := true;
  Close;
end;

procedure TfrmTrials.Timer3Timer(Sender: TObject);
begin
  lblError.Visible := not lblError.Visible;
end;

procedure TfrmTrials.ToolButton4Click(Sender: TObject);
begin
{
  bStop := false;
  repeat
    if AJoystick.ButtonPressed(ButtonToUse) then begin
      lblButton.Caption := 'Button ' + IntToStr(ButtonToUse) + ' pressed';
    end
    else
      lblButton.Caption := 'Button not pressed';
    Application.ProcessMessages;
  until bStop;
}  
end;

{
Look up the high score in an ini file to tell the user the score that they have
to beat
}
function TfrmTrials.GetHighScore: integer;
var FIni: TIniFile;
begin
  FIni := TIniFile.Create(ExtractFilePath(Application.ExeName)+'Scores.ini');
  with FIni do try
    result := ReadInteger('High scores','Score',0);
  finally
    Free;
    FIni := nil;
  end;
end;

procedure TfrmTrials.SaveHighScore(SubjectId: string; Score: integer);
var FIni: TIniFile;
begin
  FIni := TIniFile.Create(ExtractFilePath(Application.ExeName)+'Scores.ini');
  with FIni do try
    WriteString('High scores', 'Subject ID', SubjectId);
    WriteInteger('High scores','Score', Score);
  finally
    Free;
    FIni := nil;
  end;
end;

procedure TfrmTrials.PositionLabel(ALabel: TLabel);
begin
  ALabel.Left := FCenter.X + (imgBlackCircle.Width div 2)- (ALabel.Width div 2);
  ALabel.Top := FCenter.Y - 50;
end;

end.

