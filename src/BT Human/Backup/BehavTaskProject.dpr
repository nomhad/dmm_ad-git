program BehavTaskProject;

uses
  Forms,
  Behavioraltask in 'Behavioraltask.pas' {frmTask},
  Trials in 'Trials.pas' {frmTrials},
  Joy in 'Joy.pas' {frmJoystick},
  uJoystick in 'uJoystick.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmTask, frmTask);
  Application.Run;
end.
