unit uJoystick;

{
This unit uses MMSystem which contains constants and calls for getting joystick
positions. The main function used is joyGetPos which returns the results into the
TJoyInfo structure which is defined as follows:

//  PJoyInfo = ^TJoyInfo;
//  {$EXTERNALSYM joyinfo_tag}
//  joyinfo_tag = record
//    wXpos: UINT;                 { x position }
//    wYpos: UINT;                 { y position }
//    wZpos: UINT;                 { z position }
//    wButtons: UINT;              { button states }
//  end;
//  TJoyInfo = joyinfo_tag;
//  {$EXTERNALSYM JOYINFO}
//  JOYINFO = joyinfo_tag;

interface

uses Classes, Types, MMSystem, Math;

type
  TJoystickPosition = (jpCenter, jpRight, jpTop, jpLeft, jpBottom, jpUnknown);

  TJoystick = class(TObject)
  private
    JoyInfo: TJoyInfo;  // VARIABLES POUR LE JOYSTICK
    JoyCaps: TJoyCapsW;
    FOldJoystickPosition: TJoystickPosition;

    FXMin,
    FYMin,
    FXMax,
    FYMax: integer;
    FWidth,
    FHeight: integer;
    FScreenPos,
    FCenter,
    FTop,
    FBottom,
    FRight,
    FLeft,
    FFieldCenter: TPoint;
    FRadius: integer;
    FCenterOffset: TPoint;
    FTargetLeftPosition: TPoint;
    FTargetRightPosition: TPoint;
    FTargetBottomPosition: TPoint;
    FTargetTopPosition: TPoint;
    FTargetOffset: integer;
    function GetCurrentPosition: TJoystickPosition;
    function GetCurrentPoint: TPoint;
    function GetAdjustedCurrentPoint: TPoint;
    procedure SetCenter(const Value: TPoint);
    procedure SetTargetOffset(const Value: integer);
  public
    constructor Create;
    destructor Destroy;

    property Center: TPoint read FCenter write SetCenter;
    property Top: TPoint read FTop write FTop;
    property Left: TPoint read FLeft write FLeft;
    property Right: TPoint read FRight write FRight;
    property Bottom: TPoint read FBottom write FBottom;

    property YMax: integer read FYMax write FYMax;
    property YMin: integer read FYMin write FYMin;
    property XMax: integer read FXMax write FXMax;
    property XMin: integer read FXMin write FXMin;

    {
    the width and the height are set for the current are of screen that the
    joystick has to navigate
    When the x coordinate is XMax, CurrentPoint will return the width
    }
    property FieldWidth: integer read FWidth write FWidth;
    property FieldHeight: integer read FHeight write FHeight;
    property FieldCenter: TPoint read FFieldCenter write FFieldCenter;

    procedure SetPosition(WhichPosition: TJoystickPosition);

    //how accurate to be to decide the joystick is in a certain position
    property Accuracy: integer read FRadius write FRadius;
    //current point is the point on the screen the joystick would show the cursor at
    property CurrentPoint: TPoint read GetCurrentPoint;
    //the adjusted current point takes account of the visual settings so that
    //the cursor can be positioned over a specific point at the limits of
    //joystick travel
    property AdjustedCurrentPoint: TPoint read GetAdjustedCurrentPoint;
    //current position is center, top, bottom, left or right
    property CurrentPosition: TJoystickPosition read GetCurrentPosition;
    function CurrentPositionToString(APos: TJoystickPosition): string;
    property PreviousPosition: TJoystickPosition read FOldJoystickPosition write FOldJoystickPosition;
    property CenterOffset: TPoint read FCenterOffset write FCenterOffset;

    property TargetRightPosition: TPoint read FTargetRightPosition write FTargetRightPosition;
    property TargetTopPosition: TPoint read FTargetTopPosition write FTargetTopPosition;
    property TargetLeftPosition: TPoint read FTargetLeftPosition write FTargetLeftPosition;
    property TargetBottomPosition: TPoint read FTargetBottomPosition write FTargetBottomPosition;
    property TargetOffset: integer read FTargetOffset write SetTargetOffset;

    function ButtonPressed(WhichButton: integer): boolean;
  end;

var AJoystick: TJoystick;

implementation

const
  //these are the coordinates when the joystick is sitting in the middle of its field
  PositionString: array[0..5] of string = ('Center','Right','Top','Left','Bottom','Unknown');

constructor TJoystick.Create;
begin
  inherited;
  //the default values mean that you do not have to calibrate the joystick
  FRadius := 6000;

  FCenter.X := 32768;
  FCenter.Y := 32768;
  FTop.X := FCenter.X;
  FTop.Y := 0;
  FBottom.X := FCenter.X;
  FBottom.Y := 65535;
  FLeft.X := 0;
  FLeft.Y := FCenter.Y;
  FRight.X := 65535;
  FRight.Y := FCenter.Y;

  FWidth := 500;
  FHeight := 500;
  FFieldCenter.X := 250;
  FFieldCenter.Y  := 250;

  FXMin := 0;
  FXMax := 65535;
  FYMin := 0;
  FYMax := 65535;

  FCenterOffset.X := 0;
  FCenterOffset.Y := 0;

  TargetOffset := 10000;
end;

destructor TJoystick.Destroy;
begin

  inherited;
end;

{
Get the coordinates for the joystick current position
Adjust these for the screen resolution
}
function TJoystick.GetCurrentPoint: TPoint;
begin
  joyGetPos(joystickid1,@JoyInfo);
  FScreenPos.X := JoyInfo.wxpos - FCenterOffset.X;
  FScreenPos.Y := JoyInfo.wypos - FCenterOffset.Y;
  result.X := FScreenPos.X;
  result.Y := FScreenPos.Y;
end;

{
This function allows the coordinates to be adjusted for displacements that are
not equal both sides of the center
}
function TJoystick.GetAdjustedCurrentPoint: TPoint;
var eXScale,
    eYScale: real;
    eScalingFactor: real;
    eStep: real;
begin
  GetCurrentPoint;
  eStep := (FXMax - FXMin) / FWidth;
  result.X := FScreenPos.X - FXMin;
  result.X := round(result.X / eStep);

  eStep := (FYMax - FYMin) / FHeight;
  result.Y := FScreenPos.Y - FYMin;
  result.Y := round(result.Y / eStep);
end;

{
If the joystick is within FRadius of the position set, then it is assumed that
the robot moved in that direction
}
function TJoystick.GetCurrentPosition: TJoystickPosition;
var APos: TPoint;
begin
  result := jpUnknown;

  joyGetPos(joystickid1,@JoyInfo);
  APos.X := JoyInfo.wxpos - FCenterOffset.X;
  APos.Y := JoyInfo.wypos - FCenterOffset.Y;

  if (abs(APos.Y - FCenter.Y) < FRadius)
  and (abs(APos.X - FCenter.X) < FRadius) then
    //in the center
    result := jpCenter
  else if (abs(APos.X - FTargetRightPosition.X) < FRadius)
  and (abs(APos.Y - FTargetRightPosition.Y) < (FRadius)) then
      //at the right
    result := jpRight
  else if (abs(APos.X - FTargetLeftPosition.X) < FRadius)
  and (abs(APos.Y - FTargetLeftPosition.Y) < (FRadius)) then
    //at the left
    result := jpLeft
  else if (abs(APos.X - FTargetTopPosition.X) < FRadius)
  and (abs(APos.Y - FTargetTopPosition.Y) < (FRadius)) then
    //at the top
    result := jpTop
  else if (abs(APos.X - FTargetBottomPosition.X) < FRadius)
  and (abs(APos.Y - FTargetBottomPosition.Y) < (FRadius)) then
    //at the bottom
    result := jpBottom;
end;

procedure TJoystick.SetPosition(WhichPosition: TJoystickPosition);
var APosition: TPoint;
begin
  APosition := GetCurrentPoint;
  case WhichPosition of
    jpCenter: FCenter := APosition;
    jpTop: begin
      FTop.X := FCenter.X;
      FTop.Y := APosition.Y;
    end;
    jpBottom: begin
      FBottom.X := FCenter.X;
      FBottom.Y := APosition.Y;
    end;
    jpLeft: begin
      FLeft.X := APosition.X;
      FLeft.Y := FCenter.Y;
    end;
    jpRight: begin
      FRight.X := APosition.X;
      FRight.Y := FCenter.Y;
    end;
  end;
end;

procedure TJoystick.SetCenter(const Value: TPoint);
begin
  FCenter := Value;
end;

function TJoystick.CurrentPositionToString(APos: TJoystickPosition): string;
begin
  result := PositionString[integer(APos)];
end;

{
This function only tests the first button on the joystick
}
function TJoystick.ButtonPressed(WhichButton: integer): boolean;
var iHex: integer;
begin
  iHex := round(Power(2, WhichButton - 1));
  joyGetPos(joystickid1,@JoyInfo);
  result := (JoyInfo.wButtons and iHex) = iHex;
end;

{
This sets the point closest to the center where the cursor is considered to be
over the target
}
procedure TJoystick.SetTargetOffset(const Value: integer);
begin
  FTargetOffset := Value;
  FTargetRightPosition.X := FXMax - FTargetOffset;
  FTargetRightPosition.Y := FCenter.Y;
  FTargetTopPosition.X := FCenter.X;
  FTargetTopPosition.Y := FTargetOffset;
  FTargetLeftPosition.X := FTargetOffset;
  FTargetLeftPosition.Y := FCenter.Y;
  FTargetBottomPosition.X := FCenter.X;
  FTargetBottomPosition.Y := FYMax - FTargetOffset;
end;

end.
