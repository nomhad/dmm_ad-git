unit Behavioraltask;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, ImgList, Menus, IniFiles, Mask, ToolWin,
  Buttons, uJoystick;

type
  TfrmTask = class(TForm)
    PageControl1: TPageControl;
    tsSession: TTabSheet;
    Label3: TLabel;
    EdNumTrials: TMaskEdit;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    SaveDialog1: TSaveDialog;
    tsSequence: TTabSheet;
    tsTargets: TTabSheet;
    ImageSequence: TImage;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Time1: TMaskEdit;
    Time2: TMaskEdit;
    Time3: TMaskEdit;
    Time4: TMaskEdit;
    Time5: TMaskEdit;
    Time6: TMaskEdit;
    time7: TMaskEdit;
    Time8: TMaskEdit;
    Time9: TMaskEdit;
    Time10: TMaskEdit;
    MvtTime: TMaskEdit;
    ImgTarget0: TImage;
    ImgTarget1: TImage;
    ImgTarget2: TImage;
    ImgTarget3: TImage;
    Label5: TLabel;
    btnLoadTarget0: TButton;
    btnLoadTarget1: TButton;
    btnLoadtarget2: TButton;
    btnLoadtarget3: TButton;
    Edit1: TMaskEdit;
    Edit2: TMaskEdit;
    Edit3: TMaskEdit;
    Edit4: TMaskEdit;
    CoolBar1: TCoolBar;
    sbCalibrateJoystick: TSpeedButton;
    sbPreview: TSpeedButton;
    Label1: TLabel;
    edSubject: TEdit;
    cbShowAids: TCheckBox;
    GroupBox1: TGroupBox;
    cbUseButtonToStartTrials: TCheckBox;
    Label2: TLabel;
    edButtonNumber: TEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label13: TLabel;
    lblSubjectId: TLabel;
    lblScore: TLabel;
    btnResetHighScore: TBitBtn;

    procedure FormCreate(Sender: TObject);
    procedure sbCalibrateJoystickClick(Sender: TObject);
    procedure sbPreviewClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EdNumTrialsChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnResetHighScoreClick(Sender: TObject);
  private
    { D�clarations priv�es }
    FFormatSettings: TFormatSettings;

    FNumTrials: integer;

    procedure ShowHighScore;
  public
    { D�clarations publiques }
    function CheckValues: boolean; // fonction qui v�rifie les valeurs saisies sur l'interface et revoie true si elle est bien remplie
  end;

var
  frmTask: TfrmTask;

implementation

uses Trials,Joy;

{$R *.dfm}

procedure TfrmTask.FormCreate(Sender: TObject); //Initialisation des valeurs
begin
  GetLocaleFormatSettings(0, FFormatSettings);
  FFormatSettings.DecimalSeparator := '.';
  AJoystick := TJoystick.Create;
  FNumTrials := 10;
end;

procedure TfrmTask.FormShow(Sender: TObject);
begin
  ShowHighScore;
end;

procedure TfrmTask.FormDestroy(Sender: TObject);
begin
  AJoystick.Free;
end;

//Validation des s�lections effectu�es
function TfrmTask.CheckValues;
begin
  FFormatSettings.DecimalSeparator := ',';
  result:= false;

  if edSubject.Text = '' then begin
    MessageDlg('You must enter a subject ID before starting the experiment',mtError,[mbOK],0);
    EXIT;
  end;

  result:= true;
end;

procedure TfrmTask.sbCalibrateJoystickClick(Sender: TObject);
begin
  if frmJoystick = nil then
    frmJoystick := TfrmJoystick.Create(nil);
  with frmJoystick do try
    ShowModal;
  finally
    Free;
    frmJoystick := nil;
  end;
end;

procedure TfrmTask.sbPreviewClick(Sender: TObject);
begin
  if CheckValues then begin
    if frmTrials = nil then
      frmTrials := TfrmTrials.Create(nil);
    with frmTrials do try
      TrialCount := FNumTrials;
      Subject := edSubject.Text;
      ShowProgrammerAids := cbShowAids.Checked;
      UseButtonToStartTrials := cbUseButtonToStartTrials.Checked;
      ButtonToUse := StrToInt(Trim(edButtonNumber.Text));
      ShowModal;
    finally
      Free;
      frmTrials := nil;
    end;
  end;
end;

procedure TfrmTask.EdNumTrialsChange(Sender: TObject);
begin
  FNumTrials:=StrToInt(Trim(EdNumTrials.Text));
end;

procedure TfrmTask.ShowHighScore;
var FIni: TIniFile;
begin
  FIni := TIniFile.Create(ExtractFilePath(Application.ExeName)+'Scores.ini');
  with FIni do try
    lblSubjectId.Caption := ReadString('High scores','Subject ID','');
    lblScore.Caption := IntToStr(ReadInteger('High scores','Score',0));
  finally
    Free;
    FIni := nil;
  end;
end;

procedure TfrmTask.btnResetHighScoreClick(Sender: TObject);
var FIni: TIniFile;
begin
  if MessageDlg('Are you sure?', mtConfirmation,[mbYes, mbNo],0) = mrYes then begin
    FIni := TIniFile.Create(ExtractFilePath(Application.ExeName)+'Scores.ini');
    with FIni do try
      WriteString('High scores','Subject ID', ' ');
      WriteInteger('High scores','Score',0);
      lblSubjectId.Caption := '';
      lblScore.Caption := '0';
    finally
      Free;
      FIni := nil;
    end;
  end;
end;

end.


