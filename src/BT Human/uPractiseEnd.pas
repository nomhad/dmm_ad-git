unit uPractiseEnd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TfrmPractiseScore = class(TForm)
    Panel1: TPanel;
    Shape1: TShape;
    lblScore: TLabel;
    BitBtn2: TBitBtn;
    procedure FormShow(Sender: TObject);
  private
    FScore: integer;
    { Private declarations }
  public
    { Public declarations }
    property Score: integer read FScore write FScore;
  end;

var
  frmPractiseScore: TfrmPractiseScore;

implementation

{$R *.dfm}

procedure TfrmPractiseScore.FormShow(Sender: TObject);
begin
  if FScore = 1 then
    lblScore.Caption := 'Vous avez accumul� ' + IntTosTr(FScore) + ' point!'
  else
    lblScore.Caption := 'Vous avez accumul� ' + IntTosTr(FScore) + ' points';
end;

end.
