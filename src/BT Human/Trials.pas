unit Trials;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, Mask, ImgList, Math,
  uJoystick, Buttons, ToolWin, ComObj, IniFiles, MPlayer,
  uQuestionnaire1, uQuestionnaire2, uHighScore, uEnd, uSession2,  uArray,
  uPractiseStart, uPractiseEnd, uHighestScore, AdvSmoothTrackBar;

type
  TTarget = record
    Show: boolean;
    Position: integer;
    JoystickPosition: TJoystickPosition;
    RewardProbability: real;
    ImageNumber: integer;
    Picked: boolean;
    PickedCount,
    RewardedCount: integer;
    EffectiveProbability: real;
  end;

  TResults = record
    TrialCount,
    Success,
    Reward,
    Optimal: integer;
  end;

  TTrial = record
    ReactionTime,
    ITI: integer;
    Successful,
    Rewarded: boolean;
    Target1,
    Target2: integer;
    Target: array[0..1] of TTarget;
    TargetPicked: integer;
    Optimum: boolean;
    ErrorCode: integer;
  end;

  TfrmTrials = class(TForm)
    sb1: TStatusBar;
    Panel1: TPanel;
    img3: TImage;
    img4: TImage;
    img2: TImage;
    img1: TImage;
    imgCursor: TImage;
    Label1: TLabel;
    imgGreenCircle: TImage;
    imgRedCircle: TImage;
    imgBlackCircle: TImage;
    Shape2: TShape;
    Shape4: TShape;
    Shape3: TShape;
    Shape1: TShape;
    ToolBar1: TToolBar;
    btnGo: TToolButton;
    ilButtons: TImageList;
    Memo1: TMemo;
    lblMoveCursor: TLabel;
    btnStop: TToolButton;
    lblReward: TLabel;
    Timer1: TTimer;
    lblFail: TLabel;
    Timer2: TTimer;
    btnPractise: TToolButton;
    btnExit: TToolButton;
    btnSend2Excel: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    Label10: TLabel;
    lblError: TLabel;
    Timer3: TTimer;
    imgBlueCircle: TImage;
    lblStartTrial: TLabel;
    lblPractise: TLabel;
    lblSession: TLabel;
    imgReward: TImage;
    imgFail: TImage;
    mp1: TMediaPlayer;
    btnReset: TToolButton;
    tb1: TAdvSmoothTrackBar;
    ToolButton1: TToolButton;
    procedure FormShow(Sender: TObject);
    procedure btnGoClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure btnPractiseClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnSend2ExcelClick(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure tb1PositionChanged(Sender: TObject;
      Position: Double);
    procedure btnResetClick(Sender: TObject);
  private
    { D�clarations priv�es }
    iOffset: integer;
    eOffsetDivider: real;
    //which 2 of the 4 targets to show
    arToShow: array[0..1] of integer;
    //what positons to show them in. These are set in PickTargets
    arTargetPosition: array[0..1] of integer;
    FResults: array[0..2] of TResults;
    FTrial: array[0..2] of array of TTrial;
    arGuesses: array[0..1,1..4] of real; //what the subject guesses were the reward probabilities for each target
    FCurrentSession,  //session 0 is the practise, sessions 1 & 2 the real test
    FCurrentTrial: integer;
    FRewardProbabilities: array[0..3] of real;
    bStop: boolean;

    FCenter,
    FCursorOffset: TPoint;

    //one set of targets for each session so they can be saved to Excel after the
    //experiment is finished
    arTarget: array[0..2, 1..4] of TTarget;
    FModelIpAddress: string;
    FRobotAttached: boolean;

    FXLApp,
    FXLWorkbook,
    FXLWorksheet: OLEVariant;
    FExcelWorksheet: string;
    FSubject: string;
    FShowProgrammerAids: boolean;
    FUseButtonToStartTrials: boolean;
    FTrialCount: integer;
    FButtonToUse: integer;
    FSessionCount: integer;

    procedure PositionControls;
    procedure PositionLabel(ALabel: TLabel);
    procedure CentreHorizontal(AControl: TControl; Offset: integer);
    procedure CentreVertical(AControl: TControl; Offset: integer);

    procedure InitializeSessions;
    procedure SetSessionTargets;
//    procedure GetPractiseTargets;
    procedure ShowTrialStart;
    procedure ShowRedCircle;
    procedure ShowGoSignal;
    procedure ShowBlackCircle;
    procedure ShowBlueCircle;
    procedure PickTargets;// vient s�lectionner quels images � quels emplacements al�atoires
    procedure ShowTargets;
    function GetReactionTime: boolean;
    procedure HideTargets;
    function GoToTarget(var APos: TJoystickPosition): boolean;
    procedure CheckIfOptimumChosen(TargetChosen: integer);
    function WaitOnTarget(WhatPosition: TJoystickPosition): boolean;
    function GoHome: boolean;
    function WaitOnCenter: boolean;
    procedure CheckReward;
    procedure Wait(HowLong: integer);
    function GetCursorCentered: boolean;
    procedure ShowError;

    function ShowCursorPosition: TJoystickPosition;
    procedure SetImagePosition(TargetNumber: integer; Position: integer);
    procedure SetJoystickField;
    procedure InitializeTrial;
    function StartTrial: boolean;
    procedure SetTargetRewardProbabilities;
    function GetRewardProbability(index: integer): real;
    procedure SetRewardProbability(index: integer; const Value: real);

    function CreateExcelWorkbook: boolean;
    procedure SendSetupToExcel;
    procedure SendResultsToExcel;

    function GetHighScore: integer;
    procedure ShowHighScore(WhatScore: integer);
    procedure SaveHighScore(SubjectId: string; Score: integer);
    procedure ShowScore(WhatScore: integer);

    procedure ShowQuestionnaire1;
    procedure ShowSession2Start;
    procedure ShowQuestionnaire2;

    procedure PlaySound(FileName: string);

  public
    { D�clarations publiques }
    property SessionCount: integer read FSessionCount write FSessionCount default 2;
    property TrialCount: integer read FTrialCount write FTrialCount;
    property ModelIpAddress: string read FModelIpAddress write FModelIpAddress;
    property RobotAttached: boolean read FRobotAttached write FRobotAttached;
    property RewardProbabilities[index: integer]: real read GetRewardProbability write SetRewardProbability;
    property Subject: string read FSubject write FSubject;
    property ShowProgrammerAids: boolean read FShowProgrammerAids write FShowProgrammerAids;
    property UseButtonToStartTrials: boolean read FUseButtonToStartTrials write FUseButtonToStartTrials default true;
    property ButtonToUse: integer read FButtonToUse write FButtonToUse default 1;
end;

var
  frmTrials: TfrmTrials;

implementation

{$R *.dfm}

const
  //constants for the images in image list il1 so that images can be loaded into
  //imgCenter as the trial progresses
  IMG_TARGET = 0;
  IMG_GO_SIGNAL = 1;
  IMG_SUCCESS = 2;
  IMG_FAIL = 3;
  IMG_END = 4;
  IMG_WHITE = 5;

  //constants for the error messages when a trial is not completed successfully
  EC_GOOD = 0;
  EC_NO_CENTER = 1;
  EC_ITI_TOO_LONG = 2;
  EC_NO_MOVE = 3;
  EC_NO_TARGET = 4;
  EC_NO_WAIT = 5;
  EC_NO_RETURN = 6;
  EC_NO_WAIT_CENTER = 7;

  //Error messages to display at the end of a trial if all did not go well
  ErrorMessages: array[1..7] of string = ('Vous n''avez pas rejoint la maison dans le temps impartie',
                                          'Vous attendez trop longtemps avant de commencer cet essai',
                                          'Vous n''avez pas quitt� la maison dans le temps impartie',
                                          'Vous n''avez pas rejoint une cible dans le temps impartie',
                                          'Vous avez quitt� la cible trop t�t',
                                          'Vous n''avez pas rejoint la maison dans le temps impartie',
                                          'Vous avez quitt� le centre trop t�t');

  ROW_HEADER = 1;
  xlWBATWorksheet = -4167;
  NO_YES: array[0..1] of string = ('No','Yes');

  //Sound files
  SOUND_CHEER = 'cheer_8k.wav';
  SOUND_BOO = 'oh_ho_no.wav';

function TfrmTrials.ShowCursorPosition: TJoystickPosition;
var APoint: TPoint;
begin
  Application.ProcessMessages;
  APoint := AJoystick.AdjustedCurrentPoint;
  result := AJoystick.CurrentPosition;
  imgCursor.Left := APoint.X - (imgCursor.Width div 2) + FCursorOffset.X;
  imgCursor.Top := APoint.Y - (imgCursor.Height div 2) + FCursorOffset.Y;
  sb1.Panels[0].Text := 'X Coordinate: ' + IntToStr(APoint.X);
  sb1.Panels[1].Text := 'Y Coordinate: ' + IntToStr(APoint.Y);
  sb1.Panels[2].Text := 'Position: ' + AJoystick.PositionToString(result);
  Application.ProcessMessages;
end;

procedure TfrmTrials.FormShow(Sender: TObject);
var i: integer;
begin
  //The probability for each target in the certainty session is randomly
  //selected from this array.
  //May want to load these dynamically from the ini file so that different
  //probabilities can be used
  FRewardProbabilities[0] := 0;
  FRewardProbabilities[1] := 0.33;
  FRewardProbabilities[2] := 0.66;
  FRewardProbabilities[3] := 1;

  eOffsetDivider := 3;
  PositionControls;

  imgCursor.Visible := false;

  Memo1.Visible := FShowProgrammerAids;

  Randomize;
  SetTargetRewardProbabilities;
end;

procedure TfrmTrials.btnGoClick(Sender: TObject);
var iSession,
    iTrial,
    iTotal,
    iHighScore: integer;
    APos: TJoystickPosition;
begin
  iHighScore := GetHighScore;
//  MessageDlg('The high score to beat is '+ IntToStr(iHighScore), mtInformation, [mbOK],0);
  ShowHighScore(iHighScore);
  SetJoystickField;
  bStop := false;
  HideTargets;
  imgBlackCircle.Visible := true;
  imgCursor.Visible := true;
  iTotal := 0;
  for iSession := 1 to 2 do begin
    if iSession = 2 then begin
      lblSession.Visible := false;
      ShowQuestionnaire1;
      ShowSession2Start;
    end;
    FCurrentSession := iSession;
    lblSession.Caption := 'Session '+ IntToStr(FCurrentSession);
    CentreHorizontal(lblSession, 0);
    CentreVertical(lblSession, 100);
    lblSession.Visible := true;
    SetSessionTargets;
    SetLength(FTrial[FCurrentSession], FTrialCount);
    for iTrial := 0 to pred(FTrialCount) do begin
      FCurrentTrial := iTrial;
      if bStop then
        EXIT;
      if GetCursorCentered then begin
        InitializeTrial;
        if StartTrial then begin
          memo1.Lines.Add('Essais num�ro : ' + IntToStr(FCurrentTrial+1));
          PickTargets;
          ShowTrialStart;
          ShowBlackCircle;
          ShowTargets;
          if GetReactionTime then begin
        //    ShowGoSignal;
            if GoToTarget(APos) then begin
              //cursor has moved from center and reached one of the displayed targets
              if WaitOnTarget(APos) then begin
                //waited long enough on the target, move back to the center
                ShowRedCircle;
                if GoHome then begin
                  //got back to the center
                  ShowBlueCircle;
                  FResults[FCurrentSession].Success := FResults[FCurrentSession].Success + 1;
                  FTrial[FCurrentSession, FCurrentTrial].Successful := true;
                end;
              end;
            end;
          end;
        end;
      end;
      HideTargets;
      ShowError;
      CheckReward;
    end;
    memo1.Clear;
    memo1.Lines.Add('Fin de la s�rie d''essais: Session ' + IntToStr(FCurrentSession));
    memo1.Lines.Add('Nombre d''essais: ' + IntToStr(FResults[FCurrentSession].TrialCount));
    memo1.Lines.Add('Nombre d''essais men�s � bien: ' + IntToStr(FResults[FCurrentSession].Success));
    memo1.Lines.Add('Nombre de r�compenses re�ues: ' + IntToStr(FResults[FCurrentSession].Reward));
    //Keep a running total of the rewards in each session
    iTotal := iTotal + FResults[FCurrentSession].Reward;
  end;
  lblSession.Visible := false;
  ShowQuestionnaire2;
  ShowScore(iTotal);
  if iTotal > iHighScore then begin
    if frmHighestScore = nil then
      frmHighestScore := TfrmHighestScore.Create(nil);
    with frmHighestScore do try
      HighestScore := iTotal;
      ShowModal;
    finally
      Free;
      frmHighestScore := nil;
    end;
//    MessageDlg('You have the highest score!' +  #13#10 + 'Congratulations', mtInformation, [mbOK],0);
    SaveHighScore(FSubject, iTotal);
  end;
  lblSession.Visible := false;
  bStop := true;
end;

function TfrmTrials.GetCursorCentered: boolean;
var APos: TJoystickPosition;
    iStart: integer;
begin
  result := true;
  iStart := GetTickCount;
  lblMoveCursor.Visible := true;
  APos := ShowCursorPosition;
  while APos <> jpCenter do begin
    Application.ProcessMessages;
    APos := ShowCursorPosition;
    if GetTickCount - iStart > 10000 then begin
      //Allow 10 seconds to get back to the center
      FTrial[FCurrentSession, FCurrentTrial].ErrorCode := EC_NO_CENTER;
      result := false;
      EXIT;
    end;
  end;
  lblMoveCursor.Visible := false;
end;

procedure TfrmTrials.InitializeTrial;
begin
  FTrial[FCurrentSession, FCurrentTrial].ITI := -1;
  FTrial[FCurrentSession, FCurrentTrial].Successful := false;
  FTrial[FCurrentSession, FCurrentTrial].Rewarded := false;
  FTrial[FCurrentSession, FCurrentTrial].ReactionTime := -1;
  FTrial[FCurrentSession, FCurrentTrial].TargetPicked := -1;
  FTrial[FCurrentSession, FCurrentTrial].ErrorCode := EC_GOOD;
  memo1.Clear;
end;

{
If FUseButtonToStartTrials is set, then wait for the user to press the joystick
button before starting the trial.
Use this delay to work out the intertrial interval
}
function TfrmTrials.StartTrial: boolean;
var iStart: integer;
begin
  result := true;
  if FUseButtonToStartTrials then begin
    lblStartTrial.Visible := true;
    iStart := GetTickCount;
    while not AJoystick.ButtonPressed(ButtonToUse) do begin
      Application.ProcessMessages;
      ShowCursorPosition;
      if GetTickCount - iStart > 10000 then begin
        //Let the subject have a maximum of 10 seconds to think between trials
        FTrial[FCurrentSession, FCurrentTrial].ErrorCode := EC_ITI_TOO_LONG;
        lblStartTrial.Visible := false;
        result := false;
        EXIT;
      end;
    end;
    FTrial[FCurrentSession, FCurrentTrial].ITI := GetTickCount - iStart;
    lblStartTrial.Visible := false;
    memo1.Lines.Add('Intertrial interval: ' + IntToStr(FTrial[FCurrentSession, FCurrentTrial].ITI) + 'ms');
  end;
end;

{
Display the targets in random positions for the current trial
}
procedure TfrmTrials.PickTargets;
var i, j,
    iTarget1,
    iTarget2: integer;
    Selected: array[1..4]of boolean;
    AnImage: TImage;
begin
  for j := 0 to 2 do begin
    for i := 1 to 4 do begin
      AnImage := TImage(FindComponent('img' + IntToStr(i)));
      //hide all four images. show them only at the correct time during the trial
      AnImage.Visible := false;
      arTarget[j, i].Show := false;
      arTarget[j, i].Picked := false;
      arTarget[j, i].Position := -1;
    end;
  end;

  //get which two targets to show for this trial
  iTarget1 := RandomRange(1,5);
  arTarget[FCurrentSession, iTarget1].Show := true;
  repeat
    iTarget2 := RandomRange(1,5);
  until iTarget1 <> iTarget2;
  arTarget[FCurrentSession, iTarget2].Show := true;

  //then work out which positions to show them in
  arTarget[FCurrentSession, iTarget1].Position := RandomRange(1,5);
  //joystick positions start with jpCenter, so the joystick position is 1 more than the actual position
  arTarget[FCurrentSession, iTarget1].JoystickPosition := TJoystickPosition(arTarget[FCurrentSession, iTarget1].Position);
  repeat
    arTarget[FCurrentSession, iTarget2].Position := RandomRange(1,5);
  until arTarget[FCurrentSession, iTarget1].Position <> arTarget[FCurrentSession, iTarget2].Position;
  arTarget[FCurrentSession, iTarget2].JoystickPosition := TJoystickPosition(arTarget[FCurrentSession, iTarget2].Position);

  SetImagePosition(iTarget1, arTarget[FCurrentSession, iTarget1].Position);
  SetImagePosition(iTarget2, arTarget[FCurrentSession, iTarget2].Position);

  FTrial[FCurrentSession, FCurrentTrial].Target1 := iTarget1;
  FTrial[FCurrentSession, FCurrentTrial].Target2 := iTarget2;
  FTrial[FCurrentSession, FCurrentTrial].Target[0] := arTarget[FCurrentSession, iTarget1];
  FTrial[FCurrentSession, FCurrentTrial].Target[1] := arTarget[FCurrentSession, iTarget2];

  memo1.Lines.Add('Premi�re cible: ' + IntToStr(iTarget1));
  memo1.Lines.Add('Position de la premi�re cible: ' + IntToStr(arTarget[FCurrentSession, iTarget1].Position));
  memo1.Lines.Add('P(R) premi�re cible: ' + FloatToStrF(arTarget[FCurrentSession, iTarget1].RewardProbability, ffFixed, 3, 2));
  memo1.Lines.Add('Deuxi�me cible: ' + IntToStr(iTarget2));
  memo1.Lines.Add('Position de la deuxi�me cible: ' + IntToStr(arTarget[FCurrentSession, iTarget2].Position));
  memo1.Lines.Add('P(R) deuxi�me cible: ' + FloatToStrF(arTarget[FCurrentSession, iTarget2].RewardProbability, ffFixed, 3, 2));
end;

procedure TfrmTrials.SetImagePosition(TargetNumber, Position: integer);
var AnImage: TImage;
begin
  AnImage := TImage(FindComponent('img'+ IntToStr(TargetNumber)));
//  AnImage := arTarget[FCurrentSession, TargetNumber].Image;
  case Position of
    1: begin
      AnImage.Left := FCenter.X + iOffset - (AnImage.Width div 2);
      AnImage.Top := FCenter.Y - (AnImage.Height div 2);
    end;
    2: begin
      AnImage.Left := FCenter.X - (AnImage.Width div 2);
      AnImage.Top := FCenter.Y - iOffset - (AnImage.Height div 2);
    end;
    3: begin
      AnImage.Left := FCenter.X - iOffset - (AnImage.Width div 2);
      AnImage.Top := FCenter.Y - (AnImage.Height div 2);
    end;
    4: begin
      AnImage.Left := FCenter.X - (AnImage.Width div 2);
      AnImage.Top := FCenter.Y + iOffset - (AnImage.Height div 2);
    end;
  end;
end;

{
At the start of each trial a green circle surrounded by a black hollow circle
with the black cross cursor is shown for a random period between 1 and 1.5 seconds
}
procedure TfrmTrials.ShowTrialStart;
var iHowLong: integer;
begin
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := true;
  imgBlackCircle.Visible := true;
  imgBlueCircle.Visible := false;
  memo1.Lines.Add('D�but de l''essai');
  Application.ProcessMessages;
  iHowLong := 1000 + random(500);
  Wait(iHowLong);
end;

{
After  the solid green circle a hollow black circle, with the cursor inside is
shown for a random period between 1 and 1.5 seconds
}
procedure TfrmTrials.ShowBlackCircle;
var iHowLong: integer;
begin
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := false;
  imgBlackCircle.Visible := true;
  imgBlueCircle.Visible := false;
  Application.ProcessMessages;
  iHowLong := 1000 + random(500);
  Wait(iHowLong);
end;

procedure TfrmTrials.ShowBlueCircle;
//var iHowLong: integer;
begin
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := false;
  imgBlackCircle.Visible := false;
  imgBlueCircle.Visible := true;
  Application.ProcessMessages;
//  iHowLong := 1500 + random(500);
//  Wait(iHowLong);
end;

{
After the hollow black circle is shown for 1 to 1.5 seconds, the 2 targets
are displayed for 1.5 to 2 seconds before the go signal is given
}
procedure TfrmTrials.ShowTargets;
var i: integer;
    AnImage: TImage;
    iHowLong: integer;
begin
  for i := 1 to 4 do begin
    if arTarget[FCurrentSession, i].Show then begin
      AnImage := TImage(FindComponent('img' + IntToStr(i)));
//      AnImage := arTarget[i].Image;
      AnImage.Visible := true;
    end;
  end;
  memo1.Lines.Add('Presentation des cibles');
  Application.ProcessMessages;
  //We want the subjects to move as soon as the targets are shown, so no wait time
//  iHowLong := 1500 + random(500);
//  Wait(iHowLong);
end;

{
The reaction time is the time taken to move outside of the central circle  after
the targets are shown
}
function TfrmTrials.GetReactionTime: boolean;
var iStart: integer;
    APos: TJoystickPosition;
begin
  result := true;
  iStart := GetTickCount;
  APos := ShowCursorPosition;
  while APos = jpCenter do begin
    Application.ProcessMessages;
    APos := ShowCursorPosition;
    if GetTickCount - iStart > 10000 then begin
      //let the user have 10 seconds to make some sort of move away from the center
      FTrial[FCurrentSession, FCurrentTrial].ReactionTime := -1;
      FTrial[FCurrentSession, FCurrentTrial].ErrorCode := EC_NO_MOVE;
      result := false;
      EXIT;
    end;
  end;
  FTrial[FCurrentSession, FCurrentTrial].ReactionTime := GetTickCount - iStart;
  memo1.Lines.Add('Reaction time: '+ IntToStr(FTrial[FCurrentSession, FCurrentTrial].ReactionTime) + 'ms');
end;

{
The go signal is a green circle with the black cross cursor in the monkey version
No go signal is used in the human version so that the reaction time from when
the targets are shown can be measured.
Leave the code here as a reminder of how this task differs from the monkey
task
}
procedure TfrmTrials.ShowGoSignal;
begin
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := true;
  imgBlackCircle.Visible := false;
  imgBlueCircle.Visible := false;
  memo1.Lines.Add('Apparition du signal "Go"');
  Application.ProcessMessages;
end;

{
After the go signal have to move to one of the targets in a given amount of time
Show the black cross cursor moving
The APos variable is passed back to enable checking during the wait on target
period
Allow 2.5s for the movement
}
function TfrmTrials.GoToTarget(var APos: TJoystickPosition): boolean;
var i,
    iStart,
    iHowLong,
    iNow: integer;
    AShape: TShape;
    bGood: boolean;
    sPos: string;
begin
  result := false;
  iStart:= GetTickCount;
  iHowLong := 2000 + random(500);
  bGood := false;
  repeat
    iNow := GetTickCount;
    APos := ShowCursorPosition;
    if not (APos in [jpUnknown, jpCenter]) then begin
      //must be on one of the target positions
      for i := 1 to 4 do begin
        if arTarget[FCurrentSession, i].Show then begin
          //check whether it is one of the displayed targets
          if APos = arTarget[FCurrentSession, i].JoystickPosition then begin
            //one of the displayed targets has been picked
            result := true;
            arTarget[FCurrentSession, i].Picked := true;
            FTrial[FCurrentSession, FCurrentTrial].TargetPicked := i;
            CheckIfOptimumChosen(i);
            AShape := TShape(FindComponent('Shape'+IntToStr(arTarget[FCurrentSession, i].Position)));
            AShape.Visible := true;
            memo1.Lines.Add('Rejoint la cible  ' + sPos);
            bGood := true;
            //so get out of this procedure
            BREAK;
          end;
        end;
      end;
    end;
    if bGood then begin
      BREAK;
    end
    else begin
      //set all targets back to unpicked
      //this allows for inaccurate joystick movement which goes over 2 targets
      for i := 1 to 4 do
        arTarget[FCurrentSession, i].Picked := false;
    end;
  until iNow >= (iStart + iHowLong);
  if not bGood then
    FTrial[FCurrentSession, FCurrentTrial].ErrorCode := EC_NO_TARGET;
end;

{
After the cursor has been moved to a target, check if it is the optimum target
The optimum target is the target with the highest probability of reward.
If both targets have the same P(R), then the optimum has been chosen by
definition
}
procedure TfrmTrials.CheckIfOptimumChosen(TargetChosen: integer);
var bOptimum: boolean;
begin
  if FTrial[FCurrentSession, FCurrentTrial].Target[0].RewardProbability = FTrial[FCurrentSession, FCurrentTrial].Target[1].RewardProbability then begin
    //both targets have the same P(R)
    FTrial[FCurrentSession, FCurrentTrial].Optimum := true;
    FResults[FCurrentSession].Optimal := FResults[FCurrentSession].Optimal + 1;
    EXIT;
  end;

  if FTrial[FCurrentSession, FCurrentTrial].Target[0].RewardProbability > FTrial[FCurrentSession, FCurrentTrial].Target[1].RewardProbability then begin
    bOptimum := FTrial[FCurrentSession, FCurrentTrial].Target1 = TargetChosen;
  end
  else begin
    bOptimum := FTrial[FCurrentSession, FCurrentTrial].Target2 = TargetChosen;
  end;
  FTrial[FCurrentSession, FCurrentTrial].Optimum := bOptimum;
  if bOptimum then
    FResults[FCurrentSession].Optimal := FResults[FCurrentSession].Optimal + 1;
end;

{
After the circle turns green, the cursor is moved to one of the targets. It then
has to remain on that target for between 0.5 and 1s before the central
circle turns red to signal to move the cursor back to the center
}
function TfrmTrials.WaitOnTarget(WhatPosition: TJoystickPosition): boolean;
var iStart,
    iHowLong,
    i: integer;
    AShape: TShape;
    bGood: boolean;
begin
  result := false;
  iStart:= GetTickCount;
  iHowLong := 1000 + random(500);
  bGood := true;
  memo1.Lines.Add('Attend sur la cible');
  Application.ProcessMessages;
  while GetTickCount < (iStart + iHowLong) do begin
    if ShowCursorPosition <> WhatPosition then begin
      //have moved from the position
      memo1.Lines.Add('N''a pas rester sur la cible');
      FTrial[FCurrentSession, FCurrentTrial].ErrorCode := EC_NO_WAIT;
      //set all targets as unpicked so no reward will be given at the end
      for i := 1 to 4 do
        arTarget[FCurrentSession, i].Picked := false;
      bGood := false;
      BREAK;
    end;
    Application.ProcessMessages;
  end;
  for i := 1 to 4 do begin
    AShape := TShape(FindComponent('Shape'+IntToStr(i)));
    AShape.Visible := false;
  end;
  if bGood then begin
    result := true;
    memo1.Lines.Add('Reste sur la cible');
  end;
end;

{
After the cursor has been held on the target long enough, the central circle is
changed from green to red to signal the start of the go home phase
}
procedure TfrmTrials.ShowRedCircle;
begin
  imgRedCircle.Visible := true;
  imgGreenCircle.Visible := false;
  imgBlackCircle.Visible := false;
  imgBlueCircle.Visible := false;
  memo1.Lines.Add('Retour au centre');
  Application.ProcessMessages;
end;

{
After the circle turns red, the cursor has to be moved back to the central circle
within a set time
}
function TfrmTrials.GoHome: boolean;
var iStart,
//    iHowLong,
    i: integer;
//    bGood: boolean;
begin
  result := false;
  iStart:= GetTickCount;
//  iHowLong := 5000 + random(500);
//  bGood := false;
  while ShowCursorPosition <> jpCenter do begin
    Application.ProcessMessages;
    if GetTickCount - iStart > 5000 then begin
      FTrial[FCurrentSession, FCurrentTrial].ErrorCode := EC_NO_RETURN;
      for i := 1 to 4 do
        arTarget[FCurrentSession, i].Picked := false;
      EXIT;
    end;
  end;
  result := true;
  memo1.Lines.Add('Rejoint le centre ');
end;

{
When the cursor has been moved back to the center, it has to be held there for
0.8 to 1.2s
}
function TfrmTrials.WaitOnCenter: boolean;
var iStart,
    iHowLong,
    i: integer;
    bGood: boolean;
begin
  result := true;
  iStart:= GetTickCount;
  iHowLong := 800 + random(400);
  bGood := true;
  memo1.Lines.Add('Attend au centre');
  Application.ProcessMessages;
  while GetTickCount < (iStart + iHowLong) do begin
    if ShowCursorPosition <> jpCenter then begin
      //have moved from the center, so failed to hold long enough
      memo1.Lines.Add('Quitte le centre');
      bGood := false;
      result := false;
      BREAK;
    end;
    Application.ProcessMessages;
  end;
  if not bGood then begin
    //set all the targets as unpicked so as not to give a reward
    for i := 1 to 4 do
      arTarget[FCurrentSession, i].Picked := false;
    FTrial[FCurrentSession, FCurrentTrial].ErrorCode := EC_NO_WAIT_CENTER;
  end;
end;

{
After each trial need to hide the targets and then wait 0.8 to 1.2 seconds
}
procedure TfrmTrials.HideTargets;
var i,
    iHowLong: integer;
    AnImage: TImage;
begin
  for i := 1 to 4 do begin
    if arTarget[FCurrentSession, i].Show then begin
      AnImage := TImage(FindComponent('img' + IntToStr(i)));
//      AnImage := arTarget[FCurrentSession, i].Image;
      AnImage.Visible := false;
    end;
  end;
  Application.ProcessMessages;
  memo1.Lines.Add('Disparition des cibles');
  //don't wait after hiding the targets, just show the reward/fail message immediately
//  iHowLong := 800 + random(400);
//  Wait(iHowLong);
end;

{
Procedure to wait for a set number of milliseconds
}
procedure TfrmTrials.Wait(HowLong: integer);
var iStart,
    iNow: integer;
begin
  iStart:= GetTickCount;
  repeat
    ShowCursorPosition;
    iNow := GetTickCount;
    Application.ProcessMessages;
    if bStop then
      //if the stop button is pressed, get out
      EXIT;
  until iNow >= (iStart + HowLong);
end;

{
The reward probabilities are different for each session.  In one session, there
are four probabilities, P(R) = [0, 0.33, 0.66, 1]. These are randomly assigned
to the four targets used in that session. In the other session, 2 targets have
P(R) = 0.33 and 2 have P(R) = 0.66.
The order of the sessions is random
}
procedure TfrmTrials.SetTargetRewardProbabilities;
var i, j,
    iSession: integer;
    arOrder: TDynamicIntegerArray;
    arReward: TDynamicIntegerArray;
    iP: integer;
    bNewProbability: boolean;
begin
  CreateRandomArray(arOrder, 2, 1);

  //assign probabilities for the practise session
  for i := 1 to 4 do begin
    arTarget[0, i].RewardProbability := Random;
  end;

  //P(R) = [0, 0.33, 0.66, 1]. Assigned to session iOrder[0]
  CreateRandomArray(arReward, 4, 0);
  for i := 1 to 4 do
    arTarget[arOrder[0], i].RewardProbability := FRewardProbabilities[arReward[i-1]];

  for i := 1 to 2 do begin
    arTarget[arOrder[1], i].RewardProbability := 0.33;
    arTarget[arOrder[1], i+2].RewardProbability := 0.66;
  end;

  for j := 0 to 2 do
    for i := 1 to 4 do begin
      arTarget[j,i].PickedCount := 0;
      arTarget[j,i].RewardedCount := 0;
    end;
end;

function TfrmTrials.GetRewardProbability(index: integer): real;
begin
  result := FRewardProbabilities[index];
end;

procedure TfrmTrials.SetRewardProbability(index: integer; const Value: real);
begin
  FRewardProbabilities[index] := Value;
end;

procedure TfrmTrials.btnStopClick(Sender: TObject);
begin
  bStop := true;
  memo1.Lines.Clear;
end;

{
Give a reward with the probability of the chosen target
}
procedure TfrmTrials.CheckReward;
var i: integer;
    bReward,
    bGood: boolean;
begin
  //if the trial was not completed successfully do not bother to show any
  //reward/punishment message as the error message will have been shown
  if FTrial[FCurrentSession, FCurrentTrial].ErrorCode = EC_GOOD then begin
    bGood := false;
    for i := 1 to 4 do begin
      if arTarget[FCurrentSession, i].Show then begin
        if arTarget[FCurrentSession, i].Picked then begin
          arTarget[FCurrentSession, i].PickedCount := arTarget[FCurrentSession, i].PickedCount + 1;
          bReward := arTarget[FCurrentSession, i].RewardProbability >= Random;
          FTrial[FCurrentSession, FCurrentTrial].Rewarded := bReward;
          if bReward then begin
            //show a flashing red label as a reward. Wow!
            FResults[FCurrentSession].Reward := FResults[FCurrentSession].Reward + 1;
            PlaySound(SOUND_CHEER);
            lblReward.Visible := false;
            imgReward.Visible := true;
            Timer1.Enabled := true;
            Wait(2000 + random(500));
            Timer1.Enabled := false;
            lblReward.Visible := false;
            imgReward.Visible := false;
            bGood := true;
            memo1.Lines.Add('R�compense obtenue');
            arTarget[FCurrentSession, i].RewardedCount := arTarget[FCurrentSession, i].RewardedCount + 1;
          end;
          arTarget[FCurrentSession, i].EffectiveProbability :=
            arTarget[FCurrentSession, i].RewardedCount /
            arTarget[FCurrentSession, i].PickedCount;
        end;
      end;
    end;
    if not bGood then begin
      //show a flashing blue label as a punishment!
      PlaySound(SOUND_BOO);
      lblFail.Visible := false;
      imgFail.Visible := true;
      Timer2.Enabled := true;
      Wait(2000 + random(500));
      Timer2.Enabled := false;
      lblFail.Visible := false;
      imgFail.Visible := false;
      memo1.Lines.Add('Pas de r�compense');
    end;
  end;
end;

{
Let the user know what they did wrong by flashing a message on the screen
}
procedure TfrmTrials.ShowError;
begin
  if FTrial[FCurrentSession, FCurrentTrial].ErrorCode > EC_GOOD then begin
    //Don't show a message if everything went OK
    lblError.Caption := ErrorMessages[FTrial[FCurrentSession, FCurrentTrial].ErrorCode];
    lblError.Left :=  imgBlackCircle.Left + (imgBlackCircle.Width div 2)
                   - (lblError.Width  div 2);
    Timer3.Enabled := true;
    Wait(3000);
    Timer3.Enabled := false;
    lblError.Visible := false;
  end;
end;

procedure TfrmTrials.Timer1Timer(Sender: TObject);
begin
  lblReward.Visible := not lblReward.Visible;
end;

procedure TfrmTrials.Timer2Timer(Sender: TObject);
begin
  lblFail.Visible := not lblFail.Visible;
end;

procedure TfrmTrials.btnPractiseClick(Sender: TObject);
const TRIAL_COUNT = 5;
var APos: TJoystickPosition;
    i,
    iTotal: integer;
    AShape: TShape;
begin
  if frmPractise = nil then
    frmPractise := TfrmPractise.Create(nil);
  with frmPractise do try
    ShowModal;
  finally
    Free;
    frmPractise := nil;
  end;
  SetJoystickField;
  bStop := false;
  HideTargets;
  imgBlackCircle.Visible := true;
  lblSession.Caption := 'Session d''entrainement';
  lblSession.Visible := true;
  CentreHorizontal(lblSession, 0);
  CentreVertical(lblSession, 100);
//  GetPractiseTargets;
  SetTargetRewardProbabilities;
  FCurrentSession := 0;
  InitializeSessions;
  SetSessionTargets;
  SetLength(FTrial[FCurrentSession], TRIAL_COUNT);
  iTotal := 0;
  ShowCursorPosition;
  imgCursor.Visible := true;
  for i := 0 to pred(TRIAL_COUNT) do begin
    FCurrentTrial := i;
    if bStop then
      EXIT;
    if GetCursorCentered then begin
      InitializeTrial;
      if StartTrial then begin
        memo1.Lines.Add('Essais num�ro : ' + IntToStr(i+1));
        PickTargets;
        ShowTrialStart;
        ShowBlackCircle;
        ShowTargets;
        lblPractise.Visible := true;
        lblPractise.Caption := 'Deplacez le curseur vers l''une des cibles';
        PositionLabel(lblPractise);
        if GetReactionTime then begin
      //    ShowGoSignal;
          if GoToTarget(APos) then begin
            //cursor has moved from center and reached one of the displayed targets
            lblPractise.Caption := 'Laissez le curseur sur la cible';
            PositionLabel(lblPractise);
            if WaitOnTarget(APos) then begin
              //waited long enough on the target, move back to the center
              ShowRedCircle;
              lblPractise.Caption := 'Ramenez le curseur au centre';
              PositionLabel(lblPractise);
              if GoHome then begin
                lblPractise.Visible := false;
                //got back to the center
                ShowBlueCircle;
//                if WaitOnCenter then begin
                  //trial successfully completed
                  FResults[0].Success := FResults[0].Success + 1;
                  FTrial[0, FCurrentTrial].Successful := true;
//                end;
              end;
            end;
          end;
        end;
      end;
    end;
    lblPractise.Visible := false;
    lblSession.Visible := false;
    HideTargets;
    ShowError;
    CheckReward;
  end;
  memo1.Clear;
  memo1.Lines.Add('Fin de la s�rie d''essais ');
  memo1.Lines.Add('Nombre d''essais: ' + IntToStr(FResults[0].TrialCount));
  memo1.Lines.Add('Nombre d''essais men�s � bien: ' + IntToStr(FResults[0].Success));
  memo1.Lines.Add('Nombre de r�compenses re�ues: ' + IntToStr(FResults[0].Reward));
  if frmPractiseScore = nil then
    frmPractiseScore := TfrmPractiseScore.Create(nil);
  with frmPractiseScore do try
    Score := FResults[0].Reward;
    ShowModal;
  finally
    Free;
    frmPractiseScore := nil;
  end;
  bStop := true;
end;

{
At the start of the practise session, randomly select 4 targets
from the library to display in the 4 images
}
{
procedure TfrmTrials.GetPractiseTargets;
const TRIAL_COUNT = 5;
var i, j: integer;
    iImage: integer;
    bNew: boolean;
begin
  SetLength(FTrial[0], TRIAL_COUNT);
  FResults[0].TrialCount := TRIAL_COUNT;
  FResults[0].Success := 0;
  FResults[0].Reward := 0;
  FResults[0].Optimal := 0;

  for i := 1 to 4 do begin
    repeat
      bNew := true;
      iImage := RandomRange(1, 97);
      for j := i - 1 downto 1 do begin
        if arTarget[0, j].ImageNumber = iImage then
          bNew := false;
      end;
    until bNew;
    arTarget[0, i].ImageNumber := iImage;
  end;
end;
}
{
Want the joystick to be able to move to just outside the targets (left, right,
top and bottom)
}
procedure TfrmTrials.SetJoystickField;
var iWidth,
    iHeight: integer;
    APoint: TPoint;
begin
  FCursorOffset.X := Shape3.Left - 50;
  FCursorOffset.Y := Shape2.Top - 50;
  iWidth := (Shape1.Left + Shape1.Width + 50) - (Shape3.Left - 50);
  iHeight := (Shape4.Top + Shape4.Height + 50) - (Shape2.Top - 50);
  AJoystick.FieldWidth := iWidth;
  AJoystick.FieldHeight := iHeight;
end;

procedure TfrmTrials.btnSend2ExcelClick(Sender: TObject);
begin
  if CreateExcelWorkbook then begin
    SendResultsToExcel;
    SendSetupToExcel;
  end;
end;

function TfrmTrials.CreateExcelWorkbook: boolean;
begin
  result := true;
  // By using GetActiveOleObject, you use an instance of Excel that's already running,
  // if there is one.
  try
    FXLApp := GetActiveOleObject('Excel.Application');
  except
    try
      // If no instance of Excel is running, try to Create a new Excel Object
      FXLApp := CreateOleObject('Excel.Application');
    except
      ShowMessage('Impossible de d�marrer Exel / le logiciel n''est peut �tre pas install�?');
      result := false;
      Exit;
    end;
  end;
  FXLApp.Workbooks.Add(xlWBatWorkSheet);
  FXLApp.Visible := true;
  FXLWorkbook := FXLApp.Activeworkbook;
  self.SetFocus;
  Application.ProcessMessages;
end;

procedure TfrmTrials.SendResultsToExcel;
var i, j: integer;
begin
  for i := 0 to 1 do begin
    if i = 1 then
      FXLWorkbook.Worksheets.Add;
    FExcelWorksheet := 'Session ' + IntToStr(i+1);
    FXLWorkbook.Activesheet.Name := FExcelWorksheet;
    FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];
    FXLWorksheet.Cells[ROW_HEADER, 1].Value := 'Trial Number';
    FXLWorksheet.Cells[ROW_HEADER, 2].Value := 'T1';
    FXLWorksheet.Cells[ROW_HEADER, 3].Value := 'T2';
    FXLWorksheet.Cells[ROW_HEADER, 4].Value := 'P1';
    FXLWorksheet.Cells[ROW_HEADER, 5].Value := 'P2';
    FXLWorksheet.Cells[ROW_HEADER, 6].Value := 'P(R)1';
    FXLWorksheet.Cells[ROW_HEADER, 7].Value := 'P(R)2';
    FXLWorksheet.Cells[ROW_HEADER, 8].Value := 'Picked';
    FXLWorksheet.Cells[ROW_HEADER, 9].Value := 'Optimum?';
    FXLWorksheet.Cells[ROW_HEADER, 10].Value := 'Rewarded?';
    FXLWorksheet.Cells[ROW_HEADER, 11].Value := 'ITI';
    FXLWorksheet.Cells[ROW_HEADER, 12].Value := 'Reaction time';
    FXLWorksheet.Cells[ROW_HEADER, 13].Value := 'Error code';
    for j := 0 to FTrialCount - 1 do begin
      FXLWorksheet.Cells[j+2, 1].Value := j+1;
      FXLWorksheet.Cells[j+2, 2].Value := FTrial[i, j].Target1;
      FXLWorksheet.Cells[j+2, 3].Value := FTrial[i, j].Target2;
      FXLWorksheet.Cells[j+2, 4].Value := FTrial[i, j].Target[0].Position;
      FXLWorksheet.Cells[j+2, 5].Value := FTrial[i, j].Target[1].Position;
      FXLWorksheet.Cells[j+2, 6].Value := FTrial[i, j].Target[0].RewardProbability;
      FXLWorksheet.Cells[j+2, 7].Value := FTrial[i, j].Target[1].RewardProbability;
      FXLWorksheet.Cells[j+2, 8].Value := FTrial[i, j].TargetPicked;
      FXLWorksheet.Cells[j+2, 9].Value := NO_YES[integer(FTrial[i, j].Optimum)];
      FXLWorksheet.Cells[j+2, 10].Value := NO_YES[integer(FTrial[i, j].Rewarded)];
      FXLWorksheet.Cells[j+2, 11].Value := FTrial[i, j].ITI;
      FXLWorksheet.Cells[j+2, 12].Value := FTrial[i, j].ReactionTime;
      FXLWorksheet.Cells[j+2, 13].Value := FTrial[i, j].ErrorCode;
    end;
  end;
end;

procedure TfrmTrials.SendSetupToExcel;
var bSent: boolean;
    i: integer;
begin
  FXLWorkbook.Worksheets.Add;
  FExcelWorksheet := 'Setup';
  FXLWorkbook.Activesheet.Name := FExcelWorksheet;
  FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];
  bSent := false;
  while not bSent do begin
    try
      FXLWorksheet.Cells[1,3].Value := 'Session 1';
      FXLWorksheet.Cells[1,7].Value := 'Session 2';
      FXLWorksheet.Cells[2,1].Value := 'Targets';
      FXLWorksheet.Cells[3,1].Value := '1';
      FXLWorksheet.Cells[4,1].Value := '2';
      FXLWorksheet.Cells[5,1].Value := '3';
      FXLWorksheet.Cells[6,1].Value := '4';

      FXLWorksheet.Cells[2,2].Value := 'Number';
      FXLWorksheet.Cells[2,3].Value := 'Assigned P(R)';
      FXLWorksheet.Cells[2,4].Value := 'Guessed P(R)';
      FXLWorksheet.Cells[2,5].Value := 'Real P(R)';
      FXLWorksheet.Cells[2,6].Value := 'Number';
      FXLWorksheet.Cells[2,7].Value := 'Assigned P(R)';
      FXLWorksheet.Cells[2,8].Value := 'Guessed P(R)';
      FXLWorksheet.Cells[2,9].Value := 'Real P(R)';

      for i := 1 to 4 do begin
        FXLWorksheet.Cells[i + 2,2].Value := arTarget[1, i].ImageNumber;
        FXLWorksheet.Cells[i + 2,3].Value := arTarget[1, i].RewardProbability;
        FXLWorksheet.Cells[i + 2,4].Value := arGuesses[0, i];
        if arTarget[1, i].PickedCount <> 0 then
          FXLWorksheet.Cells[i + 2,5].Value := arTarget[1, i].RewardedCount / arTarget[1, i].PickedCount
        else
          FXLWorksheet.Cells[i + 2,5].Value := '0';

        FXLWorksheet.Cells[i + 2,6].Value := arTarget[2, i].ImageNumber;
        FXLWorksheet.Cells[i + 2,7].Value := arTarget[2, i].RewardProbability;
        FXLWorksheet.Cells[i + 2,8].Value := arGuesses[1, i];
        if arTarget[2, i].PickedCount <> 0 then
          FXLWorksheet.Cells[i + 2,9].Value := arTarget[2, i].RewardedCount / arTarget[2, i].PickedCount
        else
          FXLWorksheet.Cells[i + 2,9].Value := '0';
      end;

      FXLWorksheet.Cells[7,1].Value := 'Outcomes';
      FXLWorksheet.Cells[8,1].Value := 'Number of trials';
      FXLWorksheet.Cells[9,1].Value := 'Succeeded';
      FXLWorksheet.Cells[10,1].Value := 'Rewarded';
      FXLWorksheet.Cells[11,1].Value := 'Optimal';
      for i := 0 to 1 do begin
        FXLWorksheet.Cells[8,2+(4*i)].Value := FResults[i+1].TrialCount;
        FXLWorksheet.Cells[9,2+(4*i)].Value := FResults[i+1].Success;
        FXLWorksheet.Cells[10,2+(4*i)].Value := FResults[i+1].Reward;
        FXLWorksheet.Cells[11,2+(4*i)].Value := FResults[i+1].Optimal;
      end;
      FXLWorksheet.Columns[1].ColumnWidth := 20;

      FXLWorksheet.Cells[13,1].Value := 'Subject ID';
      FXLWorksheet.Cells[13,2].Value := FSubject;
      FXLWorksheet.Cells[14,1].Value := 'Date of test';
      FXLWorksheet.Cells[14,2].Value := DateToStr(Now);

      FXLWorksheet.Columns[2].ColumnWidth := 10;
      FXLWorksheet.Columns[3].ColumnWidth := 15;
      FXLWorksheet.Columns[4].ColumnWidth := 15;
      FXLWorksheet.Columns[5].ColumnWidth := 15;
      FXLWorksheet.Columns[7].ColumnWidth := 15;
      FXLWorksheet.Columns[8].ColumnWidth := 15;
      FXLWorksheet.Columns[9].ColumnWidth := 15;
      bSent := true;
    except
      on E: Exception do begin
        ShowMessage(E.Message);
      end;
    end;
  end;
end;

procedure TfrmTrials.btnExitClick(Sender: TObject);
begin
  bStop := true;
  Close;
end;

procedure TfrmTrials.Timer3Timer(Sender: TObject);
begin
  lblError.Visible := not lblError.Visible;
end;

procedure TfrmTrials.ToolButton4Click(Sender: TObject);
begin
end;

{
Look up the high score in an ini file to tell the user the score that they have
to beat
}
function TfrmTrials.GetHighScore: integer;
var FIni: TIniFile;
begin
  FIni := TIniFile.Create(ExtractFilePath(Application.ExeName)+'Scores.ini');
  with FIni do try
    result := ReadInteger('High scores','Score',0);
  finally
    Free;
    FIni := nil;
  end;
end;

procedure TfrmTrials.SaveHighScore(SubjectId: string; Score: integer);
var FIni: TIniFile;
begin
  FIni := TIniFile.Create(ExtractFilePath(Application.ExeName)+'Scores.ini');
  with FIni do try
    WriteString('High scores', 'Subject ID', SubjectId);
    WriteInteger('High scores','Score', Score);
  finally
    Free;
    FIni := nil;
  end;
end;

procedure TfrmTrials.PositionControls;
begin
  iOffset := round(Screen.Height / eOffsetDivider);

  //get the center point of the screen to position controls dynamically
  FCenter.X := (Screen.Width div 2);// - (imgCursor.Width div 2);
  FCenter.Y := (Screen.Height div 2);//  - (imgCursor.Height div 2);

{
  CentreHorizontal(img1, iOffset); //right
  CentreVertical(img1, 0);
  CentreHorizontal(img2, 0);
  CentreVertical(img2, iOffset * -1);  //top
  CentreHorizontal(img3, iOffset * -1);  //left
  CentreVertical(img3, 0);
  CentreHorizontal(img4, 0);
  CentreVertical(img4, iOffset); //bottom
}
  CentreHorizontal(Shape1, iOffset); //right
  CentreVertical(Shape1, 0);
  CentreHorizontal(Shape2, 0);
  CentreVertical(Shape2, iOffset * -1);  //top
  CentreHorizontal(Shape3, iOffset * -1);  //left
  CentreVertical(Shape3, 0);
  CentreHorizontal(Shape4, 0);
  CentreVertical(Shape4, iOffset); //bottom

{
  Shape1.Left := Img1.Left - 40;
  Shape1.Top :=  Img1.Top - 40;
  Shape2.Left := Img2.Left - 40;
  Shape2.Top :=  Img2.Top - 40;
  Shape3.Left := Img3.Left - 40;
  Shape3.Top :=  Img3.Top - 40;
  Shape4.Left := Img4.Left - 40;
  Shape4.Top :=  Img4.Top - 40;
}

  CentreHorizontal(imgRedCircle, 0);
  CentreVertical(imgRedCircle, 0);
  CentreHorizontal(imgGreenCircle, 0);
  CentreVertical(imgGreenCircle, 0);
  CentreHorizontal(imgBlackCircle, 0);
  CentreVertical(imgBlackCircle, 0);
  CentreHorizontal(imgBlueCircle, 0);
  CentreVertical(imgBlueCircle, 0);

  PositionLabel(lblMoveCursor);
  PositionLabel(lblReward);
  PositionLabel(lblStartTrial);
  PositionLabel(lblFail);
  PositionLabel(lblError);
  PositionLabel(lblPractise);
  CentreHorizontal(lblSession, 0);
  CentreVertical(lblSession, 200);

  CentreHorizontal(imgReward, 0);
  CentreVertical(imgReward, -200);
  CentreHorizontal(imgFail, 0);
  CentreVertical(imgFail, -200);
end;

procedure TfrmTrials.PositionLabel(ALabel: TLabel);
begin
  ALabel.Left := FCenter.X - (ALabel.Width div 2);
  ALabel.Top := FCenter.Y - 100;
end;

{
In this version there is one practise session and 2 real sessions.
In the first session the subject sees 4
targets with P(R) = [0, 0.33, 0.66, 1]. In the second P(R) = [0.33, 0.33, 0.66, 0.66]
We need to choose targets from the library randomly and the targets need to be
all different for each session.
Choose 8 different targets from the target library and load them into an array.
There are 96 targets in the directory Targets/BMP
}
procedure TfrmTrials.InitializeSessions;
var i, j, k,
    iSession,
    iMin,
    iMax: integer;
    bNew: boolean;
    arSession: TDynamicIntegerArray;
    arImageNum: TDynamicIntegerArray;
begin
  for iSession := 0 to 2 do begin
    FResults[iSession].TrialCount := FTrialCount;
    FResults[iSession].Success := 0;
    FResults[iSession].Reward := 0;
    FResults[iSession].Optimal := 0;
  end;

  CreateRandomArray(arSession, 3, 0);
  //load the targets into the arTarget[i,j] array
  for i := 0 to 2 do begin
    iSession := arSession[i];
    iMin := iSession * 10;
    iMax := iMin + 10;
    CreateRandomArray(arImageNum, 4, iMin, iMax);
    for j := 1 to 4 do begin
      arTarget[i, j].ImageNumber := arImageNum[j-1];
    end;
  end;
end;

procedure TfrmTrials.PlaySound(FileName: string);
begin
  mp1.Close;
  mp1.FileName := ExtractFilePath(Application.ExeName)+ 'Sounds\' + FileName;
  mp1.Open;
  mp1.Play;
end;

{
The image number for each target in each session is loaded in InitializeSessions
There are only 4 TImages, so the correct images need to be put in each at the
start of each session
}
procedure TfrmTrials.SetSessionTargets;
var i: integer;
    AnImage : TImage;
    sTarget: string;
begin
  for i := 1 to 4 do begin
    AnImage := TImage(FindComponent('img'+ IntToStr(i)));
    sTarget := IntToStr(arTarget[FCurrentSession, i].ImageNumber);
    while length(sTarget) < 2 do
      sTarget := '0'+ sTarget;
    sTarget := ExtractFilePath(Application.ExeName) + 'Targets\'+ sTarget + '_I.bmp';
    AnImage.Picture.Bitmap.LoadFromFile(sTarget);
  end;
end;

procedure TfrmTrials.ShowQuestionnaire1;
var i: integer;
begin
  if frmQ1 = nil then
    frmQ1 := TfrmQ1.Create(nil);
  with frmQ1 do try
    for i := 1 to 4 do
      Image[i] := arTarget[FCurrentSession,i].ImageNumber;
    ShowModal;
    for i := 1 to 4 do
      arGuesses[0,i] := Probability[i];
  finally
    Free;
    frmQ1 := nil;
  end;
end;

procedure TfrmTrials.ShowQuestionnaire2;
var i: integer;
begin
  if frmQ2 = nil then
    frmQ2 := TfrmQ2.Create(nil);
  with frmQ2 do try
    for i := 1 to 4 do
      Image[i] := arTarget[FCurrentSession,i].ImageNumber;
    ShowModal;
    for i := 1 to 4 do
      arGuesses[1,i] := Probability[i];
  finally
    Free;
    frmQ2 := nil;
  end;
end;

procedure TfrmTrials.ShowHighScore(WhatScore: integer);
begin
    if frmHighScore = nil then
    frmHighScore := TfrmHighScore.Create(nil);
  with frmHighScore do try
    HighScore := WhatScore;
    ShowModal;
  finally
    Free;
    frmHighScore := nil;
  end;
end;

procedure TfrmTrials.ShowScore(WhatScore: integer);
begin
  if frmEnd = nil then
    frmEnd := TfrmEnd.Create(nil);
  with frmEnd do try
    Score := WhatScore;
    ShowModal;
  finally
    Free;
    frmend := nil;
  end;
end;

procedure TfrmTrials.ShowSession2Start;
begin
  if frmSession2 = nil then
    frmSession2 := TfrmSession2.Create(nil);
  with frmSession2 do try
    ShowModal;
  finally
    Free;
    frmSession2 := nil;
  end;
end;

procedure TfrmTrials.CentreHorizontal(AControl: TControl; Offset: integer);
begin
  AControl.Left := FCenter.X + Offset - (AControl.Width div 2);
end;

procedure TfrmTrials.CentreVertical(AControl: TControl; Offset: integer);
begin
  AControl.Top := FCenter.Y + Offset - (AControl.Height div 2);
end;

procedure TfrmTrials.tb1PositionChanged(Sender: TObject; Position: Double);
var i: integer;
begin
  eOffsetDivider := tb1.Position;
  PositionControls;
  SetJoystickField;
  for i := 1 to 4 do begin
    if arTarget[FCurrentSession,i].Show then begin
      SetImagePosition(i, arTarget[FCurrentSession, i].Position);
    end;
  end;
end;

procedure TfrmTrials.btnResetClick(Sender: TObject);
begin
  SetTargetRewardProbabilities;
end;

end.

