unit uEnd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TfrmEnd = class(TForm)
    Panel1: TPanel;
    lblScore: TLabel;
    BitBtn2: TBitBtn;
    Shape1: TShape;
    procedure FormShow(Sender: TObject);
  private
    FScore: integer;
    { Private declarations }
  public
    { Public declarations }
    property Score: integer read FScore write FScore;
  end;

var
  frmEnd: TfrmEnd;

implementation

{$R *.dfm}

procedure TfrmEnd.FormShow(Sender: TObject);
begin
  lblScore.Caption := 'Votre score: ' + IntTosTr(FScore) + ' points';
end;

end.
