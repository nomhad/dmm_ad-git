unit uHighScore;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TfrmHighScore = class(TForm)
    Panel1: TPanel;
    lblHighScore: TLabel;
    Shape1: TShape;
    BitBtn1: TBitBtn;
    procedure FormShow(Sender: TObject);
  private
    FHighScore: integer;
    { Private declarations }
  public
    { Public declarations }
    property HighScore: integer read FHighScore write FHighScore;
  end;

var
  frmHighScore: TfrmHighScore;

implementation

{$R *.dfm}

procedure TfrmHighScore.FormShow(Sender: TObject);
begin
  lblHighScore.Caption := 'Score � battre: '+ IntToStr(FHighScore);
end;

end.
