unit uHighestScore;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TfrmHighestScore = class(TForm)
    Panel1: TPanel;
    lblHighScore: TLabel;
    Shape1: TShape;
    BitBtn1: TBitBtn;
  private
    FHighScore: integer;
    { Private declarations }
  public
    { Public declarations }
    property HighestScore: integer read FHighScore write FHighScore;
  end;

var
  frmHighestScore: TfrmHighestScore;

implementation

{$R *.dfm}

end.
