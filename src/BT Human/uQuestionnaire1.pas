unit uQuestionnaire1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ExtCtrls, ImgList, ComCtrls, ToolWin;

type
  TfrmQ1 = class(TForm)
    ToolBar1: TToolBar;
    btnExit: TToolButton;
    ilButtons: TImageList;
    pml1: TPanel;
    lblStartTrial: TLabel;
    img1: TImage;
    img2: TImage;
    img3: TImage;
    img4: TImage;
    tb1: TTrackBar;
    tb2: TTrackBar;
    tb3: TTrackBar;
    tb4: TTrackBar;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl1: TLabel;
    lbl2: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure tb1Change(Sender: TObject);
  private
    FImages: array[1..4] of integer;
    FProbabilities: array[1..4] of real;
    FChanged: array[1..4] of boolean;

    function GetImage(index: integer): integer;
    procedure SetImage(index: integer; const Value: integer);
    function GetProbability(index: integer): real;
    procedure SetProbability(index: integer; const Value: real);
    { Private declarations }
  public
    { Public declarations }
    property Image[index: integer]: integer read GetImage write SetImage;
    property Probability[index: integer]: real read GetProbability write SetProbability;
  end;

var
  frmQ1: TfrmQ1;

implementation

{$R *.dfm}

{ TfrmQ1 }

function TfrmQ1.GetImage(index: integer): integer;
begin
  result := FImages[index];
end;

procedure TfrmQ1.SetImage(index: integer; const Value: integer);
begin
  FImages[index] := Value;
end;

function TfrmQ1.GetProbability(index: integer): real;
begin
  result := FProbabilities[index];
end;

procedure TfrmQ1.SetProbability(index: integer; const Value: real);
begin
  FProbabilities[index] := value;
end;

procedure TfrmQ1.FormShow(Sender: TObject);
var i: integer;
    sTarget: string;
    AnImage: TImage;
begin
  for i := 1 to 4 do begin
    sTarget := IntToStr(FImages[i]);
    while length(sTarget) < 2 do
      sTarget := '0'+ sTarget;
    sTarget := ExtractFilePath(Application.ExeName) + 'Targets\'+ sTarget + '_I.bmp';
    AnImage := TImage(FindComponent('img'+ IntToStr(i)));
    AnImage.Picture.Bitmap.LoadFromFile(sTarget);

    FChanged[i] := false;

    FProbabilities[i] := 0.5;
  end;
end;

procedure TfrmQ1.btnExitClick(Sender: TObject);
begin
  Close;
  ModalResult := mrOk;
end;

procedure TfrmQ1.tb1Change(Sender: TObject);
var s: string;
    i,
    iValue: integer;
    bAllChanged: boolean;
begin
  s := TTrackBar(Sender).Name;
  s := Copy(s,3,1);
  i := StrToInt(Trim(s));
  iValue := TTrackBar(Sender).Position;
  s := IntToStr(iValue) + '%';
  FProbabilities[i] :=  iValue / 100;
  TLabel(FindComponent('lbl'+IntToStr(i))).Caption := s;
{
  FChanged[i] := true;
  bAllChanged := true;
  for i := 1 to 4 do begin
    if not FChanged[i] then
      bAllChanged := false;
  end;
  btnExit.Enabled := bAllChanged;
}
end;

end.
