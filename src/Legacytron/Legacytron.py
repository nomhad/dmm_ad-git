﻿# -*- coding: utf-8-*-
import socket
# import random
#import PIL
import Image
import os
import time

from ImageStat import Stat

# Objectif : se passer de reconnaissance de forme et avec de la couleur faire juste quelques opérations matricielles pour détecter cible.

# Les masques pour cibler les zones à étudier sont placés dans le répertoire "Zones". Blanc pour prendre en compte, noir pour ignorer.

#Déclaration globale
global NB_ZONES, Zones, Size, NB_PATTERNS, Patterns, fileLog, Threshold

# Le message transmis par le serveur
global msg
# Nombre de cibles détectées
global CountObject

# Image reçue par le serveur
global Img

# A partir de quel correspondance dans l'indice de couleur on considère la cible comme reconnue (entre 0 et 1)
# NB: si zones de masque assez larges, le blanc de l'image va fortement diminuer le matching, donc il faut baisser la valeur d'autant
Threshold = 0.3

# Nombre d'objets à renvoyer vers l'extérieur
NbExpectedObjects = 2

# Nombre de zones (positions intéressantes à étudier sur l'écran)
# Note: la zone 0 désigne le centre (pour signaux GO/NOGO)
NB_ZONES = 5
# Liste de tous ces masques
Zones = []

# Nombre de cibles
# Note: la cible 0 est utilisée comme référence pour le fond (= pas de cible affichée, du blanc très certainement)
NB_PATTERNS = 5
# On ne retiendra pas l'image en elle-même des cibles, mais un indice de couleur (retourné par VALUE)
Patterns = []
# Dénomination des cibles
Patterns_name = ["Signal","Circle","Square","Diamond","Triangle"]

# Taille de l'image à laquelle le serveur s'attend
Size = (640,480)

##
# Affiche info sur hisogramme de l'image
# @param zone : ne regarder qu'une portion de l'écran avec une zone
def STATS(PictureName, zone = None):
    if zone:
        st=Stat(PictureName,zone)
    else:
        st=Stat(PictureName)

    print "extrema", st.extrema
    print "sum    ", st.sum
    print "mean   ", st.mean
    print "median ", st.median
    print "rms    ", st.rms
    print "sum2   ", st.sum2
    print "var    ", st.var
    print "stddev ", st.stddev

##
# Evalue correspondance entre deux indices de couleur
#
# @return indice entre 0 et 1 (de nulle à parfaite)
def MATCH(cue1, cue2):
    # Pour le moment on sait faire qu'avec trois info, une sur chaque canal de l'image
    if not(len(cue1) == len(cue2) == 3):
        return 0
    # coeff de maching
    m = 0
    for i in range(len(cue1)):
        # équation: ramène différence sur intervalle [0.1], élève au carré et compte pour 1/3 de la note (on a RGB)
        # oui, en fait ce qu'on calcule c'est un coeff de différence
        #m += ( (cue1[i] - cue2[i]) / 255 )**2 / len(cue1)
        # ...sans carré pour le moment en fait
        #m += ( abs(cue1[i] - cue2[i]) / 255 ) / len(cue1)
        # et en fait on change encore, histoire d'essayer de faire pencher l'envolée du carrré du bon côté
        m += ( 1 - abs((cue1[i] - cue2[i])) / 255 )**2 / len(cue1)
    return m

##
# Renvoie un indice permettant d'évaluer la présence des couleurs dans l'image (utilise vleur "rms" fournie par ImageStat)
# @param zone : ne regarder qu'une portion de l'écran avec une zone
def VALUE(PictureName, zone = None):
    if zone:
        st=Stat(PictureName,zone)
    else:
        st=Stat(PictureName)
    #return st.rms
    return st.stddev

##
# Va tenter de reconnaître les cibles contenue dans chaque zone (dès que le seuil de reconnaissance est atteint, la plus forte proximité l'emporte comme de juste il se doit.
#
# Renseigne pour ce faire deux variables globales.
def COUNT():
    global msg, CountObject
    msg = ""
    CountObject = 0
    # Va passer en revue chacune des zones (saute le centre)
    for i in range(1,NB_ZONES):
        fileLog.write("\tDetecting Zone "+ str(i)+"\n")
        print "\tDetecting Zone "+ str(i)
        
        # Indice de couleur de la zone
        zone_cue = VALUE(Img,Zones[i])
        fileLog.write("\tColor cue : "+ str(zone_cue)+"\n")
        print "\tColor cue : ", zone_cue
        
        # Cible correspondant actuellement à la zone étudiée
        selected_target = -1
        
        # Il faut non seulement battre le seuil, mais aussi le score des cibles précédente dans cette compétition sans merci
        previous_matching = Threshold
        
        for j in range(0,NB_PATTERNS):
            print "\t\tTarget ", j, ", % matching : ",
            # évalue correspondance
            matching = MATCH(zone_cue,Patterns[j])
            # Pour les humains on affiche ça en pourcentage
            print round(matching*100), "...",
            # Si c'est le plus valide des candidats, on enregistre
            if matching >= previous_matching:
                print "OK"
                selected_target = j
                previous_matching = matching
            else:
                print "Nope"
        
        # Si on a un vainqueur, on le compte
        if selected_target > 0:
            # TODO: garde code couleur à "undefined" alors qu'il n'y a plus que ça qui compte en fait
            Code = str(i)+"|"+str(selected_target)+"|undefined"
            fileLog.write("\t"+Patterns_name[selected_target]+" : "+str(Code)+"\n")
            print "\t"+Patterns_name[selected_target]+" :",Code
            # Incrémente compteur et ajoute au message en cours
            CountObject +=1
            if msg =="":
                msg = Code
            else:
                msg += ";"+ Code
        print


def SESSION():
    global CountObject, msg, Img
    
    try:
        port = 6667
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind(("", port))
        server_socket.listen(5)

        fileLog.write("\n Server start :"+time.strftime("%Y-%m-%d %H:%M:%S\n"))
        fileLog.write("\nWaiting for a client on port :"+str(port)+"\n")
        print"Waiting for a client..."
        client_socket, adress = server_socket.accept()

        fileLog.write("Client found : "+str(adress)+"\n")
        print"Client found:"+str(adress)
        client_socket.send("Connection OK")

        fileLog.write("\n> Session : engaged <\n")
        print "\n> Session : engaged <"
        
        
        i=0 #compteur image
        while 1:
            i+=1            
            data = ""
            
            #reception de la taille image:
            lg = client_socket.recv(6)
            if lg == "":
                fileLog.write("\n\n> Session : disengaged <\n")
                print "\n> Session : disengaged <\n"
                break
            else:
                fileLog.write("\n\n- Picture Treatment : "+str(i)+"\n")
                print "\n- Picture Treatment : ",i
                fileLog.write("     Picture data size : "+str(lg)+"\n")
                print "  Picture size recieved :",lg
            
            #reception des donnees de l'image
            while len(data) < int(lg):
                data += client_socket.recv(4096)
    
            fileLog.write("     Picture data recieved : Successful \n")
            print ("  Picture data recieved : Successful \n")
            
            #image RGB (originale)
            Img = Image.fromstring("RGB",Size,data)
            
            # STATS(Img)
            
            Img.save("Map/Patt_"+str(i)+".tiff")
            fileLog.write("     Map\Patt_"+str(i)+".tiff : Created \n")
            print "  RGB picture : ok"
            
            
            #--------------------< Traitement de l'image >----------------------------------------------------------------------------------------------------
            
            #Résultats
            COUNT()
            
            fileLog.write("\n   CountObject/NbExpectedObjects : " + str(CountObject) + "/" + str(NbExpectedObjects) + "\n")
            print "\n   CountObject/NbExpectedObjects : ", CountObject, "/", NbExpectedObjects, "\n"
            
            # Deux éléments : parfait, on envoie tel quel
            if CountObject == NbExpectedObjects:
                msg = str(CountObject+1)+";"+msg
            #Si on a detecté plus de 2 éléments, la detection est erronée.
            # On ne poursuivra pas, sous peine d'entraîner le monde extérieur dans le chaos
            elif CountObject > NbExpectedObjects:
                #msg = "Detection failed!"
                msg = str(CountObject+1)+";"+msg
                fileLog.write("\n   >>>Result sent : "+str(msg)+"\n")
                #client_socket.send(msg)
                #print "\n   Sent:",msg, "\n"
                #break
            # Il nous reste 0 ou deux objets détectés : on complète le message actuel avec des cibles et directions nulles
            # (l'extérieur s'attend à deux cibles, on les lui donne même si elles sont fantasmées)
            else:
                # Version tuné de msg
                msg_completed = ""
                # ";" est utilisé pour séparer les éléments. On évite un cas ";;" en tête, qui peut apparaître dans boucle qui va suivre
                if msg != "":
                    msg_completed =  ";" + msg
                else:
                    msg_completed = msg
                # Complète jusqu'à nombre attendu avec éléments bidons
                for dumb_i in range(0, NbExpectedObjects - CountObject):
                    # FIXME: utilise code 0 en dur pour cible nulle, mais définir des constantes. Eviterait collisions éventuelles avec Count()
                    msg_completed = msg_completed + ";" + "0|0|undefined"
                # Enrobe comme il faut le message final
                msg = str(NbExpectedObjects+1)+msg_completed
            
            fileLog.write("\n   >>>Result sent : "+str(msg)+"\n")
            client_socket.send(msg)
            print "\n   Sent:",msg, "\n"
        client_socket.send("Connection Break")
        fileLog.write("Connection Break \n")
        print "Connection Break\n"
        server_socket.close()
        fileLog.write("End")
    except Exception, exp:
        exp = str(exp)
        fileLog.write("\n\n\n"+len(exp)*"="+"\nExit with error !!!\n"+len(exp)*"-"+"\n")
        print"Exit with error : \n"
        fileLog.write(str(exp)+"\n"+len(exp)*"="+"\n")
        print"  "+str(exp)+"\n"
    finally:
        fileLog.close()

#---------------< MainProcess >------------------------------------------
#Création d'un fichier Log
fileLog = open("Log_Recognition.log","w")

# Charge les zones à étudier (des masques, converti image en N&B à la volée)
# TODO: redimensionner à la volée suivant Size
for i in range(NB_ZONES):
    Zones.append(Image.open("Zones/" + str(i) + ".tiff").convert("1"))

# Calcule les valeurs des cibles (charge l'image puis l'envoie à VALUE)
for i in range(NB_PATTERNS):
    Patterns.append(VALUE(Image.open("Patterns/" + str(i) + ".tiff")))
    # fileLog.write("Target " + str(i) + " cue:" + str(Patterns[i]))
    print "Target ", i, " cue:", Patterns[i]
    # STATS(Image.open("Patterns/" + str(i) + ".tiff"))

print

FlagEnd = 0
while FlagEnd != 1:
    #Traitement d'une image
    SESSION()

    Ans = ""
    while (Ans != "N" and Ans != "Y"):
        Ans = raw_input("Load other session ? (Y/N) ").upper()
        print "Your choice : ", Ans
    if (Ans == "N"):
        FlagEnd = 1
        print "\nYou can close this process."
    else:
        print "\nLoading..."
