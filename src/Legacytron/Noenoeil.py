# -*- coding: utf-8-*-
import socket
import Image

# Objectif : à des fins de test, se substiter à Choregraphe pour envoyer au serveur de vision des images à analyser

def formatResponse(text):
    #Format string for the neural network
    objects = text.split(";")
    response = "T"
    for x in range(1,len(objects)): #delete target object
        if objects[x].split("|")[1] != "center" :
            response +=  objects[x].split("|")[0] + objects[x].split("|")[1] # renvoi "T+Forms+position "
    return response

class Noeil():
    def __init__(self,PerceptronIP,PerceptronPort):
        
        # Adresse et port pour le serveur de "Perceptron"'
        self.PerceptronIP = PerceptronIP
        self.PerceptronPort = PerceptronPort
        
    def start(self):

        self.client_socket_image = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket_image.connect((self.PerceptronIP,self.PerceptronPort ))   
        #connection a reconnaissance (recorevu)
        
        self.firstTrial = True
        self.motorcommand = "0;0" #movement, hold time
        
    def unload(self):
            # parcouru a la fin du comportement
            self.client_socket_image.close()
            pass
            
    def send(self, PictureName):
        #chargement de l'image
        Img = Image.open(PictureName)
        
        #donnees image (on envoie codée sous forme de chaine de caractères) 
        DataImg = Img.tostring()
        #DataImg = list(Img.tostring())
        #DataImg = list(Img.getdata())
        
        # un jour : conversion
        # Img.convert("L")
        
        # On est très curieux
        print  "\tdimensions : ", Img.size
        print  "\ttaille image : ", len(DataImg)
        print "\tinfos : ", Img.info
        print "\tmode :", Img.mode; "\n"
         
        print "Envoi de la taille de l'image au programme de reconnaissance..."
        self.client_socket_image.send(str(len(DataImg)))
        
        print "Envoi de l'image..."
        self.client_socket_image.send(DataImg)
    
    # Va lire la réponse du serveur
    def read(self):
        print "Réponse du serveur... ",
        listecible=""
        # FIXME: condition nulle, pourrait bloquer bètement ici si serveur retourne autre message
        while len(listecible) < 5 :
            response = self.client_socket_image.recv(512)
            listecible=formatResponse(response)
            if response == "Detection failed!":
                break
        print listecible

# Instanciation de notre oeil-client avec les paramètres adéquats pour trouver serveur vision
Globule = Noeil("127.0.0.1", 6667)

print "Connexion au serveur"
Globule.start()

# répertoire où se trouves les images à tester
REP = "Samples/"
# Préfixe associé à toutes les images
PREFIX = "Patt_"
# extension des images
EXT = ".tiff"

# Utilisateur choisie quelle image de répertoire "Samples" il veut tester
Ans = ""
while (Ans != "Q"):
    Ans = raw_input("\nQuelle numéro d'image envoyer au serveur ('Q' pour quitter) ?\n").upper()
    if Ans != "Q":
        sample = REP + PREFIX + str(Ans) + EXT
        # FIXME: contrôle du type des données
        print "Va charger : ", sample
        Globule.send(sample)
        Globule.read()

Globule.unload()
print "Ciao !"
