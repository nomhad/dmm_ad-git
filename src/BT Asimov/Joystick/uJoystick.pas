unit uJoystick;

{
This unit uses MMSystem which contains constants and calls for getting joystick
positions. The main function used is joyGetPos which returns the results into the
TJoyInfo structure which is defined as follows:

//  PJoyInfo = ^TJoyInfo;
//  {$EXTERNALSYM joyinfo_tag}
//  joyinfo_tag = record
//    wXpos: UINT;                 { x position }
//    wYpos: UINT;                 { y position }
//    wZpos: UINT;                 { z position }
//    wButtons: UINT;              { button states }
//  end;
//  TJoyInfo = joyinfo_tag;
//  {$EXTERNALSYM JOYINFO}
//  JOYINFO = joyinfo_tag;

interface

uses Classes, Types, MMSystem;

type
  TJoystickPosition = (jpCenter, jpRight, jpTop, jpLeft, jpBottom, jpUnknown);

  TJoystick = class(TObject)
  private
    JoyInfo: TJoyInfo;  // VARIABLES POUR LE JOYSTICK
    JoyCaps: TJoyCapsW;
    FOldJoystickPosition: TJoystickPosition;

    FXMax,
    FYMax: integer;
    FScreenPos,
    FCenter,
    FTop,
    FBottom,
    FRight,
    FLeft: TPoint;
    FRadius: integer;
    FVisualTop: integer;
    FVisualLeft: integer;
    FVisualRight: integer;
    FVisualBottom: integer;
    FVisualCenter: TPoint;
    function GetCurrentPosition: TJoystickPosition;
    function GetCurrentPoint: TPoint;
    function GetDisplacement: TPoint;
    function GetAdjustedCurrentPoint: TPoint;
    procedure SetCenter(const Value: TPoint);
    procedure SetVisualCenter(const Value: TPoint);
  public
    constructor Create;
    destructor Destroy;

    property Center: TPoint read FCenter write SetCenter;
    property Top: TPoint read FTop write FTop;
    property Left: TPoint read FLeft write FLeft;
    property Right: TPoint read FRight write FRight;
    property Bottom: TPoint read FBottom write FBottom;
    property Displacement: TPoint read GetDisplacement;

    property VisualCenter: TPoint read FVisualCenter write SetVisualCenter;
    property VisualTop: integer read FVisualTop write FVisualTop;
    property VisualLeft: integer read FVisualLeft write FVisualLeft;
    property VisualRight: integer read FVisualRight write FVisualRight;
    property VisualBottom: integer read FVisualBottom write FVisualBottom;

    procedure SetPostion(WhichPosition: TJoystickPosition);

    //how accurate to be to decide the joystick is in a certain position
    property Accuracy: integer read FRadius write FRadius;
    //current point is the point on the screen the joystick would show the cursor at
    property CurrentPoint: TPoint read GetCurrentPoint;
    //the adjusted current point takes account of the visual settings so that
    //the cursor can be positioned over a specific point at the limits of
    //joystick travel
    property AdjustedCurrentPoint: TPoint read GetAdjustedCurrentPoint;
    //current position is center, top, bottom, left or right
    property CurrentPosition: TJoystickPosition read GetCurrentPosition;
    property PreviousPosition: TJoystickPosition read FOldJoystickPosition write FOldJoystickPosition;
    function PositionToString(APos: TJoystickPosition): string;
  end;

var AJoystick: TJoystick;

implementation

const
  //these are the coordinates when the joystick is sitting in the middle of its field
  CENTER_OFFSET: TPoint = (X: 120; Y: 100);

constructor TJoystick.Create;
begin
  inherited;
  FRadius := 50;
  //the default values mean that you do not have to calibrate the joystick  
  FCenter.X := 250;
  FCenter.Y := 250;
  FTop.X := FCenter.X;
  FTop.Y := 102;
  FBottom.X := FCenter.X;
  FBottom.Y := 400;
  FLeft.X := 95;
  FLeft.Y := FCenter.Y;
  FRight.X := 395;
  FRight.Y := FCenter.Y;
end;

destructor TJoystick.Destroy;
begin

  inherited;
end;

{
Get the coordinates for the joystick current position
}
function TJoystick.GetCurrentPoint: TPoint;
begin
  joyGetPos(joystickid1,@JoyInfo);
  FScreenPos.X := JoyInfo.wxpos div 100;
  FScreenPos.Y := JoyInfo.wypos div 100;
  //adjust for the joystick not sitting naturally in the centeer
  result.X := FScreenPos.X - CENTER_OFFSET.X;
  result.Y := FScreenPos.Y - CENTER_OFFSET.Y;
end;

{
This function allows the coordinates to be adjusted for displacements that are
not equal both sides of the center
}
function TJoystick.GetAdjustedCurrentPoint: TPoint;
var eXScale,
    eYScale: real;
    eScalingFactor: real;
begin
  eXScale := (FVisualRight - FVisualLeft)  / 2;
  eYScale := (FVisualBottom - FVisualTop)  / 2;
  if CurrentPoint.X > FCenter.X then begin
    //joystick is to the right of the center
    eScalingFactor := eXScale / (FRight.X - FCenter.X);
    result.X := FVisualCenter.X + round(Displacement.X * eScalingFactor);
  end
  else begin
    //joystick is to the left of the center
    eScalingFactor := eXScale / (FCenter.X - FLeft.X);
    result.X := FVisualCenter.X + round(Displacement.X * eScalingFactor);
  end;
  if CurrentPoint.Y > FCenter.Y then begin
    //joystick is below the center
    eScalingFactor := eYScale / (FBottom.Y - FCenter.Y);
    result.Y := FVisualCenter.Y + round(Displacement.Y * eScalingFactor);
  end
  else begin
    //joystick is above the center
    eScalingFactor := eYScale / (FCenter.Y - FTop.Y);
    result.Y := FVisualCenter.Y + round(Displacement.Y * eScalingFactor);
  end;
end;

{
This is a fudge to get the general direction that the joystick has been moved
in. If the joystick is within FRadius of the position set, then it is assumed that
the robot moved in that direction
}
function TJoystick.GetCurrentPosition: TJoystickPosition;
begin
  result := jpUnknown;
{
  joyGetPos(joystickid1,@JoyInfo);
  FScreenPos.X := JoyInfo.wxpos div 100;
  FScreenPos.Y := JoyInfo.wypos div 100;
}
  FScreenPos := GetCurrentPoint;

  if (abs(FScreenPos.Y - FCenter.Y) < FRadius)
  and (abs(FScreenPos.X - FCenter.X) < FRadius) then
    //in the center
    result := jpCenter
  else if (abs(FScreenPos.X - FRight.X) < FRadius)
  and (abs(FScreenPos.Y - FCenter.Y) < (2 * FRadius)) then
      //at the right
    result := jpRight
  else if (abs(FScreenPos.X - FLeft.X) < FRadius)
  and (abs(FScreenPos.Y - FCenter.Y) < (2 * FRadius)) then
    //at the left
    result := jpLeft
  else if (abs(FScreenPos.Y - FTop.Y) < FRadius)
  and (abs(FScreenPos.X - FCenter.X) < (2 * FRadius)) then
    //at the top
    result := jpTop
  else if (abs(FScreenPos.Y - FBottom.Y) < FRadius)
  and (abs(FScreenPos.X - FCenter.X) < (2 * FRadius)) then
    //at the bottom
    result := jpBottom;
{
  if abs(FScreenPos.Y - FCenter.Y) < FRadius then begin
    //either left, right or center
    if abs(FScreenPos.X - FCenter.X) < FRadius then
      //in the center
      result := jpCenter
    else if abs(FScreenPos.X - FRight.X) < FRadius then
      //at the right
      result := jpRight
    else if abs(FScreenPos.X - FLeft.X) < FRadius then
      //at the left
      result := jpLeft;
  end
  else if abs(FScreenPos.X - FCenter.X) < FRadius then begin
    //either top, bottom or center
    if abs(FScreenPos.Y - FCenter.Y) < FRadius then
      //in the center
      result := jpCenter
    else if abs(FScreenPos.Y - FTop.Y) < FRadius then
      //at the top
      result := jpTop
    else if abs(FScreenPos.Y - FBottom.Y) < FRadius then
      //at the bottom
      result := jpBottom;
  end;
}  
end;

{
Work out how far the joystick is from the center on the X and Y axes
}
function TJoystick.GetDisplacement: TPoint;
var APoint: TPoint;
begin
  APoint := GetCurrentPoint;
  result.X := APoint.X - FCenter.X;
  result.Y := APoint.Y - FCenter.Y;
end;

procedure TJoystick.SetPostion(WhichPosition: TJoystickPosition);
var APosition: TPoint;
begin
  APosition := GetCurrentPoint;
  case WhichPosition of
    jpCenter: FCenter := APosition;
    jpTop: begin
      FTop.X := FCenter.X;
      FTop.Y := APosition.Y;
    end;
    jpBottom: begin
      FBottom.X := FCenter.X;
      FBottom.Y := APosition.Y;
    end;
    jpLeft: begin
      FLeft.X := APosition.X;
      FLeft.Y := FCenter.Y;
    end;
    jpRight: begin
      FRight.X := APosition.X;
      FRight.Y := FCenter.Y;
    end;
  end;
end;

procedure TJoystick.SetCenter(const Value: TPoint);
begin
  FCenter := Value;
end;

procedure TJoystick.SetVisualCenter(const Value: TPoint);
begin
end;

function TJoystick.PositionToString(APos: TJoystickPosition): string;
begin
  result := PositionString[integer(APos)];
end;

end.
