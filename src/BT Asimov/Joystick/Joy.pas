unit Joy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, Buttons, ToolWin,
  ImgList, uJoystick;

type
  TfrmJoystick = class(TForm)
    sb1: TStatusBar;
    ToolBar1: TToolBar;
    btnGo: TSpeedButton;
    sbExit: TSpeedButton;
    Panel1: TPanel;
    shpTop: TShape;
    shpBottom: TShape;
    shpRight: TShape;
    shpLeft: TShape;
    shpPosition: TShape;
    lblXMin: TLabel;
    lblYMin: TLabel;
    il1: TImageList;
    Panel2: TPanel;
    rgMovement: TRadioGroup;
    GroupBox1: TGroupBox;
    SetTop: TButton;
    SetBottom: TButton;
    SetLeft: TButton;
    SetRight: TButton;
    SetCenter: TButton;
    lblXMax: TLabel;
    lblYMax: TLabel;
    shpCenter: TShape;
    SpeedButton9: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SetTopClick(Sender: TObject);
    procedure SetCenterClick(Sender: TObject);
    procedure SetBottomClick(Sender: TObject);
    procedure SetLeftClick(Sender: TObject);
    procedure SetRightClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnGoClick(Sender: TObject);
    procedure sbExitClick(Sender: TObject);
    procedure rgMovementClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure SpeedButton9Click(Sender: TObject);
  private
    { Déclarations privées }
    boucleOn: boolean;
    FRightScalingFactor,
    FTopScalingFactor,
    FLeftScalingFactor,
    FBottomScalingFactor: real;

    procedure MoveCircle;
  public
    { Déclarations publiques }
  end;

var
  frmJoystick: TfrmJoystick;

implementation

{$R *.dfm}

procedure TfrmJoystick.FormCreate(Sender: TObject);
begin
  BoucleON := false;
end;

procedure TfrmJoystick.FormShow(Sender: TObject);
var APoint: TPoint;
begin
  sb1.Panels[0].Text := 'X Coordinate: ';
  sb1.Panels[1].Text := 'Y Coordinate: ';

  APoint.X := 250;
  APoint.Y := 250;
  AJoystick.VisualCenter := APoint;
  AJoystick.VisualTop := 50;
  AJoystick.VisualBottom := 450;
  AJoystick.VisualLeft := 50;
  AJoystick.VisualRight := 450;
//  btnGoClick(nil);
end;

procedure TfrmJoystick.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  BoucleOn := false;
end;

procedure TfrmJoystick.btnGoClick(Sender: TObject);
begin
  BoucleOn := not BoucleOn;
  if BoucleOn then begin
    il1.GetBitmap(0, btnGo.Glyph);
    btnGo.Hint := 'Stop recording joystick position';
  end
  else begin
    il1.GetBitmap(1, btnGo.Glyph);
    btnGo.Hint := 'Start recording joystick position';
  end;
  while BoucleOn do begin
    MoveCircle;
    Application.ProcessMessages;
  end;
end;

procedure TfrmJoystick.sbExitClick(Sender: TObject);
begin
  BoucleOn := false;
  Close;
end;

procedure tfrmJoystick.MoveCircle;
var APos: TJoystickPosition;
    eX,
    eY: real;
    iX,
    iY,
    iXDisplacement,
    iYDisplacement: integer;
    pDisplacement: TPoint;
begin
  sb1.Panels[0].Text := 'X Coordinate: ' + IntToStr(AJoystick.CurrentPoint.X);
  sb1.Panels[1].Text := 'Y Coordinate: ' + IntToStr(AJoystick.CurrentPoint.Y);
  case rgMovement.ItemIndex of
    0: begin //continuous, move the cursor as the joystick moves
      shpPosition.Left := AJoystick.CurrentPoint.X - (shpPosition.Width div 2);
      shpPosition.Top := AJoystick.CurrentPoint.Y - (shpPosition.Height div 2);
//      shpPosition.Top := CENTER_LEFT + (iJoyY - iY0);
    end;
    1: begin //just move to the current position when it is one of the target positions
      APos := AJoystick.CurrentPosition;
      case APos of
        jpUnknown: begin
          shpPosition.Visible := false;
        end;
        jpCenter: begin
          shpPosition.Visible := true;
          shpPosition.Top := AJoystick.Center.Y - (shpPosition.Height div 2);
          shpPosition.Left := AJoystick.Center.X - (shpPosition.Width div 2);
        end;
        jpLeft: begin
          shpPosition.Visible := true;
          shpPosition.Top := AJoystick.Left.Y - (shpPosition.Height div 2);
          shpPosition.Left := AJoystick.Left.X - (shpPosition.Width div 2);
        end;
        jpRight: begin
          shpPosition.Visible := true;
          shpPosition.Top := AJoystick.Right.Y - (shpPosition.Height div 2);
          shpPosition.Left := AJoystick.Right.X - (shpPosition.Width div 2);
        end;
        jpTop: begin
          shpPosition.Visible := true;
          shpPosition.Top := AJoystick.Top.Y - (shpPosition.Height div 2);
          shpPosition.Left := AJoystick.Top.X - (shpPosition.Width div 2);
        end;
        jpBottom: begin
          shpPosition.Visible := true;
          shpPosition.Top := AJoystick.Bottom.Y - (shpPosition.Height div 2);
          shpPosition.Left := AJoystick.Bottom.X - (shpPosition.Width div 2);
        end;
      end;
    end;
    2: begin
      //adjust the position to take acount of the set positions
      shpPosition.Left := AJoystick.AdjustedCurrentPoint.X - (shpPosition.Width div 2);
      shpPosition.Top := AJoystick.AdjustedCurrentPoint.Y - (shpPosition.Height div 2);
    end;
    3: begin
      //show the current position and turn the shapes pink when a position is reached
      shpPosition.Left := AJoystick.CurrentPoint.X - (shpPosition.Width div 2);
      shpPosition.Top := AJoystick.CurrentPoint.Y - (shpPosition.Height div 2);
      APos := AJoystick.CurrentPosition;
      case APos of
        jpUnknown: begin
          shpCenter.Brush.Color := clWhite;
          shpRight.Brush.Color := clWhite;
          shpTop.Brush.Color := clWhite;
          shpLeft.Brush.Color := clWhite;
          shpBottom.Brush.Color := clWhite;
        end;
        jpCenter: begin
          shpCenter.Brush.Color := clFuchsia;
        end;
        jpLeft: begin
          shpLeft.Brush.Color := clFuchsia;
        end;
        jpRight: begin
          shpRight.Brush.Color := clFuchsia;
        end;
        jpTop: begin
          shpTop.Brush.Color := clFuchsia;
        end;
        jpBottom: begin
          shpBottom.Brush.Color := clFuchsia;
        end;
      end;
    end;
    4: begin
      //adjust the position to take acount of the set positions plus show when
      //the cursor is considered to be in one of the 5 positions
      shpPosition.Left := AJoystick.AdjustedCurrentPoint.X - (shpPosition.Width div 2);
      shpPosition.Top := AJoystick.AdjustedCurrentPoint.Y - (shpPosition.Height div 2);
      APos := AJoystick.CurrentPosition;
      case APos of
        jpUnknown: begin
          shpCenter.Brush.Color := clWhite;
          shpRight.Brush.Color := clWhite;
          shpTop.Brush.Color := clWhite;
          shpLeft.Brush.Color := clWhite;
          shpBottom.Brush.Color := clWhite;
        end;
        jpCenter: begin
          shpCenter.Brush.Color := clFuchsia;
        end;
        jpLeft: begin
          shpLeft.Brush.Color := clFuchsia;
        end;
        jpRight: begin
          shpRight.Brush.Color := clFuchsia;
        end;
        jpTop: begin
          shpTop.Brush.Color := clFuchsia;
        end;
        jpBottom: begin
          shpBottom.Brush.Color := clFuchsia;
        end;
      end;
    end;
  end;
end;

procedure TfrmJoystick.SetCenterClick(Sender: TObject);
begin
//  AJoystick.SetPostion(jpCenter);
//  lblX0.Caption := 'X: ' + IntToStr(AJoystick.Center.X);
//  lblY0.Caption := 'Y: ' + IntToStr(AJoystick.Center.Y);
end;

procedure TfrmJoystick.SetTopClick(Sender: TObject);
begin
  AJoystick.SetPostion(jpTop);
  lblYMin.Caption := IntToStr(AJoystick.Top.Y);
  FTopScalingFactor := 200 / (AJoystick.Center.Y - AJoystick.Top.Y);
end;

procedure TfrmJoystick.SetBottomClick(Sender: TObject);
begin
  AJoystick.SetPostion(jpBottom);
  lblYMax.Caption := IntToStr(AJoystick.Bottom.Y);
  FBottomScalingFactor := 200 / (AJoystick.Bottom.Y - AJoystick.Center.Y);
end;

procedure TfrmJoystick.SetLeftClick(Sender: TObject);
begin
  AJoystick.SetPostion(jpLeft);
  lblXMin.Caption := IntToStr(AJoystick.Left.X);
  FLeftScalingFactor := 200 / (AJoystick.Center.X - AJoystick.Left.X);
end;

procedure TfrmJoystick.SetRightClick(Sender: TObject);
begin
  AJoystick.SetPostion(jpRight);
  lblXMax.Caption := IntToStr(AJoystick.Right.X);
  FRightScalingFactor := 200 / (AJoystick.Right.X - AJoystick.Center.X);
end;

procedure TfrmJoystick.rgMovementClick(Sender: TObject);
begin
  if rgMovement.ItemIndex = 0 then
    shpPosition.Visible := true;
end;

procedure TfrmJoystick.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  BoucleOn := false;
  CanClose := true;
end;

procedure TfrmJoystick.SpeedButton9Click(Sender: TObject);
begin
  boucleOn := false;
  il1.GetBitmap(1, btnGo.Glyph);
  Application.ProcessMessages;
end;

end.
