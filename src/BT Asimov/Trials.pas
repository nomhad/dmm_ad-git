unit Trials;

{
  FIXME: FTrial[FCurrentTrial].Rewarded et  FTrial[FCurrentTrial].ReactionTime
   ne sont pas mis � jour avec le robot.
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, Mask, ImgList, ComObj,
  uJoystick, Buttons, ToolWin, Sockets, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, Math, IdTCPServer, IdException, AdvSmoothTrackBar;

type
  TTarget = record
//    Number: integer;
    Show: boolean;
    Position: integer;
    JoystickPosition: TJoystickPosition;
    RewardProbability: real;
    ImageNumber: integer;
    Image: TImage;
    Picked: boolean;
    PickedCount,
    RewardedCount: integer;
    EffectiveProbability: real;
  end;

  // A simple record to store targets and position recognized by robot
  TTrialPerceptron = record
    Target1,
    Target2,
    // Corresponding positions
    Position1,
    Position2: integer
  end;

  TResults = record
    TrialCount,
    Success,
    Reward,
    Optimal: integer;
  end;

  TTrial = record
    ReactionTime,
    ITI: integer;
    Successful,
    Rewarded: boolean;
    Target1,
    Target2: integer;
    Target: array[0..1] of TTarget;
    TargetPicked: integer;
    Optimum: boolean;
    ErrorCode: integer;
  end;

  TAimTarget = record
    Number: integer;
    Position: integer;
    JPosition: TJoystickPosition;
  end;

//  TClientOnConnected = procedure (Sender: TObject) of Object;

  TfrmTrials = class(TForm)
    sb1: TStatusBar;
    Panel1: TPanel;
    img3: TImage;
    img4: TImage;
    img2: TImage;
    img1: TImage;
    imgCursor: TImage;
    Label1: TLabel;
    imgGreenCircle: TImage;
    imgRedCircle: TImage;
    imgBlackCircle: TImage;
    Memo1: TMemo;
    Memo2: TMemo;
    IdTCPClient1: TIdTCPClient;
    lblTcpModel: TLabel;
    Shape2: TShape;
    Shape4: TShape;
    Shape3: TShape;
    Shape1: TShape;
    ilButtons: TImageList;
    imgBlueCircle: TImage;
    lblError: TLabel;
    lblReward: TLabel;
    lblMoveCursor: TLabel;
    lblFail: TLabel;
    Timer1: TTimer;
    Timer2: TTimer;
    Timer3: TTimer;
    ilTargets: TImageList;
    IdTCPServer1: TIdTCPServer;
    Memo3: TMemo;
    lblTcpRobot: TLabel;
    ToolBar1: TToolBar;
    sbRun: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    btnExcel: TToolButton;
    ToolButton5: TToolButton;
    btnExit: TToolButton;
    ToolButton1: TToolButton;
    tb1: TAdvSmoothTrackBar;
    edRadius: TEdit;
    Label2: TLabel;
    rgCursor: TRadioGroup;
    btnShowTargets: TToolButton;
    btnCursorPosition: TToolButton;
    btnConnect: TToolButton;
    ToolButton4: TToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbRunClick(Sender: TObject);
    procedure sbExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure IdTCPServer1Execute(AThread: TIdPeerThread);
    procedure IdTCPServer1Connect(AThread: TIdPeerThread);
    procedure IdTCPServer1Disconnect(AThread: TIdPeerThread);
    procedure IdTCPClient1Disconnected(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure tb1PositionChanged(Sender: TObject; Position: Double);
    procedure edRadiusChange(Sender: TObject);
    procedure btnShowTargetsClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure btnCursorPositionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnConnectClick(Sender: TObject);
  private
    { D�clarations priv�es }
    FResults: TResults;
    bStop: boolean;

    eOffsetDivider: real;
    iOffset: integer;  //how far from the center to put each target. 1/3 of screen height?

    FCurrentTrial: integer;
    FTrial: array of TTrial;

    // targets recognized by robot for each trial
    FRobotTargets: array of TTrialPerceptron;

//    FPos: TJoystickPosition;
    FAimTarget: TAimTarget;
    FCenter,
    FCursorOffset: TPoint;

    arTarget: array[1..4] of TTarget;
    FarRewardProbability: array[1..4] of real;
    // If false : won't use model (testing purpose, joystick then not used)
    bUseModel: boolean;
    FModelIpAddress: string;
    FRobotAttached: boolean;
    FTrialCount: integer;
    bMovementCompleted,
    FCursorByPosition,
    bTargetsShowing: boolean;
    FDefaultOffset: real;

    FXLApp,
    FXLWorkbook,
    FXLWorksheet: OLEVariant;
    FExcelWorksheet: string;

    FClentConnected: boolean;

//    FOnConnected: TClientOnConnected;

    procedure PositionControls;
    function OpenCommunication: boolean;
    procedure OnClientConnect(Sender: TObject);
    procedure OnClientDisconnect(Sender: TObject);
    procedure EndSession;
    procedure SetJoystickField;
    procedure InitializeTrial;
    procedure ShowTrialStart;
    procedure ShowRedCircle;
    procedure ShowGoSignal;
    procedure ShowBlackCircle;
    procedure ShowBlueCircle;
    procedure PickTargets;// vient s�lectionner quels images � quels emplacements al�atoires
    procedure ShowTargets;
    function TargetsToString: String;
    function SortTargetsString(targetsString: String): String;
    procedure RecordRobotVision(targetsString: string);
    function CheckRobotVision(Targets: string): boolean;
    procedure SendTargetsToModel;
    function GetDirectionToMove: boolean;
    procedure HideTargets;
    function GoToTarget: boolean;
    procedure CheckIfOptimumChosen(TargetChosen: integer);
    function WaitOnTarget(CheckPosition: boolean): boolean;
    function GoHome: boolean;
    function WaitOnCenter: boolean;
//    function GetReward(WhatPosition: TJoystickPosition): boolean;
    function GetReward: boolean;
    procedure SendRewardToModel(Reward: boolean);
    procedure SendFailToModel;
    procedure Wait(HowLong: integer);
    procedure ShowError;

    procedure PositionLabel(ALabel: TLabel);
    procedure CentreHorizontal(AControl: TControl; Offset: integer);
    procedure CentreVertical(AControl: TControl; Offset: integer);
    function ShowCursorPosition: TJoystickPosition;
    procedure SetImagePosition(TargetNumber: integer; Position: integer);

    function CreateExcelWorkbook: boolean;
    procedure SendSetupToExcel;
    procedure SendResultsToExcel;
    function GetRewardProbability(Index: integer): real;
    procedure SetRewardProbability(Index: integer; const Value: real);
  public
    { D�clarations publiques }
    property UseModel: boolean read bUseModel write bUseModel;
    property ModelIpAddress: string read FModelIpAddress write FModelIpAddress;
//    property OnClientConnected: TClientOnConnected read FOnConnected write FOnConnected;
    property RobotAttached: boolean read FRobotAttached write FRobotAttached;
    property TrialCount: integer read FTrialCount write FTrialCount;
    property CursorByPosition: boolean read FCursorByPosition write FCursorByPosition;
    property DefaultOffset: real read FDefaultOffset write FDefaultOffset;
    property RewardProbability[Index: integer]: real read GetRewardProbability write SetRewardProbability;
end;

var
  frmTrials: TfrmTrials;

implementation

{$R *.dfm}

const
  //constants for the images in image list il1 so that images can be loaded into
  //imgCenter as the trial progresses
  IMG_TARGET = 0;
  IMG_GO_SIGNAL = 1;
  IMG_SUCCESS = 2;
  IMG_FAIL = 3;
  IMG_END = 4;
  IMG_WHITE = 5;

  //constants for the error messages when a trial is not completed successfully
  EC_GOOD = 0;
  EC_NO_CENTER = 1;
  EC_ITI_TOO_LONG = 2;
  EC_NO_MOVE = 3;
  EC_NO_TARGET = 4;
  EC_NO_WAIT = 5;
  EC_NO_RETURN = 6;
  EC_NO_WAIT_CENTER = 7;

  //Error messages to display at the end of a trial if all did not go well
  ErrorMessages: array[1..7] of string = ('You did not return the cursor to the center in the time allowed',
                                          'You waited too long to start the trial',
                                          'You did not move from the center within the allowed time',
                                          'You did not move to a target within the allowed time',
                                          'You did not keep the cursor on the target long enough',
                                          'You did not return to the center within the allowed time',
                                          'You did not keep the cursor on the center long enough');

  ROW_HEADER = 1;
  xlWBATWorksheet = -4167;
  NO_YES: array[0..1] of string = ('No','Yes');

function TfrmTrials.ShowCursorPosition: TJoystickPosition;
var APoint: TPoint;
begin
  Application.ProcessMessages;
  APoint := AJoystick.AdjustedCurrentPoint;
  result := AJoystick.CurrentPosition;
  if rgCursor.ItemIndex = 1 then begin
//  if FCursorByPosition then begin
    //just show the cursor when it is in the center or on one of the targets.
    //This helps with the very shaky joystick!
    case result of
      jpCenter: begin
        CentreHorizontal(imgCursor,0);
        CentreVertical(imgCursor,0);
      end;
      jpRight: begin
        CentreHorizontal(imgCursor,iOffset);
        CentreVertical(imgCursor,0);
      end;
      jpTop: begin
        CentreHorizontal(imgCursor,0);
        CentreVertical(imgCursor,iOffset * -1);
      end;
      jpLeft: begin
        CentreHorizontal(imgCursor,iOffset * -1);
        CentreVertical(imgCursor,0);
      end;
      jpBottom: begin
        CentreHorizontal(imgCursor,0);
        CentreVertical(imgCursor,iOffset);
      end;
    end;
  end
  else begin
    //show the actual position of the cursor
    imgCursor.Left := APoint.X - (imgCursor.Width div 2) + FCursorOffset.X;
    imgCursor.Top := APoint.Y - (imgCursor.Height div 2) + FCursorOffset.Y;
  end;
  sb1.Panels[0].Text := 'X Coordinate: ' + IntToStr(APoint.X);
  sb1.Panels[1].Text := 'Y Coordinate: ' + IntToStr(APoint.Y);
  sb1.Panels[2].Text := 'Position: ' + AJoystick.PositionToString(result);
  Application.ProcessMessages;
//  Refresh;
end;

procedure TfrmTrials.FormShow(Sender: TObject);
var i: integer;  //how far from the center to put each target. 1/3 of screen height?
    AnImage: TImage;
begin
  Randomize;
  for i := 1 to 4 do
    arTarget[i].RewardProbability := FarRewardProbability[i];

  eOffsetDivider := FDefaultOffset;
  tb1.Position := eOffsetDivider;
  PositionControls;
  imgCursor.Visible := false;

  for i := 1 to 4 do begin
    AnImage := TImage(FindComponent('img' + IntToStr(i)));
    ilTargets.GetBitmap(i-1, AnImage.Picture.Bitmap);
    AnImage.Visible := false;
  end;

  SetImagePosition(1, 1);
  SetImagePosition(2, 2);
  SetImagePosition(3, 3);
  SetImagePosition(4, 4);

  arTarget[1].Image := img1;  //circle
  arTarget[2].Image := img2;  //square
  arTarget[3].Image := img3;  //rhombus
  arTarget[4].Image := img4;  //triangle

  bTargetsShowing := false;

  // Enable model connection if we use it
  if bUseModel then begin
    btnConnectClick(nil);
  end
  // Otherwise the program is only driven by its server component
  else begin
    btnConnect.Enabled := false;
    sbRun.Enabled := false;
  end;

  IdTcpServer1.Active := true;
  bStop := true;
  SetJoystickField;

end;

procedure TfrmTrials.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if bUseModel then
    IdTCPClient1.DisconnectSocket;
  IdTcpServer1.Active := false;
end;

procedure TfrmTrials.sbExitClick(Sender: TObject);
begin
  bStop := true;
  Close;
end;

{
  Listen to events from robot.

  NB: when trial number is reached, will reset FCurrentTrial counter. If robot
  keep asking for new target ('N'), each following target codign string will
  overwrite a previous oner. You don't want that.
}
procedure TfrmTrials.IdTCPServer1Execute(AThread: TIdPeerThread);
var sRec: string;
    sDirection: string;
//    APos: TJoystickPosition;
    bReward: boolean;
begin
  sRec := AThread.Connection.ReadLn;
//  memo1.Clear;
  memo3.Lines.Add('Robot sends: ' + sRec);
  ShowCursorPosition;
  imgCursor.Visible := true;
  bStop := false;
  if sRec = 'N' then begin
    {New trial
     Show two targets on the screeen then wait for the robot to recognize them
     with the vision analysis and send back their positions
    }
    inc(FCurrentTrial);
    if FCurrentTrial = FTrialCount then begin
      //done all the trials, send the finish message
      AThread.Connection.WriteLn('F');
      memo3.Lines.Add('Send F to robot');
      FCurrentTrial := -1;
      bStop := true;
    end
    else begin
      memo1.Lines.Add('Trial number: ' + IntToStr(FCurrentTrial));
      Panel1.Repaint;
      InitializeTrial;
      PickTargets;
//      ShowTrialStart;
      ShowBlackCircle;
      ShowTargets;
      Wait(2000);
      //say that the targets are on the screen
      AThread.Connection.WriteLn('T');
      memo3.Lines.Add('Send T to robot');
      bMovementCompleted := false;
    end;
  end
  else if sRec[1] = 'T' then begin
    //The robot has detected the positions of the targets
    bMovementCompleted := false;
    // Record what is seeing the robot
    RecordRobotVision(sRec);
    if CheckRobotVision(sRec) then begin
      if bUseModel then
        SendTargetsToModel;
      if bUseModel and GetDirectionToMove then begin
        ShowGoSignal;
        //The direction is put into the global variable FAimTarget.JPosition
        sDirection := IntToStr(FAimTarget.Position);
        AThread.Connection.WriteLn(sDirection);
        memo3.Lines.Add('Send direction ' + sDirection + ' to robot');
        if GoToTarget then begin
          //cursor has moved from center and reached one of the displayed targets
//          if WaitOnTarget(false) then begin
            //waited long enough on the target, move back to the center
            Wait(1000);
            ShowRedCircle;
            if GoHome then begin
              //got back to the center
              ShowBlueCircle;
              bMovementCompleted := true;
            end
            else begin
              //did not move back to the center
              SendRewardToModel(false);
            end;
//          end
//          else begin
            //did not wait on target
//            SendRewardToModel(false);
//          end;
        end
        else begin
          //Model chose a target but did not move to the selected target
          if bUseModel then
            SendFailToModel;
        end;
      end
      else begin
        //did not get a direction from the model, so tell the robot not to move
        AThread.Connection.WriteLn('0');
        memo3.Lines.Add('Send direction 0 to robot');
      end;
    end
    else begin
      //did not get correct targets, so tell the robot not to move
      AThread.Connection.WriteLn('0');
      memo3.Lines.Add('Send direction 0 to robot');
    end;
  end
  else if sRec = 'M' then begin
    //movement has been completed by the robot
    //check if it was done correctly
    if bMovementCompleted then begin
//      if WaitOnCenter then begin
        //trial successfully completed
        FResults.Success := FResults.Success + 1;
        HideTargets;
        bReward := GetReward;
        if bUseModel then
          SendRewardToModel(bReward);
        //send back the reward value: 0 or 1
        AThread.Connection.WriteLn('R' + IntToStr(integer(bReward)));
        memo3.Lines.Add('Send R'+ IntToStr(integer(bReward)) + ' to robot');
        ShowError;
{
      end
      else begin
        //did not stay on the center long enough
        AThread.Connection.WriteLn('R0');
        memo3.Lines.Add('Send R0 to robot');
      end;
}
      bMovementCompleted := false;
    end
    else begin
      //there was an error somewhere in moving to the target or moving back
      AThread.Connection.WriteLn('R0');
      memo3.Lines.Add('Send R0 to robot');
    end;
  end
  else begin
    //something went wrong. Hide all targets and shapes
    HideTargets;
  end;
end;

procedure TfrmTrials.sbRunClick(Sender: TObject);
var i: integer;
//    APos: TJoystickPosition;
    bReward: boolean;
begin
  bStop := false;
  memo1.Clear;
  SetLength(FTrial, FTrialCount);
  SetLength(FRobotTargets, FTrialCount);
  FResults.TrialCount := FTrialCount;
  FResults.Success := 0;
  FResults.Reward := 0;
  SetJoystickField;
  HideTargets;
  imgBlackCircle.Visible := true;
  ShowCursorPosition;
  imgCursor.Visible := true;
  if OpenCommunication then begin
    for i := 0 to pred(FTrialCount) do begin
      FCurrentTrial := i;
      if bStop then
        EXIT;
      memo1.Lines.Add('Trial number: ' + IntToStr(i+1));
      Panel1.Repaint;
      InitializeTrial;
      PickTargets;
      ShowTrialStart;
      ShowBlackCircle;
      ShowTargets;
      SendTargetsToModel;
      if GetDirectionToMove then begin
        ShowGoSignal;
        if GoToTarget then begin
          //cursor has moved from center and reached one of the displayed targets
          if WaitOnTarget(true) then begin
            //waited long enough on the target, move back to the center
            ShowRedCircle;
            if GoHome then begin
              //got back to the center
              ShowBlueCircle;
              if WaitOnCenter then begin
                //trial successfully completed
                FResults.Success := FResults.Success + 1;
                bReward := GetReward;
                SendRewardToModel(bReward);
              end
              else begin
                //did not wait on center long enough
                //so send reward = 0
                SendRewardToModel(false);
              end;
            end
            else begin
              //did not go home, but had chosen and moved to a target
              //so send reward = 0
              SendRewardToModel(false);
            end;
          end
          else begin
            //did not stay on target long enough, but had chosen a target
            //so send reward = 0
            SendRewardToModel(false);
          end;
        end
        else begin
          //did not move to a target. Cannot send R0 or R1, so send that move
          //was not made
          SendFailToModel;
        end;
      end;
      HideTargets;
      ShowError;
//      Wait(2000 + random(500));
    end;
    EndSession;
    memo1.Lines.Add('Set of trials finished');
    memo1.Lines.Add('Number of trials: ' + IntToStr(FResults.TrialCount));
    memo1.Lines.Add('Number of sucesses: ' + IntToStr(FResults.Success));
    memo1.Lines.Add('Number of rewards: ' + IntToStr(FResults.Reward));
  end;
end;

{
Want the joystick to be able to move to just outside the targets (left, right,
top and bottom)
}
procedure TfrmTrials.SetJoystickField;
var iWidth,
    iHeight: integer;
begin
  FCursorOffset.X := Shape3.Left - 50;
  FCursorOffset.Y := Shape2.Top - 50;
  iWidth := (Shape1.Left + Shape1.Width + 50) - (Shape3.Left - 50);
  iHeight := (Shape4.Top + Shape4.Height + 50) - (Shape2.Top - 50);
  AJoystick.FieldWidth := iWidth;
  AJoystick.FieldHeight := iHeight;
end;

procedure TfrmTrials.InitializeTrial;
begin
  FTrial[FCurrentTrial].ITI := -1;
  FTrial[FCurrentTrial].Successful := false;
  FTrial[FCurrentTrial].Rewarded := false;
  FTrial[FCurrentTrial].ReactionTime := -1;
  FTrial[FCurrentTrial].TargetPicked := -1;
  FTrial[FCurrentTrial].ErrorCode := EC_GOOD;
end;

{
Display the targets in random positions for the current trial and record them in FTrial.

Will sort targets internally in FTrial (iTarget1's position index inferor to iTarget2's one).
  No impact except when externalizing data, easier to process then.
}
procedure TfrmTrials.PickTargets;
var i: integer;
    iTarget1,
    iTarget2: integer;
begin
  for i := 1 to 4 do begin
//    AnImage := TImage(FindComponent('img' + IntToStr(i)));
    //hide all four images. show them only at the correct time during the trial
    arTarget[i].Image.Visible := false;
    arTarget[i].Show := false;
    arTarget[i].Picked := false;
    arTarget[i].Position := -1;
  end;

  //get which two targets to show for this trial
  iTarget1 := RandomRange(1,5);
  repeat
    iTarget2 := RandomRange(1,5);
  until iTarget1 <> iTarget2;

  arTarget[iTarget1].Show := true;
  arTarget[iTarget2].Show := true;

  //then work out which positions to show them in
  arTarget[iTarget1].Position := RandomRange(1,5);
  //joystick positions start with jpCenter, so the joystick position is 1 more than the actual position
  arTarget[iTarget1].JoystickPosition := TJoystickPosition(arTarget[iTarget1].Position);
  repeat
    arTarget[iTarget2].Position := RandomRange(1,5);
  until arTarget[iTarget1].Position <> arTarget[iTarget2].Position;
  arTarget[iTarget2].JoystickPosition := TJoystickPosition(arTarget[iTarget2].Position);

  SetImagePosition(iTarget1, arTarget[iTarget1].Position);
  SetImagePosition(iTarget2, arTarget[iTarget2].Position);

  // Swap targets if needed to sort following position index
  // NB: not changing iTarget1 and iTarget2 values in case another treatment follows
  if arTarget[iTarget1].Position < arTarget[iTarget2].Position then begin
    FTrial[FCurrentTrial].Target1 := iTarget1;
    FTrial[FCurrentTrial].Target2 := iTarget2;
    FTrial[FCurrentTrial].Target[0] := arTarget[iTarget1];
    FTrial[FCurrentTrial].Target[1] := arTarget[iTarget2];
  end
  else begin
    FTrial[FCurrentTrial].Target1 := iTarget2;
    FTrial[FCurrentTrial].Target2 := iTarget1;
    FTrial[FCurrentTrial].Target[0] := arTarget[iTarget2];
    FTrial[FCurrentTrial].Target[1] := arTarget[iTarget1];
  end;

  memo1.Lines.Add('Target 1: ' + IntToStr(iTarget1));
//  memo1.Lines.Add('First target position: ' + IntToStr(arTarget[iTarget1].Position));
//  memo1.Lines.Add('P(R) first target: ' + FloatToStrF(arTarget[iTarget1].RewardProbability, ffFixed, 3, 2));
  memo1.Lines.Add('Target 2: ' + IntToStr(iTarget2));
//  memo1.Lines.Add('Second target position: ' + IntToStr(arTarget[iTarget2].Position));
//  memo1.Lines.Add('P(R) second target: ' + FloatToStrF(arTarget[iTarget2].RewardProbability, ffFixed, 3, 2));

//  Refresh;
end;

procedure TfrmTrials.SetImagePosition(TargetNumber, Position: integer);
var AnImage: TImage;
begin
//  AnImage := arTarget[TargetNumber].Image;
  AnImage := TImage(FindComponent('img'+ IntToStr(TargetNumber)));
  case Position of
    1: begin
      AnImage.Left := FCenter.X + iOffset - (AnImage.Width div 2);
      AnImage.Top := FCenter.Y - (AnImage.Height div 2);
    end;
    2: begin
      AnImage.Left := FCenter.X - (AnImage.Width div 2);
      AnImage.Top := FCenter.Y - iOffset - (AnImage.Height div 2);
    end;
    3: begin
      AnImage.Left := FCenter.X - iOffset - (AnImage.Width div 2);
      AnImage.Top := FCenter.Y - (AnImage.Height div 2);
    end;
    4: begin
      AnImage.Left := FCenter.X - (AnImage.Width div 2);
      AnImage.Top := FCenter.Y + iOffset - (AnImage.Height div 2);
    end;
  end;
end;

{
At the start of each trial a green circle surrounded by a black hollow circle
with the black cross cursor is shown for a random period between 1 and 1.5 seconds
}
procedure TfrmTrials.ShowTrialStart;
var iHowLong: integer;
begin
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := true;
  imgBlackCircle.Visible := true;
  imgBlueCircle.Visible := false;
  memo1.Lines.Add('Trial started');
  Application.ProcessMessages;
  iHowLong := 1000 + random(500);
  Wait(iHowLong);
  memo1.Lines.Add('Waited ' + IntToStr(iHowLong) + ' ms');
end;

{
After  the solid green circle a hollow black circle, with the cursor inside is
shown for a random period between 1 and 1.5 seconds
}
procedure TfrmTrials.ShowBlackCircle;
var iHowLong: integer;
begin
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := false;
  imgBlackCircle.Visible := true;
  imgBlueCircle.Visible := false;
  Application.ProcessMessages;
  memo1.Lines.Add('Showing black circle');
  iHowLong := 1500 + random(500);
  Wait(iHowLong);
  memo1.Lines.Add('Waited ' + IntToStr(iHowLong) + ' ms');
end;

procedure TfrmTrials.ShowBlueCircle;
//var iHowLong: integer;
begin
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := false;
  imgBlackCircle.Visible := false;
  imgBlueCircle.Visible := true;
  Application.ProcessMessages;
//  iHowLong := 1500 + random(500);
//  Wait(iHowLong);
end;

{
After the hollow black circle is shown for 1 to 1.5 seconds, the 2 targets
are displayed for 1.5 to 2 seconds before the go signal is given
}
procedure TfrmTrials.ShowTargets;
var i: integer;
    AnImage: TImage;
    sTarget: string;
begin
  sTarget := 'T';
  for i := 1 to 4 do begin
    if arTarget[i].Show then begin
      AnImage := arTarget[i].Image;
      AnImage.Visible := true;
      sTarget := sTarget + IntToStr(i);
    end;
  end;
  memo1.Lines.Add('Targets shown: ' + sTarget + ' (code: ' + TargetsToString + ')');
  Application.ProcessMessages;
end;

{
The basal ganglia model decides which target to move to and sends it back to this
program. When the direction is received, the target in that direction is
highlighted. Then, either the robot or a human with a joystick, moves the cursor
to (hopefully) the target chosen
}
function TfrmTrials.GetDirectionToMove: boolean;
var s: string;
    i,
    iTarget: integer;
begin
  result := false;
  s := IdTCPClient1.ReadLnWait(1000);
  Memo2.Lines.Add('Model sends direction: ' + s);
  i := StrToInt(s);
  FAimTarget.Position := i;
  FAimTarget.JPosition := TJoystickPosition(i);
  FAimTarget.Number := 0;
  for iTarget := 1 to 4 do begin
    if arTarget[iTarget].Position = FAimTarget.Position then begin
      FAimTarget.Number := iTarget;
    end;
  end;
  if FAimTarget.Number = 0 then
    ShowMessage('Target position chosen did not have a corresponding number');
  if i in [1..4] then begin
    //a valid direction was picked
    TShape(FindComponent('Shape' + IntToStr(i))).Visible := true;
    result := true;
  end
  else begin
    //no valid direction, but we don't know why. Should be zero.
    FTrial[FCurrentTrial].ErrorCode := EC_NO_TARGET;
    Memo1.Lines.Add('Did not get a valid direction from the model');
  end;
  Application.ProcessMessages;
end;

{
The go signal is a green circle with the black cross cursor
}
procedure TfrmTrials.ShowGoSignal;
var iHowLong: integer;
begin
  //wait after presentation of the targets before the go signal is shown
  iHowLong := 500 + random(500);
  Wait(iHowLong);
  imgRedCircle.Visible := false;
  imgGreenCircle.Visible := true;
  imgBlackCircle.Visible := false;
  imgBlueCircle.Visible := false;
  memo1.Lines.Add('Go signal shown');
  Application.ProcessMessages;
end;

{
After the go signal have to move to one of the targets in a given amount of time
Show the black cross cursor moving
In this version, the user (or robot) is told which position to go to. So we
only need to monitor that position to see if they do go there in the time
allowed.
Allow 2.5s for the movement
}
function TfrmTrials.GoToTarget: boolean;
var iStart,
    iHowLong: cardinal;
begin
  result := false;
  iStart:= GetTickCount;
  iHowLong := 5000;// + random(500);
  while ShowCursorPosition <> FAimTarget.JPosition do begin
    Application.ProcessMessages;
    if (GetTickCount - iStart) > iHowLong then begin
      //taken too long to move
      FTrial[FCurrentTrial].ErrorCode := EC_NO_TARGET;
      memo1.Lines.Add('Did not move to target in required time');
      HideTargets;
      EXIT;
    end;
  end;
  memo1.Lines.Add('Moved to target ' + IntToStr(FAimTarget.Number));
  arTarget[FAimTarget.Number].Picked := true;
  FTrial[FCurrentTrial].TargetPicked := FAimTarget.Number;
  CheckIfOptimumChosen(FAimTarget.Number);
  result := true;
end;

{
After the cursor has been moved to a target, check if it is the optimum target
The optimum target is the target with the highest probability of reward.
If both targets have the same P(R), then the optimum has been chosen by
definition
}
procedure TfrmTrials.CheckIfOptimumChosen(TargetChosen: integer);
var bOptimum: boolean;
begin
  if FTrial[FCurrentTrial].Target[0].RewardProbability = FTrial[FCurrentTrial].Target[1].RewardProbability then begin
    //both targets have the same P(R)
    FTrial[FCurrentTrial].Optimum := true;
    FResults.Optimal := FResults.Optimal + 1;
    EXIT;
  end;

  if FTrial[FCurrentTrial].Target[0].RewardProbability > FTrial[FCurrentTrial].Target[1].RewardProbability then begin
    bOptimum := FTrial[FCurrentTrial].Target1 = TargetChosen;
  end
  else begin
    bOptimum := FTrial[FCurrentTrial].Target2 = TargetChosen;
  end;
  FTrial[FCurrentTrial].Optimum := bOptimum;
  if bOptimum then
    FResults.Optimal := FResults.Optimal + 1;
end;

{
After the circle turns green, the cursor is moved to one of the targets. It then
has to remain on that target for a between 0.5 and 1s before the central
circle turns red to signal to move the cursor back to the center
}
function TfrmTrials.WaitOnTarget(CheckPosition: boolean): boolean;
var i,
    iStart,
    iHowLong: cardinal;
    AShape: TShape;
begin
  result := false;
  iStart:= GetTickCount;
//  iHowLong := 1000 + random(500);
  iHowLong := 800;
  memo1.Lines.Add('Waiting on target');
  Application.ProcessMessages;
  while GetTickCount < (iStart + iHowLong) do begin
    if ShowCursorPosition <> FAimTarget.JPosition then begin
      if CheckPosition then begin
        //have moved from the position
        memo1.Lines.Add('Did not stay on target long enough');
        HideTargets;
        EXIT;
      end;
    end;
    Application.ProcessMessages;
  end;
  result := true;
  memo1.Lines.Add('Stayed on target');
  for i := 1 to 4 do begin
    AShape := TShape(FindComponent('Shape'+IntToStr(i)));
    AShape.Visible := false;
  end;
end;

{
After the cursor has been held on the target long enough, the central circle is
changed from green to red to signal the start of the go home phase
}
procedure TfrmTrials.ShowRedCircle;
begin
  imgRedCircle.Visible := true;
  imgGreenCircle.Visible := false;
  imgBlackCircle.Visible := false;
  memo1.Lines.Add('Start go home');
  Application.ProcessMessages;
end;

{
After the circle turns red, the cursor has to be moved back to the central circle
within a set time
}
function TfrmTrials.GoHome: boolean;
var i,
    iStart,
    iHowLong: cardinal;
begin
  result := false;
  iStart:= GetTickCount;
  iHowLong := 5000;// + random(500);
  while ShowCursorPosition <> jpCenter do begin
    Application.ProcessMessages;
    if (GetTickCount - iStart) > iHowLong then begin
      //taken too long to move
      FTrial[FCurrentTrial].ErrorCode := EC_NO_RETURN;
      for i := 1 to 4 do
        arTarget[i].Picked := false;
      memo1.Lines.Add('Did not return to center in time allowed');
      HideTargets;
      EXIT;
    end;
  end;
  memo1.Lines.Add('Reached center');
  result := true;
end;

{
When the cursor has been moved back to the center, it has to be held there for
0.8 to 1.2s
}
function TfrmTrials.WaitOnCenter: boolean;
var i,
    iStart,
    iHowLong: cardinal;
begin
  result := false;
  iStart:= GetTickCount;
  iHowLong := 800 + random(400);
  memo1.Lines.Add('Waiting on center');
  Application.ProcessMessages;
  while GetTickCount < (iStart + iHowLong) do begin
    if ShowCursorPosition <> jpCenter then begin
      //have moved from the center, so failed to hold long enough
      memo1.Lines.Add('Moved off center');
      for i := 1 to 4 do
        arTarget[i].Picked := false;
      HideTargets;
      FTrial[FCurrentTrial].ErrorCode := EC_NO_WAIT_CENTER;
      EXIT;
    end;
    Application.ProcessMessages;
  end;
  result := true;
end;

{
After each trial need to hide the targets and then wait 0.8 to 1.2 seconds
}
procedure TfrmTrials.HideTargets;
var i,
    iHowLong: integer;
    AnImage: TImage;
begin
  for i := 1 to 4 do begin
    if arTarget[i].Show then begin
      AnImage := arTarget[i].Image;
      AnImage.Visible := false;
    end;
    TShape(FindComponent('Shape'+IntToStr(i))).Visible := false;
  end;
  Application.ProcessMessages;
  memo1.Lines.Add('Targets hidden');
  iHowLong := 800 + random(400);
  Wait(iHowLong);
end;

{
If a displayed target was chosen, decide whether to give a reward or not based
on the probability assigned to that target and send whether a reward has been
obtained or not to the learning model.
If a target is chosen but then an error is made (such as not moving back to the
center), call this procedure with Reward = false so that the weights will be
updated.
In this version, we know which target was chosen because the model told us to
choose that target, so we send that as a parameter
}
procedure TfrmTrials.SendRewardToModel(Reward: boolean);
begin
  IdTCPClient1.WriteLn('R' + IntToStr(integer(Reward)));
  memo2.Lines.Add('Reward = ' + IntToStr(integer(Reward)) + ' sent');
end;

{
If the user (or robot) does not move to the target in time, the trial has failed,
but we cannot send a reward signal as we do not want the weights to be updated
}
procedure TfrmTrials.SendFailToModel;
begin
  IdTCPClient1.WriteLn('F');
  memo2.Lines.Add('Trial failed. F sent');
end;

{
Let the user know what they did wrong by flashing a message on the screen
}
procedure TfrmTrials.ShowError;
begin
  if FTrial[FCurrentTrial].ErrorCode > EC_GOOD then begin
    //Don't show a message if everything went OK
    lblError.Caption := ErrorMessages[FTrial[FCurrentTrial].ErrorCode];
    lblError.Left :=  imgBlackCircle.Left + (imgBlackCircle.Width div 2)
                   - (lblError.Width  div 2);
    Timer3.Enabled := true;
    Wait(3000);
    Timer3.Enabled := false;
    lblError.Visible := false;
  end;
end;

{
Procedure to wait for a set number of milliseconds
}
procedure TfrmTrials.Wait(HowLong: integer);
var iStart,
    iNow: integer;
begin
  iStart:= GetTickCount;
  repeat
    ShowCursorPosition;
    iNow := GetTickCount;
    Application.ProcessMessages;
    if bStop then
      //if the stop button is pressed, get out
      EXIT;
  until iNow >= (iStart + HowLong);
//  Refresh;
end;

procedure TfrmTrials.ToolButton2Click(Sender: TObject);
begin
  bStop := true;
end;

procedure TfrmTrials.PositionControls;
begin
  iOffset := round(Screen.Height / eOffsetDivider);

  //get the center point of the screen to position controls dynamically
  FCenter.X := (Screen.Width div 2);// - (imgCursor.Width div 2);
  FCenter.Y := (Screen.Height div 2);//  - (imgCursor.Height div 2);

  CentreHorizontal(Shape1, iOffset); //right
  CentreVertical(Shape1, 0);
  CentreHorizontal(Shape2, 0);
  CentreVertical(Shape2, iOffset * -1);  //top
  CentreHorizontal(Shape3, iOffset * -1);  //left
  CentreVertical(Shape3, 0);
  CentreHorizontal(Shape4, 0);
  CentreVertical(Shape4, iOffset); //bottom

  CentreHorizontal(imgRedCircle, 0);
  CentreVertical(imgRedCircle, 0);
  CentreHorizontal(imgGreenCircle, 0);
  CentreVertical(imgGreenCircle, 0);
  CentreHorizontal(imgBlackCircle, 0);
  CentreVertical(imgBlackCircle, 0);
  CentreHorizontal(imgBlueCircle, 0);
  CentreVertical(imgBlueCircle, 0);

  PositionLabel(lblMoveCursor);
  PositionLabel(lblReward);
  PositionLabel(lblFail);
  PositionLabel(lblError);
end;

procedure TfrmTrials.PositionLabel(ALabel: TLabel);
begin
  ALabel.Left := FCenter.X - (ALabel.Width div 2);
  ALabel.Top := FCenter.Y - 100;
end;

procedure TfrmTrials.btnExitClick(Sender: TObject);
begin
  bStop := true;
  Close;
end;

{
At the start of a session send 'S' to the model to let it know this a new
session and that it needs to open a new Excel workbook
}
function TfrmTrials.OpenCommunication: boolean;
var sReply: string;
begin
  result := false;
  //send the start signal to the model
  IdTCPClient1.WriteLn('S');
  sReply := IdTCPClient1.ReadLnWait(10000);
  if sReply = 'OK' then begin
    memo2.Lines.Add(sReply);
    result := true;
  end
  else
    memo2.Lines.Add('No reply from server');
end;

{
At the end of the session, tell the model that the session is finished so that
the model setup can be sent to Excel
}
procedure TfrmTrials.EndSession;
begin
  IdTCPClient1.WriteLn('End');
end;

{
  Return a string coding for the targets shown of screen.

  Arbitrary sorting order : target with least position index first (see SortTargetsString)
}
function TfrmTrials.TargetsToString:String;
var
  sTargets: String;
  i: integer;
begin
  sTargets := 'T';
  for i := 1 to 4 do begin
    if arTarget[i].Show then begin
      //construct the string which was sent to the model
      sTargets := sTargets + IntToStr(i) + IntToStr(arTarget[i].Position);
    end;
  end;
  result := SortTargetsString(sTargets);
end;

{
  Since targets order can be different in the string used for TCP communication,
  sort it with smalletst position index first.

  targetsString : string coding 2 targets and 2 position
    T[T1][P1][T2][P2]

  if length <> 5, result = targetsString
}
function TfrmTrials.SortTargetsString(targetsString: String):String;
begin
  result := targetsString;
  // If first target's position higher
  if (Length(targetsString) = 5) and (targetsString[3] > targetsString[5]) then
  begin
    // invert targets and positions
    // err...is it bad to use "result" as a temporary variable ?
    result[2] := targetsString[4];
    result[4] := targetsString[2];
    // invert positions
    result[3] := targetsString[5];
    result[5] := targetsString[3];
  end;
end;

{
  To compute statistics regarding performance of robot vision, store its responses in an array.

  targetsString: string coding for targets. If its size differ from 5, will
    consider it as a total failure (0 stored for both targets and directions)

  NB: if the server receives several strings coding for targets in a row, the last
    one will overwrite previous.
}
procedure TfrmTrials.RecordRobotVision(targetsString: String);
begin
  // If obiously not what we expect, give it a 0 for every aspects
  if Length(targetsString) <> 5 then
    targetsString := 'T0000'
  else
    // Sort targets to make comparison easier when processing data
    targetsString := SortTargetsString(targetsString);
  // Store targets, then positions.
  FRobotTargets[FCurrentTrial].Target1 := StrToInt(targetsString[2]);
  FRobotTargets[FCurrentTrial].Position1 := StrToInt(targetsString[3]);
  FRobotTargets[FCurrentTrial].Target2 := StrToInt(targetsString[4]);
  FRobotTargets[FCurrentTrial].Position2 := StrToInt(targetsString[5]);
end;

{
Check if the robot has detected the same targets in the same positions as were
shown (both strings are sorted identically).
}
function TfrmTrials.CheckRobotVision(Targets: string): boolean;
var
  sTargets, sRobotTargets: string;
begin
  // get current targets coded by a string
  sTargets := TargetsToString;
  // sort targets sent by robot the same way
  sRobotTargets := SortTargetsString(Targets);

  memo1.Lines.Add(sTargets + ' sent to robot');
  memo1.Lines.Add(Targets + ' received from robot' + ' (' +  sRobotTargets + ' sorted)');
  
  result := (sTargets = sRobotTargets);
  if result then
    memo1.Lines.Add('Robot correctly identified targets')
  else
    memo1.Lines.Add('Robot did not identify targets');
end;

{
When the robot has detected the correct targets, tell the  model to choose a direction
}
procedure TfrmTrials.SendTargetsToModel;
var sTarget,
    sReceived: string;
begin
  sTarget := TargetsToString; 
//  Wait(1500 + Random(500));
  //send the string to the BG model
  IdTCPClient1.WriteLn(sTarget);
  memo2.Lines.Add(sTarget + ' sent to BG model');

  //wait for a reply from the model saying that the targets were received
  sReceived := IdTCPClient1.ReadLnWait(10000);
  memo2.Lines.Add('From model: ' + sReceived);

  memo1.Lines.Add('Targets sent to model');
end;

{
This is called when the robot, acting as a TCP client, connect to the task server.
It opens communications with the BG model, which is also acting as a TCP server.
Doing this initializes the weights in the model.

FIXME: a new client will call a second time this function and probably bother
first one's experiment if not finished.
}
procedure TfrmTrials.IdTCPServer1Connect(AThread: TIdPeerThread);
begin
  memo1.Clear;
  memo3.Lines.Add('Connected');
  SetLength(FTrial, FTrialCount);
  SetLength(FRobotTargets, FTrialCount);
  FResults.TrialCount := FTrialCount;
  FResults.Success := 0;
  FResults.Reward := 0;
  SetJoystickField;
  HideTargets;
  imgBlackCircle.Visible := true;
  // incremented right after the first 'N' is received.
  FCurrentTrial := -1;
  if bUseMOdel and not OpenCommunication then begin
    memo2.Lines.Add('No reply from server');
  end;
  AThread.Connection.WriteLn('OK');
end;

function TfrmTrials.GetReward: boolean;
var iTarget: integer;
begin
  result := false;
  memo1.Lines.Add('Successful trial');
  iTarget := FTrial[FCurrentTrial].TargetPicked;
  memo1.Lines.Add('Reward probability: ' + FloatToStrF(arTarget[iTarget].RewardProbability, ffFixed, 3,2));
  if arTarget[iTarget].RewardProbability >= Random then begin
    result := true;
    FResults.Reward := FResults.Reward + 1;
    //show a flashing red label as a reward. Wow!
    lblReward.Visible := false;
    memo1.Lines.Add('Reward given');
  end
  else begin
    memo1.Lines.Add('No reward given');
  end;



{
  result := false;
  memo1.Lines.Add('Successful trial');
  for i := 1 to 4 do begin
    if arTarget[i].Show then begin
      if arTarget[i].JoystickPosition = WhatPosition then begin
        memo1.Lines.Add('Reward probability: ' + FloatToStrF(arTarget[i].RewardProbability, ffFixed, 3,2));
        if Random < arTarget[i].RewardProbability then begin
          result := true;
          FResults.Reward := FResults.Reward + 1;
          //show a flashing red label as a reward. Wow!
          lblReward.Visible := false;
          memo1.Lines.Add('Reward given');
//          Timer1.Enabled := true;
//          Wait(2000 + random(500));
//          Timer1.Enabled := false;
//          lblReward.Visible := false;
        end
        else begin
          //show a flashing blue label as a punishment!
//          lblFail.Visible := false;
          memo1.Lines.Add('No reward given');
//          Timer2.Enabled := true;
//          Wait(2000 + random(500));
//          Timer2.Enabled := false;
//          lblFail.Visible := false;
        end;
        BREAK;
      end;
    end;
  end;
}
end;

{
When the robot disconnects from the task server, also disconnect the BG model
}
procedure TfrmTrials.IdTCPServer1Disconnect(AThread: TIdPeerThread);
begin
  memo3.Lines.Add('Disconnected');
  if bUseModel then
    EndSession;
end;

procedure TfrmTrials.CentreHorizontal(AControl: TControl; Offset: integer);
begin
  AControl.Left := FCenter.X + Offset - (AControl.Width div 2);
end;

procedure TfrmTrials.CentreVertical(AControl: TControl; Offset: integer);
begin
  AControl.Top := FCenter.Y + Offset - (AControl.Height div 2);
end;

procedure TfrmTrials.IdTCPClient1Disconnected(Sender: TObject);
begin
  memo2.Lines.Add('Disconnected');
end;

procedure TfrmTrials.FormResize(Sender: TObject);
begin
  memo2.Left := Width - 200;
  lblTcpModel.Left := Width - 200;
  memo3.Left := Width - 200;
  lblTcpRobot.Left := Width - 200;
end;

procedure TfrmTrials.tb1PositionChanged(Sender: TObject; Position: Double);
var i: integer;
begin
  eOffsetDivider := tb1.Position;
  PositionControls;
  SetJoystickField;
  for i := 1 to 4 do begin
    if bStop then
      SetImagePosition(i, i)
    else if arTarget[i].Show then begin
      SetImagePosition(i, arTarget[i].Position)
    end;
  end;
end;

procedure TfrmTrials.edRadiusChange(Sender: TObject);
begin
  AJoystick.Accuracy := StrToInt(edRadius.Text);
end;

procedure TfrmTrials.btnShowTargetsClick(Sender: TObject);
var i: integer;
    AnImage: TImage;
begin
  bTargetsShowing := not bTargetsShowing;
  bStop := false;
  for i := 1 to 4 do begin
    AnImage := TImage(FindComponent('img' + IntToStr(i)));
    ilTargets.GetBitmap(i-1, AnImage.Picture.Bitmap);
    AnImage.Visible := bTargetsShowing;
  end;
  if bTargetsShowing then begin
    btnShowTargets.Caption := 'Hide targets';
  end
  else begin
    btnShowTargets.Caption := 'Show targets';
  end;
  imgCursor.Visible := bTargetsShowing;
  while bTargetsShowing  and not bStop do
    ShowCursorPosition;
end;

procedure TfrmTrials.btnExcelClick(Sender: TObject);
begin
  if CreateExcelWorkbook then begin
    SendResultsToExcel;
    SendSetupToExcel;
  end;
end;

function TfrmTrials.CreateExcelWorkbook: boolean;
begin
  result := true;
  // By using GetActiveOleObject, you use an instance of Excel that's already running,
  // if there is one.
  try
    FXLApp := GetActiveOleObject('Excel.Application');
  except
    try
      // If no instance of Excel is running, try to Create a new Excel Object
      FXLApp := CreateOleObject('Excel.Application');
    except
      ShowMessage('Impossible de d�marrer Exel / le logiciel n''est peut �tre pas install�?');
      result := false;
      Exit;
    end;
  end;
  FXLApp.Workbooks.Add(xlWBatWorkSheet);
  FXLApp.Visible := true;
  FXLWorkbook := FXLApp.Activeworkbook;
  self.SetFocus;
  Application.ProcessMessages;
end;

procedure TfrmTrials.SendResultsToExcel;
var i: integer;
begin
  FExcelWorksheet := 'Session 1';
  FXLWorkbook.Activesheet.Name := FExcelWorksheet;
  FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];
  FXLWorksheet.Cells[ROW_HEADER, 1].Value := 'Trial Number';
  FXLWorksheet.Cells[ROW_HEADER, 2].Value := 'T1';
  FXLWorksheet.Cells[ROW_HEADER, 3].Value := 'T2';
  FXLWorksheet.Cells[ROW_HEADER, 4].Value := 'P1';
  FXLWorksheet.Cells[ROW_HEADER, 5].Value := 'P2';
  FXLWorksheet.Cells[ROW_HEADER, 6].Value := 'P(R)1';
  FXLWorksheet.Cells[ROW_HEADER, 7].Value := 'P(R)2';
  FXLWorksheet.Cells[ROW_HEADER, 8].Value := 'Picked';
  FXLWorksheet.Cells[ROW_HEADER, 9].Value := 'Optimum?';
  FXLWorksheet.Cells[ROW_HEADER, 10].Value := 'Rewarded?';
  FXLWorksheet.Cells[ROW_HEADER, 11].Value := 'ITI';
  FXLWorksheet.Cells[ROW_HEADER, 12].Value := 'Reaction time';
  FXLWorksheet.Cells[ROW_HEADER, 13].Value := 'Error code';
  FXLWorksheet.Cells[ROW_HEADER, 14].Value := 'T1 for robot';
  FXLWorksheet.Cells[ROW_HEADER, 15].Value := 'T2 for robot';
  FXLWorksheet.Cells[ROW_HEADER, 16].Value := 'P1 for robot';
  FXLWorksheet.Cells[ROW_HEADER, 17].Value := 'P2 for robot';
  for i := 0 to FTrialCount - 1 do begin
    FXLWorksheet.Cells[i+2, 1].Value := i+1;
    FXLWorksheet.Cells[i+2, 2].Value := FTrial[i].Target1;
    FXLWorksheet.Cells[i+2, 3].Value := FTrial[i].Target2;
    FXLWorksheet.Cells[i+2, 4].Value := FTrial[i].Target[0].Position;
    FXLWorksheet.Cells[i+2, 5].Value := FTrial[i].Target[1].Position;
    FXLWorksheet.Cells[i+2, 6].Value := FTrial[i].Target[0].RewardProbability;
    FXLWorksheet.Cells[i+2, 7].Value := FTrial[i].Target[1].RewardProbability;

    FXLWorksheet.Cells[i+2, 8].Value := FTrial[i].TargetPicked;
    FXLWorksheet.Cells[i+2, 9].Value := NO_YES[integer(FTrial[i].Optimum)];
    FXLWorksheet.Cells[i+2, 10].Value := NO_YES[integer(FTrial[i].Rewarded)];
    FXLWorksheet.Cells[i+2, 11].Value := FTrial[i].ITI;
    FXLWorksheet.Cells[i+2, 12].Value := FTrial[i].ReactionTime;
    FXLWorksheet.Cells[i+2, 13].Value := FTrial[i].ErrorCode;
    FXLWorksheet.Cells[i+2, 14].Value := FRobotTargets[i].Target1;
    FXLWorksheet.Cells[i+2, 15].Value := FRobotTargets[i].Target2;
    FXLWorksheet.Cells[i+2, 16].Value := FRobotTargets[i].Position1;
    FXLWorksheet.Cells[i+2, 17].Value := FRobotTargets[i].Position2;
  end;
end;

procedure TfrmTrials.SendSetupToExcel;
var bSent: boolean;
    i: integer;
begin
  FXLWorkbook.Worksheets.Add;
  FExcelWorksheet := 'Setup';
  FXLWorkbook.Activesheet.Name := FExcelWorksheet;
  FXLWorksheet := FXLWorkbook.Worksheets[FExcelWorksheet];
  bSent := false;
  while not bSent do begin
    try
      FXLWorksheet.Cells[2,1].Value := 'Targets';
      FXLWorksheet.Cells[3,1].Value := '1';
      FXLWorksheet.Cells[4,1].Value := '2';
      FXLWorksheet.Cells[5,1].Value := '3';
      FXLWorksheet.Cells[6,1].Value := '4';

      FXLWorksheet.Cells[2,2].Value := 'Number';
      FXLWorksheet.Cells[2,3].Value := 'Assigned P(R)';
      FXLWorksheet.Cells[2,5].Value := 'Real P(R)';
      FXLWorksheet.Cells[2,6].Value := 'Number';

      for i := 1 to 4 do begin
        FXLWorksheet.Cells[i + 2,2].Value := arTarget[i].ImageNumber;
        FXLWorksheet.Cells[i + 2,3].Value := arTarget[i].RewardProbability;
        if arTarget[i].PickedCount <> 0 then
          FXLWorksheet.Cells[i + 2,5].Value := arTarget[i].RewardedCount / arTarget[i].PickedCount
        else
          FXLWorksheet.Cells[i + 2,5].Value := '0';
      end;

      FXLWorksheet.Cells[7,1].Value := 'Outcomes';
      FXLWorksheet.Cells[8,1].Value := 'Number of trials';
      FXLWorksheet.Cells[9,1].Value := 'Succeeded';
      FXLWorksheet.Cells[10,1].Value := 'Rewarded';
      FXLWorksheet.Cells[11,1].Value := 'Optimal';
      for i := 0 to 1 do begin
        FXLWorksheet.Cells[8,2+(4*i)].Value := FResults.TrialCount;
        FXLWorksheet.Cells[9,2+(4*i)].Value := FResults.Success;
        FXLWorksheet.Cells[10,2+(4*i)].Value := FResults.Reward;
        FXLWorksheet.Cells[11,2+(4*i)].Value := FResults.Optimal;
      end;
      FXLWorksheet.Columns[1].ColumnWidth := 20;

      FXLWorksheet.Cells[14,1].Value := 'Date of test';
      FXLWorksheet.Cells[14,2].Value := DateToStr(Now);

      FXLWorksheet.Columns[2].ColumnWidth := 10;
      FXLWorksheet.Columns[3].ColumnWidth := 15;
      FXLWorksheet.Columns[4].ColumnWidth := 15;
      FXLWorksheet.Columns[5].ColumnWidth := 15;
      FXLWorksheet.Columns[7].ColumnWidth := 15;
      FXLWorksheet.Columns[8].ColumnWidth := 15;
      FXLWorksheet.Columns[9].ColumnWidth := 15;
      bSent := true;
    except
      on E: Exception do begin
        ShowMessage(E.Message);
      end;
    end;
  end;
end;

procedure TfrmTrials.btnCursorPositionClick(Sender: TObject);
var i: integer;
begin
  bStop := false;
  imgCursor.Visible := true;
  imgBlackCircle.Visible := true;
  for i := 1 to 4 do
    TShape(FindComponent('Shape'+IntToStr(i))).Visible := true;
  while not bStop do
    ShowCursorPosition;
  imgCursor.Visible := false;
  imgBlackCircle.Visible := false;
  for i := 1 to 4 do
    TShape(FindComponent('Shape'+IntToStr(i))).Visible := false;
end;

{
This is assigned to the IdTcpClient at create time
}
procedure TfrmTrials.OnClientConnect(Sender: TObject);
begin
  FClentConnected := true;
end;

procedure TfrmTrials.OnClientDisconnect(Sender: TObject);
begin
  FClentConnected := false;
end;

procedure TfrmTrials.FormCreate(Sender: TObject);
begin
  IdTCPClient1.OnConnected := OnClientConnect;
  IdTCPClient1.OnDisconnected := OnClientDisconnect;
  FClentConnected := false;
end;

{
Set up a connection to the tcp server on the BG model
}
procedure TfrmTrials.btnConnectClick(Sender: TObject);
var s: string;
begin
  if not FClentConnected then begin
    with IdTCPClient1 do begin
      Host := FModelIpAddress;
      try
        Connect;
        s := ReadLnWait(10);
        memo2.Lines.Add(s);
        // FIXME: fa�on un peu cavali�re de v�rifier connexion.
        if s = 'Connected' then begin
          FClentConnected := true;
          btnConnect.Caption := 'Disconnect model';
        end
        else
          ShowMessage('Unable to connect to TCP server');
      except
        on E: EIdAlreadyConnected do begin
          ShowMessage(E.Message);
          Abort;
        end;
        on E: Exception do begin
          ShowMessage(E.Message);
          Abort;
        end;
      end;
    end;
  end
  else begin
    IdTCPClient1.Disconnect;
    btnConnect.Caption := 'Connect to model';
    FClentConnected := false;
  end;
end;

function TfrmTrials.GetRewardProbability(Index: integer): real;
begin
  result := FarRewardProbability[Index];
end;

procedure TfrmTrials.SetRewardProbability(Index: integer; const Value: real);
begin
  FarRewardProbability[Index] := Value;
end;

end.


