unit Behavioraltask;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, ImgList, Menus, IniFiles,
  Mask, ToolWin, Buttons,
  uJoystick;

type
  TfrmTask = class(TForm)
    PageControl1: TPageControl;
    tsSession: TTabSheet;
    Label3: TLabel;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    SaveDialog1: TSaveDialog;
    tsSequence: TTabSheet;
    tsTargets: TTabSheet;
    ImageSequence: TImage;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Time1: TMaskEdit;
    Time2: TMaskEdit;
    Time3: TMaskEdit;
    Time4: TMaskEdit;
    Time5: TMaskEdit;
    Time6: TMaskEdit;
    time7: TMaskEdit;
    Time8: TMaskEdit;
    Time9: TMaskEdit;
    Time10: TMaskEdit;
    MvtTime: TMaskEdit;
    ImgTarget0: TImage;
    ImgTarget1: TImage;
    ImgTarget2: TImage;
    ImgTarget3: TImage;
    Label5: TLabel;
    btnLoadTarget0: TButton;
    btnLoadTarget1: TButton;
    btnLoadtarget2: TButton;
    btnLoadtarget3: TButton;
    CoolBar1: TCoolBar;
    sbCalibrateJoystick: TSpeedButton;
    sbPreview: TSpeedButton;
    Label1: TLabel;
    cbIpAddress: TComboBox;
    edTrialCount: TEdit;
    cbCursorByPosition: TCheckBox;
    Label2: TLabel;
    edDefaultOffset: TEdit;
    edPR1: TEdit;
    edPR4: TEdit;
    edPR3: TEdit;
    edPR2: TEdit;
    // For testing purpose : won't try to reach model
    cbUseModel: TCheckBox;

    procedure FormCreate(Sender: TObject);
    procedure sbCalibrateJoystickClick(Sender: TObject);
    procedure sbPreviewClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edTrialCountChange(Sender: TObject);
    procedure edPR1Change(Sender: TObject);
    procedure edPR2Change(Sender: TObject);
    procedure edPR3Change(Sender: TObject);
    procedure edPR4Change(Sender: TObject);
  private
    { D�clarations priv�es }
    FFormatSettings: TFormatSettings;
    FNumTrials: integer;
    FarPR: array[1..4] of real;
  public
    { D�clarations publiques }
    function CheckValues(): boolean; // fonction qui v�rifie les valeurs saisies sur l'interface et revoie true si elle est bien remplie
  end;

var
  frmTask: TfrmTask;

implementation

uses Trials,Joy;

{$R *.dfm}

procedure TfrmTask.FormCreate(Sender: TObject); //Initialisation des valeurs
var i: integer;
begin
  Randomize();
  GetLocaleFormatSettings(0, FFormatSettings);
  FFormatSettings.DecimalSeparator := '.';
  AJoystick := TJoystick.Create;
  FNumTrials := 10;
  for i := 1 to 4 do
    FarPR[i] := 0.333333 * (i - 1);
end;

procedure TfrmTask.FormDestroy(Sender: TObject);
begin
  AJoystick.Free;
end;

//Validation des s�lections effectu�es
function TfrmTask.CheckValues;
begin
  result:= true;
end;

//Sauvegarde / Chargement des anciennes configurationsprocedure TfrmTask.LoadFileClick(Sender: TObject);
procedure TfrmTask.sbCalibrateJoystickClick(Sender: TObject);
begin
  WinExec(PChar('rundll32.exe shell32.dll,'+
                          'Control_RunDLL ' +
                          'joy.cpl'),
                          SW_SHOWNORMAL) ;

{
  if frmJoystick = nil then
    frmJoystick := TfrmJoystick.Create(nil);
  with frmJoystick do try
    ShowModal;
  finally
    Free;
    frmJoystick := nil;
  end;
}  
end;

procedure TfrmTask.sbPreviewClick(Sender: TObject);
var i: integer;
begin
  if CheckValues then begin
    if frmTrials = nil then
      frmTrials := TfrmTrials.Create(nil);
    with frmTrials do try
      UseModel := cbUseModel.Checked;
      ModelIpAddress := cbIpAddress.Items[cbIpAddress.ItemIndex];
      TrialCount := FNumTrials;
      CursorByPosition := cbCursorByPosition.Checked;
      DefaultOffset := StrToFloat(edDefaultOffset.Text);
      for i := 1 to 4 do
        RewardProbability[i] := FarPR[i];
      ShowModal;
    finally
      Free;
      frmTrials := nil;
    end;
  end;
end;

procedure TfrmTask.edTrialCountChange(Sender: TObject);
begin
  FNumTrials:=StrToInt(Trim(edTrialCount.Text));
end;

procedure TfrmTask.edPR1Change(Sender: TObject);
begin
  FarPr[1] := StrToFloat(edPR1.Text, FFormatSettings);
end;

procedure TfrmTask.edPR2Change(Sender: TObject);
begin
  FarPr[2] := StrToFloat(edPR2.Text, FFormatSettings);
end;

procedure TfrmTask.edPR3Change(Sender: TObject);
begin
  FarPr[3] := StrToFloat(edPR3.Text, FFormatSettings);
end;

procedure TfrmTask.edPR4Change(Sender: TObject);
begin
  FarPr[4] := StrToFloat(edPR4.Text, FFormatSettings);
end;

end.
