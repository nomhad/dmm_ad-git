unit Parameters;

interface

uses SysUtils, Controls;

type

  TParam = class(Tobject)
  private
    ilTargets: TImageList;
  public
    NumbTrials:integer;
    arDelays:array[1..11] of double; //Tableau contenant les valeurs des temps pour la session
//    Targets: array[0..3] of string; // tableau contenant les chemins vers les images
    ProbTargets:array[0..3] of double; //Tableau des probabilit�s associ�es
    idxImages: integer; // utilis� pour savoir combien d'images sont contenues dans target[]
    TabCheckLoad: array[1..4] of boolean;

    TextToRecord: string;
//    repertoire: string;
//    value: real;
    lastconfiguration: string;

    FFormatSettings: TFormatSettings;

    constructor Create;
    destructor Destroy; override;
    function CalculateDelay(IndxStart,IndxFinish: integer):integer;
    property Targets: TImageList read ilTargets write ilTargets;
  end;

  TSessionData= class(Tobject)  // un objet pour enregistrer chaque
     Num_Session:integer;
     Trial:integer;
     //Positions:array[4,4]of integer;//Position[0]=>Top,[1]=>right,[2]=>bottom,[4]=>left
     //elements: value=1=> Target[1] ... value=4= Target[4] and value=0=> no image at this position.
     Choice:integer;
     BestChoice:integer;
     Reward:boolean;
     constructor Create();
  end;

var Params: TParam;

implementation

constructor TParam.Create;
var i: integer;
begin
  inherited Create;
{
  for i := 0 to 3 do begin
    Targets[i]:= '\ImagesData\white.bmp';
    TabCheckLoad[i+1] := False;
  end;
}  
  TextToRecord := '';
  GetLocaleFormatSettings(0, FFormatSettings);
  FFormatSettings.DecimalSeparator := ',';
  ilTargets := TImageList.Create(nil);
end;

destructor TParam.Destroy;
begin
  ilTargets.Free;
  inherited;
end;

function TParam.CalculateDelay(IndxStart,IndxFinish:integer):integer;
var eDelay: real;
begin
  eDelay := arDelays[IndxFinish] - arDelays[IndxStart];
  if eDelay > 0 then
    result := trunc(eDelay * 1000);
end;

constructor  TSessionData.Create;
begin
  inherited Create;
end;


end.
