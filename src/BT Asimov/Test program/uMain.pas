unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,ScktComp, IdBaseComponent, IdComponent, IdTCPServer, StdCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    IdTCPServer1: TIdTCPServer;
    Label1: TLabel;
    Label2: TLabel;
    edSend: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure IdTCPServer1Execute(AThread: TIdPeerThread);
    procedure IdTCPServer1Disconnect(AThread: TIdPeerThread);
    procedure IdTCPServer1Connect(AThread: TIdPeerThread);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  IdTCPServer1.Active := true;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
var hProc:THandle;
begin
  IdTCPServer1.ThreadMgr.TerminateThreads;
  hProc := OpenProcess(PROCESS_TERMINATE, False, GetCurrentProcessId);
  TerminateProcess(hProc, 0);
end;

procedure TForm1.IdTCPServer1Execute(AThread: TIdPeerThread);
var i:integer;
    sRec : string;
begin
  sRec := AThread.Connection.ReadLn;
  if sRec <> '' then begin
    //when robot first connects possibly
    Memo1.Lines.Add(AThread.Connection.Socket.Binding.IP + ' sends :') ;
    Memo1.Lines.Add(sRec);
    AThread.Connection.WriteLn(edSend.Text);
  end
  else if 
end;

procedure TForm1.IdTCPServer1Disconnect(AThread: TIdPeerThread);
begin
  AThread.Terminate;
end;

procedure TForm1.IdTCPServer1Connect(AThread: TIdPeerThread);
begin
  AThread.Connection.WriteLn('OK');
end;

end.
