object Form1: TForm1
  Left = 661
  Top = 240
  Width = 276
  Height = 334
  Caption = 'Server'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 8
    Width = 49
    Height = 13
    Caption = 'Received:'
  end
  object Label2: TLabel
    Left = 16
    Top = 248
    Width = 42
    Height = 13
    Caption = 'To send:'
  end
  object Memo1: TMemo
    Left = 16
    Top = 24
    Width = 209
    Height = 209
    TabOrder = 0
  end
  object edSend: TEdit
    Left = 16
    Top = 264
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object IdTCPServer1: TIdTCPServer
    Bindings = <>
    CommandHandlers = <>
    DefaultPort = 6667
    Greeting.NumericCode = 0
    MaxConnectionReply.NumericCode = 0
    MaxConnections = 1
    OnConnect = IdTCPServer1Connect
    OnExecute = IdTCPServer1Execute
    OnDisconnect = IdTCPServer1Disconnect
    ReplyExceptionCode = 0
    ReplyTexts = <>
    ReplyUnknownCommand.NumericCode = 0
    TerminateWaitTime = 0
    Left = 232
    Top = 8
  end
end
