program pTaskServer;

uses
  Forms,
  Behavioraltask in 'Behavioraltask.pas' {frmTask},
  Trials in 'Trials.pas' {frmTrials},
  uJoystick in 'uJoystick.pas',
  Joy in 'Joy.pas' {frmJoystick};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Task Server';
  Application.CreateForm(TfrmTask, frmTask);
  Application.Run;
end.
