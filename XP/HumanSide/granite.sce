// Nombre de cibles, même si risque de ne pas changer de mon vivant
NB_TARGETS=4;
// Les dernières itérations, où on considère que l'apprentissage est effectué
RECORDING_WINDOW=5;
// Vitesse maximale à laquelle un sujet peu choisir une cible (si ne devais pas réfléchir, physiologique)
MOVE_TIME_BOTTOM_LIMIT = 200;
// Limite supérieure pour ce même mouvement (échec si supérieur, condition expérimentale)
MOVE_TIME_UPPER_LIMIT = 10000;

// ** Va définir les numéros des colonnes utilisées
// ID des sujets
nColID=1;
// Session
nColSession=2;
// Attention pour les probas : getSessionStats suppose que les NB_TARGETS se suivent depuis la colomme indiquée
// Probas assignées pour les 4 cibles
nColFirstP=3;
// Probas estimées pour les 4 cibles
nColFirstGuessP=7;
// Numéro de l'itération
nColIterationNumber=11;
// Première cible présentée
nColT1=12;
// Seconde cible présentée
nColT2=13;
// Cible choisie
nColPicked=16;
// Coup optimum
nColOptimum=17;
// Si récompense
nColRewarded=18;
// Temps de réaction
nColReactionTime=20;
// Code d'erreur
nColError=21;