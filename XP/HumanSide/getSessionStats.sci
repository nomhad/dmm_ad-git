// Retourne les étiquettes des données que fournit getSessionStats
function mat = getSessionStatsHead()
    // Dans l'ordre :
    // * proportion de choix optimaux
    // * temps de réaction moyen, puis normalisé
    // * probas attribuées aux 4 cibles
    // * probas réelles au cours XP
    // * probas estimées par sujet
    // * probas inférées d'après derniers choix du sujet
    
    // Pour chaque set de probas on va construire les chaînes suivant le nombre de cibles
    matAssignedProbT=[];
    matEffectiveProbT=[];
    matGuessedProbT=[];
    matInferredProbT=[];
    for i=1:NB_TARGETS
        matAssignedProbT=[matAssignedProbT,"AssignedProbT"+string(i)];
        matEffectiveProbT=[matEffectiveProbT,"EffectiveProbT"+string(i)];
        matGuessedProbT=[matGuessedProbT,"GuessedProbT"+string(i)];
        matInferredProbT=[matInferredProbT,"InferredProbT"+string(i)];
    end
    
    // On concatène ensuite le tous avec les colonnes plus classiques
    mat=["ProportionOptimum","ReactionTime","NormalizedReactionTime",matAssignedProbT,matEffectiveProbT,matGuessedProbT,matInferredProbT]
endfunction

// À partir des données propres à une session, va renvoyer une matrices contenant quelques iformations croustillantes.
// verbose: afficher ou non infos détaillées sur les données traitées
function mat = getSessionStats(data,verbose)

// Un "inform" conditionnel suivant demande de verbosité
deff('inform(x)','if exists(''verbose'') & verbose then, disp(x), end')
 
// Des constantes
exec('granite.sce');  
// Chargement des fonctions utilisées
exec('inferP.sci');

// ** Tri des données
// Récupère colonne contenant les numéros des itérations
SujetColIterationNumber=SujetSessionData(:,nColIterationNumber);
// Va les trier, laisse en ordre inverse, on est intéressé que par les incides
[tmp, indices] = gsort(SujetColIterationNumber);
// Et on applique ce tri aux données extraites
data=data(indices,:);

// Pour faciliter compilation données sur cibles, on utilise une petite stucture
// proba_assigned : probabilité attribuée par expérimentateur
// proba_guessed : probabilité estimée par sujet
// presented : nombre de fois où apparu sur l'écran
// chosen : nombre de fois où choisie
// rewarded : nombre de fois où récompensé
// passive_rewarded : c'est l'autre cible qui a été récompensée
// optimum : le choix était optimum
// error : une erreur est survenue lors de cet essai
// picked_error : une erreur est survenue alors que la cible était choisie
// last_optimum : le nombre de choix optimum sur les RECORDING_WINDOW derniers essais
TargetDataStruct=struct('proba_assigned',0,'proba_guessed',0,'presented', 0,'chosen',0,'rewarded',0,'passive_rewarded',0,'optimum',0,'error', 0, 'picked_error', 0, 'last_optimum',0);
// Version simplifiée pour les données générales
SessionDataStruct=struct('presented',0,'optimum',0);
// La variables qui va nous enrober ça
SessionData=SessionDataStruct;
// Infos sur cibles, inutile de s'impatienter  : va être étendu juste après (d'horrible façon ceci-dit)
// TODO: appliquer méthode idiomatique pour définir type matrice, et directement à NB_TARGETS éléments
Targets=[TargetDataStruct]

// On récupères des infos brutes sur les cibles (prend sur première ligne, car sont sensées être identique sur toute la session)
for i = 0:NB_TARGETS+1
    // Elargissement du cercle des amis
    if i > 0 then
        Targets=[Targets,TargetDataStruct];
    end
    // Probas assignés puis estimées
    Targets(i+1).proba_assigned = data(1,nColFirstP+i);
    Targets(i+1).proba_guessed = data(1,nColFirstGuessP+i);
end

// récupère le nomble de lignes des données session
k = size(data)
k=k(1)
// Parcours la liste
for i = 1:k
    // FIXME: rustine pour rentrer chez moi
    // (existe essais avec code erreur 2 sans cibles)
    if (data(i,nColT1) == 0) | (data(i,nColT2) == 0) then
        continue
    end
    // Met à jour compteurs des présentations
    // TODO: trouver un ++ !
    SessionData.presented = SessionData.presented+1;
    Targets(data(i,nColT1)).presented = Targets(data(i,nColT1)).presented+1;
    Targets(data(i,nColT2)).presented = Targets(data(i,nColT2)).presented+1;
    // Et compteurs erreurs
    if data(i,nColError) <> 0 then
        Targets(data(i,nColT1)).error = Targets(data(i,nColT1)).error+1;
        Targets(data(i,nColT2)).error = Targets(data(i,nColT2)).error+1;
    end
    // Et enfin ceux pour optimum
    if data(i,nColOptimum) == 1 then
        SessionData.optimum = SessionData.optimum+1;
        Targets(data(i,nColT1)).optimum = Targets(data(i,nColT1)).optimum+1;
        Targets(data(i,nColT2)).optimum = Targets(data(i,nColT2)).optimum+1;
    end
    // Si une cible est choisie
    if data(i,nColPicked) > -1 then
            // La cible en question
            pickedTarget = data(i,nColPicked);
            // Compteur erreur spécial quand choix effectué
            if data(i,nColError) <> 0 then
                Targets(pickedTarget).picked_error = Targets(pickedTarget).picked_error+1;
            end
        // Met à jour le nombre de fois où les cibles sont choisies
        Targets(pickedTarget).chosen = Targets(pickedTarget).chosen + 1;
        // Idem avec récompense
        if data(i,nColRewarded) == 1 then
            Targets(pickedTarget).rewarded = Targets(pickedTarget).rewarded + 1;
            // Et pour l'autre cible (pour tester effet contexte)
            if pickedTarget == data(i,nColT1) then
                sidekickTarget = data(i,nColT2);
            else
                sidekickTarget = data(i,nColT1);
            end
            Targets(sidekickTarget).passive_rewarded = Targets(sidekickTarget).passive_rewarded + 1;
        end  
    end
    // Comme l'ordre des itération est inversé, les cinq premiers passages pour chaque cible correspondent aux cinq dernières présentations.
    // Au dernier tour on retient le nombre de choix optimaux
    if  Targets(data(i,nColT1)).presented == RECORDING_WINDOW then
       Targets(data(i,nColT1)).last_optimum = Targets(data(i,nColT1)).optimum;
    end
    if  Targets(data(i,nColT2)).presented == RECORDING_WINDOW then
       Targets(data(i,nColT2)).last_optimum = Targets(data(i,nColT2)).optimum;
    end
end

// Affiche données
inform("Données générales :")
inform(SessionData)
inform("Données sur les cibles :")
for i = 1:NB_TARGETS
    inform(Targets(i))
end

// Stucture utilisée afin de sortir les résultats
// proba_effective : probabilité de récompense effective de la cible
// proba_effective_error : idem mais en retenant aussi essais avec erreur
// proba_inference : estimation de la proba par le sujet en se basant sur ses choix
// active_outcome : nombre de fois où gain / nb présentation
// contextual_outcome : idem mais en comptant aussi les gains sur l'autre cible
TargetStatsStruct=struct('proba_effective', -1,'proba_effective_error',-1,'proba_inference',-1,'active_outcome',0,'contextual_outcome',0);
// Une autre version pour les données d'ordre général
// average_RT: délais moyen entre présentation des cibles et mouvement
// norm_average_RT: normalisé sur [0,1]
// proportion_optimum: nombre de choix optimum parmis toutes les présentations
SessionStatsStruct=struct('average_RT',0,'norm_average_RT',0,'proportion_optimum',0);

// On prend en considération nos cibles, sans faire de favoritisme (sera étendu en suivant)
TargetsStats=[TargetStatsStruct];
SessionStats=SessionStatsStruct;

// Enfin un peu de vrais calculs
inform("Statistiques sur les cibles :")
for i = 1:NB_TARGETS
    // Elargissement du cercle des amis
    if i > 1 then
        TargetsStats=[TargetsStats,TargetsStats];
    end
    // Les probas
    if Targets(i).chosen <> 0 then
        // réelles
        if Targets(i).chosen <> Targets(i).picked_error then
            TargetsStats(i).proba_effective = Targets(i).rewarded / (Targets(i).chosen - Targets(i).picked_error);
        end
        // Et sans gommer les erreurs
        TargetsStats(i).proba_effective_error = Targets(i).rewarded / Targets(i).chosen;
    end
    // Proportions plus hasardeuses
    if Targets(i).presented <> 0 then
        TargetsStats(i).active_outcome = Targets(i).rewarded / Targets(i).presented;
        TargetsStats(i).contextual_outcome =  (Targets(i).rewarded + Targets(i).passive_rewarded) / Targets(i).presented;
    end
    // Le calcul déjà plus magique
    TargetsStats(i).proba_inference = inferP(TargetsStats(i).proba_effective, Targets(i).last_optimum/RECORDING_WINDOW);
    // Et affiche pour finir
    inform(TargetsStats(i))
end

// ** Sur la session en général
// sur l'optimalité des décisions
inform("Proportion optimum :")
if SessionData.presented <> 0 then
    SessionStats.proportion_optimum = SessionData.optimum/SessionData.presented
end
inform(SessionStats.proportion_optimum)
// On s'intéresse maintenant au temps de réaction
// NB: comme dans Stakhanov on fait une simple moyenne, normalise pour pouvoir comparer
ColReactionTime = data(:,nColReactionTime);
// Garde que les essai où c'est pertinent
SelectReactionTime = find(ColReactionTime>-1);
ColReactionTime = ColReactionTime(SelectReactionTime);
// moyenne : sommation d'une simplicité enfantine et nombre de lignes
SessionStats.average_RT = sum(ColReactionTime) / size(ColReactionTime,'r');
// Pour normaliser : on coupe limite inférieure et fait une petite règle de trois
if MOVE_TIME_UPPER_LIMIT <> MOVE_TIME_BOTTOM_LIMIT then
    SessionStats.norm_average_RT = (SessionStats.average_RT - MOVE_TIME_BOTTOM_LIMIT) / (MOVE_TIME_UPPER_LIMIT - MOVE_TIME_BOTTOM_LIMIT);
end
inform('Temps de réaction moyen :')
inform(SessionStats.average_RT)
inform('Normalisé :')
inform(SessionStats.norm_average_RT)

// Pour l'extérieur on ne retourne qu'une partie des données, en accord avec fonction getSessionStatsHead plus haut.

// Pour chaque set de probas on va construire les matrices suivant le nombre de cibles
matAssignedProbT=[];
matEffectiveProbT=[];
matGuessedProbT=[];
matInferredProbT=[];
for i=1:NB_TARGETS
    matAssignedProbT=[matAssignedProbT,Targets(i).proba_assigned];
    matEffectiveProbT=[matEffectiveProbT,TargetsStats(i).proba_effective];
    matGuessedProbT=[matGuessedProbT,Targets(i).proba_guessed];
    matInferredProbT=[matInferredProbT,TargetsStats(i).proba_inference];
end
// Servi à chaud pour l'extérieur : émincé d'infos générales sur sont lit de concaténation
mat = [SessionStats.proportion_optimum,SessionStats.average_RT,SessionStats.norm_average_RT,matAssignedProbT,matEffectiveProbT,matGuessedProbT,matInferredProbT];

endfunction