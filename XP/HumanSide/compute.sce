// ** Des variables essentielles
// Répertoire où se trouve les fichiers à traiter
HOME_REP="./"
// Fichier données XP
DATA_FILE="Session_2010.csv"
// fichier de sortie
OUTPUT_FILE="results.csv"
// Le motif qui sépare les colonnes dans ce fatras de données
CSV_COLUMN_SEPARATOR=","

// Des constantes, surtout concernant l'agencement des données en entrée
exec('granite.sce');
// Chargement des fonctions utilisées
// veut pas qu'il se plaigne trop souvent d'une redéfinition de fonction
funcprot(0)
exec('getSessionStats.sci');

// Initialise les résultats avec les en-têtes des colonnes
results = getSessionStatsHead();

// Chargement du fichier (séparateur: ,)
disp("Chargement des données de ''" + HOME_REP + DATA_FILE + "''...")
Data=read_csv(HOME_REP+DATA_FILE,CSV_COLUMN_SEPARATOR);
// Zape la première ligne, qui ne contient que les en-têtex
Data=Data(2:$,:);
// Convertit contenu en nombres (attention à ce qu'il n'y ait pas de cases vides !)
Data=evstr(Data);

disp("Traitement des données...")

// Extrait les ID des sujets
ColID=Data(:,nColID);

// On va boucler sur chaque ID (une minuscule apostrophe pour une tranposée nécessaire au passage de i)
for i=unique(ColID)'
    sujet_id = i;
    disp("Sujet : " + string(sujet_id))

    // Extraction des données sujet
    // TODO: syntaxe genre SQL faciliterair la chose
    // Récupère lignes du sujet i
    SelectID=find(ColID==sujet_id);  
    // On extrait toutes les données du sujet
    SujetData=Data(SelectID,:);
    
    // On va regarder ses sessions
    ColSession=SujetData(:,nColSession);
    // Boucle dessus
    for j=unique(ColSession)'
        sujet_session = j;
        disp("Session : " + string(sujet_session))
        // Récupère lignes avec session en cours
        SelectSession=find(ColSession==sujet_session);
        // On extrait toutes les données de sujet i session j
        SujetSessionData=SujetData(SelectSession,:);

        // On extrait le tout
        tmp_results=getSessionStats(SujetSessionData);
        disp("Résultat : ")
        disp(tmp_results)
        // Et on concatène dans résultats
        results=[results;string(tmp_results)]
    end  
end

disp("Écritures des données dans ''" + HOME_REP + OUTPUT_FILE)
write_csv(results,HOME_REP + OUTPUT_FILE,CSV_COLUMN_SEPARATOR)




