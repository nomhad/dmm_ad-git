// Cette fonction retourne une estimation de la probabilité utilisée par le sujet au cours de l'XP. Il faut pour ce faire lui fournir une probabilité de référence (la proba réelle ou effective suivant l'étude) et une proportion qui représente l'adéquation entre les choix du sujet et cette probabilité.
// S'en suivra une cuisine interne pour sortir le miracle.

// Algo :
// P_{en pratique}(T_i) = 0.5 + (P_{obervée}(T_i) - 0.5) * f(n_Optimum / n_Présentation) 
// f(x) = (2x-1)^2

function [inference_proba] = inferP(reference_proba,match_proportion)
    // polynome intermédiaire
    deff('[y]=f(x)','y=(2*x-1)^2')
    inference_proba =  0.5 + (reference_proba - 0.5) * (f(match_proportion));
endfunction
